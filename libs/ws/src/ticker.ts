import {ArrowType} from './tick';

export const TICKER_UPDATES_EVENT = 'ticker_updates'


export interface ITickerUpdates {
    ticker: string;
    pair: string;
    arrow: ArrowType;
    price: string;
    priceChange: string;
    priceChangePercent: string;
}
