export const NOTIFICATIONS_EVENT = 'notifications';

export enum NotificationType {
    ORDER_COMPLETED = 'order completed',
    ORDER_CANCELED = 'order canceled',
}

export interface INotification {
    readonly id: string;
    readonly title: string;
    readonly message: string;
    readonly type: NotificationType;
    readonly readStatus: boolean;
    readonly asOfDate: number;
}
