export const enum CeDealSide {
    ASK = "ask",
    BID = "bid"
}

export const enum CeOrderStatus {
    OPEN = 'open',
    CANCEL = 'cancel',
    COMPLETE = 'complete',
}

export const NEW_COMMON_TRADE_EVENT = 'new_common_trade';
export const NEW_TRADE_DETAILS_EVENT = 'new_trade_details';
export const ORDER_STATUS_CHANGED_EVENT = 'order_status_changed';
export const TRADE_ACCOUNTED_EVENT = 'trade_accounted';

export interface ICommonTrade {
    readonly price: string;
    readonly quantity: string;
    readonly time: number;
    readonly side: CeDealSide;
}

export interface ITradeDetails {
    readonly orderId: string;
    readonly tradeId: string;
    readonly pair: string;
    readonly price: string;
    readonly quantity: string;
    readonly side: CeDealSide;
    readonly feeCurrency: string;
    readonly feeValue: string;
    readonly time: number;
}

export interface IBalanceResponse {
    readonly userId: string; // 'ca742a5e-3954-4a09-958e-307dc0c38e76'
    readonly currency: string; // 'BTC'
    readonly balance: string; // '2000000000'
    readonly accountNumber: string; // 'C2600xBTCxca742a5e-3954-4a09-958e-307dc0c38e76x00'
    readonly updated: number; // '1665348894245'
}
