export const NEW_TICK_EVENT = 'new_tick'
export type ArrowType = 'double up' | 'up' | 'no arrow' | 'down' | 'double down';

export interface IOrder {
    id: string;
    customerId: string;
    pair: string;
    price: string;
    quantity: string;
    stopLimitPrice?: string;
    buyFlg: boolean;
    base: string;
    quote: string;
    time: number;
    completed: boolean;
}

export interface ITrade {
    readonly taker: IOrder;
    readonly maker: IOrder;
    readonly volume: string;
    readonly matchingTime: number;
    readonly arrowType?: ArrowType;
}

export interface ICandle {
    openPrice: string;
    closePrice: string;
    lowPrice: string;
    highPrice: string;
    volume: string;
    bid: string;
    ask: string;
}

export interface IOrderBookSide {
    price: string;
    volume: string;
}

export interface IOrderBook {
    buy?: IOrderBookSide[];
    sell?: IOrderBookSide[];
}

export interface ITick {
    asOfDate: number;
    orderBook?: IOrderBook;
    trade?: ITrade;
    candles?: ICandle
    updateOrders?: boolean;
}
