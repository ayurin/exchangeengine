export const SL_ACTIVATE_EVENT = 'sl_activate'

export interface IBffWsSlActivation {
    slOrderId: string;
    slOrderOwnerId: string; //User Id
    orderTriggerTakerId: string;
    orderTriggerMakerId: string;
    activateTime: number;
}

export interface ISlActivationOrder extends IBffWsSlActivation {
    pair: string;
}
