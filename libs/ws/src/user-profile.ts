export const PROFILE_EVENT = 'profile'

export interface IBffwsProfile {
    id: string;
    partial: Partial<IUser>
}

export type UserStatus = 'CREATED' | 'REGISTERED';

export enum ELockTime {
    DAY = 'DAY',
    MONTH = 'MONTH',
    FOREVER = 'FOREVER',
    TEN_MINUTES = 'TEN_MINUTES',
}

export enum ELockReason {
    EXCESS_LOGIN_ATTEMPTS = 'Excess login attempts',
    SUSPICIOUS_BEHAVIOR = 'Suspicious behavior',
    BLACKLIST = 'Blacklist',
    TOO_MANY_TFA_REQUEST = 'Too many tfa request',
    TOO_MANY_LOGIN_REQUEST = 'Too many login request',
}

export interface IUser {
    id: string;
    fullName: string;
    shortName: string;
    email: string;
    status: UserStatus;
    kyc: boolean;
    tfaEnable: boolean;
    blocked: boolean;
    taxNumber: string;
    createdAt: Date;
    deletedAt?: Date;
    lockReason?: ELockReason;
    lockTime?: ELockTime;
}
