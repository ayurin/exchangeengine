import { VerifyOptions } from 'jsonwebtoken';
import { HttpException, HttpStatus } from '@nestjs/common';
import { EProtect } from './decorators/protect.decorator';

export const verifyOptions: VerifyOptions = {
    algorithms: ['RS256'],
};

export const handleCheckProtectResponse = (
    payload: { isValid: boolean; message?: string; status: HttpStatus },
    protect: EProtect,
): void => {
    const { isValid, status, message } = payload;

    if (status !== HttpStatus.OK) {
        throw new HttpException(message ?? 'Unknown error', status);
    }

    if (!isValid) {
        throw new HttpException(`${protect} code is not valid`, HttpStatus.FORBIDDEN);
    }
};

export abstract class AbstractJwtGuard {
    private static readonly AUTH_PUBLIC_KEY_URI = (() => {
        const internalAuthDomain = process.env.INTERNAL_AUTH_DOMAIN;
        if (!internalAuthDomain) {
            throw new Error('the INTERNAL_AUTH_DOMAIN environment variable is not defined');
        }

        return `http://${internalAuthDomain}/api/v1/public_key`;
    })();

    private static publicKey$: Promise<string> | null = null;

    protected static getPublicKey(): Promise<string> {
        if (this.publicKey$ === null) {
            this.publicKey$ = fetch(this.AUTH_PUBLIC_KEY_URI).then((res) => {
                if (res.ok !== true) {
                    throw new Error(`fetching auth public key error ${res.status}: ${res.statusText} (${this.AUTH_PUBLIC_KEY_URI})`);
                }
                return res.text();
            });
        }

        return this.publicKey$;
    }
}
