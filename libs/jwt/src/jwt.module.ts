import { DynamicModule, Module } from '@nestjs/common';
import { JWT_OPTIONS } from './constants';
import { JwtService } from './services/jwt.service';
import { JWTOptions } from './types';

@Module({})
export class JwtModule {
    public static forRoot(options: JWTOptions): DynamicModule {
        return {
            module: JwtModule,
            providers: [
                JwtService,
                {
                    provide: JWT_OPTIONS,
                    useValue: options,
                },
            ],
            exports: [JwtService],
        };
    }
}
