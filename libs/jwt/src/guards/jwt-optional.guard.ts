import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import type { IncomingMessage } from 'http';
import { verify } from 'jsonwebtoken';

import { TAllJwtPayload } from '../types';
import { AUTH_HEADER } from '../index';
import { AbstractJwtGuard, verifyOptions } from '../utils';
import { TokenAudience, USE_AUDIENCES_TOKEN } from '../decorators/audience.decorator';
import { Reflector } from '@nestjs/core';

@Injectable()
export class JwtOptionalGuard extends AbstractJwtGuard implements CanActivate {
    constructor(private readonly reflector: Reflector) {
        super();
    }

    public async canActivate(ctx: ExecutionContext): Promise<boolean> {
        try {
            const req = ctx.switchToHttp().getRequest<IncomingMessage & { user: TAllJwtPayload }>();

            const token = req.headers[AUTH_HEADER]?.replace('Bearer ', '');
            if (typeof token !== 'string') {
                return true;
            }

            const parsed = await JwtOptionalGuard.verifyAndParse(token).catch(() => null);
            if (parsed === null) {
                return true;
            }

            req.user = parsed;

            const validationResult = await this.validateTokenPayload(req.user, ctx);

            return validationResult;
        } catch (err) {
            return true;
        }
    }

    private static async verifyAndParse(token: string): Promise<TAllJwtPayload> {
        const publicKey = await super.getPublicKey();

        return new Promise((resolve, reject) => {
            verify(token, publicKey, verifyOptions, (err, payload) => {
                if (err !== null) {
                    reject(err);
                    return;
                }

                resolve(payload as TAllJwtPayload);
            });
        });
    }

    private async validateTokenPayload(payload: TAllJwtPayload, context: ExecutionContext): Promise<boolean> {
        const contextHandler = context.getHandler();

        // Check, that token aud is valid for current method if set
        const audiences = this.reflector.get<TokenAudience[]>(USE_AUDIENCES_TOKEN, contextHandler);
        const tokenIsValid = audiences && audiences.length ? audiences.includes(payload.aud) : true;

        if (!tokenIsValid) {
            throw new Error('no access for such user');
        }

        return tokenIsValid;
    }
}
