import { CanActivate, ExecutionContext, HttpException, HttpStatus, Injectable, UnauthorizedException } from '@nestjs/common';
import { CAPTCHA_HEADER, EProtect, handleCheckProtectResponse } from '../index';
import { RabbitService, RmqClientService, AUTH_CHECK_CAPTCHA_PATTERN } from '@app/utils';

@Injectable()
export class CaptchaGuard implements CanActivate {
    constructor(private readonly rabbitService: RabbitService, private readonly rmqClientService: RmqClientService) {}

    public async canActivate(ctx: ExecutionContext): Promise<boolean> {
        try {
            const req = ctx.switchToHttp().getRequest();
            await this.checkCaptchaCode(req.headers[CAPTCHA_HEADER] as string);

            return true;
        } catch (err) {
            const { message, status } = err;

            switch (status) {
                case HttpStatus.FORBIDDEN:
                    throw new HttpException((message as string) ?? 'Forbidden', HttpStatus.FORBIDDEN);
                case HttpStatus.TOO_MANY_REQUESTS:
                    throw new HttpException((message as string) ?? 'Too many request', HttpStatus.TOO_MANY_REQUESTS);
                case HttpStatus.BAD_REQUEST:
                    throw new HttpException((message as string) ?? 'Bad request', HttpStatus.BAD_REQUEST);
                default:
                    throw new UnauthorizedException();
            }
        }
    }

    private async checkCaptchaCode(token: string): Promise<void> {
        const result: { isValid: boolean; message?: string; status: HttpStatus } = await this.rmqClientService.send(
            AUTH_CHECK_CAPTCHA_PATTERN,
            this.rabbitService.buildMessage({ token }),
        );

        handleCheckProtectResponse(result, EProtect.CAPTCHA);
    }
}
