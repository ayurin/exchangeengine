import { CanActivate, ExecutionContext, HttpException, HttpStatus, Injectable, UnauthorizedException } from '@nestjs/common';
import type { IncomingHttpHeaders, IncomingMessage } from 'http';
import { decode, verify } from 'jsonwebtoken';

import { TAllJwtPayload } from '../types';
import {
    AUTH_HEADER,
    EProtect,
    handleCheckProtectResponse,
    TFA_HEADER,
    TokenAudience,
    USE_AUDIENCES_TOKEN,
    USE_PROTECT_TOKEN,
} from '../index';
import { AbstractJwtGuard, verifyOptions } from '../utils';
import { Reflector } from '@nestjs/core';
import { RabbitService, AUTH_CHECK_TFA_CODE_PATTERN, RmqClientService, AUTH_CHECK_CAPTCHA_PATTERN } from '@app/utils';

export interface ISocket {
    readonly handshake: {
        readonly headers: IncomingHttpHeaders;
    };
}

@Injectable()
export class JwtGuard extends AbstractJwtGuard implements CanActivate {
    constructor(
        private readonly reflector: Reflector,
        private readonly rabbitService: RabbitService,
        private readonly rmqClientService: RmqClientService,
    ) {
        super();
    }

    public async canActivate(ctx: ExecutionContext): Promise<boolean> {
        try {
            const req = ctx.switchToHttp().getRequest<IncomingMessage & { user: TAllJwtPayload }>();

            const token = req.headers[AUTH_HEADER]?.replace('Bearer ', '') as string;
            const parsed = await JwtGuard.verifyAndParse(token).catch(() => null);
            if (parsed === null) {
                throw new UnauthorizedException();
            }

            req.user = parsed;

            this.validateAudience(req.user, ctx);
            await this.checkProtects(req.user, ctx);

            return true;
        } catch (err) {
            const { message, status } = err;

            switch (status) {
                case HttpStatus.FORBIDDEN:
                    throw new HttpException((message as string) ?? 'Forbidden', HttpStatus.FORBIDDEN);
                case HttpStatus.TOO_MANY_REQUESTS:
                    throw new HttpException((message as string) ?? 'Too many request', HttpStatus.TOO_MANY_REQUESTS);
                case HttpStatus.BAD_REQUEST:
                    throw new HttpException((message as string) ?? 'Bad request', HttpStatus.BAD_REQUEST);
                default:
                    throw new UnauthorizedException();
            }
        }
    }

    public static async checkWsAuthorizationAndGetUserID(socket: ISocket): Promise<string | null> {
        try {
            const token = socket.handshake.headers[AUTH_HEADER]?.replace('Bearer ', '');
            if (typeof token !== 'string') {
                return null;
            }

            const parsed = await this.verifyAndParse(token).catch(() => null);
            if (parsed === null) {
                return null;
            }

            return parsed.id;
        } catch {
            return null;
        }
    }

    public static getWSUserIDWithoutCheck(socket: ISocket): string | null {
        try {
            const token = socket.handshake.headers[AUTH_HEADER]?.replace('Bearer ', '');
            if (typeof token !== 'string') {
                return null;
            }

            const parsed = decode(token, { json: true }) as TAllJwtPayload | null;
            if (parsed === null) {
                return null;
            }

            return parsed.id;
        } catch {
            return null;
        }
    }

    private static async verifyAndParse(token: string): Promise<TAllJwtPayload> {
        const publicKey = await super.getPublicKey();

        return new Promise<TAllJwtPayload>((resolve, reject) => {
            verify(token, publicKey, verifyOptions, (err, payload) => {
                if (err !== null) {
                    reject(err);
                    return;
                }

                resolve(payload as TAllJwtPayload);
            });
        });
    }

    private validateAudience(payload: TAllJwtPayload, context: ExecutionContext): void {
        const contextHandler = context.getHandler();

        // Check, that token aud is valid for current method if set
        const audiences = this.reflector.get<TokenAudience[]>(USE_AUDIENCES_TOKEN, contextHandler);
        if (!audiences?.length) {
            return;
        }

        if (!audiences.includes(payload.aud)) {
            throw new Error('no access for such user');
        }
    }

    private async checkProtects(payload: TAllJwtPayload, context: ExecutionContext): Promise<void> {
        const contextHandler = context.getHandler();

        const protects = this.reflector.get<EProtect[]>(USE_PROTECT_TOKEN, contextHandler);
        if (!protects?.length) {
            return;
        }

        const req = context.switchToHttp().getRequest();

        if (protects.includes(EProtect.TFA)) {
            await this.checkTfaCode(payload, req.headers[TFA_HEADER] as string);
        }
    }

    private async checkTfaCode(payload: TAllJwtPayload, code: string): Promise<void> {
        const result: { isValid: boolean; message?: string; status: HttpStatus } = await this.rmqClientService.send(
            AUTH_CHECK_TFA_CODE_PATTERN,
            this.rabbitService.buildMessage({ userId: payload.id, code, aud: payload.aud }),
        );

        handleCheckProtectResponse(result, EProtect.TFA);
    }
}
