import { applyDecorators, CanActivate, UseGuards } from '@nestjs/common';
import { ApiBearerAuth } from '@nestjs/swagger';
import { TokenAudience, UseAudiences, EProtect, UseProtect } from './index';
import { JwtGuard, JwtOptionalGuard } from '../guards';

// eslint-disable-next-line @typescript-eslint/ban-types
export type TGuardType = CanActivate | Function;

export interface TApiOptionsAuth {
    audiences?: TokenAudience | TokenAudience[];
    protects?: EProtect | EProtect[];
    guard: TGuardType | TGuardType[];
}

const castToArray = <T = unknown>(value: T | T[]): T[] => (Array.isArray(value) ? value : [value]);

const isProtect = (val: unknown): val is EProtect => Object.values(EProtect).includes(val as EProtect);

const isAudience = (val: unknown): val is TokenAudience => Object.values(TokenAudience).includes(val as TokenAudience);

interface IParsedArgs {
    audiences: TokenAudience[];
    protects: EProtect[];
}

const parseArgs = (args: (TokenAudience | EProtect)[] = []): IParsedArgs => {
    return args.reduce(
        (acc, cur) => {
            if (isProtect(cur)) {
                acc.protects.push(cur);
            } else if (isAudience(cur)) {
                acc.audiences.push(cur);
            }

            return acc;
        },
        { audiences: [], protects: [] } as IParsedArgs,
    );
};

const applyGuards = (options: TApiOptionsAuth): ClassDecorator & MethodDecorator => {
    const { guard, audiences, protects } = options;

    const decorators = [ApiBearerAuth(), UseGuards(...castToArray(guard))];

    if (audiences) {
        decorators.push(UseAudiences(...castToArray(audiences)));
    }

    if (protects) {
        decorators.push(UseProtect(...castToArray(protects)));
    }

    return applyDecorators(...decorators);
};

export const ApiAuth = (...args: (TokenAudience | EProtect)[]): ClassDecorator & MethodDecorator => {
    return applyGuards({ guard: JwtGuard, ...parseArgs(args) });
};

export const ApiOptionalAuth = (...args: (TokenAudience | EProtect)[]): ClassDecorator & MethodDecorator => {
    return applyGuards({ guard: JwtOptionalGuard, ...parseArgs(args) });
};

export const ApiCompositeAuth = (options: TApiOptionsAuth): ClassDecorator & MethodDecorator => {
    return applyGuards(options);
};
