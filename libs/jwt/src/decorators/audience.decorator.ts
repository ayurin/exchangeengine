import { SetMetadata } from '@nestjs/common';
import { CustomDecorator } from '@nestjs/common/decorators/core/set-metadata.decorator';

export const USE_AUDIENCES_TOKEN = 'useAudiences';

export enum TokenAudience {
    ACCOUNT = 'account',
    ADMIN = 'admin',
}

export const UseAudiences = (...audiences: TokenAudience[]): CustomDecorator => SetMetadata(USE_AUDIENCES_TOKEN, audiences);
