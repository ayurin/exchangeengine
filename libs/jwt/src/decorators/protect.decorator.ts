import { applyDecorators, SetMetadata, UseGuards } from '@nestjs/common';
import { ApiHeader } from '@nestjs/swagger';
import { CAPTCHA_HEADER, TFA_HEADER } from '../constants';
import { CaptchaGuard } from '../guards';

export const USE_PROTECT_TOKEN = 'useProtect';

export enum EProtect {
    TFA = 'tfa',
    CAPTCHA = 'captcha',
}

export const CaptchaProtect = (): ClassDecorator & MethodDecorator =>
    applyDecorators(UseGuards(CaptchaGuard), ApiHeader({ name: CAPTCHA_HEADER, description: 'header for check captcha' }));

export const UseProtect = (...protects: EProtect[]): ClassDecorator & MethodDecorator => {
    const decorators: (MethodDecorator & ClassDecorator)[] = [];

    if (protects.includes(EProtect.TFA)) {
        decorators.push(ApiHeader({ name: TFA_HEADER, description: 'header for check tfa' }));
    }

    if (protects.includes(EProtect.CAPTCHA)) {
        decorators.push(CaptchaProtect());
    }

    decorators.push(SetMetadata(USE_PROTECT_TOKEN, protects));

    return applyDecorators(...decorators);
};
