import { createParamDecorator, ExecutionContext, ParamDecoratorEnhancer } from '@nestjs/common';

import { IncomingMessage } from 'http';
import { TAllJwtPayload } from '../types';

// eslint-disable-next-line @typescript-eslint/naming-convention
export const JwtPayload: () => ParamDecoratorEnhancer = createParamDecorator<undefined, ExecutionContext, TAllJwtPayload | undefined>(
    (data: unknown, executionCtx): TAllJwtPayload | undefined => {
        const req = executionCtx.switchToHttp().getRequest<IncomingMessage & { user: TAllJwtPayload | undefined }>();
        return req.user;
    },
);
