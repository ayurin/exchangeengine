import { JWTOptions, TAllJwtPayload } from '../types';
import { Inject, Injectable } from '@nestjs/common';
import { decode, SignOptions, sign } from 'jsonwebtoken';

import { JWT_OPTIONS } from '../constants';
import { TokenAudience } from '../decorators/audience.decorator';

const baseJwtOptions: SignOptions = {
    algorithm: 'RS256',
};

export interface IRequestPayload {
    readonly id: string;
    readonly aud: TokenAudience;
}

@Injectable()
export class JwtService {
    constructor(@Inject(JWT_OPTIONS) private options: JWTOptions) {}

    public setOptions(options: JWTOptions): void {
        this.options = {
            ...this.options,
            ...options,
        };
    }

    public generateToken(payload: IRequestPayload): Promise<string> {
        return this.generate(payload, String(this.options.jwtAccessExpires));
    }

    public decodeUserId(refreshToken: string): string | undefined {
        const decoded = decode(refreshToken) as TAllJwtPayload;
        return decoded?.id;
    }

    private async generate(payload: TAllJwtPayload, expiresIn: string): Promise<string> {
        return new Promise<string>((resolve, reject) => {
            const jwtOptions: SignOptions = {
                ...baseJwtOptions,
                expiresIn,
            };

            sign(payload, String(this.options.privateKey), jwtOptions, (err, token: string) => {
                if (err !== null) {
                    reject(err);
                    return;
                }
                resolve(token);
            });
        });
    }
}
