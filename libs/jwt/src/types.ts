import { JwtPayload } from 'jsonwebtoken';
import { TokenAudience } from './decorators/audience.decorator';

export type TUserJwtPayload = JwtPayload & {
    readonly id: string;
    readonly aud: TokenAudience;
};

export type TAdminJwtPayload = JwtPayload & {
    readonly id: string;
    readonly aud: TokenAudience;
};

export type TAllJwtPayload = TUserJwtPayload | TAdminJwtPayload;

export interface JWTOptions {
    privateKey?: string;
    publicKey?: string;
    jwtAccessExpires?: string;
    jwtRefreshExpires?: string;
}
