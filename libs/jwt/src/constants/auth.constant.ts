export const AUTH_HEADER = 'authorization';
export const TFA_HEADER = 'x-tfa-code';
export const CAPTCHA_HEADER = 'x-captcha-token';
