export const ACCOUNT_QUEUE = 'account';
export const AUTH_QUEUE = 'auth';
export const MATCHER_QUEUE = 'matcher';
export const TRADING_QUEUE = 'trading';
export const BFFWS_QUEUE = 'bffws';
export const ADMIN_QUEUE = 'admin';
export const NOTIFICATIONS_QUEUE = 'notifications';
