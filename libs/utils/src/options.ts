import {
  ACCOUNT_QUEUE,
  ADMIN_QUEUE,
  AUTH_QUEUE,
  BFFWS_QUEUE,
  MATCHER_QUEUE,
  NOTIFICATIONS_QUEUE,
  TRADING_QUEUE,
} from "./queues";
import { RmqOptions, Transport } from "@nestjs/microservices";

const RMQ_USER = process.env.RMQ_USER;
const RMQ_PASSWORD = process.env.RMQ_PASSWORD;
const RMQ_HOST = process.env.RMQ_HOST;
const RMQ_PORT = process.env.RMQ_PORT;
const RMQ_URL = `amqp://${RMQ_USER}:${RMQ_PASSWORD}@${RMQ_HOST}:${RMQ_PORT}`;

export const tradingRmqOptions: RmqOptions = {
  transport: Transport.RMQ,
  options: {
    urls: [RMQ_URL],
    queue: TRADING_QUEUE,
    noAck: false,
    queueOptions: {
      durable: false,
    },
  },
};

export const matcherRmqOptions: RmqOptions = {
  transport: Transport.RMQ,
  options: {
    urls: [RMQ_URL],
    queue: MATCHER_QUEUE,
    noAck: false,
    queueOptions: {
      durable: false,
    },
  },
};

export const accountRmqOptions: RmqOptions = {
  transport: Transport.RMQ,
  options: {
    urls: [RMQ_URL],
    queue: ACCOUNT_QUEUE,
    noAck: false,
    queueOptions: {
      durable: false,
    },
  },
};

export const authRmqOptions: RmqOptions = {
  transport: Transport.RMQ,
  options: {
    urls: [RMQ_URL],
    queue: AUTH_QUEUE,
    noAck: false,
    queueOptions: {
      durable: false,
    },
  },
};

export const bffwsRmqOptions: RmqOptions = {
  transport: Transport.RMQ,
  options: {
    urls: [RMQ_URL],
    queue: BFFWS_QUEUE,
    noAck: false,
    queueOptions: {
      durable: false,
    },
  },
};

export const adminRmqOptions: RmqOptions = {
  transport: Transport.RMQ,
  options: {
    urls: [RMQ_URL],
    queue: ADMIN_QUEUE,
    noAck: false,
    queueOptions: {
      durable: false,
    },
  },
};

export const notificationsRmqOptions: RmqOptions = {
  transport: Transport.RMQ,
  options: {
    urls: [RMQ_URL],
    queue: NOTIFICATIONS_QUEUE,
    noAck: false,
    queueOptions: {
      durable: false,
    },
  },
};
