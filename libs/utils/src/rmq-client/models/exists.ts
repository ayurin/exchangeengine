export interface IExists {
    readonly exists: boolean;
}

export class Exists implements IExists {
    constructor(public readonly exists: boolean) {}
}
