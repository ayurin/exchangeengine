export interface IGenerateTfaSecretPayload {
  userId: string
  email: string
}

export interface IGenerateTfaSecretResponse {
  barcode?: string
  status: number
  message?: string
}

export interface ICheckTfaCodePayload {
  code: string
  userId: string
  aud: 'admin' | 'account'
}

export interface ICheckCaptchaPayload {
  token: string
}

export interface ICheckCodeResponse {
  isValid?: boolean
  status: number
  message?: string
}

export interface IDeleteTfaSecretPayload {
  userId: string
}
