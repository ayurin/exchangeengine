import { IRmqOrder } from './order';

export interface IMatchedTradeBranch extends Omit<IRmqOrder, 'stopLimitPrice'> {
    // readonly price: string;
    readonly completed: boolean;
}

export interface IRmqCancelReport {
    readonly id: string;
    readonly pair: string;
}

export interface IMatcherTrade {
    readonly taker: IMatchedTradeBranch;
    readonly maker: IMatchedTradeBranch;
    readonly volume: string;
    readonly matchingTime: number;
}
