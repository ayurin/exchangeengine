export interface IRmqResult {
    readonly success: boolean;
    readonly message: string;
}

export class RmqResult implements IRmqResult {
    constructor(public readonly success: boolean, public readonly message: string) {}
}

interface IRmqSuccess extends IRmqResult {
    readonly success: true;
}

export class RmqSuccess extends RmqResult implements IRmqSuccess {
    public readonly success!: true;

    constructor(message?: string | null) {
        super(true, message || 'ok');
    }
}

interface IRmqFail extends IRmqResult {
    readonly success: false;
}

export class RmqFail extends RmqResult implements IRmqFail {
    public readonly success!: false;

    constructor(message: string) {
        super(false, message);
    }
}