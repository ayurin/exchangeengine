export interface IAccountingFromOrder {
    customerId: string;
    orderId: string;
    currency: string;
    amount: string;
    feeCurrency: string;
    fee: string;
}

export interface ICurrencyAmount {
    readonly amount: string;
    readonly currency: string;
}

export interface ISettleOrderSide {
    readonly orderId: string;
    readonly customerId: string;
    readonly forExchange: ICurrencyAmount;
    readonly forFee: ICurrencyAmount;
    readonly forRefunds?: ICurrencyAmount;
    readonly forFeeRefunds?: ICurrencyAmount;
}

export interface IIncompleteSettleOrder {
    readonly buyOrder: ISettleOrderSide;
    readonly sellOrder: ISettleOrderSide;
}

export interface ISettleOrder extends IIncompleteSettleOrder {
    readonly tradeId: string;
}

export interface IGetBalance {
    userId: string;
}

export interface IGetBalanceResponse {
    account_number: string;
    currencyId: string;
    balance: string;
}