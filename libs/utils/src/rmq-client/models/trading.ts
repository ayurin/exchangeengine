import { ArrowType } from '@app/ws';
import { IRmqOrder } from './order';

export interface IGetPriceChangeForAssets {
    readonly assets: string[];
    readonly fiat: string;
}

export interface IPriceForAsset {
    asset: string;
    price: string;
}

export interface IPriceChangeForAsset {
    asset: string;
    price: string;
    old_price: string;
    price_change: string;
    price_change_percent: string;
    arrow: ArrowType;
}

export interface ITradingSLActivate {
    slOrder: IRmqOrder;
    matchingTime: number;
    orderTriggerTakerId: string;
    orderTriggerMakerId: string;
}