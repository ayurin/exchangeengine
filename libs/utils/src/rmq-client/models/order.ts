import { EOrderType } from "../../types";

export interface IRmqOrder {
  id: string;
  customerId: string;
  pair: string;
  price: string | null;
  quantity: string | null;
  quantityMarketTotal: string | null;
  stopLimitPrice: string | null;
  buyFlg: boolean;
  base: string;
  quote: string;
  time: number;
  totalQuantity: string | null;
  orderType: EOrderType;
}

export interface IRmqCancelOrder {
  id: string;
  pair: string;
}
