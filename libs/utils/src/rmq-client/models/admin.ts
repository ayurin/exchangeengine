export interface IGetAdminPayload {
  id: string
}

export interface IGetAdminResponse {
  id: string
  email: string
  status: string
  tfaEnable: boolean
}
