import { NotificationType } from '@app/ws';

export interface INotificationData {
    readonly userId: string;
    readonly title: string;
    readonly message?: string;
    readonly type: NotificationType;
    readonly showInHistory: boolean;
    readonly asOfDate: number;
}

// этот интефейс точно измениться, тк мы будем использовать шаблоны и набор параметров для этих шаблонов
export interface IEmailNotificationPayload {
    readonly to: string
    readonly payload: { text: string, subject: string }
}
