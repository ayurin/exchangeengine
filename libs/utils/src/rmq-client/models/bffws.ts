import { INotification, ITick } from '@app/ws';

export interface IBffwsTick {
    data: ITick;
    pair: string;
    userId?: string;
}

export interface IBffwsNotification {
    data: INotification;
    userId: string;
}
