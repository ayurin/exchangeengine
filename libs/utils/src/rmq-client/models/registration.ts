export interface IAuthRegistrationData {
    readonly userId: string;
    readonly login: string;
    readonly password: string;
}
