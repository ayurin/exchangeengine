import { Injectable, OnModuleInit } from "@nestjs/common";
import { Client, ClientProxy, RmqRecord } from "@nestjs/microservices";
import {
  accountRmqOptions,
  adminRmqOptions,
  authRmqOptions,
  bffwsRmqOptions,
  matcherRmqOptions,
  notificationsRmqOptions,
  tradingRmqOptions,
} from "../options";
import {
  ACCOUNT_QUEUE,
  ADMIN_QUEUE,
  AUTH_QUEUE,
  BFFWS_QUEUE,
  MATCHER_QUEUE,
  NOTIFICATIONS_QUEUE,
  TRADING_QUEUE,
} from "../queues";
import { catchError, firstValueFrom, Observable } from "rxjs";
import {
  ACCOUNT_QUEUE_MAP,
  AccountPatterns,
  AccountRmqArgs,
  ADMIN_QUEUE_MAP,
  AdminPatterns,
  AdminRmqArgs,
  AUTH_QUEUE_MAP,
  AuthPatterns,
  AuthRmqArgs,
  BFFWS_QUEUE_MAP,
  BffwsPatterns,
  BffwsRmqArgs,
  MATCHER_QUEUE_MAP,
  MatcherPatterns,
  MatcherRmqArgs,
  NOTIFICATION_QUEUE_MAP,
  NotificationPatterns,
  NotificationRmqArgs,
  TRADING_QUEUE_MAP,
  TradingPatterns,
  TradingRmqArgs,
} from "./patterns";

type RmqPattern =
  | AccountPatterns
  | BffwsPatterns
  | MatcherPatterns
  | NotificationPatterns
  | AuthPatterns
  | TradingPatterns
  | AdminPatterns;

type RmqQueue =
  | typeof ACCOUNT_QUEUE
  | typeof AUTH_QUEUE
  | typeof MATCHER_QUEUE
  | typeof TRADING_QUEUE
  | typeof BFFWS_QUEUE
  | typeof ADMIN_QUEUE
  | typeof NOTIFICATIONS_QUEUE;

export type RmqArgs = AccountRmqArgs &
  BffwsRmqArgs &
  MatcherRmqArgs &
  NotificationRmqArgs &
  AuthRmqArgs &
  TradingRmqArgs &
  AdminRmqArgs;

const RMQ_CLIENT_CONFIG: Record<RmqPattern, RmqQueue> = {
  ...ACCOUNT_QUEUE_MAP,
  ...AUTH_QUEUE_MAP,
  ...BFFWS_QUEUE_MAP,
  ...MATCHER_QUEUE_MAP,
  ...NOTIFICATION_QUEUE_MAP,
  ...TRADING_QUEUE_MAP,
  ...ADMIN_QUEUE_MAP,
};

interface IError {
  message: IError | string;
  path: string;
  statusCode: number;
  timestamp: string;
}

@Injectable()
export class RmqClientService implements OnModuleInit {
  @Client(tradingRmqOptions)
  private tradingClient: ClientProxy;

  @Client(matcherRmqOptions)
  private matcherClient: ClientProxy;

  @Client(accountRmqOptions)
  private accountClient: ClientProxy;

  @Client(authRmqOptions)
  private authClient: ClientProxy;

  @Client(bffwsRmqOptions)
  private bffwsClient: ClientProxy;

  @Client(adminRmqOptions)
  private adminClient: ClientProxy;

  @Client(notificationsRmqOptions)
  private notificationClient: ClientProxy;

  private clientMap: Record<RmqQueue, ClientProxy>;

  public emit<Response, Pattern extends keyof RmqArgs>(
    pattern: Pattern,
    data: RmqRecord<RmqArgs[Pattern]>
  ): Promise<Response> {
    return firstValueFrom(this.emit$(pattern, data));
  }

  public send<Response, Pattern extends keyof RmqArgs>(
    pattern: Pattern,
    data: RmqRecord<RmqArgs[Pattern]>
  ): Promise<Response> {
    return firstValueFrom(this.send$(pattern, data));
  }

  public emit$<Response, Pattern extends keyof RmqArgs>(
    pattern: Pattern,
    data: RmqRecord<RmqArgs[Pattern]>
  ): Observable<Response> {
    const client = this.getClient(pattern);
    return client
      .emit(pattern, data)
      .pipe(catchError((err) => RmqClientService.catchError(err)));
  }

  public send$<Response, Pattern extends keyof RmqArgs>(
    pattern: Pattern,
    data: RmqRecord<RmqArgs[Pattern]>
  ): Observable<Response> {
    const client = this.getClient(pattern);
    return client
      .send(pattern, data)
      .pipe(catchError((err) => RmqClientService.catchError(err)));
  }

  public emitAnything<Request, Response>(
    queue: RmqQueue,
    pattern: string,
    data: RmqRecord<Request>
  ): Promise<Response> {
    return firstValueFrom(this.emitAnything$(queue, pattern, data));
  }

  public sendAnything<Request, Response>(
    queue: RmqQueue,
    pattern: string,
    data: RmqRecord<Request>
  ): Promise<Response> {
    return firstValueFrom(this.sendAnything$(queue, pattern, data));
  }

  public emitAnything$<Request, Response>(
    queue: RmqQueue,
    pattern: string,
    data: RmqRecord<Request>
  ): Observable<Response> {
    const client = this.clientMap[queue];
    return client
      .emit(pattern, data)
      .pipe(catchError((err) => RmqClientService.catchError(err)));
  }

  public sendAnything$<Request, Response>(
    queue: RmqQueue,
    pattern: string,
    data: RmqRecord<Request>
  ): Observable<Response> {
    const client = this.clientMap[queue];
    return client
      .send(pattern, data)
      .pipe(catchError((err) => RmqClientService.catchError(err)));
  }

  public static catchError(err: IError): Promise<never> {
    let message = err.message;
    if (typeof message === "string") {
      try {
        message = JSON.parse(message) as IError;
      } catch {
        throw err;
      }
    }
    if (
      message?.message !== undefined &&
      message?.path !== undefined &&
      message?.statusCode !== undefined &&
      message?.timestamp !== undefined
    ) {
      throw message;
    }
    throw err;
  }

  private getClient(pattern: RmqPattern): ClientProxy {
    const queue = RMQ_CLIENT_CONFIG[pattern];
    const client = this.clientMap[queue];
    if (!client) {
      throw new Error(`Client by pattern ${pattern} not found`);
    }

    return client;
  }

  public onModuleInit(): void {
    this.clientMap = {
      [ACCOUNT_QUEUE]: this.accountClient,
      [AUTH_QUEUE]: this.authClient,
      [BFFWS_QUEUE]: this.bffwsClient,
      [MATCHER_QUEUE]: this.matcherClient,
      [TRADING_QUEUE]: this.tradingClient,
      [ADMIN_QUEUE]: this.adminClient,
      [NOTIFICATIONS_QUEUE]: this.notificationClient,
    };
  }
}
