import { Global, Module } from "@nestjs/common";
import {RmqClientService} from './rmq-client.service';

@Global()
@Module({
    providers: [RmqClientService],
    exports: [RmqClientService],
})
export class RmqClientModule {}
