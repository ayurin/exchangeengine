import { NOTIFICATIONS_QUEUE } from "../../queues";
import { IEmailNotificationPayload, INotificationData } from "../models";

export const NEW_NOTIFICATION_PATTERN = "NEW_NOTIFICATION";
export const NOTIFICATION_EMAIL_PATTERN = "NOTIFICATION_EMAIL_PATTERN";

export type NotificationPatterns =
  | typeof NEW_NOTIFICATION_PATTERN
  | typeof NOTIFICATION_EMAIL_PATTERN;

export const NOTIFICATION_QUEUE_MAP: Record<
  NotificationPatterns,
  typeof NOTIFICATIONS_QUEUE
> = {
  [NEW_NOTIFICATION_PATTERN]: NOTIFICATIONS_QUEUE,
  [NOTIFICATION_EMAIL_PATTERN]: NOTIFICATIONS_QUEUE,
};

export class NotificationRmqArgs
  implements Record<NotificationPatterns, unknown>
{
  public [NEW_NOTIFICATION_PATTERN]: INotificationData;
  public [NOTIFICATION_EMAIL_PATTERN]: IEmailNotificationPayload;
}
