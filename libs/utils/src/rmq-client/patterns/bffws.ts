import { BFFWS_QUEUE } from "../../queues";
import { IBffwsNotification, IBffwsTick } from "../models";
import {
  IBffwsProfile,
  IBffWsSlActivation,
  ITickerUpdates,
} from "@app/ws";

export const BFFWS_TICK_PATTERN = "TICK";
export const BFFWS_TICKER_UPDATES_PATTERN = "TICKER_UPDATES";
export const BFFWS_NOTIFICATION_PATTERN = "NOTIFICATIONS";
export const BFFWS_SL_ACTIVATE_PATTERN = "BFFWS_SL_ACTIVATE_PATTERN";
export const BFFWS_PROFILE_PATTERN = "BFFWS_PROFILE_PATTERN";

export type BffwsPatterns =
  | typeof BFFWS_TICK_PATTERN
  | typeof BFFWS_TICKER_UPDATES_PATTERN
  | typeof BFFWS_NOTIFICATION_PATTERN
  | typeof BFFWS_SL_ACTIVATE_PATTERN
  | typeof BFFWS_PROFILE_PATTERN;

export const BFFWS_QUEUE_MAP: Record<BffwsPatterns, typeof BFFWS_QUEUE> = {
  [BFFWS_TICK_PATTERN]: BFFWS_QUEUE,
  [BFFWS_TICKER_UPDATES_PATTERN]: BFFWS_QUEUE,
  [BFFWS_NOTIFICATION_PATTERN]: BFFWS_QUEUE,
  [BFFWS_SL_ACTIVATE_PATTERN]: BFFWS_QUEUE,
  [BFFWS_PROFILE_PATTERN]: BFFWS_QUEUE,
};

export class BffwsRmqArgs implements Record<BffwsPatterns, unknown> {
  public [BFFWS_TICK_PATTERN]: IBffwsTick;
  public [BFFWS_TICKER_UPDATES_PATTERN]: ITickerUpdates;
  public [BFFWS_NOTIFICATION_PATTERN]: IBffwsNotification;
  public [BFFWS_SL_ACTIVATE_PATTERN]: IBffWsSlActivation;
  public [BFFWS_PROFILE_PATTERN]: IBffwsProfile;
}
