import { ADMIN_QUEUE } from "../../queues";
import { IGetAdminPayload } from "../models";

export const ADMIN_GET_ADMINISTRATOR_PATTERN =
  "ADMIN_GET_ADMINISTRATOR_PATTERN";

export type AdminPatterns = typeof ADMIN_GET_ADMINISTRATOR_PATTERN;

export const ADMIN_QUEUE_MAP: Record<AdminPatterns, typeof ADMIN_QUEUE> = {
  [ADMIN_GET_ADMINISTRATOR_PATTERN]: ADMIN_QUEUE,
};

export class AdminRmqArgs implements Record<AdminPatterns, unknown> {
  public [ADMIN_GET_ADMINISTRATOR_PATTERN]: IGetAdminPayload;
}
