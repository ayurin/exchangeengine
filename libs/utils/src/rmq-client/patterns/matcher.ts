import { MATCHER_QUEUE } from "../../queues";
import { IRmqCancelOrder, IRmqOrder } from "../models";

export const MATCHER_PLACE_ORDER_PATTERN = "PLACE_ORDER";
export const MATCHER_CANCEL_ORDER_PATTERN = "CANCEL_ORDER";

export type MatcherPatterns =
  | typeof MATCHER_PLACE_ORDER_PATTERN
  | typeof MATCHER_CANCEL_ORDER_PATTERN;

export const MATCHER_QUEUE_MAP: Record<MatcherPatterns, typeof MATCHER_QUEUE> =
  {
    [MATCHER_PLACE_ORDER_PATTERN]: MATCHER_QUEUE,
    [MATCHER_CANCEL_ORDER_PATTERN]: MATCHER_QUEUE,
  };

export class MatcherRmqArgs implements Record<MatcherPatterns, unknown> {
  public [MATCHER_PLACE_ORDER_PATTERN]: IRmqOrder;
  public [MATCHER_CANCEL_ORDER_PATTERN]: IRmqCancelOrder;
}
