import { Inject, Injectable, Scope } from '@nestjs/common';
import { REQUEST } from '@nestjs/core';
import { RmqRecord, RmqRecordBuilder } from '@nestjs/microservices';
import { randomUUID } from 'crypto';
import { IncomingMessage } from 'http';
import { IRmqContextHost } from '../types';
import { CORRELATION_ID_HEADER } from '../constants';

@Injectable({ scope: Scope.REQUEST })
export class RabbitService {
    private readonly correlationId: string;

    private static readonly builder = new RmqRecordBuilder();

    constructor(@Inject(REQUEST) req: IncomingMessage | IRmqContextHost) {
		this.correlationId = (req as IncomingMessage)?.headers?.[CORRELATION_ID_HEADER] as string
			|| (req as IRmqContextHost)?.context?.args?.[0]?.correlationId as string;
	}

    public buildMessage<T>(payload: T): RmqRecord<T> {
        return RabbitService.builder
            .setData(payload)
            .setOptions({
                appId: process.env.APP_NAME,
                messageId: randomUUID(),
                correlationId: this.correlationId,
            } as any)
            .build();
    }
}
