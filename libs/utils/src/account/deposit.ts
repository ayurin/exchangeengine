import { IsOptional, IsString } from "class-validator";
import { ApiProperty, ApiPropertyOptional } from "@nestjs/swagger";
import { PaginatedResponse, PaginationQuery } from "../pagination";

export type GetTxPeriod = "last week" | "30 days" | "90 days";

export const getTxPeriods: GetTxPeriod[] = ["last week", "30 days", "90 days"];

export class DepositRequestDto extends PaginationQuery {
  @ApiPropertyOptional({ type: "string", enum: getTxPeriods })
  @IsOptional()
  @IsString()
  public period?: GetTxPeriod;

  @ApiPropertyOptional({
    example: "search string",
    description:
      "Search by iLike (lower(%value%)) in id, user_ids, user.full_name and currency",
  })
  @IsOptional()
  @IsString()
  public search?: string;
}

export enum EOperationStatus {
  Active = "active",
  Rejected = "rejected",
  Confirmed = "confirmed",
}

export type RecentTxType = "deposit" | "withdraw";

export class DepositResponse {
  @ApiProperty({ example: "3CC08408-5B6C-4F07-9A3C-B8B0C2718EA5" })
  public id: string;

  @ApiProperty({ example: 1668619484335 })
  public dateTime: number;

  @ApiProperty({
    type: "string",
    enum: ["deposit", "withdraw"],
    example: "deposit",
  })
  public type?: RecentTxType;

  @ApiProperty({ example: "Spot Wallet" })
  public depositWallet: string;

  @ApiProperty({ example: "BTC" })
  public asset: string;

  @ApiProperty({ example: (253_1100_0000).toString() })
  public amount: string;

  @ApiProperty({ example: "0xdc3fd43453ea369e19ef672b7bb5056eef12f20e" })
  public destination: string;

  @ApiProperty({
    example:
      "0x370c92408ce34a60c34d016c23be921995886d564731bd40e8a4b0d7502342ef",
  })
  public txId: string;

  @ApiProperty({ type: "string", enum: EOperationStatus, example: "confirmed" })
  @IsOptional()
  @IsString()
  public status?: EOperationStatus;
  @ApiProperty({ example: "CF177FB1-3DCB-4EF9-910E-F9E8B15D650E" })
  public customerId: string;

  @ApiProperty({ example: "John" })
  public firstName?: string;

  @ApiProperty({ example: "Snow" })
  public lastName?: string;

  @ApiProperty({ example: new Date() })
  public createdAt: Date;
}

export class PaginatedDepositResponse extends PaginatedResponse<DepositResponse> {
  @ApiProperty({ type: DepositResponse, isArray: true })
  public data: DepositResponse[];
}
