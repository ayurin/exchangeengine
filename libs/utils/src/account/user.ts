import { ELockReason, ELockTime } from "@app/ws";
import { ApiProperty, ApiPropertyOptional } from "@nestjs/swagger";
import {
  IsBoolean,
  IsEmail,
  IsEnum,
  IsNotEmpty,
  IsOptional,
  IsString,
} from "class-validator";
import { PaginatedResponse } from "../pagination";
import { Type } from "class-transformer";

export class RegistrationBody {
  @ApiProperty({ example: "Yusuf ibn Ayyub ibn Shadi" })
  @IsString()
  @IsNotEmpty()
  public firstName!: string;

  @ApiProperty({ example: "Saladin" })
  @IsString()
  @IsNotEmpty()
  public lastName!: string;

  @ApiProperty({ example: "saladin@haqqex.com" })
  @IsEmail()
  @IsNotEmpty()
  public email!: string;

  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  public password!: string;
}

export enum EKycStatus {
  Basic = "BASIC",
  Standard = "STANDARD",
  Pro = "PRO",
  Unverified = "UNVERIFIED",
}

export enum EUserStatus {
  CREATED = "CREATED",
  REGISTERED = "REGISTERED",
  // to be continued...
}

export class PatchUserBody {
  @ApiPropertyOptional({ example: "b56329c0-613e-41ff-bf81-f83d08b8f020" })
  @IsString()
  @IsOptional()
  public id?: string;

  @ApiPropertyOptional({ example: "Yusuf ibn Ayyub ibn Shadi" })
  @IsString()
  @IsOptional()
  public firstName?: string;

  @ApiPropertyOptional({ example: "Saladin" })
  @IsString()
  @IsOptional()
  public lastName?: string;

  @ApiPropertyOptional({
    example: EKycStatus.Unverified,
    enum: Object.values(EKycStatus),
  })
  @IsEnum(EKycStatus)
  @IsOptional()
  public kycStatus?: EKycStatus;

  @ApiPropertyOptional({ example: true })
  @IsBoolean()
  @IsOptional()
  public tfaEnable?: boolean;
}

export class UserResponse {
  @ApiProperty({ example: "b56329c0-613e-41ff-bf81-f83d08b8f020" })
  public id: string;

  @ApiProperty({ example: "Yusuf ibn Ayyub ibn Shadi" })
  public firstName: string;

  @ApiProperty({ example: "Saladin" })
  public lastName: string;

  @ApiProperty({ example: "email@sdfdsf.com" })
  public email: string;

  @ApiProperty({ enum: EUserStatus, example: EUserStatus.CREATED })
  public status: EUserStatus;

  @ApiProperty({
    enum: Object.values(EKycStatus),
    example: EKycStatus.Unverified,
  })
  public kycStatus: EKycStatus;

  @ApiProperty({ example: true })
  public tfaEnable: boolean;

  @ApiProperty({ example: "asjkb21b3" })
  public taxNumber: string;

  @ApiProperty({ example: "100000000" })
  public assetsValue: string;

  @ApiProperty()
  public createdAt!: Date;
}

export class PaginatedUserResponse extends PaginatedResponse<UserResponse> {
  @ApiProperty({ type: UserResponse, isArray: true })
  @Type(() => UserResponse)
  public data!: UserResponse[];
}

export class LockUserBody {
  @ApiPropertyOptional({ example: "b56329c0-613e-41ff-bf81-f83d08b8f020" })
  @IsString()
  @IsOptional()
  public id?: string;

  @ApiProperty({ enum: ELockReason })
  @IsEnum(ELockReason)
  @IsNotEmpty()
  public lockReason: ELockReason;

  @ApiProperty({ enum: ELockTime })
  @IsEnum(ELockTime)
  @IsNotEmpty()
  public lockTime: ELockTime;
}
