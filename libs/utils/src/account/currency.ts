import { ECurrencyType } from "../types";
import { ApiProperty, ApiPropertyOptional } from "@nestjs/swagger";
import { Transform } from "class-transformer";

export class CurrencyResponse {
  @ApiProperty({ example: 8 })
  @Transform(({ value }) => String(value), { toClassOnly: true })
  public decimals: string;

  @ApiProperty({ example: "Bitcoin" })
  public fullName: string;

  @ApiProperty({ example: "07d674c2-f835-4702-8bc7-8b7658b57aac" })
  public id: string;

  @ApiPropertyOptional({ example: "BTC" })
  public isoCode?: string;

  @ApiProperty({ example: "BTC" })
  public shortName: string;

  @ApiPropertyOptional({ example: "4000000000" })
  public coinEstimateValue?: string;

  @ApiPropertyOptional({ example: "4000000000" })
  public fiatEstimateValue?: string;

  @ApiProperty({
    enum: ECurrencyType,
    description: `${ECurrencyType.CRYPTO} = crypto,
        ${ECurrencyType.STABLE} = stable coin,
        ${ECurrencyType.FIAT} =  fiat`,
  })
  public type: ECurrencyType;
}
