import { ApiProperty, ApiPropertyOptional } from "@nestjs/swagger";
import {
  IsEnum,
  IsNotEmpty,
  IsOptional,
  IsString,
  IsUUID,
} from "class-validator";
import { PaginatedResponse, PaginationQuery } from "../pagination";
import { CurrencyResponse } from "./currency";
import { Type } from "class-transformer";

export enum EBlockchains {
  Bitcoin = "bitcoin",
  Ethereum = "ethereum",
  Polkadot = "polkadot",
  Solana = "solana",
  Cosmos = "cosmos",
  Tron = "tron",
  Neo = "neo",
  Cardano = "cardano",
  Dogecoin = "dogecoin",
  Xrp = "xrp",
  Litecoin = "litecoin",
  Islamicoin = "islamicoin",
}

export class NetworkQueryDto extends PaginationQuery {
  @ApiPropertyOptional()
  @IsString()
  @IsOptional()
  public search?: string;

  @ApiPropertyOptional()
  @IsString()
  @IsOptional()
  public currency?: string;
}

export class NetworkPostDto {
  @ApiProperty({ example: EBlockchains.Bitcoin })
  @IsEnum(EBlockchains)
  @IsNotEmpty()
  public name: string;

  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  public nativeToken: string;
}

export class NetworkPatchDto {
  @ApiPropertyOptional()
  @IsEnum(EBlockchains)
  @IsOptional()
  public name?: string;

  @ApiPropertyOptional()
  @IsString()
  @IsOptional()
  public nativeToken?: string;

  @ApiPropertyOptional()
  @IsString()
  @IsOptional()
  public id?: string;
}

export class NetworkResponseDto {
  @ApiProperty({ example: "69167015-A216-4DF5-93BD-B702CF6FA804" })
  public id: string;

  @ApiProperty({ example: "ERC20" })
  public name: string;

  @ApiProperty({ example: "Ethereum" })
  public nativeToken: string;

  @ApiPropertyOptional()
  public arrivalTime?: string;

  @ApiPropertyOptional()
  public feeCoinValue?: string;

  @ApiPropertyOptional()
  public feeFiatValue?: string;

  @ApiProperty()
  public createdAt: Date;

  @ApiProperty()
  public updatedAt: Date;
}

export class ManyNetworksResponseDto extends PaginatedResponse<NetworkResponseDto> {
  @ApiProperty({ type: NetworkResponseDto, isArray: true })
  @Type(() => NetworkResponseDto)
  public data: NetworkResponseDto[];
}

export enum ENetworkCurrencyStatus {
  Enable = "enable",
  Disable = "disable",
  Maintenance = "maintenance",
}

export class NetworkCurrencyPostDto {
  @ApiProperty({ example: "1DA78B9C-6256-42C0-9FE7-727D33762F2C" })
  @IsUUID()
  @IsNotEmpty()
  public currencyId: string;

  @ApiProperty({ example: "2A05B97E-56CB-4912-9E8C-1FD80FEC9555" })
  @IsUUID()
  @IsNotEmpty()
  public networkId: string;

  @ApiProperty({
    example: ENetworkCurrencyStatus.Enable,
    enum: Object.values(ENetworkCurrencyStatus),
  })
  @IsEnum(ENetworkCurrencyStatus)
  @IsNotEmpty()
  public status: ENetworkCurrencyStatus;
}

export class NetworkCurrencyQueryDto extends PaginationQuery {
  @ApiPropertyOptional({
    description: "May be currency id or currency short name",
  })
  @IsString()
  @IsOptional()
  public currency?: string;

  @ApiPropertyOptional({ description: "May be network id or network name" })
  @IsString()
  @IsOptional()
  public network?: string;
}

export class NetworkCurrencyDto {
  @ApiProperty({
    example: ENetworkCurrencyStatus.Maintenance,
    enum: Object.values(ENetworkCurrencyStatus),
  })
  public status: ENetworkCurrencyStatus;

  @ApiProperty()
  public createdAt: Date;

  @ApiProperty()
  public updatedAt: Date;

  @ApiProperty({ type: CurrencyResponse })
  @Type(() => CurrencyResponse)
  public currency: CurrencyResponse;

  @ApiProperty({ type: NetworkResponseDto })
  @Type(() => NetworkResponseDto)
  public network: NetworkResponseDto;
}

export class ManyNetworkCurrencyResponseDto extends PaginatedResponse<NetworkCurrencyDto> {
  @ApiProperty({ type: NetworkCurrencyDto })
  @Type(() => NetworkCurrencyDto)
  public data: NetworkCurrencyDto[];
}
