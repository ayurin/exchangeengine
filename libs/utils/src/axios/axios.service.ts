import { Injectable, HttpException, Scope, Inject } from '@nestjs/common';
import { AxiosRequestConfig, AxiosStatic } from 'axios';
import { IncomingMessage } from "http";
import { REQUEST } from "@nestjs/core";
import { CORRELATION_ID_HEADER, AUTH_HEADER } from '../constants';
import { IRmqContextHost } from '../types';

const axios: AxiosStatic = require('axios');

@Injectable({ scope: Scope.REQUEST })
export class AxiosService {
	private readonly correlationId: string;
	private readonly authorization?: string;

	constructor(@Inject(REQUEST) req: IncomingMessage | IRmqContextHost) {
		this.correlationId = (req as IncomingMessage)?.headers?.[CORRELATION_ID_HEADER] as string
			|| (req as IRmqContextHost)?.context?.args?.[0]?.correlationId as string;
		this.authorization =  (req as IncomingMessage)?.headers?.[AUTH_HEADER]
	}


	public get<T>(url: string, config?: AxiosRequestConfig): Promise<T> {
		return axios.get<T>(url, this.mergeConfig(config))
			.then((res) => res.data)
			.catch(this.parseError);
	}

	public async post<T>(url: string, requestData?: any, config?: AxiosRequestConfig): Promise<T> {
		try {
			const res = await axios.post<T>(url, requestData, this.mergeConfig(config));
			return res.data;
		} catch (err: any) {
			return this.parseError(err);
		}
	}

	public request<T>(config: AxiosRequestConfig): Promise<T> {
		return axios.request<T>(this.mergeConfig(config))
			.then((res) => res.data)
			.catch(this.parseError);
	}

	private parseError(err: any): never {
		if(err?.response?.data) {
			// @ts-ignore:next-line
			const msg = err.response.data?.message;
			// @ts-ignore:next-line
			throw new HttpException(msg, err.response.data.statusCode);
		} else {
			throw new HttpException(err.message, err.status);
		}
	}

	private mergeConfig(config?: AxiosRequestConfig): AxiosRequestConfig {
		return Object.assign(
			{},
			config,
			{
				headers: {
					[CORRELATION_ID_HEADER]: this.correlationId,
					[AUTH_HEADER]: this.authorization
				}
			},
		) as AxiosRequestConfig;
	}
}
