import { Channel, Message } from 'amqplib';
import { RmqContext } from '@nestjs/microservices';

export const RmqWraper = (_target: Object, _key: String, descriptor: any) => {
	const originalMethod = descriptor.value;
	descriptor.value = async function (...args: [any, RmqContext]): Promise<any> {
			const [, context] = args;
			const channel = context.getChannelRef() as Channel;
			const originalMsg = context.getMessage() as Message;
			try {
					const res = await originalMethod.apply(this, args);
					channel.ack(originalMsg);
					return res;
			} catch {
					await new Promise(r => setTimeout(r, 10000));
					channel.reject(originalMsg, true);
			}
	};
	return descriptor;
};

export const SQLRaWraper = (_target: Object, _key: String, descriptor: any) => {
	const originalMethod = descriptor.value;
	descriptor.value = async function (...args: any[]): Promise<any> {
			const queryRunner = (this as any).dataSource.createQueryRunner();
			await queryRunner.connect();
			await queryRunner.startTransaction();
			try {
					const res = await originalMethod.apply(this, [...args, queryRunner]);
					await queryRunner.commitTransaction();
					return res
			} catch (err) {
					await queryRunner.rollbackTransaction();
					throw err
			} finally {
					await queryRunner.release();
			}
	};
	return descriptor;
};