export enum EOrderBookSide {
    ASKS = 'asks',
    BIDS = 'bids',
}

export enum EOrderSide {
    BUY = 'buy',
    SELL = 'sell',
}

export enum EOrderRole {
    MAKER = 'maker',
    TAKER = 'taker',
}

export enum EOrderType {
    LIMIT = 'limit',
    MARKET_QUANTITY = 'marketQuantity',
    MARKET_TOTAL = 'marketTotal',
    STOP_LIMIT = 'stopLimit',
    STOP_LIMIT_PRIORITY = 'stopLimitPriority',
    // STOP_MARKET = 'stopMarket',
}

export enum EValidityPeriod {
    GTC = 'GTC',
}

export enum ECurrencyType {
    CRYPTO = 'C',
    STABLE = 'S',
    FIAT = 'F',
}

export enum ECurrencyPairType {
    // C - Crypto
    // S - StableCoin
    // F - Fiat
    CC = 'CC',
    CS = 'CS',
    SC = 'SC',
    SS = 'SS',
    CF = 'CF',
    SF = 'SF',
    FC = 'FC',
    FS = 'FS',
    FF = 'FF',
}

export interface IRmqContextHost {
	readonly context?: {
		readonly args?: [{
			readonly correlationId?: string;
		}]
	};
}
