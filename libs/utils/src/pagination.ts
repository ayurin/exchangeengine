import { ApiProperty, ApiPropertyOptional } from "@nestjs/swagger";
import {
  IsIn,
  IsNumber,
  IsNumberString,
  IsOptional,
  IsString,
  Max,
  Min,
} from "class-validator";
import { Transform } from "class-transformer";

export interface IPaginationQuery {
  page?: number;
  limit?: number;
  orderBy?: string;
  order?: number;
}

export class PaginationOptions {
  public skip: number = 0;
  public take: number = 20;
  public order: number = -1;
  public orderSQL: "ASC" | "DESC" = "DESC";
  public orderBy?: string;

  constructor(pagination: IPaginationQuery) {
    this.take = pagination?.limit ?? this.take;

    const page = pagination?.page ?? 1;
    if (page > 1) {
      this.skip = this.take * (page - 1);
    }

    this.order = pagination?.order ?? -1;
    this.orderSQL = this.order === -1 ? "DESC" : "ASC";
    this.orderBy = pagination?.orderBy;
  }
}

export class PaginationQuery implements IPaginationQuery {
  @ApiPropertyOptional({ type: "integer" })
  @IsOptional()
  @Transform(({ value }) => parseInt(value as string, 10))
  @IsNumber()
  @Min(1)
  public page?: number;

  @ApiPropertyOptional({ type: "integer" })
  @IsOptional()
  @Transform(({ value }) => parseInt(value as string, 10))
  @IsNumber()
  @Min(1)
  @Max(500)
  public limit?: number;

  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  public orderBy?: string;

  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  public search?: string;

  @ApiPropertyOptional({ type: "integer", enum: [1, -1] })
  @IsOptional()
  @Transform(({ value }) => parseInt(value as string, 10))
  @IsNumber()
  @IsIn([-1, 1])
  public order?: 1 | -1;

  public get paginationOptions(): PaginationOptions {
    return new PaginationOptions(this);
  }
}

export class PaginateApiQuery {
  @ApiPropertyOptional({ type: "integer" })
  @IsOptional()
  @IsNumberString()
  public page?: number;

  @ApiPropertyOptional({ type: "integer" })
  @IsOptional()
  @IsNumberString()
  public limit?: number;

  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  public sortBy?: string;

  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  public searchBy?: string;

  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  public search?: string;
}

export class ApiMeta {
  @ApiProperty()
  public itemsPerPage!: number;

  @ApiProperty()
  public totalItems!: number;

  @ApiProperty()
  public currentPage!: number;

  @ApiProperty()
  public totalPages!: number;

  @ApiPropertyOptional()
  public sortBy?: any;

  @ApiPropertyOptional()
  public searchBy?: any;

  @ApiPropertyOptional()
  public search?: string;

  @ApiPropertyOptional()
  public filter?: any;
}

export class ApiLinks {
  @ApiPropertyOptional()
  public first?: string;

  @ApiPropertyOptional()
  public previous?: string;

  @ApiProperty()
  public current!: string;

  @ApiPropertyOptional()
  public next?: string;

  @ApiPropertyOptional()
  public last?: string;
}

export class PaginatedApiResponse<T> {
  @ApiProperty({ type: Object, isArray: true })
  public data!: Array<T>;

  @ApiProperty({ type: ApiMeta })
  public meta!: ApiMeta;

  @ApiProperty({ type: ApiLinks })
  public links!: ApiLinks;
}

export class PaginatedResponse<T> implements IPaginationQuery {
  @ApiProperty({ type: Object, isArray: true })
  public data!: Array<T>;

  @ApiProperty({ type: "integer" })
  public total: number;

  @ApiPropertyOptional({ type: "integer" })
  public page?: number;

  @ApiPropertyOptional({ type: "integer" })
  public limit?: number;

  @ApiPropertyOptional({ type: "string" })
  public orderBy?: string;

  @ApiPropertyOptional({ type: "integer" })
  public order?: number;

  constructor(data: Array<T>, total: number, pagination: IPaginationQuery) {
    Object.assign(this, pagination);
    this.data = data;
    this.total = total;
  }
}
