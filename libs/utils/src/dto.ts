import {ApiProperty, ApiPropertyOptional} from '@nestjs/swagger';
import { IsArray, IsNotEmpty, IsNumber, IsUUID } from 'class-validator';

export class OkResponse {
    constructor(message?: string) {
        this.message = message;
    }
    @ApiProperty({ example: true })
    public readonly ok = true;

    @ApiPropertyOptional({ example: 'Some message' })
    public readonly message?: string;
}

export const toOkResponse = (message?: string) => new OkResponse(message);

export class IdDto {
    @ApiProperty({ example: 'ecc12d97-5347-488e-bead-b86945ec3f48' })
    @IsUUID(4)
    @IsNotEmpty()
    public readonly id!: string;
}

export class IdListDto {
    @ApiProperty({ example: ['ecc12d97-5347-488e-bead-b86945ec3f48'], isArray: true })
    @IsArray()
    @IsNotEmpty()
    public readonly id!: string[];
}

export class IdNumDto {
    @ApiProperty({ example: 1 })
    @IsNumber()
    @IsNotEmpty()
    public readonly id!: number;
}

export class IdNumListDto {
    @ApiProperty({ example: [1], isArray: true })
    @IsArray()
    @IsNotEmpty()
    public readonly id!: number[];
}

export class IdResponse {
    @ApiProperty({ example: 'ecc12d97-5347-488e-bead-b86945ec3f48' })
    public readonly id: string;

    constructor(id: string) {
        this.id = id;
    }
}
