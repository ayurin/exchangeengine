import { randomBytes } from 'crypto';
import { deserialize, serialize } from 'v8';

import { HttpException, HttpStatus } from '@nestjs/common';
import { endOfDay, isValid, startOfDay } from 'date-fns';
import { Big } from 'big.js';
import { ECurrencyPairType, ECurrencyType } from './types';

export const DEFAULT_PAGE_SIZE = 100;
export const DEFAULT_START_PAGE_NUMBER = 1;

export const DEFAULT_SCALE = 8;
export const DEFAULT_PRECISION = 8;
export const BINANCE_PRECISION = 8;

export const ZERO_BIGINT = BigInt(0);

export const getRandomArbitrary = (min: number, max: number): number => Math.random() * (max - min) + min;

export const getRandomInt = (min: number, max: number): number => {
    const ceilMin = Math.ceil(min);
    return Math.floor(Math.random() * (Math.floor(max) - ceilMin + 1)) + ceilMin;
};

export const randomSign = (): 1 | -1 => (Math.random() > 0.5 ? 1 : -1);

export const toScaledBig = (val: string | number | Big, scale = DEFAULT_SCALE): Big => Big(val).times(10 ** scale);

export const toUnscaledBig = (val: string | number | Big, scale = DEFAULT_SCALE): Big => Big(val).div(10 ** scale);

export const toUnscaledString = (
    val: string | number | Big,
    precision = DEFAULT_PRECISION,
    scale = DEFAULT_SCALE,
): string =>
    Big(val)
        .div(10 ** scale)
        .toFixed(precision);

export const toBigIntFromBinance = (val: string): bigint => {
    return BigInt(
        Big(val)
            .mul(10 ** BINANCE_PRECISION)
            .toString(),
    );
};

export const fromBigIntToBinance = (val: bigint): string => {
    return Big(val.toString())
        .div(10 ** BINANCE_PRECISION)
        .toString();
};

export const multiplyAmounts = (a: bigint | string, b: bigint | string, scale = DEFAULT_SCALE): string => {
    return ((BigInt(a) * BigInt(b)) / BigInt(10 ** scale)).toString();
}

export const divideAmounts = (divisible: bigint | string, divisor: bigint | string, scale = DEFAULT_SCALE): string => {
    return ((BigInt(divisible) * BigInt(10 ** scale)) / BigInt(divisor)).toString();
}

export const SYMBOL_DELIMITER = '-';

export const transformToBigintWithNull = (value: unknown): bigint | null => {
    if (
        typeof value === 'string' ||
        typeof value === 'number' ||
        typeof value === 'bigint' ||
        typeof value === 'boolean'
    ) {
        return BigInt(value);
    }
    return null;
};

export const transformToBigint = (value: unknown): bigint => {
    if (
        typeof value === 'string' ||
        typeof value === 'number' ||
        typeof value === 'bigint' ||
        typeof value === 'boolean'
    ) {
        return BigInt(value);
    }
    return ZERO_BIGINT;
};

export const transformToStringWithNull = (value: unknown): string | null => (value ? String(value) : null);

export const transformToString = (value: number | bigint): string => String(value);

export const transformToTimestamp = (value: Date): number => value.valueOf();

export const transformToDate = (value: unknown, type: 'start' | 'end' | 'now'): Date | null => {
    const date = new Date(value as string);
    if (!value || !isValid(date)) return null;
    if (type === 'start') return startOfDay(date);
    if (type === 'end') return endOfDay(date);
    return date;
};

export const extractBase = (symbol: string): string => {
    if (symbol.includes(SYMBOL_DELIMITER)) {
        return symbol.split(SYMBOL_DELIMITER)[0];
    }
    return '';
};

export const extractQuote = (symbol: string): string => {
    if (symbol.includes(SYMBOL_DELIMITER)) {
        return symbol.split(SYMBOL_DELIMITER)[1];
    }
    return '';
};

export function valueOrFail<T>(value: T | undefined | null, name: string): T {
    if (value) return value;
    throw new HttpException(`${name} not found`, HttpStatus.NOT_FOUND);
}

export function checkExists<T, K extends keyof T>(value: T | null, name: string, key: K): void {
    if (value) {
        throw new HttpException(`${name} '${value[key]}' already exists`, HttpStatus.NOT_ACCEPTABLE);
    }
}

export const generateRandomString = (): string => {
    // it is steel 16 random bytes like uuid but shorter in a utf8 veiw
    return randomBytes(16).toString('base64url');
};

export const tfStringToNum = (value: unknown): number | null => {
    if (typeof value === 'number') return value;
    return typeof value === 'string' ? parseInt(value, 10) : null;
};

export const checkColumnIn = (name: string, values: (number | string)[]): string => {
    const preparedValue = values.map((value) => (typeof value === 'string' ? `'${value}'` : value));
    return `${name} in (${preparedValue.join(`, `)})`;
};

export const transformToUppercase = (value: unknown): string | null => {
    if (typeof value === 'string') {
        return value.toUpperCase();
    }
    return null;
};

export const encodeBase64 = (str: string): string => Buffer.from(str, 'utf8').toString('base64');
export const decodeBase64 = (str: string): string => Buffer.from(str, 'base64').toString('utf8');

export const serializeAnythingToV8Base64 = <T = any>(arg: T): string => serialize(arg).toString('base64');
export const deserializeFromV8Base64 = <T = any>(str: string): T => deserialize(Buffer.from(str, 'base64')) as T;

export const promiseDelay = (ms: number): Promise<void> => new Promise((resolve) => setTimeout(resolve, ms));

export const calculatePairType = (baseCurrencyType: ECurrencyType | { type: ECurrencyType }, quoteCurrencyType: ECurrencyType | {type: ECurrencyType}): ECurrencyPairType => {
    const baseCurrency = typeof baseCurrencyType === 'string' ? baseCurrencyType : baseCurrencyType?.type;
    if (!baseCurrency) {
        throw new Error('incorrect base currency type');
    }
    const quoteCurrency = typeof quoteCurrencyType === 'string' ? quoteCurrencyType : quoteCurrencyType?.type;
    if (!quoteCurrency) {
        throw new Error('incorrect quote currency type');
    }
    // @ts-ignore
    return ECurrencyPairType[baseCurrency + quoteCurrency];
}

function baseUnique<T>(value: T, index: number, self: T[], withoutNull: boolean): boolean {
    return withoutNull && !value ? false : self.findIndex((item) => item === value) === index;
}
export function uniq<T>(array: T[], withoutNull = false): T[] {
    return array.filter((value, index, self) => baseUnique(value, index, self, withoutNull));
}

function baseUniqueBy<T, K extends keyof T>(value: T, index: number, self: T[], iteratee: K): boolean {
    return self.findIndex((item) => item[iteratee] === value[iteratee]) === index;
}

/**
 * Метод для быстрой фильтрации списка по уникальности определленного свойства у элементов, например по id.
 */
export function uniqBy<T, K extends keyof T>(array: T[], iteratee: K): T[] {
    return array && array.length ? array.filter((value, index, self) => baseUniqueBy(value, index, self, iteratee)) : [];
}

export function reduceBigIntBy<T, K extends keyof T>(list: T[], key: K): bigint {
    if (!list?.length) return BigInt(0);
    return <bigint>list.reduce((acc: bigint, item) => acc + BigInt(item[key] as unknown as string), BigInt(0));
}

export function reduceBigInt(list: (bigint | boolean | number | string)[]): bigint {
    if (!list?.length) return BigInt(0);
    return <bigint>list.reduce((acc: bigint, item) => acc + BigInt(item), BigInt(0));
}

export function percentFrom(all: bigint | boolean | number | string, part: bigint | boolean | number | string): bigint {
    if (!all || BigInt(all) === ZERO_BIGINT) {
        return ZERO_BIGINT;
    }
    return BigInt(part) * BigInt(100) * BigInt(10**8) / BigInt(all);
}

export const enumValidMessage = <T>(name: string, enumValue: Record<string, T>): { message: string } => {
    return { message: `${name} must be a valid enum value - ${Object.values(enumValue).join(', ')}` };
};

export const isLockError = (err: unknown): boolean | undefined => {
    return (err as Error).message?.startsWith('could not obtain lock on row in relation');
};