import { PaginateApiQuery, PaginatedApiResponse } from "../pagination";
import { ApiProperty, ApiPropertyOptional } from "@nestjs/swagger";
import {
  IsArray,
  IsBoolean,
  IsEnum,
  IsNotEmpty,
  IsNumberString,
  IsOptional,
  IsString,
} from "class-validator";
import { Transform } from "class-transformer";
import { enumValidMessage } from "../common";
import { ArrowType } from "@app/ws";
import { ECurrencyPairType } from "../types";

export class GetCurrencyPairDto extends PaginateApiQuery {
  @ApiPropertyOptional({ example: null })
  @IsOptional()
  @Transform(({ value }) => JSON.parse(value as string) as boolean)
  @IsBoolean()
  public favorite?: boolean;

  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  public ticker: string;
}

export class AddPairBody {
  @ApiProperty({ example: "BTC" })
  @IsString()
  @IsNotEmpty()
  public base!: string;

  @ApiProperty({ example: "USDC" })
  @IsString()
  @IsNotEmpty()
  public quote!: string;

  @ApiProperty({ example: "1000000" })
  @IsNumberString()
  @IsNotEmpty()
  public minOrderAmount!: string;

  @ApiProperty({ example: "10000000000" })
  @IsNumberString()
  @IsNotEmpty()
  public maxOrderAmount!: string;

  @ApiProperty({ example: "1000000" })
  @IsNumberString()
  @IsNotEmpty()
  public amountIncrement!: string;

  @ApiProperty({ example: "1000000" })
  @IsNumberString()
  @IsNotEmpty()
  public priceIncrement!: string;
}

export class PatchPairBody {
  @ApiPropertyOptional()
  @IsString()
  @IsNotEmpty()
  public id?: string;

  @ApiPropertyOptional({ example: "BTC" })
  @IsString()
  @IsOptional()
  public base?: string;

  @ApiPropertyOptional({ example: "USDC" })
  @IsString()
  @IsOptional()
  public quote?: string;

  @ApiPropertyOptional({ example: "1000000" })
  @IsNumberString()
  @IsOptional()
  public minOrderAmount?: string;

  @ApiPropertyOptional({ example: "10000000000" })
  @IsNumberString()
  @IsOptional()
  public maxOrderAmount?: string;

  @ApiPropertyOptional({ example: "1000000" })
  @IsNumberString()
  @IsOptional()
  public amountIncrement?: string;

  @ApiPropertyOptional({ example: "1000000" })
  @IsNumberString()
  @IsOptional()
  public priceIncrement?: string;
}

export enum ECurrencyPairStatus {
  ACTIVE = "active",
  INACTIVE = "inactive",
}

export class SetPairListStatusBody {
  @ApiProperty({ enum: ECurrencyPairStatus })
  @IsEnum(ECurrencyPairStatus, enumValidMessage("status", ECurrencyPairStatus))
  @IsNotEmpty()
  public status: ECurrencyPairStatus;

  @ApiProperty({ type: "string", isArray: true })
  @IsArray()
  @IsNotEmpty()
  public ids: string[];
}

export class CurrencyPairResponse {
  @ApiProperty()
  public id: string;

  @ApiProperty({ example: "BTC-USDC" })
  public pair: string;

  @ApiProperty({ example: "BTC" })
  public base: string;

  @ApiProperty({ example: "USDC" })
  public quote: string;

  @ApiProperty({ enum: ECurrencyPairType })
  public type: ECurrencyPairType;

  @ApiProperty({ example: "1000000" })
  public minOrderAmount: string;

  @ApiProperty({ example: "10000000000" })
  public maxOrderAmount?: string;

  @ApiProperty({ example: "1000000" })
  public amountIncrement: string;

  @ApiProperty({ example: "1000000" })
  public priceIncrement: string;

  @ApiProperty({ example: false })
  public favorite: boolean;

  @ApiProperty({ enum: ECurrencyPairStatus })
  public status: ECurrencyPairStatus;

  @ApiProperty()
  public createdAt: Date;

  @ApiProperty()
  public updatedAt: Date;

  @ApiProperty({ example: "202560000000" })
  public price: string;

  @ApiProperty({ example: "-219000000" })
  public deltaPct24: string;

  @ApiProperty({ example: `['0.01', '0.10', '0.50', '1.00']` })
  public groupingLevels: string[];

  @ApiPropertyOptional({ example: "up" })
  public arrow: ArrowType;
}

export class PaginatedExtendedCurrencyPairResponse extends PaginatedApiResponse<CurrencyPairResponse> {
  @ApiProperty({ type: CurrencyPairResponse, isArray: true })
  public data!: CurrencyPairResponse[];
}
