import {ExceptionFilter} from "./filter/";
import {Module, Scope} from "@nestjs/common";
import {HandlerResolverService} from "./filter/handler-resolver.service";
import {APP_FILTER} from "@nestjs/core";
import {TypeOrmExceptionHandler} from "./filter/handlers/type-orm-exception.handler";
import {AxiosExceptionHandler} from "./filter/handlers/axios-exception.handler";
import {InternalExceptionHandler} from "./filter/handlers/internal-exception.handler";
import {JwtExceptionHandler} from "./filter/handlers/jwt-exception.handler";

@Module({
providers:[
    {
        provide: APP_FILTER,
        scope: Scope.REQUEST,
        useClass: ExceptionFilter,
    },
    HandlerResolverService,
    TypeOrmExceptionHandler,
    AxiosExceptionHandler,
    InternalExceptionHandler,
    JwtExceptionHandler,
    ]
})
export class ExceptionModule{}