import { HttpException } from "@nestjs/common";

export const SESSION_EXPIRED_CODE = 419;
export const SESSION_EXPIRED_MESSAGE = 'Session Expired';

export class SessionExpiredExcepion extends HttpException {
    constructor() {
        super({
            statusCode: SESSION_EXPIRED_CODE,
            message: SESSION_EXPIRED_MESSAGE,
            error: SESSION_EXPIRED_MESSAGE,
        }, SESSION_EXPIRED_CODE);
    }
}
