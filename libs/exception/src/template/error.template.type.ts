import {ApiProperty} from "@nestjs/swagger";

export class ErrorTemplateType{
    @ApiProperty()
    public statusCode!: number;
    @ApiProperty()
    public path!: string;
    @ApiProperty()
    public timestamp!: string;
    @ApiProperty()
    public message!: string;
}