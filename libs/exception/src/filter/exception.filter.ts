import { IncomingMessage, ServerResponse } from 'http';
import {
    ArgumentsHost,
    Catch,
    ContextType,
    ExceptionFilter as IExceptionFilter,
    HttpStatus,
    RpcExceptionFilter as IRpcExceptionFilter,
} from '@nestjs/common';
import { BaseRpcExceptionFilter, RmqContext, RpcException } from '@nestjs/microservices';
import { HandlerResolverService } from "./handler-resolver.service";
import { ErrorTemplateType } from '../template';
import { Observable, of } from 'rxjs';

@Catch()
export class ExceptionFilter extends BaseRpcExceptionFilter implements IExceptionFilter, IRpcExceptionFilter {
    constructor(
        private readonly handlerResolverService: HandlerResolverService,
    ) {
        super();
    }

    public catch(exception: any, host: ArgumentsHost): Observable<void> {
        const handler = this.handlerResolverService.resolve(exception);

        const contextType = host.getType() as ContextType;
        const errorPath = this.getPath(host, contextType);

        const responseBody = handler.handle(exception, errorPath, new Date().toISOString());

        return this.replyWithError(host, contextType, responseBody);
    }

    private replyWithError(host: ArgumentsHost, contextType: ContextType, responseBody: ErrorTemplateType): Observable<void> {
        switch (contextType) {
            case 'http':
                return of(this.replyViaHttp(host, responseBody));

            case 'rpc':
                return this.replyViaRpc(host, responseBody);

            case 'ws':
                // TODO: implement ws context handling
                console.error(`I don't know how to handle a WS contextType at the ExceptionFilter yet`);
                process.exit(1);

            default:
                console.error(`unknown contextType at the ExceptionFilter: ${contextType}`);
                process.exit(1);
        }
    }

    private replyViaHttp(host: ArgumentsHost, responseBody: ErrorTemplateType): void {
        const ctx = host.switchToHttp();
        let response = ctx.getResponse<ServerResponse | {raw: ServerResponse}>();
        response = response instanceof ServerResponse
            ? response
            : response?.raw;

        if (!(response instanceof ServerResponse)) {
            console.error(`can't get a ServerResponse object at the ExceptionFilter`);
            throw responseBody;
        }

        // TODO: remove when cors will be set up by devops
        response.setHeader('Access-Control-Allow-Origin', '*');
        response.writeHead(
            responseBody.statusCode || HttpStatus.INTERNAL_SERVER_ERROR,
            { 'Content-Type': 'application/json' },
        );
        response.end(JSON.stringify(responseBody));
    }

    private replyViaRpc(host: ArgumentsHost, responseBody: ErrorTemplateType): Observable<void> {
        const rpcError = new RpcException({
            code: responseBody.statusCode,
            // so far it has not been possible to find normal ways in Nest
            // to transmit the metadata in response with an error,
            // so we will sew this data into the message body
            message: JSON.stringify(responseBody),
        });

        return super.catch(rpcError, host);
    }

    private getPath(host: ArgumentsHost, contextType: ContextType): string {
        switch (contextType) {
            case 'http':
                return this.getPathFromHttp(host);

            case 'rpc':
                return this.getPathFromRpc(host);

            case 'ws':
                // TODO: implement ws context handling
                console.error(`I don't know how to handle a WS contextType at the ExceptionFilter yet`);
                process.exit(1);

            default:
                console.error(`unknown contextType at the ExceptionFilter: ${contextType}`);
                process.exit(1);
        }
    }

    private getPathFromHttp(host: ArgumentsHost): string {
        const httpHost = host.switchToHttp();
        const req = httpHost.getRequest<IncomingMessage>();
        return req.url as string;
    }

    private getPathFromRpc(host: ArgumentsHost): string {
        const rpcHost = host.switchToRpc();
        const ctx = rpcHost.getContext() as RmqContext;
        return ctx.getPattern();
    }
}