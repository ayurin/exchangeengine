import { Injectable} from "@nestjs/common";
import {ErrorTemplateType} from "../../template";
import axios, { AxiosError } from "axios";
import { ExceptionHandlerInterface } from "./exception-handler.interface";
import { IsSoughtForExpressionInterface } from "../handler-resolver.service";

@Injectable()
export class AxiosExceptionHandler implements ExceptionHandlerInterface, IsSoughtForExpressionInterface {
    handle(exception: AxiosError, path: string, timestamp: string): ErrorTemplateType {
        const responseBody = new ErrorTemplateType()
        Object.assign(responseBody, exception?.response?.data)
        responseBody.path = path;
        responseBody.timestamp = timestamp;
        return responseBody;
    }

    isSoughtForException(exception: Error): boolean {
        return axios.isAxiosError(exception);
    }
}