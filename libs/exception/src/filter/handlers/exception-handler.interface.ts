import {ErrorTemplateType} from "../../template";

export interface ExceptionHandlerInterface {
    handle(exception: any, path:string, timestamp: string): ErrorTemplateType;
}
