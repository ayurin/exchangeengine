import {HttpStatus, Injectable} from "@nestjs/common";
import {QueryFailedError, TypeORMError} from 'typeorm';
import {ErrorTemplateType} from "../../template";
import { IsSoughtForExpressionInterface } from "../handler-resolver.service";
import { ExceptionHandlerInterface } from "./exception-handler.interface";

@Injectable()
export class TypeOrmExceptionHandler implements ExceptionHandlerInterface, IsSoughtForExpressionInterface {
    handle(exception: Error, path: string, timestamp: string): ErrorTemplateType {
        const responseBody = new ErrorTemplateType()
        Object.assign(responseBody,
            {
                statusCode: HttpStatus.BAD_REQUEST,
                timestamp: timestamp,
                path: path,
                message: exception.message
            });
        return responseBody;
    }

    isSoughtForException(exception: Error): boolean {
        return (exception instanceof QueryFailedError) || (exception instanceof TypeORMError);
    }
}