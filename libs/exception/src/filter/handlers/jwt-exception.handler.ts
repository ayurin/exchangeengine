import { HttpStatus, Injectable} from "@nestjs/common";
import { ErrorTemplateType } from "../../template";
import { ExceptionHandlerInterface } from "./exception-handler.interface";
import { IsSoughtForExpressionInterface } from "../handler-resolver.service";
import { JsonWebTokenError, TokenExpiredError } from "jsonwebtoken";
import { SESSION_EXPIRED_CODE, SESSION_EXPIRED_MESSAGE } from "../../exceptions";

@Injectable()
export class JwtExceptionHandler implements ExceptionHandlerInterface, IsSoughtForExpressionInterface {
    handle(exception: JsonWebTokenError, path: string, timestamp: string): ErrorTemplateType {
        const responseBody = new ErrorTemplateType()
        responseBody.statusCode = exception instanceof TokenExpiredError
            ? SESSION_EXPIRED_CODE
            : HttpStatus.UNAUTHORIZED;
        responseBody.message = exception instanceof TokenExpiredError
            ? SESSION_EXPIRED_MESSAGE
            : 'Unauthorized';
        responseBody.path = path;
        responseBody.timestamp = timestamp;
        return responseBody;
    }

    isSoughtForException(exception: Error): boolean {
        return exception instanceof JsonWebTokenError;
    }
}