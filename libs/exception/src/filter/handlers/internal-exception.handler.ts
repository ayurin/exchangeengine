import {HttpException, HttpStatus, Injectable} from "@nestjs/common";
import {ErrorTemplateType} from "../../template";
import { ExceptionHandlerInterface } from "./exception-handler.interface";

@Injectable()
export class InternalExceptionHandler implements ExceptionHandlerInterface {
    handle(exception: any, path: string, timestamp: string): ErrorTemplateType {
        const responseBody = new ErrorTemplateType()
        let httpStatus =
            exception instanceof HttpException
                ? exception.getStatus()
                : HttpStatus.INTERNAL_SERVER_ERROR;
        let errorMessage =
            exception instanceof HttpException
                ? exception.getResponse()
                : exception.message;
        Object.assign(responseBody,
            {
                statusCode: httpStatus,
                timestamp: timestamp,
                path: path,
                message: errorMessage
            });
        return responseBody;
    }

}