import {Injectable} from "@nestjs/common";
import {TypeOrmExceptionHandler} from "./handlers/type-orm-exception.handler";
import {ExceptionHandlerInterface} from "./handlers/exception-handler.interface";
import {AxiosExceptionHandler} from "./handlers/axios-exception.handler";
import {InternalExceptionHandler} from "./handlers/internal-exception.handler";
import { JwtExceptionHandler } from "./handlers/jwt-exception.handler";

export interface IsSoughtForExpressionInterface {
    isSoughtForException(exception: any): boolean;
}
@Injectable()
export class HandlerResolverService{
    private readonly handlers: (IsSoughtForExpressionInterface & ExceptionHandlerInterface) [];
    constructor(
        private readonly internalExceptionHandler: InternalExceptionHandler,
        private readonly typeOrmExceptionHandler: TypeOrmExceptionHandler,
        private readonly axiosExceptionHandler: AxiosExceptionHandler,
        private readonly jwtExceptionHandler: JwtExceptionHandler,
        ) {
        this.handlers = [this.typeOrmExceptionHandler, this.axiosExceptionHandler, this.jwtExceptionHandler]
    }
    resolve(exception: Error): ExceptionHandlerInterface {
        const handler = this.handlers.find(h => h.isSoughtForException(exception));

        if (!handler) {
            return this.internalExceptionHandler;
        }

        return handler;
    }

}