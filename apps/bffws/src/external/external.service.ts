import { IBffwsNotification, IBffwsTick } from '@app/utils';
import {
    IBffwsProfile,
    IBffWsSlActivation,
    ICandle,
    ITickerUpdates,
    NEW_TICK_EVENT,
    NOTIFICATIONS_EVENT,
    PROFILE_EVENT,
    SL_ACTIVATE_EVENT,
    TICKER_UPDATES_EVENT,
} from '@app/ws';
import { IOrder } from '@app/ws';
import { Injectable } from '@nestjs/common';

import { GatewayService } from '../gateway';

@Injectable()
export class ExternalService {
    constructor(private gatewayService: GatewayService) {}

    public async handleNewTick({ data, userId, pair }: IBffwsTick): Promise<void> {
        let wsData = data;
        if (data.trade) {
            const candles = ExternalService.mapToCandle(data.trade.maker, data.trade.volume);
            wsData = { ...data, candles };
        }
        await this.gatewayService.sendData(NEW_TICK_EVENT, wsData, { pair }, userId);
    }

    public async slActivation(data: IBffWsSlActivation): Promise<void> {
        await this.gatewayService.sendData(SL_ACTIVATE_EVENT, data, undefined, data.slOrderOwnerId);
    }

    public async handleNewTickerUpdates(data: ITickerUpdates): Promise<void> {
        await this.gatewayService.sendData(TICKER_UPDATES_EVENT, data, { ticker: data.ticker });
        await this.gatewayService.sendData(TICKER_UPDATES_EVENT, data, { favorite: data.pair });
    }

    public async handleNotifications({ userId, ...data }: IBffwsNotification): Promise<void> {
        await this.gatewayService.sendData(NOTIFICATIONS_EVENT, data, undefined, userId);
    }

    public async handleUserProfile(data: IBffwsProfile): Promise<void> {
        await this.gatewayService.sendData(PROFILE_EVENT, data, undefined, data.id);
    }

    private static mapToCandle({ price }: IOrder, volume: string): ICandle {
        return {
            ask: price,
            bid: price,
            closePrice: price,
            highPrice: price,
            lowPrice: price,
            openPrice: price,
            volume,
        };
    }
}
