import {
    BFFWS_NOTIFICATION_PATTERN,
    BFFWS_PROFILE_PATTERN,
    BFFWS_SL_ACTIVATE_PATTERN,
    BFFWS_TICK_PATTERN,
    BFFWS_TICKER_UPDATES_PATTERN,
    IBffwsNotification,
    IBffwsTick,
    OkResponse,
} from '@app/utils';
import { IBffwsProfile, IBffWsSlActivation, ITickerUpdates } from '@app/ws';
import { Body, Controller, HttpStatus, Post } from '@nestjs/common';
import { Ctx, MessagePattern, Payload, RmqContext } from '@nestjs/microservices';
import { ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { Channel, Message } from 'amqplib';

import { BffwsTick } from './dto/tick.dto';
import { ExternalService } from './external.service';

@ApiTags('external')
@Controller('external')
export class ExternalController {
    constructor(private externalService: ExternalService) {}

    @Post('tick')
    @ApiOperation({ summary: 'Test tick' })
    @ApiResponse({ status: HttpStatus.OK, type: OkResponse })
    public async placeOrder(@Body() data: BffwsTick): Promise<OkResponse> {
        await this.externalService.handleNewTick(data);
        return new OkResponse();
    }

    @MessagePattern(BFFWS_TICK_PATTERN)
    public async handleNewTick(@Payload() data: IBffwsTick, @Ctx() context: RmqContext): Promise<void> {
        const channel = context.getChannelRef() as Channel;
        const originalMsg = context.getMessage() as Message;

        await this.externalService.handleNewTick(data);

        channel.ack(originalMsg);
    }

    @MessagePattern(BFFWS_TICKER_UPDATES_PATTERN)
    public async handleNewTickerUpdates(@Payload() data: ITickerUpdates, @Ctx() context: RmqContext): Promise<void> {
        const channel = context.getChannelRef() as Channel;
        const originalMsg = context.getMessage() as Message;

        await this.externalService.handleNewTickerUpdates(data);

        channel.ack(originalMsg);
    }

    @MessagePattern(BFFWS_NOTIFICATION_PATTERN)
    public async handleNotifications(@Payload() data: IBffwsNotification, @Ctx() context: RmqContext): Promise<void> {
        const channel = context.getChannelRef() as Channel;
        const originalMsg = context.getMessage() as Message;

        await this.externalService.handleNotifications(data);

        channel.ack(originalMsg);
    }

    @MessagePattern(BFFWS_PROFILE_PATTERN)
    public async handleUserProfile(@Payload() data: IBffwsProfile, @Ctx() context: RmqContext): Promise<void> {
        const channel = context.getChannelRef() as Channel;
        const originalMsg = context.getMessage() as Message;

        await this.externalService.handleUserProfile(data);

        channel.ack(originalMsg);
    }

    @MessagePattern(BFFWS_SL_ACTIVATE_PATTERN)
    public async slActivation(@Payload() data: IBffWsSlActivation, @Ctx() context: RmqContext): Promise<void> {
        const channel = context.getChannelRef() as Channel;
        const originalMsg = context.getMessage() as Message;

        await this.externalService.slActivation(data);

        channel.ack(originalMsg);
    }
}
