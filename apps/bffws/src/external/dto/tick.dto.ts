import { IBffwsTick } from '@app/utils';
import { ICandle, IOrderBook, IOrderBookSide, ITick } from '@app/ws';
import { ArrowType, IOrder, ITrade } from '@app/ws';
import { ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { IsBoolean, IsNotEmpty, IsNumber, IsOptional, IsString, ValidateNested } from 'class-validator';

export class OrderBookSideDto implements IOrderBookSide {
    @ApiProperty({ example: '800000000000' })
    @IsString()
    public price: string;

    @ApiProperty({ example: '100000000' })
    @IsString()
    public volume: string;
}

export class OrderBookDto implements IOrderBook {
    @ApiProperty({ isArray: true })
    @IsOptional()
    @ValidateNested()
    @Type(() => OrderBookSideDto)
    public buy?: OrderBookSideDto[];

    @ApiProperty({ isArray: true })
    @IsOptional()
    @ValidateNested()
    @Type(() => OrderBookSideDto)
    public sell?: OrderBookSideDto[];
}

export class CandleDto implements ICandle {
    @ApiProperty({ example: '100000000' })
    @IsString()
    public ask: string;

    @ApiProperty({ example: '100000000' })
    @IsString()
    public bid: string;

    @ApiProperty({ example: '100000000' })
    @IsString()
    public closePrice: string;

    @ApiProperty({ example: '100000000' })
    @IsString()
    public highPrice: string;

    @ApiProperty({ example: '100000000' })
    @IsString()
    public lowPrice: string;

    @ApiProperty({ example: '100000000' })
    @IsString()
    public openPrice: string;

    @ApiProperty({ example: '100000000' })
    @IsString()
    public volume: string;
}

export class RmqOrderReportDto implements IOrder {
    @ApiProperty({ example: 'BTC' })
    @IsString()
    public base: string;

    @ApiProperty({ example: false })
    @IsBoolean()
    public buyFlg: boolean;

    @ApiProperty({ example: true })
    @IsBoolean()
    public readonly completed: boolean;

    @ApiProperty({ example: 'c9083d98-192d-4943-ab7c-1fa12c9fe3eb' })
    @IsString()
    public customerId: string;

    @ApiProperty({ example: 'fe42c499-4a51-42b6-8ace-20408b3246ae' })
    @IsString()
    public id: string;

    @ApiProperty({ example: 'BTC-USDC' })
    @IsString()
    public pair: string;

    @ApiProperty({ example: '800000000000' })
    @IsString()
    public readonly price: string;

    @ApiProperty({ example: '100000000' })
    @IsString()
    public quantity: string;

    @ApiProperty({ example: 'USDC' })
    @IsString()
    public quote: string;

    @ApiProperty({ example: '0', type: 'string' })
    @IsOptional()
    @IsString()
    public stopLimitPrice?: string;

    @ApiProperty({ example: 1665779825349 })
    @IsNumber()
    public time: number;

    @ApiProperty({ example: 1 })
    @IsNumber()
    public version: number;
}

export class TradeDto implements ITrade {
    @ApiProperty()
    @IsOptional()
    @ValidateNested()
    @Type(() => RmqOrderReportDto)
    public readonly maker: RmqOrderReportDto;

    @ApiProperty({ example: 1665779825 })
    @IsNumber()
    public readonly matchingTime: number;

    @ApiProperty()
    @IsOptional()
    @ValidateNested()
    @Type(() => RmqOrderReportDto)
    public readonly taker: RmqOrderReportDto;

    @ApiProperty({ example: '100000000' })
    @IsString()
    public readonly volume: string;

    @ApiProperty({ example: 'up' })
    @IsString()
    public readonly arrowType?: ArrowType;
}

export class TickDto implements ITick {
    @ApiProperty({ example: 1665779825 })
    @IsNumber()
    public asOfDate: number;

    @ApiProperty()
    @IsOptional()
    @ValidateNested()
    @Type(() => CandleDto)
    public candles?: CandleDto;

    @ApiProperty()
    @IsOptional()
    @ValidateNested()
    @Type(() => OrderBookDto)
    public orderBook?: OrderBookDto;

    @ApiProperty()
    @IsOptional()
    @ValidateNested()
    @Type(() => TradeDto)
    public trade?: TradeDto;

    @ApiProperty()
    @IsOptional()
    @IsBoolean()
    public updateOrders?: boolean;
}

export class BffwsTick implements IBffwsTick {
    @ApiProperty()
    @IsNotEmpty()
    @ValidateNested()
    @Type(() => TickDto)
    public data: TickDto;

    @ApiProperty()
    @IsOptional()
    @IsString()
    public userId?: string;

    @ApiProperty()
    @IsString()
    public pair: string;
}
