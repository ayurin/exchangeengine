import { Module } from '@nestjs/common';

import { GatewayModule } from '../gateway';

import { ExternalController } from './external.controller';
import { ExternalService } from './external.service';

@Module({
    imports: [GatewayModule],
    controllers: [ExternalController],
    providers: [ExternalService],
})
export class ExternalModule {}
