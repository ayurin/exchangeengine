import { join } from 'path';

import { ExceptionModule } from '@app/exception';
import { Module } from '@nestjs/common';
import { ServeStaticModule } from '@nestjs/serve-static';
import { LoggerModule } from 'nestjs-pino';
import { RedisModule } from 'nestjs-redis';

import { ApiDocModule } from './api-doc/api-doc.module';
import { loggerModuleAsyncParams, redisModuleOption } from './env';
import { ExternalModule } from './external';
import { GatewayModule } from './gateway';
import { HealthModule } from './health';

@Module({
    imports: [
        ServeStaticModule.forRoot({
            rootPath: join(__dirname, './client'),
            serveRoot: '/public',
        }),
        ExceptionModule,
        LoggerModule.forRootAsync(loggerModuleAsyncParams),
        RedisModule.register(redisModuleOption),
        HealthModule,
        ExternalModule,
        GatewayModule,
        ApiDocModule,
    ],
    controllers: [],
    providers: [],
})
export class AppModule {}
