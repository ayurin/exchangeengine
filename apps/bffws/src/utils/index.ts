const MINUTE = 1;
const HOUR = 60 * MINUTE;
const DAY = 24 * HOUR;
const WEEK = 7 * DAY;
const INTERVAL_CONFIG: Record<number, string> = {
    [MINUTE]: '1m',
    [5 * MINUTE]: '5m',
    [10 * MINUTE]: '10m',
    [15 * MINUTE]: '15m',
    [30 * MINUTE]: '30m',
    [HOUR]: '1h',
    [4 * HOUR]: '4h',
    [DAY]: '1d',
    [WEEK]: '1w',
};

export function getTimeFrames(now = new Date()): string[] {
    const intervals: string[] = [];
    const [minute, hour, day] = [now.getMinutes(), now.getHours(), now.getDay()];
    const minutes = BigInt(minute + hour * HOUR + (day === 1 ? 7 : 1) * DAY);
    Object.keys(INTERVAL_CONFIG).forEach((i) => {
        if (minutes % BigInt(i) === 0n) {
            intervals.push(INTERVAL_CONFIG[i] as string);
        }
    });
    return intervals;
}
