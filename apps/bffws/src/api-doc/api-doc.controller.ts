import { readFileSync } from 'fs';

import { Controller, Get } from '@nestjs/common';
import { ApiExcludeController } from '@nestjs/swagger';
import { OpenAPIObject } from '@nestjs/swagger/dist/interfaces';

@Controller('api-doc')
@ApiExcludeController()
export class ApiDocController {
    @Get()
    public getApiDoc(): Promise<OpenAPIObject> {
        return readFileSync(require.resolve('../swagger.json'), 'utf-8') as unknown as Promise<OpenAPIObject>;
    }
}
