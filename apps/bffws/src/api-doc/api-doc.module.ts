import { Module } from '@nestjs/common';

import { ApiDocController } from './api-doc.controller';

@Module({
    controllers: [ApiDocController],
})
export class ApiDocModule {}
