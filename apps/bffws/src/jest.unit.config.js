const baseConfig = require('./jest.config');

module.exports = {
    ...baseConfig,
    collectCoverageFrom: [...baseConfig.collectCoverageFrom, '!**/types/**/*'],
    testPathIgnorePatterns: ['./node_modules/', '(.*).controller(.*)', './e2e/'],
};
