import { JwtGuard } from '@app/jwt';
import { OnGatewayConnection, OnGatewayDisconnect, OnGatewayInit, WebSocketGateway, WebSocketServer } from '@nestjs/websockets';
import { Redis } from 'ioredis';
import { InjectPinoLogger, PinoLogger } from 'nestjs-pino';
import { RedisService } from 'nestjs-redis';
import { Server, Socket } from 'socket.io';

const CLIENT_REDIS_KEY = 'bff_ws_clients';

@WebSocketGateway({ cors: true })
export class GatewayService implements OnGatewayInit, OnGatewayConnection, OnGatewayDisconnect {
    @WebSocketServer()
    private readonly server: Server;

    private readonly redis: Redis;

    constructor(
        @InjectPinoLogger(GatewayService.name)
        private logger: PinoLogger,
        private redisService: RedisService,
    ) {
        this.redis = redisService.getClient();
    }

    public async sendData<T>(event: string, data: T, query?: Record<string, string>, userId?: string): Promise<void> {
        const clients = await this.getClientsForSend(userId);
        const filteredClients = clients.filter((client) => GatewayService.hasQuery(client, query));
        if (filteredClients?.length) {
            const clientIds = filteredClients.map((client) => client.id);
            this.server.in(clientIds).emit(event, data);
            this.logger.info({ event, data }, 'Send to ws');
        }
    }

    private async getClientsForSend(userId?: string): Promise<Socket[]> {
        if (userId) {
            const clientIds = await this.getClientIdsFromRedis(userId);
            return clientIds.reduce((acc, clientId) => {
                const client = this.server.sockets.sockets.get(clientId);
                if (client) acc.push(client);
                return acc;
            }, [] as Socket[]);
        }
        return Array.from(this.server.sockets.sockets.values());
    }

    private static hasQuery(client: Socket, query?: Record<string, string>): boolean {
        if (query) {
            const clientQuery = client.handshake.query;
            return Object.keys(query).every((key) => {
                const clientQueryValue = String(clientQuery[key] || '');
                const values = clientQueryValue.split(',');
                if (values?.length > 1) return values.includes(query[key]);
                return query[key] === clientQueryValue;
            });
        }
        return true;
    }

    public async afterInit(): Promise<void> {
        await this.redis.del(CLIENT_REDIS_KEY);
        this.logger.info('gateway init');
    }

    public async handleConnection(client: Socket): Promise<void> {
        this.logger.info({ id: client.id, ...client.handshake.query }, 'New client connection');
        const jwtId = await JwtGuard.checkWsAuthorizationAndGetUserID(client);
        if (!jwtId) {
            return;
        }

        await this.setClientToRedis(jwtId, client.id);
        const clientIds = await this.getClientIdsFromRedis(jwtId);
        this.logger.info({ clientIds }, `Client connected id:${client.id} jwt:${jwtId}`);
    }

    public async handleDisconnect(client: Socket): Promise<void> {
        const jwtId = await JwtGuard.checkWsAuthorizationAndGetUserID(client);
        if (!jwtId) {
            return;
        }
        await this.delClientFromRedis(jwtId, client.id);

        const clientIds = await this.getClientIdsFromRedis(jwtId);
        this.logger.info({ clientIds }, `Client disconnected id:${client.id} jwt:${jwtId}`);
    }

    private async setClientToRedis(jwtId: string, clientId: string): Promise<void> {
        const clientIds = await this.getClientIdsFromRedis(jwtId);
        clientIds.push(clientId);
        await this.redis.hset(CLIENT_REDIS_KEY, jwtId, JSON.stringify(clientIds));
    }

    private async delClientFromRedis(jwtId: string, clientId: string): Promise<void> {
        const clientIds = await this.getClientIdsFromRedis(jwtId);
        const filteredIds = clientIds.filter((id) => id !== clientId);
        if (!filteredIds.length) {
            await this.redis.hdel(CLIENT_REDIS_KEY, jwtId);
        } else {
            await this.redis.hset(CLIENT_REDIS_KEY, jwtId, JSON.stringify(filteredIds));
        }
    }

    private async getClientIdsFromRedis(jwtId: string): Promise<string[]> {
        const clientStrIds = await this.redis.hget(CLIENT_REDIS_KEY, jwtId);
        return typeof clientStrIds === 'string' ? (JSON.parse(clientStrIds) as string[]) : [];
    }
}
