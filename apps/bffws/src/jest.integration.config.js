process.env.DOTENV_CONFIG_PATH = '../.env.test';

module.exports = {
    preset: 'ts-jest',
    testEnvironment: 'node',
    clearMocks: true,
    resetMocks: false,
    testLocationInResults: true,
    testPathIgnorePatterns: ['/dist'],
    testRegex: ['./e2e/(.*).controller(.*)'],
    reporters: ['default', 'jest-sonar'],
    moduleNameMapper: {
        '^@bffws/(.*)$': '<rootDir>/$1',
    },
    setupFiles: ['dotenv/config'],
};
