import { Module } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { FastifyAdapter, NestFastifyApplication } from '@nestjs/platform-fastify';
import waitPort from 'wait-port';

const TEST_HOST = '0.0.0.0';
const TEST_PORT = 8889;

@Module({})
class ApplicationModule {}

export async function bootstrapMicroservice(): Promise<NestFastifyApplication> {
    const app = await NestFactory.create<NestFastifyApplication>(ApplicationModule, new FastifyAdapter());

    await app.listen(TEST_PORT, TEST_HOST);
    await waitPort({ host: TEST_HOST, port: TEST_PORT });
    return app;
}
