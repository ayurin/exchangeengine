import { INestApplication, ValidationPipe } from '@nestjs/common';
import { TestingModule } from '@nestjs/testing';

import { appConfig } from './config';
import { appVersioningOptions } from './env';

export function createTestMicroservice(module: TestingModule, appValidationPipe: ValidationPipe): INestApplication {
    const app = module.createNestApplication();
    if (appValidationPipe) {
        app.useGlobalPipes(appValidationPipe);
    }

    app.setGlobalPrefix(appConfig.API_PREFIX);
    app.enableVersioning(appVersioningOptions);

    return app;
}
