import { ValidationPipe } from '@nestjs/common';
import { FastifyAdapter, NestFastifyApplication } from '@nestjs/platform-fastify';
// eslint-disable-next-line import/no-extraneous-dependencies
import { TestingModule } from '@nestjs/testing';

import { API_PREFIX, appVersioningOptions } from './env';

export function createTestMicroservice(
    module: TestingModule,
    appValidationPipe: ValidationPipe,
): NestFastifyApplication {
    const app = module.createNestApplication<NestFastifyApplication>(new FastifyAdapter());
    if (appValidationPipe) {
        app.useGlobalPipes(appValidationPipe);
    }

    app.setGlobalPrefix(API_PREFIX);
    app.enableVersioning(appVersioningOptions);

    return app;
}
