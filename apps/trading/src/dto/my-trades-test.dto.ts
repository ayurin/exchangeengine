import { randomUUID } from 'crypto';

import { EOrderSide } from '@app/utils';
import { ApiProperty } from '@nestjs/swagger';
import { IsEnum, IsNotEmpty, IsNumber, IsString, IsUUID, Max, Min } from 'class-validator';

const uuid = randomUUID();

export class MyTradesTestDto {
    @ApiProperty({ example: 1 })
    @IsNumber()
    @Min(0)
    @Max(2)
    @IsNotEmpty()
    public jwtId: number;

    @ApiProperty({ example: uuid })
    @IsUUID()
    @IsNotEmpty()
    public tradeId: string;

    @ApiProperty({ example: 'BTC-USD' })
    @IsString()
    @IsNotEmpty()
    public symbol!: string;

    @ApiProperty({ example: '0x2a3946310fef9724c26780094ebcb261e0D0efb0' })
    @IsString()
    @IsNotEmpty()
    public userAccount: string;

    @ApiProperty({ enum: EOrderSide, example: EOrderSide.SELL })
    @IsEnum(EOrderSide)
    public type: EOrderSide;

    @ApiProperty({ example: '100' })
    @IsString()
    @IsNotEmpty()
    public price: string;

    @ApiProperty({ example: '1' })
    @IsString()
    @IsNotEmpty()
    public volume: string;

    @ApiProperty({ example: '100' })
    @IsString()
    @IsNotEmpty()
    public amount: string;

    @ApiProperty({ example: '10' })
    @IsString()
    @IsNotEmpty()
    public fee: string;

    @ApiProperty({ example: 'BTC' })
    @IsString()
    @IsNotEmpty()
    public feeCurrency: string;
}
