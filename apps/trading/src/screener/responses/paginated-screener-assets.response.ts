import { PaginatedApiResponse } from '@app/utils';
import { ApiProperty } from '@nestjs/swagger';

import { ScreenerAssetsResponse } from './screener-assets.response';

export class PaginatedScreenerAssetsResponse extends PaginatedApiResponse<ScreenerAssetsResponse> {
    @ApiProperty({ type: ScreenerAssetsResponse, isArray: true })
    public readonly data: ScreenerAssetsResponse[];
}
