import { ApiProperty } from '@nestjs/swagger';

export class ScreenerAssetsResponse {
    @ApiProperty({ example: 'true' })
    public isFavorite: boolean;

    @ApiProperty({ example: 'false' })
    public isHalal: boolean;

    @ApiProperty({ example: 'BTC' })
    public shortName: string;

    @ApiProperty({ example: 'Bitcoin' })
    public fullName: string;

    @ApiProperty({ example: '198090000000' })
    public price: string;

    @ApiProperty({ example: '20000000' })
    public change: string;

    @ApiProperty({ example: '300000000' })
    public changeDay: string;

    @ApiProperty({ example: '400000000' })
    public changeWeek: string;

    @ApiProperty({ example: '500000000' })
    public changeMonth: string;

    @ApiProperty({ example: '198090000000' })
    public volume: string;

    @ApiProperty({ example: '198090000000' })
    public marketCap: string;

    @ApiProperty({ example: '' })
    public chart: string;

    @ApiProperty({ example: '# 1' })
    public rank: string;

    @ApiProperty({ example: '178090000000' })
    public minPrice: string;

    @ApiProperty({ example: '208090000000' })
    public maxPrice: string;

    @ApiProperty({ example: '188090000000' })
    public minDayPrice: string;

    @ApiProperty({ example: '201090000000' })
    public maxDayPrice: string;

    @ApiProperty({ example: '90' })
    public technicalRating: string;
}
