import { Module } from '@nestjs/common';

import { ScreenerController } from './screener.controller';

@Module({
    controllers: [ScreenerController],
})
export class ScreenerModule {}
