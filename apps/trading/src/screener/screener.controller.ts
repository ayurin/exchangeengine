import { Controller, Get, HttpStatus } from '@nestjs/common';
import { ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { Paginate, PaginateQuery } from 'nestjs-paginate';

import { SCREENER_ASSETS_MOCK_RESULT } from './constants/screener-assets-mock-result.constant';
import { PaginatedScreenerAssetsResponse } from './responses/paginated-screener-assets.response';

@ApiTags('screener')
@Controller('screener')
export class ScreenerController {
    @Get('assets')
    @ApiOperation({ summary: 'Get screener assets' })
    @ApiResponse({ status: HttpStatus.OK, type: PaginatedScreenerAssetsResponse })
    public getScreenerAssets(@Paginate() paginateQuery: PaginateQuery): PaginatedScreenerAssetsResponse {
        const totalItems = SCREENER_ASSETS_MOCK_RESULT.length;
        const limit = paginateQuery.limit || 10;

        return {
            data: SCREENER_ASSETS_MOCK_RESULT,
            meta: {
                itemsPerPage: limit,
                totalItems,
                currentPage: paginateQuery.page || 1,
                totalPages: Math.ceil(totalItems / limit),
            },
            links: { current: '' },
        };
    }
}
