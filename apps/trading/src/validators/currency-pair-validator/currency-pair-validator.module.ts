import { Module } from '@nestjs/common';

import { CurrencyPairRepository } from '@trading/dao';
import { CurrencyPairConstraint } from '@trading/validators';

@Module({
    providers: [CurrencyPairConstraint, CurrencyPairRepository],
    exports: [CurrencyPairConstraint],
})
export class CurrencyPairValidatorModule {}
