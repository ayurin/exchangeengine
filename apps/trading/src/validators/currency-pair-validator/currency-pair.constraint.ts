import { Injectable } from '@nestjs/common';
import {
    registerDecorator,
    ValidationOptions,
    ValidatorConstraint,
    ValidatorConstraintInterface,
} from 'class-validator';

import { CurrencyPairRepository } from '@trading/dao';

const DEFAULT_OPTIONS: ValidationOptions = {
    message: 'Currency pair not found',
};

@ValidatorConstraint({ async: true })
@Injectable()
export class CurrencyPairConstraint implements ValidatorConstraintInterface {
    constructor(private currencyPairRepository: CurrencyPairRepository) {}

    public validate(pair: unknown): Promise<boolean> {
        return this.currencyPairRepository.findOneBy({ pair: String(pair) }).then((currencyPair) => {
            return !!currencyPair;
        });
    }
}

export function IsCurrencyPair(validationOptions = DEFAULT_OPTIONS) {
    return function isCurrencyPair(object: object, propertyName: string) {
        registerDecorator({
            target: object.constructor,
            propertyName,
            options: validationOptions,
            constraints: [],
            validator: CurrencyPairConstraint,
        });
    };
}
