import { join } from 'path';

import { ExceptionModule } from '@app/exception';
import { RabbitModule, RmqClientModule } from '@app/utils';
import { Module } from '@nestjs/common';
import { ServeStaticModule } from '@nestjs/serve-static';
import { TypeOrmModule } from '@nestjs/typeorm';
import { LoggerModule } from 'nestjs-pino';
import { RedisModule } from 'nestjs-redis';

import { ApiDocModule } from '@trading/api-doc/api-doc.module';
import { CandlesModule } from '@trading/candles';
import { CurrencyPairModule } from '@trading/currency-pair';
import { dataSourceOptions, loggerModuleAsyncParams, redisModuleOption } from '@trading/env';
import { FeeModule } from '@trading/fee';
import { HealthModule } from '@trading/health/health.module';
import { OrderModule } from '@trading/order';
import { OrderBookModule } from '@trading/order-book';
import { TradesModule } from '@trading/trades';

import { ScreenerModule } from './screener/screener.module';
import { SettingsModule } from './settings/settings.module';

@Module({
    imports: [
        ServeStaticModule.forRoot({
            rootPath: join(__dirname, '../client'),
            serveRoot: '/public',
        }),
        ExceptionModule,
        LoggerModule.forRootAsync(loggerModuleAsyncParams),
        RedisModule.register(redisModuleOption),
        TypeOrmModule.forRoot(dataSourceOptions),
        HealthModule,
        OrderBookModule,
        TradesModule,
        OrderModule,
        CandlesModule,
        FeeModule,
        ApiDocModule,
        CurrencyPairModule,
        SettingsModule,
        ScreenerModule,
        RmqClientModule,
        RabbitModule,
    ],
})
export class AppModule {}
