import { MigrationInterface, QueryRunner } from 'typeorm';

export class DealSeed1660679165390 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`INSERT INTO "deal" 
            (id, buy_order_id, sell_order_id, volume, symbol, taker_buy_flg, as_of_date)
            VALUES(uuid_generate_v4(), uuid_generate_v4(), uuid_generate_v4(), 1000000000, 'ETH-BTC', true, now());`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`DELETE FROM "deal"`);
    }
}
