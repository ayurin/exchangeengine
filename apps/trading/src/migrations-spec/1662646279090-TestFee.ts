import { MigrationInterface, QueryRunner } from 'typeorm';

import { CurrencyPairEntity, FeeEntity } from '@trading/dao';
import {
    fakePairs,
    FIRST_TARGET_MAKER_FEE_MOCH,
    FIRST_TARGET_TAKER_FEE_MOCH,
    FIRST_TYPE_MAKER_FEE_MOCH,
    FIRST_TYPE_TAKER_FEE_MOCH,
    SECOND_TARGET_MAKER_FEE_MOCH,
    SECOND_TARGET_TAKER_FEE_MOCH,
    SECOND_TYPE_MAKER_FEE_MOCH,
    SECOND_TYPE_TAKER_FEE_MOCH,
} from '@trading/e2e/moch/fee.moch';

export class TestFee1662646279090 implements MigrationInterface {
    public async up(qr: QueryRunner): Promise<void> {
        const now = Date.now();

        const someFakePair = fakePairs[0];

        await qr.query(`
            INSERT INTO "${FeeEntity.TABLE_NAME}"
            (currency_pair_id, maker_fee_percent, taker_fee_percent, currency_type, created_at)
            SELECT
                id,
                ${FIRST_TARGET_MAKER_FEE_MOCH} as maker_fee_percent,
                ${FIRST_TARGET_TAKER_FEE_MOCH} as taker_fee_percent,
                '${someFakePair.type}' as currency_type,
                '${new Date(now).toISOString()}' as created_at
            FROM "${CurrencyPairEntity.TABLE_NAME}"
            WHERE "pair" = '${someFakePair.pair}';

            INSERT INTO "${FeeEntity.TABLE_NAME}"
            (currency_pair_id, maker_fee_percent, taker_fee_percent, currency_type, created_at)
            SELECT
                id,
                ${SECOND_TARGET_MAKER_FEE_MOCH} as maker_fee_percent,
                ${SECOND_TARGET_TAKER_FEE_MOCH} as taker_fee_percent,
                '${someFakePair.type}' as currency_type,
                '${new Date(now + 5000).toISOString()}' as created_at
            FROM "${CurrencyPairEntity.TABLE_NAME}"
            WHERE "pair" = '${someFakePair.pair}';

            INSERT INTO "${FeeEntity.TABLE_NAME}"
            (currency_pair_id, maker_fee_percent, taker_fee_percent, currency_type, created_at)
            VALUES
            (
                1,
                ${FIRST_TYPE_MAKER_FEE_MOCH},
                ${FIRST_TYPE_TAKER_FEE_MOCH},
                '${someFakePair.type}',
                '${new Date(now + 15000).toISOString()}'
            ),
            (
                1,
                ${SECOND_TYPE_MAKER_FEE_MOCH},
                ${SECOND_TYPE_TAKER_FEE_MOCH},
                '${someFakePair.type}',
                '${new Date(now + 20000).toISOString()}'
            )
        `);
    }

    public async down(qr: QueryRunner): Promise<void> {
        const someFakePair = fakePairs[0];

        await qr.query(`
            DELETE FROM "${FeeEntity.TABLE_NAME}"
            WHERE id >= (
                SELECT MIN(id) FROM "${FeeEntity.TABLE_NAME}"
                WHERE "currency_pair_id" = (
                    SELECT id FROM "${CurrencyPairEntity.TABLE_NAME}"
                    WHERE "pair" = '${someFakePair.pair}'
                )
            )
        `);
    }
}
