import { MigrationInterface, QueryRunner } from 'typeorm';

import { OrderEntity } from '@trading/dao';

export class OrderDeleteFlg1660558529993 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`INSERT INTO "${OrderEntity.TABLE_NAME}"
            (id, buy_flg, price, volume, stop_limit_price, user_account, symbol, as_of_date, delete_flg)
             VALUES(
                uuid_generate_v4(),
                true,
                800000000000,
                100000000,
                1000000000000,
                'test',
                'BTC-USDT',
                now(),
                true
             );`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`DELETE FROM "${OrderEntity.TABLE_NAME}"`);
    }
}
