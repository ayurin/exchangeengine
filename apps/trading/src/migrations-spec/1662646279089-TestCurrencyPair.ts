import { MigrationInterface, QueryRunner } from 'typeorm';

import { CurrencyPairEntity } from '@trading/dao';
import { fakePairs } from '@trading/e2e/moch/fee.moch';

export class TestCurrencyPair1662646279089 implements MigrationInterface {
    public async up(qr: QueryRunner): Promise<void> {
        await qr.query(`
            INSERT INTO "${CurrencyPairEntity.TABLE_NAME}"
            ("pair", "base", "quote", "type", "min_order_amount") VALUES
            ${fakePairs
                .map((p) => `('${p.pair}', '${p.base}', '${p.quote}', '${p.type}', ${p.minOrderAmount})`)
                .join(',\n')}
        `);
    }

    public async down(qr: QueryRunner): Promise<void> {
        await qr.query(`
            DELETE FROM "${CurrencyPairEntity.TABLE_NAME}"
            WHERE "pair" IN (${fakePairs.map((p) => `'${p.pair}'`).join(', ')})
        `);
    }
}
