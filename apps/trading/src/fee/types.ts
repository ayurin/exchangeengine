export interface IFeeResponse {
    readonly makerFee: number;
    readonly takerFee: number;
}
