import { ApiProperty } from '@nestjs/swagger';
import { IsNumberString, IsOptional } from 'class-validator';

export class FeeRequestDto {
    @ApiProperty({ example: '800000000000' })
    @IsOptional()
    @IsNumberString()
    public makerFeePercent?: string;

    @ApiProperty({ example: '800000000000' })
    @IsOptional()
    @IsNumberString()
    public takerFeePercent?: string;
}
