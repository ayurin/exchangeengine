import { ApiProperty } from '@nestjs/swagger';

import { FeeEntity } from '@trading/dao';

export class FeeResponseDto {
    @ApiProperty({ example: 1 })
    public id: number;

    @ApiProperty({ example: 11333 })
    public makerFeePercent: number;

    @ApiProperty({ example: 33112 })
    public takerFeePercent: number;

    @ApiProperty({ example: '0caed7bb-e602-4a02-b897-10dd72f35953' })
    public currencyPairId: string;

    public static response(fee: FeeEntity): FeeResponseDto {
        return {
            id: fee.id,
            makerFeePercent: Number(fee.makerFeePercent),
            takerFeePercent: Number(fee.takerFeePercent),
            currencyPairId: fee.currencyPairId,
        };
    }
}
