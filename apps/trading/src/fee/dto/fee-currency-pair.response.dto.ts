import { ApiProperty } from '@nestjs/swagger';

import { IFeeResponse } from '../types';

export class FeeCurrencyPairResponse implements IFeeResponse {
    @ApiProperty({ example: 100000 })
    public readonly makerFee: number;

    @ApiProperty({ example: 200000 })
    public readonly takerFee: number;
}
