import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString } from 'class-validator';

export class FeeQuery {
    @ApiProperty({ example: 'BNB-USDC' })
    @IsString()
    @IsNotEmpty()
    public readonly currencyPair!: string;
}
