import { Module } from '@nestjs/common';

import { CurrencyPairRepository, FeeRepository } from '@trading/dao';

import { FeeController } from './fee.controller';
import { FeeService } from './fee.service';

@Module({
    controllers: [FeeController],
    providers: [FeeService, FeeRepository, CurrencyPairRepository],
    exports: [FeeService],
})
export class FeeModule {}
