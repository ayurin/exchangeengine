import { BadRequestException, Injectable, NotFoundException } from '@nestjs/common';

import { CurrencyPairRepository, FeeEntity, FeeRepository } from '@trading/dao';
import { FeeRequestDto } from '@trading/fee/dto';

import { IFeeResponse } from './types';

@Injectable()
export class FeeService {
    constructor(
        private readonly feeRepository: FeeRepository,
        private readonly currencyPairRepository: CurrencyPairRepository,
    ) {}

    public async getFee(currencyPair: string): Promise<IFeeResponse> {
        return this.feeRepository.getFeeOrFail(currencyPair);
    }

    public async upsertFeeByCurrencyPairId(currencyPairId: string, body: FeeRequestDto): Promise<FeeEntity> {
        let fee = await this.feeRepository.findOneBy({ currencyPairId });

        if (!fee) {
            const currencyPair = await this.currencyPairRepository.findOneBy({ id: currencyPairId });
            if (!currencyPair) {
                throw new NotFoundException('currency pair not found');
            }

            fee = new FeeEntity({ currencyPairId });
        }

        const { makerFeePercent, takerFeePercent } = body;
        if (!makerFeePercent && !takerFeePercent) {
            throw new BadRequestException('at least one parameter is required');
        }

        if (makerFeePercent) {
            fee.makerFeePercent = BigInt(makerFeePercent);
        }

        if (takerFeePercent) {
            fee.takerFeePercent = BigInt(takerFeePercent);
        }

        await this.feeRepository.save(fee);
        return fee;
    }
}
