import { Body, Controller, Get, HttpStatus, Param, Patch, Query } from '@nestjs/common';
import { ApiBody, ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';

import { FeeCurrencyPairResponse, FeeQuery, FeeRequestDto, FeeResponseDto } from './dto';
import { FeeService } from './fee.service';

@ApiTags('Fee')
@Controller('fee')
export class FeeController {
    constructor(private readonly feeService: FeeService) {}

    @Get()
    @ApiOperation({ summary: 'Get fee for the currency pair' })
    @ApiResponse({ status: HttpStatus.OK, type: FeeCurrencyPairResponse })
    public getFee(@Query() query: FeeQuery): Promise<FeeCurrencyPairResponse> {
        return this.feeService.getFee(query.currencyPair);
    }

    @Patch(':currencyPairId')
    @ApiOperation({ summary: 'Update fee' })
    @ApiBody({ type: FeeRequestDto })
    @ApiResponse({ status: HttpStatus.OK, type: FeeResponseDto })
    public async updateFee(@Body() body: FeeRequestDto, @Param('currencyPairId') id: string): Promise<FeeResponseDto> {
        const fee = await this.feeService.upsertFeeByCurrencyPairId(id, body);
        return FeeResponseDto.response(fee);
    }
}
