/* eslint-disable @typescript-eslint/no-floating-promises, jest/require-hook */
import * as fs from 'fs';
import * as path from 'path';

import { tradingRmqOptions } from '@app/utils';
import { NestFactory } from '@nestjs/core';
import { RmqOptions } from '@nestjs/microservices';
import { FastifyAdapter, NestFastifyApplication } from '@nestjs/platform-fastify';
import { SwaggerModule } from '@nestjs/swagger';
import { useContainer } from 'class-validator';
import { Logger } from 'nestjs-pino';

import { API_PREFIX, appValidationPipe, appVersioningOptions, hybridMicroserviceOptions, swaggerConfig } from './env';

import { AppModule } from './app.module';
import { appConfig } from './config';

(async () => {
    const app = await NestFactory.create<NestFastifyApplication>(AppModule, new FastifyAdapter());
    app.useGlobalPipes(appValidationPipe);
    app.useLogger(app.get(Logger));
    app.setGlobalPrefix(API_PREFIX);
    app.enableVersioning(appVersioningOptions);
    app.connectMicroservice<RmqOptions>(tradingRmqOptions, hybridMicroserviceOptions);

    // TODO: remove after it will be configured by devops
    app.enableCors({ origin: '*' });

    const document = SwaggerModule.createDocument(app, swaggerConfig);
    SwaggerModule.setup(API_PREFIX, app, document);
    const outputPath = path.join(__dirname, 'swagger.json');
    fs.writeFileSync(outputPath, JSON.stringify(document), { encoding: 'utf8' });

    useContainer(app.select(AppModule), { fallbackOnErrors: true });

    await app.startAllMicroservices();
    await app.listen(appConfig.APP_PORT, '0.0.0.0');
})();
