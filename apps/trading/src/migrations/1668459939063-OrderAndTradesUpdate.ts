import { MigrationInterface, QueryRunner } from 'typeorm';

import { OrderEntity } from '@trading/dao';

export class OrderAndTradesUpdate1668459939063 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "orders" ADD "cancel_status" character varying NOT NULL DEFAULT 'none'`);
        await queryRunner.query(
            `ALTER TABLE "orders" ADD CONSTRAINT "CHK_ae66c75edff74e65befd4e2624" CHECK (cancel_status in ('none', 'canceled', 'canceling'))`,
        );
        await queryRunner.query(`
        UPDATE "${OrderEntity.TABLE_NAME}"
        SET cancel_status = old_${OrderEntity.TABLE_NAME}.cancel_status 
        FROM (SELECT id,
                CASE WHEN ${OrderEntity.TABLE_NAME}.status = 'passing' THEN 'canceling'
                WHEN ${OrderEntity.TABLE_NAME}.status = 'cancel' THEN 'canceled'
                ELSE 'none'
                END as cancel_status
                FROM "${OrderEntity.TABLE_NAME}") as old_${OrderEntity.TABLE_NAME}
                WHERE ${OrderEntity.TABLE_NAME}.id = old_${OrderEntity.TABLE_NAME}.id 
        `);
        await queryRunner.query(`ALTER TABLE "orders" DROP COLUMN "status"`);
        await queryRunner.query(`ALTER TABLE "orders" DROP COLUMN "executed_quantity"`);
        await queryRunner.query(`ALTER TABLE "orders" DROP COLUMN "auto_version"`);
        await queryRunner.query(`ALTER TABLE "orders" DROP COLUMN "version"`);
        await queryRunner.query(`ALTER TABLE "trades" DROP COLUMN "buyer_version"`);
        await queryRunner.query(`ALTER TABLE "trades" DROP COLUMN "seller_version"`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "trades" ADD "seller_version" smallint NOT NULL DEFAULT '1'`);
        await queryRunner.query(`ALTER TABLE "trades" ADD "buyer_version" smallint NOT NULL DEFAULT '1'`);
        await queryRunner.query(`ALTER TABLE "orders" ADD "auto_version" integer NOT NULL DEFAULT '1'`);
        await queryRunner.query(`ALTER TABLE "orders" ADD "version" integer NOT NULL DEFAULT '1'`);
        await queryRunner.query(`ALTER TABLE "orders" ADD "executed_quantity" bigint NOT NULL DEFAULT '0'`);
        await queryRunner.query(`ALTER TABLE "orders" ADD "status" character varying NOT NULL DEFAULT 'open'`);
        await queryRunner.query(`ALTER TABLE "orders" DROP CONSTRAINT "CHK_ae66c75edff74e65befd4e2624"`);
        await queryRunner.query(`ALTER TABLE "orders" DROP COLUMN "cancel_status"`);
    }
}
