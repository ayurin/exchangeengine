import { MigrationInterface, QueryRunner } from 'typeorm';

import { CandlesEntity } from '@trading/dao';

export class AlterCandles1665533844653 implements MigrationInterface {
    public async up(qr: QueryRunner): Promise<void> {
        await qr.query(`ALTER TABLE ${CandlesEntity.TABLE_NAME} RENAME COLUMN "open_time" TO "time";`);
        await qr.query(`ALTER TABLE ${CandlesEntity.TABLE_NAME} DROP COLUMN "close_time";`);
        await qr.query(`ALTER TABLE ${CandlesEntity.TABLE_NAME} DROP COLUMN "interval"`);
        await qr.query(`ALTER TABLE ${CandlesEntity.TABLE_NAME} ADD "resolution" VARCHAR NOT NULL DEFAULT '1s'`);
    }

    public async down(qr: QueryRunner): Promise<void> {
        await qr.query(`ALTER TABLE ${CandlesEntity.TABLE_NAME} RENAME COLUMN "time" TO "open_time";`);
        await qr.query(`ALTER TABLE ${CandlesEntity.TABLE_NAME} ADD "close_time" TIMESTAMP NOT NULL DEFAULT now();`);
        await qr.query(`ALTER TABLE ${CandlesEntity.TABLE_NAME} DROP COLUMN "resolution"`);
        await qr.query(`ALTER TABLE ${CandlesEntity.TABLE_NAME} ADD "interval" INT NOT NULL DEFAULT 0`);
    }
}
