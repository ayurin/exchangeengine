import { MigrationInterface, QueryRunner } from 'typeorm';

import { CurrencyPairEntity } from '@trading/dao';

export class CurrencyPairPriceIncrement1669680224743 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(
            `ALTER TABLE "${CurrencyPairEntity.TABLE_NAME}" ADD "price_increment" NUMERIC NOT NULL DEFAULT 1000000;`,
        );
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "${CurrencyPairEntity.TABLE_NAME}" DROP COLUMN "price_increment"`);
    }
}
