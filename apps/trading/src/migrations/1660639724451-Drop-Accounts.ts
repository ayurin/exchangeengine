import { MigrationInterface, QueryRunner } from 'typeorm';

export class DropAccounts1660639724451 implements MigrationInterface {
    public async up(qr: QueryRunner): Promise<void> {
        await qr.query(`DROP TABLE IF EXISTS "accounts"`);
    }

    public down(): Promise<void> {
        return Promise.resolve();
    }
}
