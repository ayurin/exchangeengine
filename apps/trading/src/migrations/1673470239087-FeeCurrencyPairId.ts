import { MigrationInterface, QueryRunner } from 'typeorm';

import { DEFAULT_CURRENCY } from '@trading/dao';

export class FeeCurrencyPairId1673470239087 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`delete from "fee" where currency_type != '${DEFAULT_CURRENCY}'`);
        await queryRunner.query(`ALTER TABLE "fee" DROP COLUMN "currency_pair_id"`);
        await queryRunner.query(`ALTER TABLE "fee" ADD "currency_pair_id" uuid`);
        await queryRunner.query(
            `ALTER TABLE "fee" ADD CONSTRAINT "UQ_0cbba366626be55ae4938aa230b" UNIQUE ("currency_pair_id")`,
        );
        await queryRunner.query(
            `ALTER TABLE "fee" ADD CONSTRAINT "FK_0cbba366626be55ae4938aa230b" FOREIGN KEY ("currency_pair_id") REFERENCES "currency_pair"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
        );
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "fee" DROP CONSTRAINT "FK_0cbba366626be55ae4938aa230b"`);
        await queryRunner.query(`ALTER TABLE "fee" DROP CONSTRAINT "UQ_0cbba366626be55ae4938aa230b"`);
        await queryRunner.query(`ALTER TABLE "fee" DROP COLUMN "currency_pair_id"`);
        await queryRunner.query(`ALTER TABLE "fee" ADD "currency_pair_id" SERIAL NOT NULL`);
    }
}
