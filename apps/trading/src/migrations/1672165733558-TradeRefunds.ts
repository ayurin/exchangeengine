import { MigrationInterface, QueryRunner } from 'typeorm';

export class TradeRefunds1672165733558 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "trades" ADD "buyer_fee_refunds" bigint NOT NULL DEFAULT 0`);
        await queryRunner.query(`ALTER TABLE "trades" ADD "buyer_refunds" bigint NOT NULL DEFAULT 0`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "trades" DROP COLUMN "buyer_refunds"`);
        await queryRunner.query(`ALTER TABLE "trades" DROP COLUMN "buyer_fee_refunds"`);
    }
}
