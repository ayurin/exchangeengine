import { MigrationInterface, QueryRunner } from 'typeorm';

import { OrderEntity } from '@trading/dao';

export class OrderField1663183735416 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(
            `ALTER TABLE "${OrderEntity.TABLE_NAME}" ADD "type" character varying NOT NULL DEFAULT 'limit'`,
        );

        await queryRunner.query(`ALTER TABLE "${OrderEntity.TABLE_NAME}" ADD "side" character varying`);
        await queryRunner.query(`
        UPDATE "${OrderEntity.TABLE_NAME}"
        SET side = old_${OrderEntity.TABLE_NAME}.side 
        FROM (SELECT id,
                CASE WHEN ${OrderEntity.TABLE_NAME}.buy_flg = true THEN 'buy'
                ELSE 'sell'
                END as side
                FROM "${OrderEntity.TABLE_NAME}") as old_${OrderEntity.TABLE_NAME}
                WHERE ${OrderEntity.TABLE_NAME}.id = old_${OrderEntity.TABLE_NAME}.id 
        `);
        await queryRunner.query(`ALTER TABLE "${OrderEntity.TABLE_NAME}" ALTER COLUMN "side" SET NOT NULL`);
        await queryRunner.query(`ALTER TABLE "${OrderEntity.TABLE_NAME}" DROP COLUMN "buy_flg"`);

        await queryRunner.query(`ALTER TABLE "${OrderEntity.TABLE_NAME}" ADD "quantity" bigint`);
        await queryRunner.query(`
        UPDATE "${OrderEntity.TABLE_NAME}"
        SET quantity = old_${OrderEntity.TABLE_NAME}.volume 
        FROM (SELECT id, volume 
                FROM "${OrderEntity.TABLE_NAME}") as old_${OrderEntity.TABLE_NAME}
                WHERE ${OrderEntity.TABLE_NAME}.id = old_${OrderEntity.TABLE_NAME}.id 
        `);
        await queryRunner.query(`ALTER TABLE "${OrderEntity.TABLE_NAME}" ALTER COLUMN "quantity" SET NOT NULL`);
        await queryRunner.query(`ALTER TABLE "${OrderEntity.TABLE_NAME}" DROP COLUMN "volume"`);

        await queryRunner.query(`ALTER TABLE "${OrderEntity.TABLE_NAME}" ADD "status" character varying`);
        await queryRunner.query(`
        UPDATE "${OrderEntity.TABLE_NAME}"
        SET status = old_${OrderEntity.TABLE_NAME}.status 
        FROM (SELECT id,
                CASE WHEN ${OrderEntity.TABLE_NAME}.delete_flg = true THEN 'cancel'
                ELSE 'open'
                END as status
                FROM "${OrderEntity.TABLE_NAME}") as old_${OrderEntity.TABLE_NAME}
                WHERE ${OrderEntity.TABLE_NAME}.id = old_${OrderEntity.TABLE_NAME}.id 
        `);
        await queryRunner.query(`ALTER TABLE "${OrderEntity.TABLE_NAME}" ALTER COLUMN "status" SET NOT NULL`);
        await queryRunner.query(`ALTER TABLE "${OrderEntity.TABLE_NAME}" DROP COLUMN "delete_flg"`);

        await queryRunner.query(`ALTER TABLE "${OrderEntity.TABLE_NAME}" ADD "customer_id" character varying`);
        await queryRunner.query(`
        UPDATE "${OrderEntity.TABLE_NAME}"
        SET customer_id = old_${OrderEntity.TABLE_NAME}.user_account 
        FROM (SELECT id, user_account 
                FROM "${OrderEntity.TABLE_NAME}") as old_${OrderEntity.TABLE_NAME}
                WHERE ${OrderEntity.TABLE_NAME}.id = old_${OrderEntity.TABLE_NAME}.id 
        `);
        await queryRunner.query(`ALTER TABLE "${OrderEntity.TABLE_NAME}" ALTER COLUMN "customer_id" SET NOT NULL`);
        await queryRunner.query(`ALTER TABLE "${OrderEntity.TABLE_NAME}" DROP COLUMN "user_account"`);

        await queryRunner.query(`ALTER TABLE "${OrderEntity.TABLE_NAME}" ADD "pair" character varying`);
        await queryRunner.query(`
        UPDATE "${OrderEntity.TABLE_NAME}"
        SET pair = old_${OrderEntity.TABLE_NAME}.symbol 
        FROM (SELECT id, symbol 
                FROM "${OrderEntity.TABLE_NAME}") as old_${OrderEntity.TABLE_NAME}
                WHERE ${OrderEntity.TABLE_NAME}.id = old_${OrderEntity.TABLE_NAME}.id 
        `);
        await queryRunner.query(`ALTER TABLE "${OrderEntity.TABLE_NAME}" ALTER COLUMN "pair" SET NOT NULL`);
        await queryRunner.query(`ALTER TABLE "${OrderEntity.TABLE_NAME}" DROP COLUMN "symbol"`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "${OrderEntity.TABLE_NAME}" DROP COLUMN "type"`);

        await queryRunner.query(`ALTER TABLE "${OrderEntity.TABLE_NAME}" ADD "delete_flg" boolean`);
        await queryRunner.query(`
        UPDATE "${OrderEntity.TABLE_NAME}"
        SET delete_flg = old_${OrderEntity.TABLE_NAME}.delete_flg 
        FROM (SELECT id,
                CASE WHEN ${OrderEntity.TABLE_NAME}.status = 'cancel' THEN true
                ELSE false
                END as delete_flg
                FROM "${OrderEntity.TABLE_NAME}") as old_${OrderEntity.TABLE_NAME}
                WHERE ${OrderEntity.TABLE_NAME}.id = old_${OrderEntity.TABLE_NAME}.id 
        `);
        await queryRunner.query(`ALTER TABLE "${OrderEntity.TABLE_NAME}" ALTER COLUMN "delete_flg" SET NOT NULL`);
        await queryRunner.query(`ALTER TABLE "${OrderEntity.TABLE_NAME}" DROP COLUMN "status"`);

        await queryRunner.query(`ALTER TABLE "${OrderEntity.TABLE_NAME}" ADD "volume" bigint`);
        await queryRunner.query(`
        UPDATE "${OrderEntity.TABLE_NAME}"
        SET volume = old_${OrderEntity.TABLE_NAME}.quantity 
        FROM (SELECT id, quantity 
                FROM "${OrderEntity.TABLE_NAME}") as old_${OrderEntity.TABLE_NAME}
                WHERE ${OrderEntity.TABLE_NAME}.id = old_${OrderEntity.TABLE_NAME}.id 
        `);
        await queryRunner.query(`ALTER TABLE "${OrderEntity.TABLE_NAME}" ALTER COLUMN "volume" SET NOT NULL`);
        await queryRunner.query(`ALTER TABLE "${OrderEntity.TABLE_NAME}" DROP COLUMN "quantity"`);

        await queryRunner.query(`ALTER TABLE "${OrderEntity.TABLE_NAME}" ADD "buy_flg" boolean`);
        await queryRunner.query(`
        UPDATE "${OrderEntity.TABLE_NAME}"
        SET buy_flg = old_${OrderEntity.TABLE_NAME}.buy_flg 
        FROM (SELECT id,
                CASE WHEN ${OrderEntity.TABLE_NAME}.side = 'buy' THEN true
                ELSE false
                END as buy_flg
                FROM "${OrderEntity.TABLE_NAME}") as old_${OrderEntity.TABLE_NAME}
                WHERE ${OrderEntity.TABLE_NAME}.id = old_${OrderEntity.TABLE_NAME}.id 
        `);
        await queryRunner.query(`ALTER TABLE "${OrderEntity.TABLE_NAME}" ALTER COLUMN "buy_flg" SET NOT NULL`);
        await queryRunner.query(`ALTER TABLE "${OrderEntity.TABLE_NAME}" DROP COLUMN "side"`);

        await queryRunner.query(`ALTER TABLE "${OrderEntity.TABLE_NAME}" ADD "user_account" character varying`);
        await queryRunner.query(`
        UPDATE "${OrderEntity.TABLE_NAME}"
        SET user_account = old_${OrderEntity.TABLE_NAME}.customer_id 
        FROM (SELECT id, customer_id 
                FROM "${OrderEntity.TABLE_NAME}") as old_${OrderEntity.TABLE_NAME}
                WHERE ${OrderEntity.TABLE_NAME}.id = old_${OrderEntity.TABLE_NAME}.id 
        `);
        await queryRunner.query(`ALTER TABLE "${OrderEntity.TABLE_NAME}" ALTER COLUMN "user_account" SET NOT NULL`);
        await queryRunner.query(`ALTER TABLE "${OrderEntity.TABLE_NAME}" DROP COLUMN "customer_id"`);

        await queryRunner.query(`ALTER TABLE "${OrderEntity.TABLE_NAME}" ADD "symbol" character varying`);
        await queryRunner.query(`
        UPDATE "${OrderEntity.TABLE_NAME}"
        SET symbol = old_${OrderEntity.TABLE_NAME}.pair 
        FROM (SELECT id, pair 
                FROM "${OrderEntity.TABLE_NAME}") as old_${OrderEntity.TABLE_NAME}
                WHERE ${OrderEntity.TABLE_NAME}.id = old_${OrderEntity.TABLE_NAME}.id 
        `);
        await queryRunner.query(`ALTER TABLE "${OrderEntity.TABLE_NAME}" ALTER COLUMN "symbol" SET NOT NULL`);
        await queryRunner.query(`ALTER TABLE "${OrderEntity.TABLE_NAME}" DROP COLUMN "pair"`);
    }
}
