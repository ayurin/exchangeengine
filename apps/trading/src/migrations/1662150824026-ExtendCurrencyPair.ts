import { MigrationInterface, QueryRunner } from 'typeorm';

import { CurrencyPairEntity, DEFAULT_CURRENCY } from '@trading/dao';

export class ExtendCurrencyPair1662150824026 implements MigrationInterface {
    public async up(qr: QueryRunner): Promise<void> {
        await qr.query(`
            ALTER TABLE "${CurrencyPairEntity.TABLE_NAME}"
                ADD COLUMN "min_order_amount" NUMERIC,
                ADD COLUMN "amount_increment" NUMERIC  NOT NULL  DEFAULT 0.01;
        `);

        await qr.query(`
            UPDATE "${CurrencyPairEntity.TABLE_NAME}"
            SET "min_order_amount" = 100
            WHERE "pair" = '${DEFAULT_CURRENCY}'
        `);

        await qr.query(`
            ALTER TABLE "${CurrencyPairEntity.TABLE_NAME}"
            ALTER COLUMN "min_order_amount"
            SET NOT NULL;
        `);
    }

    public async down(qr: QueryRunner): Promise<void> {
        await qr.query(`
            ALTER TABLE "${CurrencyPairEntity.TABLE_NAME}"
                DROP COLUMN "min_order_amount",
                DROP COLUMN "amount_increment";
        `);
    }
}
