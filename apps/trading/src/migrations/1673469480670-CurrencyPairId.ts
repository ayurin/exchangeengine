import { MigrationInterface, QueryRunner } from 'typeorm';

export class CurrencyPairId1673469480670 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "currency_pair" DROP CONSTRAINT "CURRENCY_PAIR_PRIMARY_KEY"`);
        await queryRunner.query(`ALTER TABLE "currency_pair" DROP COLUMN "id"`);
        await queryRunner.query(`ALTER TABLE "currency_pair" ADD "id" uuid NOT NULL DEFAULT uuid_generate_v4()`);
        await queryRunner.query(
            `ALTER TABLE "currency_pair" ADD CONSTRAINT "PK_87b2b346740a4d58bfc5e95282c" PRIMARY KEY ("id")`,
        );
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "currency_pair" DROP CONSTRAINT "PK_87b2b346740a4d58bfc5e95282c"`);
        await queryRunner.query(`ALTER TABLE "currency_pair" DROP COLUMN "id"`);
        await queryRunner.query(`ALTER TABLE "currency_pair" ADD "id" SERIAL NOT NULL`);
        await queryRunner.query(
            `ALTER TABLE "currency_pair" ADD CONSTRAINT "CURRENCY_PAIR_PRIMARY_KEY" PRIMARY KEY ("id")`,
        );
    }
}
