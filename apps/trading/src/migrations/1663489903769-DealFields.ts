import { MigrationInterface, QueryRunner } from 'typeorm';

export class DealFields1663489903769 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "deal" ADD "quantity" bigint`);
        await queryRunner.query(`
        UPDATE "deal"
        SET quantity = old_deal.volume 
        FROM (SELECT id, volume 
                FROM "deal") as old_deal
                WHERE deal.id = old_deal.id 
        `);
        await queryRunner.query(`ALTER TABLE "deal" ALTER COLUMN "quantity" SET NOT NULL`);
        await queryRunner.query(`ALTER TABLE "deal" DROP COLUMN "volume"`);

        await queryRunner.query(`ALTER TABLE "deal" ADD "pair" character varying`);
        await queryRunner.query(`
        UPDATE "deal"
        SET pair = old_deal.symbol 
        FROM (SELECT id, symbol 
                FROM "deal") as old_deal
                WHERE deal.id = old_deal.id 
        `);
        await queryRunner.query(`ALTER TABLE "deal" ALTER COLUMN "pair" SET NOT NULL`);
        await queryRunner.query(`ALTER TABLE "deal" DROP COLUMN "symbol"`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "deal" ADD "volume" bigint`);
        await queryRunner.query(`
        UPDATE "deal"
        SET volume = old_deal.quantity 
        FROM (SELECT id, quantity 
                FROM "deal") as old_deal
                WHERE deal.id = old_deal.id 
        `);
        await queryRunner.query(`ALTER TABLE "deal" ALTER COLUMN "volume" SET NOT NULL`);
        await queryRunner.query(`ALTER TABLE "deal" DROP COLUMN "quantity"`);

        await queryRunner.query(`ALTER TABLE "deal" ADD "symbol" character varying`);
        await queryRunner.query(`
        UPDATE "deal"
        SET symbol = old_deal.pair 
        FROM (SELECT id, pair 
                FROM "deal") as old_deal
                WHERE deal.id = old_deal.id 
        `);
        await queryRunner.query(`ALTER TABLE "deal" ALTER COLUMN "symbol" SET NOT NULL`);
        await queryRunner.query(`ALTER TABLE "deal" DROP COLUMN "pair"`);
    }
}
