import { MigrationInterface, QueryRunner } from 'typeorm';

import { CurrencyPairEntity } from '@trading/dao';

export class CurrencyPairType1674074836804 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`update ${CurrencyPairEntity.TABLE_NAME}
        set type = regexp_replace(type, '(\\w)', 'F')
        where base in ('USD', 'AED', 'EUR')`);
        await queryRunner.query(`update ${CurrencyPairEntity.TABLE_NAME}
        set type = regexp_replace(type, '(\\w)$', 'F')
        where quote in ('USD', 'AED', 'EUR')`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`update ${CurrencyPairEntity.TABLE_NAME}
        set type = regexp_replace(type, '(\\w)', 'S')
        where base in ('USD', 'AED', 'EUR')`);
        await queryRunner.query(`update ${CurrencyPairEntity.TABLE_NAME}
        set type = regexp_replace(type, '(\\w)$', 'S')
        where quote in ('USD', 'AED', 'EUR')`);
    }
}
