import { MigrationInterface, QueryRunner } from 'typeorm';

export class CurrencyPair1670911197673 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "currency_pair" ADD "max_order_amount" numeric`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "currency_pair" DROP COLUMN "max_order_amount"`);
    }
}
