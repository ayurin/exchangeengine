import { MigrationInterface, QueryRunner } from 'typeorm';

import { CurrencyPairEntity } from '@trading/dao';

const NEW_DELIMITER = '-';
const OLD_DELIMITER = '/';

export class UpdatePairDelimiter1663311279407 implements MigrationInterface {
    public async up(qr: QueryRunner): Promise<void> {
        await qr.query(
            `UPDATE "${CurrencyPairEntity.TABLE_NAME}" SET "pair" = replace("pair", '${OLD_DELIMITER}', '${NEW_DELIMITER}')`,
        );
    }

    public async down(qr: QueryRunner): Promise<void> {
        await qr.query(
            `UPDATE "${CurrencyPairEntity.TABLE_NAME}" SET "pair" = replace("pair", '${NEW_DELIMITER}', '${OLD_DELIMITER}')`,
        );
    }
}
