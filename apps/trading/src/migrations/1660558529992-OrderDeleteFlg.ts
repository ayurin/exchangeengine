import { MigrationInterface, QueryRunner } from 'typeorm';

import { OrderEntity } from '@trading/dao';

export class OrderDeleteFlg1660558529992 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(
            `ALTER TABLE "${OrderEntity.TABLE_NAME}" ADD "delete_flg" boolean NOT NULL DEFAULT false`,
        );
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "${OrderEntity.TABLE_NAME}" DROP COLUMN "delete_flg"`);
    }
}
