import { MigrationInterface, QueryRunner } from 'typeorm';

import { CurrencyPairEntity, DEFAULT_CURRENCY, FeeEntity } from '@trading/dao';

const OLD_MAKER_FEE_PERCENT = 0.1;
const OLD_TAKER_FEE_PERCENT = 0.2;

// 100% is 10^8 now
const NEW_MAKER_FEE_PERCENT = OLD_MAKER_FEE_PERCENT * 10 ** 8;
const NEW_TAKER_FEE_PERCENT = OLD_TAKER_FEE_PERCENT * 10 ** 8;

export class UpdateFeeScale1664255960429 implements MigrationInterface {
    public async up(qr: QueryRunner): Promise<void> {
        await qr.query(`
            UPDATE "${FeeEntity.TABLE_NAME}"
            SET maker_fee_percent = ${NEW_MAKER_FEE_PERCENT},
                taker_fee_percent = ${NEW_TAKER_FEE_PERCENT}
            WHERE currency_pair_id IN (
                SELECT id FROM "${CurrencyPairEntity.TABLE_NAME}"
                WHERE pair = '${DEFAULT_CURRENCY}'
            )
        `);
    }

    public async down(qr: QueryRunner): Promise<void> {
        await qr.query(`
            UPDATE "${FeeEntity.TABLE_NAME}"
            SET maker_fee_percent = ${OLD_MAKER_FEE_PERCENT},
                taker_fee_percent = ${OLD_TAKER_FEE_PERCENT}
            WHERE currency_pair_id IN (
                SELECT id FROM "${CurrencyPairEntity.TABLE_NAME}"
                WHERE pair = '${DEFAULT_CURRENCY}'
            )
        `);
    }
}
