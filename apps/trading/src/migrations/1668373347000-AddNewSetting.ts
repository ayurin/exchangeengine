import { MigrationInterface, QueryRunner } from 'typeorm';

import { SettingsEntity } from '@trading/dao';

const KEY = 'resolution';
const VALUE = '["1m", "5m", "15m", "30m", "1h", "2h", "6h", "12h", "1d", "3d", "1w"]';
const UQ_CONSTRAINT = 'UQ_settings_table_key';

export class AddNewSettings1668373347000 implements MigrationInterface {
    public async up(qr: QueryRunner): Promise<void> {
        await qr.query(`
            ALTER TABLE ${SettingsEntity.TABLE_NAME}
            ADD CONSTRAINT "${UQ_CONSTRAINT}" UNIQUE ("key");
        `);

        await qr.query(`
            INSERT INTO ${SettingsEntity.TABLE_NAME}(key, value) VALUES ('${KEY}', '${VALUE}')
            ON CONFLICT DO NOTHING;
        `);
    }

    public async down(qr: QueryRunner): Promise<void> {
        await qr.query(`DELETE FROM ${SettingsEntity.TABLE_NAME} WHERE key='${KEY}';`);
        await qr.query(`
            ALTER TABLE ${SettingsEntity.TABLE_NAME}
            DROP CONSTRAINT "${UQ_CONSTRAINT}";
        `);
    }
}
