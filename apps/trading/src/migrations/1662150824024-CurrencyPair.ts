import { MigrationInterface, QueryRunner } from 'typeorm';

import { CurrencyPairEntity, DEFAULT_CURRENCY } from '@trading/dao';

export class CurrencyPair1662150824024 implements MigrationInterface {
    public async up(qr: QueryRunner): Promise<void> {
        await qr.query(`
            CREATE TABLE "${CurrencyPairEntity.TABLE_NAME}" (
                "id" SERIAL NOT NULL,
                "pair" character varying UNIQUE NOT NULL,
                "base" character varying NOT NULL,
                "quote" character varying NOT NULL,
                "type" character varying NOT NULL,
                "created_at" TIMESTAMP NOT NULL DEFAULT now(),
                CONSTRAINT "CURRENCY_PAIR_PRIMARY_KEY" PRIMARY KEY ("id")
            )
        `);

        await qr.query(`
            INSERT INTO "${CurrencyPairEntity.TABLE_NAME}"
            ("pair", "base", "quote", "type") VALUES
            ('${DEFAULT_CURRENCY}', '${DEFAULT_CURRENCY}', '${DEFAULT_CURRENCY}', '${DEFAULT_CURRENCY}')
        `);
    }

    public async down(qr: QueryRunner): Promise<void> {
        await qr.query(`DROP TABLE "${CurrencyPairEntity.TABLE_NAME}"`);
    }
}
