import { MigrationInterface, QueryRunner } from 'typeorm';

import { OrderEntity } from '@trading/dao';

export class Orders1660502094043 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "${OrderEntity.TABLE_NAME}" (
            "id" uuid NOT NULL DEFAULT uuid_generate_v4(),
            "buy_flg" boolean NOT NULL,
            "price" bigint,
            "volume" bigint NOT NULL,
            "stop_limit_price" bigint,
            "user_account" character varying NOT NULL,
            "symbol" character varying NOT NULL,
            "sort_order" SERIAL NOT NULL,
            "as_of_date" TIMESTAMP NOT NULL DEFAULT now(),
            CONSTRAINT "PK_710e2d4957aa5878dfe94e4ac2f" PRIMARY KEY ("id"))`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`DROP TABLE "${OrderEntity.TABLE_NAME}"`);
    }
}
