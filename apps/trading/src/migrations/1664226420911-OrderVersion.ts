import { MigrationInterface, QueryRunner } from 'typeorm';

export class OrderVersion1664226420911 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "fee" DROP CONSTRAINT "FEE_CURRENCY_PAIR_FOREIGN_KEY"`);
        await queryRunner.query(`ALTER TABLE "orders" ADD "version" integer NOT NULL DEFAULT '1'`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "orders" DROP COLUMN "version"`);
        await queryRunner.query(
            `ALTER TABLE "fee" ADD CONSTRAINT "FEE_CURRENCY_PAIR_FOREIGN_KEY" FOREIGN KEY ("currency_pair_id") REFERENCES "currency_pair"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
        );
    }
}
