import { MigrationInterface, QueryRunner } from 'typeorm';

// eslint-disable-next-line @typescript-eslint/naming-convention
export class delDefaultCurrency1673810724525 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`delete from currency_pair where pair='default'`);
        await queryRunner.query('ALTER TABLE fee DROP COLUMN currency_type');
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query('ALTER TABLE fee ADD currency_type varchar NOT NULL');
        await queryRunner.query(`INSERT INTO trading.currency_pair(pair,
                                                                   base,
                                                                   quote,
                                                                   type,
                                                                   min_order_amount,
                                                                   amount_increment,
                                                                   status,
                                                                   price_increment,
                                                                   max_order_amount,
                                                                   id)
                                                            VALUES('default',
                                                                   'default',
                                                                   'default',
                                                                   'default',
                                                                   1000000,
                                                                   1000000,
                                                                   'active',
                                                                   1000000,
                                                                   NULL,
                                                                   '623f3a33-0ca0-4810-b88e-60431f52bfdc'::uuid);
        `);
    }
}
