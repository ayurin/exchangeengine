import { MigrationInterface, QueryRunner } from 'typeorm';

import { TradesEntity } from '@trading/dao';

export class TradesVersion1664907017741 implements MigrationInterface {
    public async up(qr: QueryRunner): Promise<void> {
        await qr.query(`
            ALTER TABLE "${TradesEntity.TABLE_NAME}"
                ADD COLUMN "buyer_version" smallint NOT NULL,
                ADD COLUMN "seller_version" smallint NOT NULL
        `);
    }

    public async down(qr: QueryRunner): Promise<void> {
        await qr.query(`
            ALTER TABLE "${TradesEntity.TABLE_NAME}"
                DROP COLUMN "buyer_version",
                DROP COLUMN "seller_version"
        `);
    }
}
