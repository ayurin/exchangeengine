import { MigrationInterface, QueryRunner } from 'typeorm';

import { TradesEntity } from '@trading/dao';

export class Trades1660050461256 implements MigrationInterface {
    public async up(qr: QueryRunner): Promise<void> {
        await qr.query(`CREATE TABLE "${TradesEntity.TABLE_NAME}" (
            "id" SERIAL NOT NULL,
            "price" bigint NOT NULL,
            "qty" bigint NOT NULL,
            "quote_qty" bigint NOT NULL,
            "time" bigint NOT NULL,
            "is_buyer_maker" boolean NOT NULL,
            "is_best_match" boolean NOT NULL,
            "symbol" character varying NOT NULL,
            CONSTRAINT "TRADES_PRIMARY_KEY" PRIMARY KEY ("id")
        )`);
    }

    public async down(qr: QueryRunner): Promise<void> {
        await qr.query(`DROP TABLE "${TradesEntity.TABLE_NAME}"`);
    }
}
