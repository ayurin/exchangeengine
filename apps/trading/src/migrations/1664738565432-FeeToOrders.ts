import { MigrationInterface, QueryRunner } from 'typeorm';

import { OrderEntity } from '@trading/dao';

export class FeeToOrders1664738565432 implements MigrationInterface {
    public async up(qr: QueryRunner): Promise<void> {
        await qr.query(`
            ALTER TABLE "${OrderEntity.TABLE_NAME}"
            ADD COLUMN "maker_fee_percent" bigint NOT NULL DEFAULT 0;
            ALTER TABLE "${OrderEntity.TABLE_NAME}"
            ADD COLUMN "taker_fee_percent" bigint NOT NULL DEFAULT 0;
            ALTER TABLE "${OrderEntity.TABLE_NAME}"
            ADD COLUMN "fee_currency" character varying NOT NULL DEFAULT '';
            ALTER TABLE "${OrderEntity.TABLE_NAME}"
            ADD COLUMN "fee_amount" bigint NOT NULL DEFAULT 0;
        `);
    }

    public async down(qr: QueryRunner): Promise<void> {
        await qr.query(`
            ALTER TABLE "${OrderEntity.TABLE_NAME}"
            DROP COLUMN "maker_fee_percent";
            ALTER TABLE "${OrderEntity.TABLE_NAME}"
            DROP COLUMN "taker_fee_percent";
            ALTER TABLE "${OrderEntity.TABLE_NAME}"
            DROP COLUMN "fee_currency";
            ALTER TABLE "${OrderEntity.TABLE_NAME}"
            DROP COLUMN "fee_amount";
        `);
    }
}
