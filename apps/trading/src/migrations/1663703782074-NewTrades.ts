import { MigrationInterface, QueryRunner } from 'typeorm';

import { TradesEntity } from '@trading/dao';

export class NewTrades1663703782074 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`DROP TABLE IF EXISTS "deal"`);
        await queryRunner.query(`DROP TABLE IF EXISTS "trades"`);
        await queryRunner.query(
            `CREATE TABLE "trades" (
            "id" uuid NOT NULL DEFAULT uuid_generate_v4(), 
            "buy_order_id" uuid NOT NULL, "sell_order_id" uuid NOT NULL,
            "quantity" bigint NOT NULL, "price" bigint NOT NULL,
            "quote_quantity" bigint NOT NULL,
            "pair" character varying NOT NULL,
            "taker_buy_flg" boolean NOT NULL,
            "is_best_match" boolean NOT NULL,
            "as_of_date" TIMESTAMP NOT NULL DEFAULT now(),
            CONSTRAINT "PK_c6d7c36a837411ba5194dc58595" PRIMARY KEY ("id"))`,
        );
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`DROP TABLE "trades"`);
        await queryRunner.query(`CREATE TABLE "${TradesEntity.TABLE_NAME}" (
            "id" SERIAL NOT NULL,
            "price" bigint NOT NULL,
            "qty" bigint NOT NULL,
            "quote_qty" bigint NOT NULL,
            "time" bigint NOT NULL,
            "is_buyer_maker" boolean NOT NULL,
            "is_best_match" boolean NOT NULL,
            "symbol" character varying NOT NULL,
            CONSTRAINT "TRADES_PRIMARY_KEY" PRIMARY KEY ("id")
        )`);
        await queryRunner.query(`CREATE TABLE "deal" (
            "id" uuid NOT NULL DEFAULT uuid_generate_v4(),
            "buy_order_id" uuid NOT NULL,
            "sell_order_id" uuid NOT NULL,
            "volume" bigint NOT NULL,
            "symbol" character varying NOT NULL,
            "taker_buy_flg" boolean NOT NULL,
            "as_of_date" TIMESTAMP NOT NULL DEFAULT now(),
            CONSTRAINT "PK_9ce1c24acace60f6d7dc7a7189e" PRIMARY KEY ("id")
        )`);
        await queryRunner.query(`ALTER TABLE "deal" ADD "quantity" bigint`);
        await queryRunner.query(`
        UPDATE "deal"
        SET quantity = old_deal.volume 
        FROM (SELECT id, volume 
                FROM "deal") as old_deal
                WHERE deal.id = old_deal.id 
        `);
        await queryRunner.query(`ALTER TABLE "deal" ALTER COLUMN "quantity" SET NOT NULL`);
        await queryRunner.query(`ALTER TABLE "deal" DROP COLUMN "volume"`);

        await queryRunner.query(`ALTER TABLE "deal" ADD "pair" character varying`);
        await queryRunner.query(`
        UPDATE "deal"
        SET pair = old_deal.symbol 
        FROM (SELECT id, symbol 
                FROM "deal") as old_deal
                WHERE deal.id = old_deal.id 
        `);
        await queryRunner.query(`ALTER TABLE "deal" ALTER COLUMN "pair" SET NOT NULL`);
        await queryRunner.query(`ALTER TABLE "deal" DROP COLUMN "symbol"`);
    }
}
