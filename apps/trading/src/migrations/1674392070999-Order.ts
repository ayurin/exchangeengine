import { MigrationInterface, QueryRunner } from "typeorm"

export class Order1674392070999 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        queryRunner.query('ALTER TABLE orders ALTER COLUMN quantity DROP NOT NULL');
        queryRunner.query('ALTER TABLE orders ADD quantity_market_total int8 NULL');
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        queryRunner.query('ALTER TABLE trading.orders DROP COLUMN quantity_market_total');
        queryRunner.query('ALTER TABLE trading.orders ALTER COLUMN quantity SET NOT NULL');
    }
}
