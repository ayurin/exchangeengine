import { MigrationInterface, QueryRunner } from 'typeorm';

export class FavoritePair1664997891307 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "favorite_pair" (
            "id" uuid NOT NULL DEFAULT uuid_generate_v4(),
            "pair" character varying NOT NULL,
            "favorite" boolean NOT NULL,
            "customer_id" character varying NOT NULL,
            CONSTRAINT "PK_9b4701ea7a9888f6d906fa892c7" PRIMARY KEY ("id"))
        `);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`DROP TABLE "favorite_pair"`);
    }
}
