import { MigrationInterface, QueryRunner } from 'typeorm';

export class AskBidForTrade1665852307961 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "trades" ADD "bid" bigint`);
        await queryRunner.query(`ALTER TABLE "trades" ADD "ask" bigint`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "trades" DROP COLUMN "ask"`);
        await queryRunner.query(`ALTER TABLE "trades" DROP COLUMN "bid"`);
    }
}
