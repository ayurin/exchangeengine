import { MigrationInterface, QueryRunner } from 'typeorm';

export class CurrencyPairFields1669569786831 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "currency_pair" ADD "status" character varying NOT NULL DEFAULT 'active'`);
        await queryRunner.query(`ALTER TABLE "currency_pair" ADD "updated_at" TIMESTAMP NOT NULL DEFAULT now()`);
        await queryRunner.query(`ALTER TABLE "currency_pair" ALTER COLUMN "amount_increment" DROP DEFAULT`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "currency_pair" ALTER COLUMN "amount_increment" SET DEFAULT 0.01`);
        await queryRunner.query(`ALTER TABLE "currency_pair" DROP COLUMN "updated_at"`);
        await queryRunner.query(`ALTER TABLE "currency_pair" DROP COLUMN "status"`);
    }
}
