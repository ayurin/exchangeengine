import { MigrationInterface, QueryRunner } from 'typeorm';

export class Deal1660679165390 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "deal" (
            "id" uuid NOT NULL DEFAULT uuid_generate_v4(),
            "buy_order_id" uuid NOT NULL,
            "sell_order_id" uuid NOT NULL,
            "volume" bigint NOT NULL,
            "symbol" character varying NOT NULL,
            "taker_buy_flg" boolean NOT NULL,
            "as_of_date" TIMESTAMP NOT NULL DEFAULT now(),
            CONSTRAINT "PK_9ce1c24acace60f6d7dc7a7189e" PRIMARY KEY ("id")
        )`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`DROP TABLE "deal"`);
    }
}
