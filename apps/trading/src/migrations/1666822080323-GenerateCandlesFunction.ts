import { MigrationInterface, QueryRunner } from 'typeorm';

import { CandlesEntity, TradesEntity } from '@trading/dao';

export class GenerateCandlesFunction1666822080323 implements MigrationInterface {
    public async up(qr: QueryRunner): Promise<void> {
        await qr.query(`
            CREATE OR REPLACE FUNCTION f_refresh_candles(in_root_date date DEFAULT NULL) RETURNS integer
            LANGUAGE plpgsql
            AS $function$
            DECLARE v_root_date date;
            BEGIN
            IF in_root_date IS NULL THEN
                SELECT date_trunc('month', date_trunc('month', max(as_of_date)) - interval '1 day') INTO v_root_date 
                FROM ${TradesEntity.TABLE_NAME};
            ELSE 
                v_root_date := in_root_date;
            END IF; 
            INSERT INTO ${CandlesEntity.TABLE_NAME} WITH q AS (
            SELECT
                joindate AS start_date,
                lead(joindate, 1, to_date('31.12.9999','DD.MM.YYYY')) OVER (ORDER BY joindate ASC) - interval '1 microseconds' AS end_date,
                '1m' AS resolution
            FROM generate_series(v_root_date, current_timestamp, interval '1 minute') g(joindate)
            UNION ALL
            SELECT 
                joindate AS start_date,
                lead(joindate, 1, to_date('31.12.9999','DD.MM.YYYY')) OVER (ORDER BY joindate ASC) - interval '1 microseconds' AS end_date,
                '5m' AS resolution
            FROM generate_series(v_root_date, current_timestamp, interval '5 minute') g(joindate)
            UNION ALL
            SELECT
                joindate AS start_date,
                lead(joindate, 1, to_date('31.12.9999','DD.MM.YYYY')) OVER (ORDER BY joindate ASC) - interval '1 microseconds' AS end_date,
                '10m' AS resolution
            FROM generate_series(v_root_date, current_timestamp, interval '10 minute') g(joindate)
            UNION ALL
            SELECT 
                joindate AS start_date,
                lead(joindate, 1, to_date('31.12.9999','DD.MM.YYYY')) OVER (ORDER BY joindate ASC) - interval '1 microseconds' AS end_date,
                '15m' AS resolution
            FROM generate_series(v_root_date, current_timestamp, interval '15 minute') g(joindate)
            UNION ALL
            SELECT
                joindate AS start_date,
                lead(joindate, 1, to_date('31.12.9999','DD.MM.YYYY')) OVER (ORDER BY joindate ASC) - interval '1 microseconds' AS end_date,
                '30m' AS resolution
            FROM generate_series(v_root_date, current_timestamp, interval '30 minute') g(joindate)
            UNION ALL
            SELECT
                joindate AS start_date,
                lead(joindate, 1, to_date('31.12.9999','DD.MM.YYYY')) OVER (ORDER BY joindate ASC) - interval '1 microseconds' AS end_date,
                '1h' AS resolution
            FROM generate_series(v_root_date, current_timestamp, interval '1 hour') g(joindate)
            UNION ALL
            SELECT
                joindate AS start_date,
                lead(joindate, 1, to_date('31.12.9999','DD.MM.YYYY')) OVER (ORDER BY joindate ASC) - interval '1 microseconds' AS end_date,
                '4h' AS resolution
            FROM generate_series(v_root_date, current_timestamp, interval '4 hour') g(joindate)
            UNION ALL
            SELECT
                joindate AS start_date,
                lead(joindate, 1, to_date('31.12.9999','DD.MM.YYYY')) OVER (ORDER BY joindate ASC) - interval '1 microseconds' AS end_date,
                '1d' AS resolution
            FROM generate_series(v_root_date, current_timestamp, interval '1 day') g(joindate)
            UNION ALL
            SELECT 
                joindate AS start_date,
                lead(joindate, 1, to_date('31.12.9999','DD.MM.YYYY')) OVER (ORDER BY joindate ASC) - interval '1 microseconds' AS end_date,
                '1w' AS resolution
            FROM generate_series(v_root_date, current_timestamp, interval '1 week') g(joindate)
            )
            SELECT 
                uuid_generate_v4() AS id,
                start_date AS time,
                max(price) AS high_price,
                min(price) AS low_price,
                min(open_price) AS open_price,
                min(close_price) AS close_price,
                sum(volume) AS volume,
                pair AS symbol,
                now() AS created_at,
                resolution
            FROM (
                SELECT start_date,
                end_date,
                resolution,
                pair,
                quantity AS volume,
                CASE WHEN min(as_of_date) OVER (PARTITION BY start_date, resolution, pair) = as_of_date THEN price ELSE NULL END AS open_price,
                CASE WHEN max(as_of_date) OVER (PARTITION BY start_date, resolution, pair) = as_of_date THEN price ELSE NULL END AS close_price,
                t.price,
                t.taker_buy_flg,
                t.is_best_match
                FROM q
                INNER JOIN ${TradesEntity.TABLE_NAME} t 
                ON t.as_of_date BETWEEN q.start_date AND end_date
                WHERE NOT EXISTS (
                    SELECT *
                    FROM ${CandlesEntity.TABLE_NAME} c
                    WHERE c.time >= q.start_date
                    AND c.resolution = q.resolution
                    AND c.symbol = t.pair
                )
            ) xxx
            GROUP BY time, resolution, symbol;
            RETURN 1;
            END;
            $function$;        
        `);
    }

    public async down(qr: QueryRunner): Promise<void> {
        await qr.query(`DROP FUNCTION f_refresh_candles`);
    }
}
