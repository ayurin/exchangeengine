import { MigrationInterface, QueryRunner } from 'typeorm';

import { CandlesEntity } from '@trading/dao';

export class CreateCandles1660200430524 implements MigrationInterface {
    public async up(qr: QueryRunner): Promise<void> {
        await qr.query(`
            CREATE TABLE ${CandlesEntity.TABLE_NAME} (
                id UUID DEFAULT uuid_generate_v4() NOT NULL,
                open_time TIMESTAMP NOT NULL DEFAULT now(),
                close_time TIMESTAMP NOT NULL DEFAULT now(),
                high_price NUMERIC,
                low_price NUMERIC,
                open_price NUMERIC,
                close_price NUMERIC,
                volume NUMERIC NOT NULL DEFAULT 0,
                symbol VARCHAR NOT NULL DEFAULT '',
                interval INT NOT NULL,
                created_at TIMESTAMP NOT NULL DEFAULT now(),
                CONSTRAINT "CANDLES_PRIMARY_KEY" PRIMARY KEY (id)
            );
        `);
    }

    public async down(qr: QueryRunner): Promise<void> {
        await qr.query(`
            DROP TABLE ${CandlesEntity.TABLE_NAME};
        `);
    }
}
