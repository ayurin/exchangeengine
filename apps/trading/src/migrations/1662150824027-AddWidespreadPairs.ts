import { ECurrencyPairType } from '@app/utils';
import { MigrationInterface, QueryRunner } from 'typeorm';

import { CurrencyPairEntity } from '@trading/dao';

export class AddWidespreadPairs1662150824027 implements MigrationInterface {
    public async up(qr: QueryRunner): Promise<void> {
        await qr.query(`
            INSERT INTO "${CurrencyPairEntity.TABLE_NAME}"
            ("pair", "base", "quote", "type", "min_order_amount") VALUES
            ('BTC/ETH', 'BTC', 'ETH', '${ECurrencyPairType.CC}', 1),
            ('ETH/BTC', 'ETH', 'BTC', '${ECurrencyPairType.CC}', 1),
            ('BTC/USDT', 'BTC', 'USDT', '${ECurrencyPairType.CS}', 1),
            ('USDT/BTC', 'USDT', 'BTC', '${ECurrencyPairType.SC}', 1),
            ('BTC/USDC', 'BTC', 'USDC', '${ECurrencyPairType.CS}', 1),
            ('USDC/BTC', 'USDC', 'BTC', '${ECurrencyPairType.SC}', 1),
            ('ETH/USDT', 'ETH', 'USDT', '${ECurrencyPairType.CS}', 1),
            ('USDT/ETH', 'USDT', 'ETH', '${ECurrencyPairType.SC}', 1),
            ('ETH/USDC', 'ETH', 'USDC', '${ECurrencyPairType.CS}', 1),
            ('USDC/ETH', 'USDC', 'ETH', '${ECurrencyPairType.SC}', 1),
            ('USDT/USDC', 'USDT', 'USDC', '${ECurrencyPairType.SS}', 1),
            ('USDC/USDT', 'USDC', 'USDT', '${ECurrencyPairType.SS}', 1),
            ('BTC/EUR', 'BTC', 'EUR', '${ECurrencyPairType.CS}', 1),
            ('EUR/BTC', 'EUR', 'BTC', '${ECurrencyPairType.SC}', 1),
            ('ETH/EUR', 'ETH', 'EUR', '${ECurrencyPairType.CS}', 1),
            ('EUR/ETH', 'EUR', 'ETH', '${ECurrencyPairType.SC}', 1),
            ('BTC/USD', 'BTC', 'USD', '${ECurrencyPairType.CS}', 1),
            ('USD/BTC', 'USD', 'BTC', '${ECurrencyPairType.SC}', 1),
            ('ETH/USD', 'ETH', 'USD', '${ECurrencyPairType.CS}', 1),
            ('USD/ETH', 'USD', 'ETH', '${ECurrencyPairType.SC}', 1),
            ('EUR/USDC', 'EUR', 'USDC', '${ECurrencyPairType.SS}', 1),
            ('USDC/EUR', 'USDC', 'EUR', '${ECurrencyPairType.SS}', 1),
            ('EUR/USDT', 'EUR', 'USDT', '${ECurrencyPairType.SS}', 1),
            ('USDT/EUR', 'USDT', 'EUR', '${ECurrencyPairType.SS}', 1),
            ('USD/USDC', 'USD', 'USDC', '${ECurrencyPairType.SS}', 1),
            ('USDC/USD', 'USDC', 'USD', '${ECurrencyPairType.SS}', 1),
            ('USD/USDT', 'USD', 'USDT', '${ECurrencyPairType.SS}', 1),
            ('USDT/USD', 'USDT', 'USD', '${ECurrencyPairType.SS}', 1)
        `);
    }

    public async down(qr: QueryRunner): Promise<void> {
        await qr.query(`
            DELETE FROM "${CurrencyPairEntity.TABLE_NAME}"
            WHERE "pair" IN (
                'BTC/ETH',
                'ETH/BTC',
                'BTC/USDT',
                'USDT/BTC',
                'BTC/USDC',
                'USDC/BTC',
                'ETH/USDT',
                'USDT/ETH',
                'ETH/USDC',
                'USDC/ETH',
                'USDT/USDC',
                'USDC/USDT',
                'BTC/EUR',
                'EUR/BTC',
                'ETH/EUR',
                'EUR/ETH',
                'BTC/USD',
                'USD/BTC',
                'ETH/USD',
                'USD/ETH',
                'EUR/USDC',
                'USDC/EUR',
                'EUR/USDT',
                'USDT/EUR',
                'USD/USDC',
                'USDC/USD',
                'USD/USDT',
                'USDT/USD'
            )
        `);
    }
}
