import { MigrationInterface, QueryRunner } from 'typeorm';

import { SettingsEntity } from '@trading/dao';

export class CreateSettings1666304045493 implements MigrationInterface {
    public async up(qr: QueryRunner): Promise<void> {
        await qr.query(`CREATE TABLE ${SettingsEntity.TABLE_NAME} (
            id UUID DEFAULT uuid_generate_v4() NOT NULL,
            key CHARACTER VARYING NOT NULL,
            value CHARACTER VARYING,
            created_at TIMESTAMP NOT NULL DEFAULT now(),
            updated_at TIMESTAMP NOT NULL DEFAULT now()
        )`);
    }

    public async down(qr: QueryRunner): Promise<void> {
        await qr.query(`DROP TABLE ${SettingsEntity.TABLE_NAME}`);
    }
}
