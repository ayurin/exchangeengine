import { MigrationInterface, QueryRunner } from 'typeorm';

import { TradesEntity } from '@trading/dao';

export class TradesFees1664480345983 implements MigrationInterface {
    public async up(qr: QueryRunner): Promise<void> {
        await qr.query(`
            ALTER TABLE "${TradesEntity.TABLE_NAME}"
                ADD COLUMN "buyer_fee" bigint NOT NULL,
                ADD COLUMN "seller_fee" bigint NOT NULL;
        `);
    }

    public async down(qr: QueryRunner): Promise<void> {
        await qr.query(`
            ALTER TABLE "${TradesEntity.TABLE_NAME}"
                DROP COLUMN "buyer_fee",
                DROP COLUMN "seller_fee";
        `);
    }
}
