import { MigrationInterface, QueryRunner } from 'typeorm';

import { OrderEntity } from '@trading/dao';

export class ExecutedQuantity1663792969923 implements MigrationInterface {
    public async up(qr: QueryRunner): Promise<void> {
        await qr.query(`
            ALTER TABLE "${OrderEntity.TABLE_NAME}"
            ADD COLUMN "executed_quantity" bigint NOT NULL DEFAULT 0;
        `);
    }

    public async down(qr: QueryRunner): Promise<void> {
        await qr.query(`
            ALTER TABLE "${OrderEntity.TABLE_NAME}"
            DROP COLUMN "executed_quantity";
        `);
    }
}
