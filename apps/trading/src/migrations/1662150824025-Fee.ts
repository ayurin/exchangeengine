import { MigrationInterface, QueryRunner } from 'typeorm';

import { CurrencyPairEntity, DEFAULT_CURRENCY, FeeEntity } from '@trading/dao';

const INITIAL_MAKER_FEE_PERCENT = 0.1;
const INITIAL_TAKER_FEE_PERCENT = 0.2;

export class Fee1662150824025 implements MigrationInterface {
    public async up(qr: QueryRunner): Promise<void> {
        await qr.query(`
            CREATE TABLE "${FeeEntity.TABLE_NAME}" (
                "id" SERIAL NOT NULL,
                "currency_pair_id" SERIAL NOT NULL,
                "maker_fee_percent" NUMERIC NOT NULL,
                "taker_fee_percent" NUMERIC NOT NULL,
                "currency_type" character varying NOT NULL,
                "created_at" TIMESTAMP NOT NULL DEFAULT now(),
                CONSTRAINT "FEE_PRIMARY_KEY" PRIMARY KEY ("id"),
                CONSTRAINT "FEE_CURRENCY_PAIR_FOREIGN_KEY"
                    FOREIGN KEY ("currency_pair_id")
                    REFERENCES "${CurrencyPairEntity.TABLE_NAME}"("id")
                    ON DELETE NO ACTION ON UPDATE NO ACTION
            )
        `);

        await qr.query(`
            INSERT INTO "${FeeEntity.TABLE_NAME}"
            (currency_pair_id, maker_fee_percent, taker_fee_percent, currency_type)
            SELECT id,
                ${INITIAL_MAKER_FEE_PERCENT},
                ${INITIAL_TAKER_FEE_PERCENT},
                '${DEFAULT_CURRENCY}'
            FROM "${CurrencyPairEntity.TABLE_NAME}"
            where pair = '${DEFAULT_CURRENCY}'
        `);
    }

    public async down(qr: QueryRunner): Promise<void> {
        await qr.query(`DROP TABLE "${FeeEntity.TABLE_NAME}"`);
    }
}
