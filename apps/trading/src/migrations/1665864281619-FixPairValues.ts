import { MigrationInterface, QueryRunner } from 'typeorm';

import { CurrencyPairEntity } from '@trading/dao';

export class FixPairValues1665864281619 implements MigrationInterface {
    public async up(qr: QueryRunner): Promise<void> {
        const pairs = (await qr.query(`
            SELECT id, min_order_amount, amount_increment FROM "${CurrencyPairEntity.TABLE_NAME}"
        `)) as Array<{ id: number; min_order_amount: string; amount_increment: string }>;

        await Promise.all(
            pairs.reduce<Array<Promise<any>>>((acc, p) => {
                if (parseInt(p.amount_increment, 10) !== parseFloat(p.amount_increment)) {
                    acc.push(
                        qr.query(`
                            UPDATE "${CurrencyPairEntity.TABLE_NAME}"
                            SET min_order_amount = ${Number(p.amount_increment) * 10 ** 8},
                                amount_increment = ${Number(p.amount_increment) * 10 ** 8}
                            WHERE id = '${p.id}'
                        `),
                    );
                }
                return acc;
            }, []),
        );
    }

    public down(): Promise<void> {
        return Promise.resolve();
    }
}
