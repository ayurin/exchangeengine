import { Module } from '@nestjs/common';

import { SettingsRepository } from '@trading/dao/settings.repository';

import { SettingsController } from './settings.controller';
import { SettingsService } from './settings.service';

@Module({
    controllers: [SettingsController],
    providers: [SettingsService, SettingsRepository],
})
export class SettingsModule {}
