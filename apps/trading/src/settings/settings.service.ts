import { OkResponse } from '@app/utils';
import { Injectable } from '@nestjs/common';
import { plainToInstance } from 'class-transformer';
import { RedisService } from 'nestjs-redis';
import { DataSource } from 'typeorm';

import { CandlesEntity, OrderEntity, SettingsRepository, TradesEntity } from '@trading/dao';

import { AddSettingDto, SettingsResponse, UpdateSettingDto } from './dto';

@Injectable()
export class SettingsService {
    protected readonly redis = this.redisService.getClient();

    constructor(
        private readonly settingsRepository: SettingsRepository,
        private readonly dataSource: DataSource,
        private readonly redisService: RedisService,
    ) {}

    public async getSettings(): Promise<SettingsResponse> {
        const list = await this.settingsRepository.find();
        return plainToInstance(SettingsResponse, { list });
    }

    public async addSetting(data: AddSettingDto): Promise<OkResponse> {
        await this.settingsRepository.add(data);
        return new OkResponse();
    }

    public async updateSetting(id: string, data: UpdateSettingDto): Promise<OkResponse> {
        await this.settingsRepository.updateWithCheck(id, data);
        return new OkResponse();
    }

    public async updateSettingKeyValue(data: UpdateSettingDto): Promise<OkResponse> {
        await this.settingsRepository.updateKeyValueWithCheck(data);
        return new OkResponse();
    }

    public async clearData(): Promise<OkResponse> {
        await this.dataSource.query(`
        DO $$
        begin
        execute 'delete from ${OrderEntity.TABLE_NAME}';
        execute 'delete from ${TradesEntity.TABLE_NAME}';
        execute 'delete from ${CandlesEntity.TABLE_NAME}';
        commit;
        end;
        $$;
        `);
        await this.redis.flushall();

        return new OkResponse();
    }
}
