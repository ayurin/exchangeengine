import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString } from 'class-validator';

export class UpdateSettingDto {
    @ApiProperty({ example: 'resolution' })
    @IsString()
    @IsNotEmpty()
    public key: string;

    @ApiProperty({ example: '["1m", "5m", "15m"]' })
    @IsString()
    public value: string;
}
