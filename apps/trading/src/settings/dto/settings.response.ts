import { ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';

import { SettingResponse } from './setting.response';

const SETTINGS_RESPONSE_EXAMPLE = {
    id: '5000ca7d-b848-46cd-813a-4e81d4a928ac',
    key: 'resolution',
    value: '["1m", "5m", "15m", "1h", "4h"]',
    createdAt: '2022-10-20T19:37:53.785Z',
    updatedAt: '2022-10-20T20:08:37.858Z',
};

export class SettingsResponse {
    @ApiProperty({ example: [SETTINGS_RESPONSE_EXAMPLE] })
    @Type(() => SettingResponse)
    public list: SettingResponse[];
}
