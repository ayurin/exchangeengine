import { ApiProperty } from '@nestjs/swagger';

export class SettingResponse {
    @ApiProperty({ example: 'ac5dcd00-82be-429a-bedb-1a908251d496' })
    public id: string;

    @ApiProperty({ example: 'resolution' })
    public key: string;

    @ApiProperty({ example: '["1m", "5m", "15m"]' })
    public value: string;
}
