import { OkResponse } from '@app/utils';
import { Body, Controller, Get, HttpStatus, Param, Patch, Post } from '@nestjs/common';
import { ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';

import { AddSettingDto, SettingsResponse, UpdateSettingDto } from './dto';
import { SettingsService } from './settings.service';

@ApiTags('settings')
@Controller('settings')
export class SettingsController {
    constructor(private readonly settingsService: SettingsService) {}

    @Get()
    @ApiOperation({ summary: 'Get all settings' })
    @ApiResponse({ status: HttpStatus.OK, type: SettingsResponse })
    public getSettings(): Promise<SettingsResponse> {
        return this.settingsService.getSettings();
    }

    // TODO: 24/10/2022 - Protect with administrator guard
    @Post()
    @ApiOperation({ summary: 'Add new setting' })
    @ApiResponse({ status: HttpStatus.OK, type: OkResponse })
    public addSetting(@Body() data: AddSettingDto): Promise<OkResponse> {
        return this.settingsService.addSetting(data);
    }

    // TODO: 24/10/2022 - Protect with administrator guard
    @Patch(':settingId')
    @ApiOperation({ summary: 'Update setting' })
    @ApiResponse({ status: HttpStatus.OK, type: OkResponse })
    public updateSetting(@Param('settingId') id: string, @Body() data: UpdateSettingDto): Promise<OkResponse> {
        return this.settingsService.updateSetting(id, data);
    }

    // TODO: 24/10/2022 - Protect with administrator guard
    @Patch('/by-key')
    @ApiOperation({ summary: 'Update setting value by key' })
    @ApiResponse({ status: HttpStatus.OK, type: OkResponse })
    public updateSettingKeyValue(@Body() data: UpdateSettingDto): Promise<OkResponse> {
        return this.settingsService.updateSettingKeyValue(data);
    }

    // TODO: 24/10/2022 - Protect with administrator guard
    @Post('/clear-data')
    @ApiOperation({ summary: 'Clear trades, orders tables and redis' })
    @ApiResponse({ status: HttpStatus.OK, type: OkResponse })
    public clearData(): Promise<OkResponse> {
        return this.settingsService.clearData();
    }
}
