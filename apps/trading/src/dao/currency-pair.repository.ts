import { GetCurrencyPairDto } from '@app/utils';
import { Injectable } from '@nestjs/common';
import { DataSource, In, Not, Repository, SelectQueryBuilder } from 'typeorm';

import { CurrencyPairEntity, DEFAULT_CURRENCY } from '@trading/dao/entities';

@Injectable()
export class CurrencyPairRepository extends Repository<CurrencyPairEntity> {
    private currencyPairRepository: Repository<CurrencyPairEntity>;

    constructor(private readonly dataSource: DataSource) {
        super(dataSource.getRepository(CurrencyPairEntity).target, dataSource.manager, dataSource.createQueryRunner());
        this.currencyPairRepository = this.dataSource.getRepository(CurrencyPairEntity);
    }

    public getCurrenciesPairQueryBuilder(
        query: GetCurrencyPairDto,
        customerId: string | null,
        favoritePairs: string[],
    ): SelectQueryBuilder<CurrencyPairEntity> {
        const qb = this.currencyPairRepository.createQueryBuilder();
        qb.where({ pair: Not(DEFAULT_CURRENCY) });
        if (typeof query?.favorite === 'boolean' && !!customerId) {
            if (!favoritePairs?.length)
                favoritePairs.push('это временный костыль, потому что In с пустым массивом не работает');
            qb.andWhere(query.favorite ? { pair: In(favoritePairs) } : { pair: Not(In(favoritePairs)) });
        }
        if (query?.ticker) {
            qb.andWhere({ quote: query.ticker?.toUpperCase() });
        }
        return qb;
    }
}
