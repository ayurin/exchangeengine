import { Column, CreateDateColumn, Entity, PrimaryGeneratedColumn } from 'typeorm';

const TABLE_NAME = 'candles';

@Entity(TABLE_NAME)
export class CandlesEntity {
    public static readonly TABLE_NAME = TABLE_NAME;

    @PrimaryGeneratedColumn('uuid')
    public id!: string;

    // open time of candle
    @Column({ nullable: false, type: 'timestamp' })
    public time!: Date;

    @Column({ type: 'numeric' })
    public highPrice!: string;

    @Column({ type: 'numeric' })
    public lowPrice!: string;

    @Column({ type: 'numeric' })
    public openPrice!: string;

    @Column({ type: 'numeric' })
    public closePrice!: string;

    @Column({ nullable: false, type: 'numeric', default: 0 })
    public volume!: string;

    // pair
    @Column({ nullable: false })
    public symbol!: string;

    // interval
    @Column({ nullable: false })
    public resolution!: string;

    @CreateDateColumn({ type: 'timestamp' })
    public createdAt!: Date;
}
