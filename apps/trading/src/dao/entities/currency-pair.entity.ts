import { ECurrencyPairStatus, ECurrencyPairType, SYMBOL_DELIMITER } from '@app/utils';
import { Column, CreateDateColumn, Entity, PrimaryGeneratedColumn, UpdateDateColumn } from 'typeorm';

const TABLE_NAME = 'currency_pair';

export const DEFAULT_CURRENCY = 'default';

@Entity(TABLE_NAME)
export class CurrencyPairEntity {
    public static readonly TABLE_NAME = TABLE_NAME;

    public static readonly DELIMITER = SYMBOL_DELIMITER;

    @PrimaryGeneratedColumn('uuid')
    public id!: string;

    @Column({ unique: true })
    public pair!: string; // 'BTC-USDC'

    @Column()
    public base!: string; // 'BTC'

    @Column()
    public quote!: string; // 'USDC'

    @Column({ type: 'varchar' })
    public type!: ECurrencyPairType;

    @Column({ type: 'numeric' })
    public minOrderAmount!: string;

    @Column({ type: 'numeric', nullable: true })
    public maxOrderAmount?: string;

    @Column({ type: 'numeric' })
    public amountIncrement!: string;

    @Column({ type: 'numeric' })
    public priceIncrement!: string;

    @Column({ type: 'varchar', default: ECurrencyPairStatus.ACTIVE })
    public status!: ECurrencyPairStatus;

    @CreateDateColumn({ type: 'timestamp' })
    public createdAt!: Date;

    @UpdateDateColumn({ type: 'timestamp' })
    public updatedAt!: Date;
}
