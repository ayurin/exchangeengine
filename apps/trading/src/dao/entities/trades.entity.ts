import { Column, CreateDateColumn, Entity, PrimaryGeneratedColumn } from 'typeorm';

const TABLE_NAME = 'trades';

const stringOrNull = (val: bigint | null | undefined): string | null =>
    val === null || val === undefined ? null : String(val);
const bigintOrNull = (val: string | null | undefined): bigint | null =>
    val === null || val === undefined ? null : BigInt(val);

@Entity(TABLE_NAME)
export class TradesEntity {
    public static readonly TABLE_NAME = TABLE_NAME;

    @PrimaryGeneratedColumn('uuid')
    public id!: string;

    @Column({ type: 'uuid' })
    public buyOrderId!: string;

    @Column({ type: 'uuid' })
    public sellOrderId!: string;

    @Column({ type: 'bigint', transformer: { to: String, from: BigInt } })
    public quantity!: bigint;

    @Column({ type: 'bigint', transformer: { to: String, from: BigInt } })
    public price!: bigint;

    @Column({ type: 'bigint', transformer: { to: String, from: BigInt } })
    public quoteQuantity!: bigint;

    @Column()
    public pair!: string;

    @Column()
    public takerBuyFlg!: boolean;

    @Column({ type: 'bigint', transformer: { to: String, from: BigInt } })
    public buyerFee!: bigint;

    @Column({ type: 'bigint', transformer: { to: String, from: BigInt } })
    public sellerFee!: bigint;

    @Column({ type: 'bigint', transformer: { to: String, from: BigInt } })
    public buyerFeeRefunds!: bigint;

    @Column({ type: 'bigint', transformer: { to: String, from: BigInt } })
    public buyerRefunds!: bigint;

    @Column()
    public isBestMatch!: boolean;

    @Column({ type: 'bigint', nullable: true, transformer: { to: stringOrNull, from: bigintOrNull } })
    public bid?: bigint | null;

    @Column({ type: 'bigint', nullable: true, transformer: { to: stringOrNull, from: bigintOrNull } })
    public ask?: bigint | null;

    @CreateDateColumn({ type: 'timestamp' })
    public asOfDate!: Date;
}
