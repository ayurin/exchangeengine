import { EOrderSide, EOrderType } from '@app/utils';
import { Check, Column, CreateDateColumn, Entity, Generated, PrimaryGeneratedColumn } from 'typeorm';

const TABLE_NAME = 'orders';
export type OrderCancelStatus = 'none' | 'canceled' | 'canceling';

@Entity(TABLE_NAME)
export class OrderEntity {
    public static readonly TABLE_NAME = TABLE_NAME;

    @PrimaryGeneratedColumn('uuid')
    public id!: string;

    @Column({ nullable: false })
    public side!: EOrderSide;

    @Column({ type: 'bigint', nullable: true, transformer: { to: String, from: BigInt } })
    public price?: bigint | null;

    @Column({ type: 'bigint', transformer: { to: String, from: BigInt } })
    public quantity?: bigint;

    @Column({ type: 'bigint', transformer: { to: String, from: BigInt } })
    public quantityMarketTotal?: bigint;

    @Column({ type: 'bigint', nullable: true, transformer: { to: String, from: BigInt } })
    public stopLimitPrice?: bigint | null;

    @Column({ nullable: false })
    public customerId!: string;

    @Column({ nullable: false })
    public pair!: string;

    @Check(`cancel_status in ('none', 'canceled', 'canceling')`)
    @Column({ nullable: false, default: 'none' })
    public cancelStatus!: OrderCancelStatus;

    @Column({ nullable: false, default: EOrderType.LIMIT })
    public type!: EOrderType;

    @Column({ type: 'bigint', transformer: { to: String, from: BigInt } })
    public makerFeePercent!: bigint;

    @Column({ type: 'bigint', transformer: { to: String, from: BigInt } })
    public takerFeePercent!: bigint;

    @Column({ nullable: false })
    public feeCurrency!: string;

    @Column({ type: 'bigint', transformer: { to: String, from: BigInt } })
    public feeAmount!: bigint;

    @Column()
    @Generated('increment')
    public sortOrder?: number;

    @CreateDateColumn({ type: 'timestamp' })
    public asOfDate!: Date;
}
