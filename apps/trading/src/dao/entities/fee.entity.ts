import { Column, CreateDateColumn, Entity, JoinColumn, OneToOne, PrimaryGeneratedColumn } from 'typeorm';

import { CurrencyPairEntity } from '@trading/dao';

const TABLE_NAME = 'fee';

@Entity(TABLE_NAME)
export class FeeEntity {
    public static readonly TABLE_NAME = TABLE_NAME;

    @PrimaryGeneratedColumn('increment')
    public id!: number;

    @OneToOne(() => CurrencyPairEntity)
    @JoinColumn({ name: 'currency_pair_id' })
    public currencyPair: CurrencyPairEntity;

    @Column({ nullable: false })
    public currencyPairId: string;

    @Column({ type: 'numeric', transformer: { to: String, from: BigInt } })
    public makerFeePercent!: bigint;

    @Column({ type: 'numeric', transformer: { to: String, from: BigInt } })
    public takerFeePercent!: bigint;

    @CreateDateColumn({ type: 'timestamp' })
    public createdAt!: Date;

    constructor(partial: Partial<FeeEntity>) {
        this.update(partial);
    }

    public update(partial: Partial<FeeEntity>): FeeEntity {
        Object.assign(this, partial);
        return this;
    }
}
