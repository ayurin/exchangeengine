import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

const TABLE_NAME = 'favorite_pair';

@Entity(TABLE_NAME)
export class FavoritePairEntity {
    public static readonly TABLE_NAME = TABLE_NAME;

    @PrimaryGeneratedColumn('uuid')
    public id!: string;

    @Column()
    public pair!: string;

    @Column()
    public favorite!: boolean;

    @Column()
    public customerId!: string;
}
