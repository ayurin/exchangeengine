import { EOrderSide, IRmqCancelOrder, SYMBOL_DELIMITER } from '@app/utils';
import { Injectable } from '@nestjs/common';
import { DataSource, In, Repository } from 'typeorm';

import { OrderCancelStatus, OrderEntity, TradesEntity } from '@trading/dao/entities';
import { checkOrdersCompletedQuery, getMyOrdersQuery, getOrdersForCancelQuery } from '@trading/dao/query';
import { GetOrdersDto, OrderResponse } from '@trading/order/dto';

export interface IOrderForCancel {
    orderId: string;
    customerId: string;
    currency: string;
    amount: string;
    feeCurrency: string;
    fee: string;
    cancelStatus: OrderCancelStatus;
    pair: string;
}

export interface IOrderCompleted {
    id: string;
    completed: boolean;
    customerId: string;
}

@Injectable()
export class OrderRepository extends Repository<OrderEntity> {
    constructor(private readonly dataSource: DataSource) {
        super(dataSource.getRepository(OrderEntity).target, dataSource.manager, dataSource.createQueryRunner());
    }

    public checkOrdersCompleted(orderIds: string[]): Promise<IOrderCompleted[]> {
        const query = checkOrdersCompletedQuery(orderIds);
        return this.dataSource.query(query) as Promise<IOrderCompleted[]>;
    }

    public async getMyOrders(customerId: string, filters: GetOrdersDto): Promise<[OrderResponse[], number]> {
        const query = getMyOrdersQuery(customerId, filters);
        const data = (await this.dataSource.query(query)) as (OrderResponse & { total: string })[];
        return [data, parseInt(data?.[0]?.total || '0', 10)];
    }

    public async getOrderForCancel(id: string, customerId?: string): Promise<IOrderForCancel | undefined> {
        const qb = this.createQueryBuilder('o');
        qb.select('o.id', 'orderId');
        qb.addSelect('o.customerId', 'customerId');
        qb.addSelect('o.cancelStatus', 'cancelStatus');
        qb.addSelect(
            `
            CASE
                WHEN o.side = '${EOrderSide.SELL}' THEN split_part(o.pair::text, '${SYMBOL_DELIMITER}', 1)::text
                ELSE split_part(o.pair::text, '${SYMBOL_DELIMITER}', 2)::text
            END
            `,
            'currency',
        );
        qb.addSelect(
            `
            CASE
                WHEN o.side = '${EOrderSide.SELL}' THEN (o.quantity::numeric - coalesce(SUM(t.quantity), 0)::numeric)::text
                WHEN o.side = '${EOrderSide.BUY}' THEN
                    (TRUNC(o.quantity::numeric * o.price::numeric / (10^8)::numeric) - coalesce(SUM(t.quote_quantity), 0)::numeric - coalesce(SUM(t.buyer_refunds), 0)::numeric)::text
                ELSE null
            END 
            `,
            'amount',
        );
        qb.addSelect(
            `
            CASE
                WHEN o.side = '${EOrderSide.BUY}' THEN (o.fee_amount::numeric - coalesce(SUM(t.buyer_fee), 0)::numeric - coalesce(SUM(t.buyer_fee_refunds), 0)::numeric)::text
                WHEN o.side = '${EOrderSide.SELL}' THEN 0::text
                ELSE null
            END 
            `,
            'fee',
        );
        qb.addSelect('o.feeCurrency', 'feeCurrency');
        qb.addSelect('o.pair', 'pair');
        qb.leftJoin(TradesEntity, 't', 'o.id = t.buy_order_id or o.id = t.sell_order_id');
        qb.groupBy('o.id');
        qb.where({ id });
        if (customerId) {
            qb.andWhere({ customerId });
        }

        return qb.getRawOne<IOrderForCancel>();
    }

    public getOrdersForCancel(customerId: string): Promise<IRmqCancelOrder[]> {
        const query = getOrdersForCancelQuery(customerId);
        return this.dataSource.query(query) as Promise<IRmqCancelOrder[]>;
    }

    public getOrdersCountByPairs(pairs: string[]): Promise<{ pair: string; count: number }[]> {
        const qb = this.createQueryBuilder()
            .where({ pair: In(pairs) })
            .groupBy('pair')
            .select('pair', 'pair')
            .addSelect('count(pair)::numeric', 'count');
        return qb.getRawMany<{ pair: string; count: number }>();
    }
}
