import { Injectable } from '@nestjs/common';
import { fromUnixTime } from 'date-fns';
import { DataSource, Repository } from 'typeorm';

import { CandlesQueryResult } from '@trading/candles/types';
import { CandlesEntity } from '@trading/dao/entities';

import { getQuotesQuery } from './query';

const DEFAULT_LIMIT = 500;

@Injectable()
export class CandlesRepository extends Repository<CandlesEntity> {
    constructor(private readonly dataSource: DataSource) {
        super(dataSource.getRepository(CandlesEntity).target, dataSource.manager, dataSource.createQueryRunner());
    }

    public async getCandles(
        symbol: string,
        resolution: string,
        startTime?: number,
        endTime?: number,
        limit?: number,
    ): Promise<CandlesEntity[]> {
        const take = limit || DEFAULT_LIMIT;

        return this.dataSource
            .getRepository(CandlesEntity)
            .createQueryBuilder()
            .where((qb) => {
                qb.andWhere('symbol = :symbol', { symbol });
                qb.andWhere('resolution = :resolution', { resolution });
                if (startTime) qb.andWhere('time >= :startTime', { startTime: fromUnixTime(startTime) });
                if (endTime) qb.andWhere('time < :endTime', { endTime: fromUnixTime(endTime) });
            })
            .limit(take)
            .getMany();
    }

    public async getQuotes(): Promise<CandlesQueryResult[]> {
        const queryResult = await this.dataSource.query(getQuotesQuery('1d', 2));

        return queryResult.map((item) => ({
            symbol: item.symbol,
            price: item.close_price,
            change: item.change,
        }));
    }
}
