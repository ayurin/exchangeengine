import { Injectable } from '@nestjs/common';
import { DataSource, Repository } from 'typeorm';

import { FeeEntity } from '@trading/dao/entities';
import { IFeeResponse } from '@trading/fee';

export interface IFeeRow {
    readonly maker_fee: string | null;
    readonly taker_fee: string | null;
}

@Injectable()
export class FeeRepository extends Repository<FeeEntity> {
    constructor(private readonly dataSource: DataSource) {
        super(dataSource.getRepository(FeeEntity).target, dataSource.manager, dataSource.createQueryRunner());
    }

    public async getFeeOrFail(currencyPair: string): Promise<IFeeResponse> {
        const feeRow = await this.getFee(currencyPair);

        if (feeRow.maker_fee === null || feeRow.taker_fee === null) {
            throw new Error('something wrong with the Fee table');
        }

        return { makerFee: Number(feeRow.maker_fee), takerFee: Number(feeRow.taker_fee) };
    }

    public async getFee(currencyPair: string): Promise<IFeeRow> {
        const rows = (await this.dataSource.query(
            `
            select 
                    coalesce(x.maker_fee_percent,y.maker_fee_percent) as maker_fee,
                    coalesce(x.taker_fee_percent,y.taker_fee_percent) as taker_fee
            from (
                    select *
                    from fee f
                    where currency_pair_id is null) y
            left join (
                        select *
                        from fee f
                        inner join currency_pair cp on f.currency_pair_id=cp.id 
            where pair=$1) x on 1=1`,
            [currencyPair],
        )) as IFeeRow[];

        return {
            maker_fee: rows?.[0]?.maker_fee || null,
            taker_fee: rows?.[0]?.taker_fee || null,
        };
    }
}
