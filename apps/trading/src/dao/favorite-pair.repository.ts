import { Injectable } from '@nestjs/common';
import { DataSource, Repository } from 'typeorm';

import { FavoritePairEntity } from '@trading/dao/entities/favorite-pair.entity';

@Injectable()
export class FavoritePairRepository extends Repository<FavoritePairEntity> {
    constructor(private readonly dataSource: DataSource) {
        super(dataSource.getRepository(FavoritePairEntity).target, dataSource.manager, dataSource.createQueryRunner());
    }
}
