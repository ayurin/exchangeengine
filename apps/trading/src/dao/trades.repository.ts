import { IPriceChangeForAsset, IPriceForAsset } from '@app/utils';
import { ITickerUpdates } from '@app/ws';
import { Injectable } from '@nestjs/common';
import { DataSource, Repository, SelectQueryBuilder } from 'typeorm';

import { OrderEntity, TradesEntity } from '@trading/dao/entities';
import { getPriceChangeForAssetsQuery, getPriceChangeForPairQuery, getPriceForAssetsQuery } from '@trading/dao/query';
import { GetTradesDto } from '@trading/trades/dto/get-trades.dto';

@Injectable()
export class TradesRepository extends Repository<TradesEntity> {
    private repository: Repository<TradesEntity>;

    constructor(private readonly dataSource: DataSource) {
        super(dataSource.getRepository(TradesEntity).target, dataSource.manager, dataSource.createQueryRunner());
        this.repository = this.dataSource.getRepository(TradesEntity);
    }

    public getTradesQueryBuilder(filter?: GetTradesDto): SelectQueryBuilder<TradesEntity> {
        const qb = this.repository.createQueryBuilder();
        if (filter?.pair) {
            qb.andWhere({ pair: filter.pair });
        }
        return qb;
    }

    public getPriceChangeForAssets(assets: string[], fiat: string): Promise<IPriceChangeForAsset[]> {
        const query = getPriceChangeForAssetsQuery(assets, fiat);
        return this.dataSource.query(query) as Promise<IPriceChangeForAsset[]>;
    }

    public getPriceForAssets(assets: string[], fiat: string): Promise<IPriceForAsset[]> {
        const query = getPriceForAssetsQuery(assets, fiat);
        return this.dataSource.query(query) as Promise<IPriceForAsset[]>;
    }

    public async getPriceChangeForPair(pair: string): Promise<ITickerUpdates | undefined> {
        const query = getPriceChangeForPairQuery(pair);
        const result = (await this.dataSource.query(query)) as ITickerUpdates[];
        return result?.length ? result[0] : undefined;
    }

    public getMyTradesQueryBuilder(customerId: string): SelectQueryBuilder<TradesEntity> {
        return this.repository
            .createQueryBuilder('t')
            .select(['t.id', 't.quantity', 't.pair', 't.asOfDate'])
            .leftJoin(OrderEntity, 'b', 'b.id = t.buy_order_id')
            .leftJoin(OrderEntity, 's', 's.id = t.sell_order_id')
            .where('b.customerId = :customerId', { customerId })
            .orWhere('s.customerId = :customerId', { customerId });
    }
}
