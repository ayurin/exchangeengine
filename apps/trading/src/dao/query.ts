import { SYMBOL_DELIMITER } from '@app/utils';
import { format } from 'date-fns';

import { getDatesFromPeriodType, GetOrdersDto } from '@trading/order/dto';

/*  *
 *   resolution: candle interval (example: '1m', '1h', '1d')
 *   rank: number of candle in a descending order by time (example: 1, 2, 3)
 *
 *   as we count delta of 24 interval period, as preferable args are:
 *   resolution: '1h', rank: 25
 *   resolution: '1d', rank: 2
 * */
export const getQuotesQuery = (resolution: string, rank: number): string => {
    return `
        WITH current_candle AS
        (
            WITH max_time AS
            (
                SELECT symbol, time
                FROM (
                    SELECT symbol, time, dense_rank() OVER (PARTITION BY symbol ORDER BY time DESC) AS rnk
                    FROM candles
                ) t
                WHERE rnk = 1
            )
            SELECT DISTINCT c.symbol, c.time, c.close_price
            FROM max_time mt
            LEFT JOIN candles c
            ON c.symbol = mt.symbol
            AND c.time = mt.time
        ),
        previous_candle AS
        (
            WITH second_max_time AS 
            (
                SELECT symbol, time
                FROM (
                    SELECT symbol, time, dense_rank() OVER (PARTITION BY symbol ORDER BY time DESC) as rnk
                    FROM candles
                    WHERE resolution = '${resolution}'
                ) t
                WHERE rnk = ${rank}
            )
            SELECT DISTINCT c.symbol, c.time, c.close_price
            FROM second_max_time smt
            LEFT JOIN candles c
            ON c.symbol = smt.symbol
            AND c.time = smt.time
            WHERE c.resolution = '${resolution}'
        )
        SELECT cc.symbol, cc.close_price::text, TRUNC((cc.close_price - pc.close_price) / pc.close_price * 100 * (10^8)::numeric)::text AS change
        FROM current_candle AS cc
        LEFT JOIN previous_candle AS pc
        ON cc.symbol = pc.symbol
    `;
};

export const getPriceChangeForAssetsQuery = (assets: string[], fiat: string): string => {
    const assetPairs = assets.map((asset) => `'${asset}${SYMBOL_DELIMITER}${fiat}'`);
    const assetPairsQuery = assetPairs.join(', ');
    return `select split_part(pair, '${SYMBOL_DELIMITER}', 1) as asset,
    split_part(pair, '${SYMBOL_DELIMITER}', 2) as ticker,
    price::text,
    old_price::text,
    price_change::text,
    case when price_change <> 0 and old_price > 0 then trunc(price_change::numeric * 100 / old_price::numeric * 10^8)::text
      when price_change = 0  then 0::text
      else (100 * 10^8)::text
    end as price_change_percent,
    case when price_change > 0 then 'up'
      when price_change < 0  then 'down'
      else 'no arrow'
    end as arrow
    from (select *,
      coalesce(price, 0)::numeric - coalesce(old_price, 0)::numeric as price_change
      from (select price,
          pair,
          as_of_date as date,
          rank() over (partition by pair order by as_of_date desc) as rnk
          from trades
        ) as t_new
      left join (select price as old_price,
          pair as old_pair,
          as_of_date as old_date,
          rank() over (partition by pair order by as_of_date desc) as old_rnk
          from trades where as_of_date < now() - interval '1 day'
      )  as t_24 on t_24.old_pair = t_new.pair and t_24.old_rnk = 1
    ) as c
    where c.rnk = 1 and c.pair in(${assetPairsQuery});`;
};

export const getPriceForAssetsQuery = (assets: string[], fiat: string): string => {
    const assetPairs = assets.map((asset) => `'${asset}${SYMBOL_DELIMITER}${fiat}'`);
    const assetPairsQuery = assetPairs.join(', ');
    return `select split_part(pair, '${SYMBOL_DELIMITER}', 1) as asset,
    price::text
    from (
    select price,
          pair,
          rank() over (partition by pair order by as_of_date desc) as rnk
          from trades
    ) as c
    where c.rnk = 1 and c.pair in(${assetPairsQuery});`;
};

export const getPriceChangeForPairQuery = (pair: string): string => {
    return `select split_part(pair, '${SYMBOL_DELIMITER}', 2) as ticker,
    price::text,
    price_change::text as "priceChange",
    pair as pair,
    case when price_change <> 0 and old_price > 0 then trunc(price_change::numeric * 100 / old_price::numeric * 10^8)::text
      when price_change = 0  then 0::text
      else (100 * 10^8)::text
    end as "priceChangePercent",
    case when price_change > 0 then 'up'
      when price_change < 0  then 'down'
      else 'no arrow'
    end as arrow
    from (select *,
      coalesce(price, 0)::numeric - coalesce(old_price, 0)::numeric as price_change
      from (select price,
          pair,
          as_of_date as date,
          rank() over (partition by pair order by as_of_date desc) as rnk
          from trades
        ) as t_new
      left join (select price as old_price,
          pair as old_pair,
          as_of_date as old_date,
          rank() over (partition by pair order by as_of_date desc) as old_rnk
          from trades where as_of_date < now() - interval '1 day'
      )  as t_24 on t_24.old_pair = t_new.pair and t_24.old_rnk = 1
    ) as c
    where c.rnk = 1 and c.pair = '${pair}';`;
};

export const getMyOrdersQuery = (customerId: string, filters: GetOrdersDto): string => {
    const customerWhere = `o.customer_id = '${customerId}'`;
    const statusWhere = filters?.status ? `where status ${filters.status === 'open' ? '=' : '!='} 'open'` : '';
    const pairWhere = filters?.pair ? `and o.pair = '${filters.pair}'` : '';
    const { dateFrom, dateTo } = getDatesFromPeriodType(filters.period, filters.dateFrom, filters.dateTo);
    const dateFromQuery = dateFrom ? `and o.as_of_date::date >= '${format(dateFrom, 'yyyy-MM-dd')}'::date` : '';
    const dateToQuery = dateTo ? `and o.as_of_date::date <= '${format(dateTo, 'yyyy-MM-dd')}'::date` : '';
    const paginationOptions = filters.paginationOptions;
    return `select "id",
            "asOfDate",
            pair,
            side,
            type,
            expiry,
            price,
            quantity,
            fee,
            "realizedProfit",
            progress,
            status,
            count(z.*) over() AS total
        from(select o.id,
            o.cancel_status,
            o.as_of_date as "asOfDate",
            o.pair,
            o.side,
            o.type,
            'GTC' as expiry,
            o.price,
            o.quantity,
            case when sum(t.quantity) >= o.quantity then 'completed'
                when o.cancel_status = 'none' then 'open'
                else o.cancel_status
            end as status,
            case when sum(t.quantity) > 0
                then trunc(sum(t.quantity)::numeric * 100::numeric / o.quantity::numeric * (10 ^ 8)::numeric)::text
                else 0::text
            end as progress,
                case when sum(t.quote_quantity) > 0 then sum(t.quote_quantity)::text
                else 0::text
            end as "realizedProfit",
            case when o.side = 'buy' and sum(t.buyer_fee) > 0 then sum(t.buyer_fee)::text
                when o.side = 'sell' and sum(t.seller_fee) > 0 then sum(t.seller_fee)::text
                else 0::text
            end as fee
        from orders o
        left join trades t on o.id = t.buy_order_id or o.id = t.sell_order_id
        where ${customerWhere}
        ${pairWhere}
        ${dateFromQuery}
        ${dateToQuery}
        group by o.id
        order by "asOfDate" desc
    ) z
        ${statusWhere}
        limit ${paginationOptions.take} offset ${paginationOptions.skip};`;
};

export const getOrdersForCancelQuery = (customerId: string): string => {
    const customerWhere = `o.customer_id = '${customerId}'`;
    return `select "id",
            pair
        from(select o.id,
            o.pair,
            case when sum(t.quantity) >= o.quantity then 'completed'
                when o.cancel_status = 'none' then 'open'
                else o.cancel_status
            end as status
        from orders o
        left join trades t on o.id = t.buy_order_id or o.id = t.sell_order_id
        where ${customerWhere}
        group by o.id
    ) z
        where status = 'open'`;
};

export const checkOrdersCompletedQuery = (orderIds: string[]): string => {
    return `select *
            from(
               select o.id,
               coalesce(sum(t.quantity), 0) >= o.quantity as completed,
               o.customer_id as "customerId"
               from orders o
               left join trades t on o.id in (t.buy_order_id, t.sell_order_id)
               group by o.id
            ) x
            where id in ('${orderIds.join(`','`)}');`;
};
