import { checkExists, valueOrFail } from '@app/utils';
import { Injectable } from '@nestjs/common';
import { instanceToPlain } from 'class-transformer';
import { DataSource, Not, Repository } from 'typeorm';

import { AddSettingDto, UpdateSettingDto } from '@trading/settings/dto';

import { SettingsEntity } from './entities/settings.entity';

@Injectable()
export class SettingsRepository extends Repository<SettingsEntity> {
    private settingsRepository = this.dataSource.getRepository(SettingsEntity);

    constructor(private dataSource: DataSource) {
        super(dataSource.getRepository(SettingsEntity).target, dataSource.manager, dataSource.createQueryRunner());
    }

    public async add(data: AddSettingDto): Promise<SettingsEntity> {
        const existEntity = await this.settingsRepository.findOneBy({ key: data.key });

        checkExists(existEntity, 'Settings', 'key');

        const entity = this.settingsRepository.create(instanceToPlain(data));
        return this.settingsRepository.save(entity);
    }

    public async updateWithCheck(id: string, data: UpdateSettingDto): Promise<void> {
        await this.getOneByIdOrFail(id);
        const existKeyEntity = await this.settingsRepository.findOneBy({ key: data.key, id: Not(id) });
        checkExists(existKeyEntity, 'Settings', 'key');

        await this.settingsRepository.update(id, instanceToPlain(data));
    }

    public async updateKeyValueWithCheck(data: UpdateSettingDto): Promise<void> {
        const { id } = await this.getOneByKeyOrFail(data.key);

        await this.settingsRepository.update(id, { value: data.value });
    }

    public async getOneByIdOrFail(id: string): Promise<SettingsEntity> {
        const entity = await this.settingsRepository.findOneBy({ id });

        return valueOrFail(entity, 'Settings');
    }

    public async getOneByKeyOrFail(key: string): Promise<SettingsEntity> {
        const entity = await this.settingsRepository.findOneBy({ key });

        return valueOrFail(entity, 'Settings');
    }
}
