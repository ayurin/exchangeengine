import { provideServiceDefaultMock } from '@haqqex-backend/testing';
import { Test, TestingModule } from '@nestjs/testing';

import { OrderRepository } from '@trading/dao';
import { OrderService } from '@trading/order/order.service';

import { OrderController } from '../order/order.controller';

describe('orderController', () => {
    let controller: OrderController;
    let service: OrderService;
    let entity: OrderRepository;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            controllers: [OrderController],
            providers: [provideServiceDefaultMock(OrderService), provideServiceDefaultMock(OrderRepository)],
        }).compile();

        controller = module.get<OrderController>(OrderController);
        service = module.get<OrderService>(OrderService);
        entity = module.get(OrderRepository);
    });

    it('should be defined', () => {
        expect(controller).toBeDefined();
    });
});
