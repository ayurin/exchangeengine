import { ECurrencyType } from '@app/utils';
import { BadRequestException, Injectable } from '@nestjs/common';

import { CurrencyService } from '@trading/currency-pair/currency.service';
import { ICurrencyType } from '@trading/currency-pair/dto';

interface IFakeCurrency {
    readonly name: string;
    readonly type: ECurrencyType;
}

export const fakeSystemCurrencies: IFakeCurrency[] = [
    {
        name: 'HIP',
        type: ECurrencyType.CRYPTO,
    },
    {
        name: 'HOP',
        type: ECurrencyType.STABLE,
    },
];

@Injectable()
export class CurrencyServiceMoch extends CurrencyService {
    public override getCurrencyType(currencyShortName: string): Promise<ICurrencyType> {
        const curreny = fakeSystemCurrencies.find((c) => c.name === currencyShortName);
        if (curreny === undefined) {
            throw new BadRequestException(`The ${currencyShortName} is not registered at our system`);
        }

        return Promise.resolve(curreny);
    }
}
