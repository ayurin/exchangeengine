import { ECurrencyPairType } from '@app/utils';

import { CurrencyPairEntity } from '@trading/dao';

export const fakePairs: Array<
    Omit<CurrencyPairEntity, 'id' | 'createdAt' | 'amountIncrement' | 'priceIncrement' | 'status' | 'updatedAt'>
> = [
    { pair: 'ORLY-YARLY', base: 'ORLY', quote: 'YARLY', type: ECurrencyPairType.CS, minOrderAmount: '1' },
    { pair: 'ROCK-ROLL', base: 'ROCK', quote: 'ROLL', type: ECurrencyPairType.CS, minOrderAmount: '1' },
    { pair: 'YARLY-ORLY', base: 'YARLY', quote: 'ORLY', type: ECurrencyPairType.SC, minOrderAmount: '1' },
    { pair: 'WAIT-OHSH', base: 'WAIT', quote: 'OHSH', type: ECurrencyPairType.CC, minOrderAmount: '1' },
    { pair: 'FIFA-NHL', base: 'FIFA', quote: 'NHL', type: ECurrencyPairType.SS, minOrderAmount: '1' },
];

// 100% is 10^8
export const FIRST_TARGET_MAKER_FEE_MOCH = 3000_0000;
export const SECOND_TARGET_MAKER_FEE_MOCH = 3300_0000;
export const FIRST_TARGET_TAKER_FEE_MOCH = 4000_0000;
export const SECOND_TARGET_TAKER_FEE_MOCH = 4400_0000;
export const FIRST_TYPE_MAKER_FEE_MOCH = 5000_0000;
export const SECOND_TYPE_MAKER_FEE_MOCH = 5500_0000;
export const FIRST_TYPE_TAKER_FEE_MOCH = 6000_0000;
export const SECOND_TYPE_TAKER_FEE_MOCH = 6600_0000;
