// eslint-disable-next-line import/no-extraneous-dependencies
import { Test, TestingModule } from '@nestjs/testing';

import { OrderBookController } from '../order-book/order-book.controller';

describe('orderBookController', () => {
    let controller: OrderBookController;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            controllers: [OrderBookController],
        }).compile();

        controller = module.get<OrderBookController>(OrderBookController);
    });

    it('should be defined', () => {
        expect(controller).toBeDefined();
    });
});
