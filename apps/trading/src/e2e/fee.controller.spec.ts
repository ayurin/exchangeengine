import { ExceptionModule } from '@app/exception';
import { HttpStatus } from '@nestjs/common';
import { FastifyAdapter, NestFastifyApplication } from '@nestjs/platform-fastify';
import { Test, TestingModule } from '@nestjs/testing';
import { TypeOrmModule } from '@nestjs/typeorm';
import { LoggerModule } from 'nestjs-pino';

import { appConfig } from '@trading/config';
import { API_PREFIX, appValidationPipe, appVersioningOptions, dataSourceOptions } from '@trading/env';
import { FeeModule, IFeeResponse } from '@trading/fee';

import {
    fakePairs,
    SECOND_TARGET_MAKER_FEE_MOCH,
    SECOND_TARGET_TAKER_FEE_MOCH,
    SECOND_TYPE_MAKER_FEE_MOCH,
    SECOND_TYPE_TAKER_FEE_MOCH,
} from './moch/fee.moch';

describe('tradeHistoryController', () => {
    const FEE_ENDPOINT = `http://127.0.0.1:${appConfig.APP_PORT}/api/v1/fee?`;

    const fetchFees = (currencyPair: string): Promise<IFeeResponse> => {
        return fetch(FEE_ENDPOINT + new URLSearchParams({ currencyPair }).toString()).then((res) => {
            expect(res.ok).toBe(true);
            return res.json() as Promise<IFeeResponse>;
        });
    };

    let defaultFee: IFeeResponse;

    let app: NestFastifyApplication;

    beforeAll(async () => {
        const module: TestingModule = await Test.createTestingModule({
            imports: [ExceptionModule, LoggerModule.forRoot(), TypeOrmModule.forRoot(dataSourceOptions), FeeModule],
        }).compile();

        app = module.createNestApplication(new FastifyAdapter());
        app.useGlobalPipes(appValidationPipe);
        app.setGlobalPrefix(API_PREFIX);
        app.enableVersioning(appVersioningOptions);

        await app.listen(appConfig.APP_PORT, '127.0.0.1');

        defaultFee = await fetchFees('IDONT/KNOW');
    });

    afterAll(async () => {
        if (app) {
            await app.close();
        }
    });

    it('currencyPair query param is required', async () => {
        const res = await fetch(FEE_ENDPOINT);
        expect(res.ok).toBe(false);
        expect(res.status).toBe(HttpStatus.BAD_REQUEST);
        const parsed = (await res.json()) as { message: { message: string[] } };
        expect(parsed.message.message).toContain('currencyPair should not be empty');
    });

    it('should return a correct fee for the pair with a specified fee', async () => {
        const pairFee = await fetchFees(fakePairs[0].pair);
        expect(pairFee.makerFee).toBe(SECOND_TARGET_MAKER_FEE_MOCH);
        expect(pairFee.takerFee).toBe(SECOND_TARGET_TAKER_FEE_MOCH);
    });

    it('should return a correct fee for the pair with not specified fee but with type-specified fee', async () => {
        const pairTypeFee = await fetchFees(fakePairs[1].pair);
        expect(pairTypeFee.makerFee).toBe(SECOND_TYPE_MAKER_FEE_MOCH);
        expect(pairTypeFee.takerFee).toBe(SECOND_TYPE_TAKER_FEE_MOCH);
    });

    it('should return default fee for the pair with not specified pair and type fees', async () => {
        const unknownFee = await fetchFees(fakePairs[2].pair);
        expect(unknownFee.makerFee).toBe(defaultFee.makerFee);
        expect(unknownFee.takerFee).toBe(defaultFee.takerFee);
    });
});
