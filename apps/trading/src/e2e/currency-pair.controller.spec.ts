import { ExceptionModule } from '@app/exception';
import {
    AddPairBody,
    calculatePairType,
    IdResponse,
    PaginatedExtendedCurrencyPairResponse,
    SYMBOL_DELIMITER,
} from '@app/utils';
import { HttpStatus } from '@nestjs/common';
import { FastifyAdapter, NestFastifyApplication } from '@nestjs/platform-fastify';
import { Test, TestingModule } from '@nestjs/testing';
import { TypeOrmModule } from '@nestjs/typeorm';
import { LoggerModule } from 'nestjs-pino';
import { In } from 'typeorm';

import { appConfig } from '@trading/config';
import { CurrencyPairModule } from '@trading/currency-pair';
import { CurrencyService } from '@trading/currency-pair/currency.service';
import { CurrencyPairRepository } from '@trading/dao';
import { API_PREFIX, appValidationPipe, appVersioningOptions, dataSourceOptions } from '@trading/env';

import { CurrencyServiceMoch, fakeSystemCurrencies } from './moch/currency.service.moch';

describe('currencyPairController', () => {
    let app: NestFastifyApplication;

    const URL = `http://127.0.0.1:${appConfig.APP_PORT}/api/v1/currency-pair`;
    const searchParams = {
        searchBy: 'pair',
        search: 'BTC-ETH',
    };
    const jsonHeaders = {
        'Content-Type': 'application/json', // eslint-disable-line @typescript-eslint/naming-convention
    };

    beforeAll(async () => {
        const module: TestingModule = await Test.createTestingModule({
            imports: [
                ExceptionModule,
                LoggerModule.forRoot(),
                TypeOrmModule.forRoot(dataSourceOptions),
                CurrencyPairModule,
            ],
        })
            .overrideProvider(CurrencyService)
            .useClass(CurrencyServiceMoch)
            .compile();

        app = module.createNestApplication(new FastifyAdapter());
        app.useGlobalPipes(appValidationPipe);
        app.setGlobalPrefix(API_PREFIX);
        app.enableVersioning(appVersioningOptions);

        await app.listen(appConfig.APP_PORT, '127.0.0.1');
    });

    afterAll(async () => {
        if (app) {
            const rep = app.get(CurrencyPairRepository);
            await rep.delete({ base: In([fakeSystemCurrencies[0].name, fakeSystemCurrencies[1].name]) });

            await app.close();
        }
    });

    it('fetching a currency pairs list', async () => {
        const { data } = await fetch(`${URL}?${new URLSearchParams(searchParams)}`).then(
            (res) => res.json() as Promise<PaginatedExtendedCurrencyPairResponse>,
        );

        expect(data?.[0]?.pair).toBe('BTC-ETH');
    });

    describe('add new pair', () => {
        it(`can't add the pair with an unknown currency`, async () => {
            const unknownCurrency = 'OMG';

            const body: AddPairBody = {
                base: unknownCurrency,
                quote: fakeSystemCurrencies[0].name,
                minOrderAmount: '1000000',
                maxOrderAmount: '1000000000',
                amountIncrement: '1000000',
                priceIncrement: '1000000',
            };

            const res = await fetch(URL, {
                method: 'POST',
                headers: jsonHeaders,
                body: JSON.stringify(body),
            });

            expect(res.ok).toBe(false);
            expect(res.status).toBe(HttpStatus.BAD_REQUEST);

            const err = (await res.json()) as { message: { message: string | string[] } };
            expect(err.message.message).toContain(`The ${unknownCurrency} is not registered at our system`);
        });

        it(`should add a new pair correctly`, async () => {
            const body: AddPairBody = {
                base: fakeSystemCurrencies[0].name,
                quote: fakeSystemCurrencies[1].name,
                minOrderAmount: '1000000',
                maxOrderAmount: '1000000000',
                amountIncrement: '1000000',
                priceIncrement: '1000000',
            };

            const res = await fetch(URL, {
                method: 'POST',
                headers: jsonHeaders,
                body: JSON.stringify(body),
            });

            expect(res.ok).toBe(true);

            const { id } = (await res.json()) as IdResponse;
            expect(typeof id).toBe(`string`);

            const rep = app.get(CurrencyPairRepository);
            const newPair = await rep.findOneByOrFail({ id });
            expect(newPair.type).toBe(calculatePairType(fakeSystemCurrencies[0].type, fakeSystemCurrencies[1].type));
        });

        it(`can't add a same pair twice`, async () => {
            const body: AddPairBody = {
                base: fakeSystemCurrencies[1].name,
                quote: fakeSystemCurrencies[0].name,
                minOrderAmount: '1000000',
                maxOrderAmount: '1000000000',
                amountIncrement: '1000000',
                priceIncrement: '1000000',
            };

            const res1 = await fetch(URL, {
                method: 'POST',
                headers: jsonHeaders,
                body: JSON.stringify(body),
            });

            expect(res1.ok).toBe(true);

            const res2 = await fetch(URL, {
                method: 'POST',
                headers: jsonHeaders,
                body: JSON.stringify(body),
            });

            expect(res2.ok).toBe(false);
            expect(res2.status).toBe(HttpStatus.CONFLICT);

            const err = (await res2.json()) as { message: { message: string | string[] } };
            expect(err.message.message).toContain(
                `The ${body.base}${SYMBOL_DELIMITER}${body.quote} pair already exists`,
            );
        });
    });
});
