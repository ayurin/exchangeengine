import { INestApplication } from '@nestjs/common';
import { Test } from '@nestjs/testing';
import { getRepositoryToken, TypeOrmModule } from '@nestjs/typeorm';
import { LoggerModule } from 'nestjs-pino';
import * as request from 'supertest';

import { CandlesModule } from '@trading/candles/candles.module';
import { CandlesEntity, CandlesRepository } from '@trading/dao';
import { appValidationPipe, dataSourceOptions } from '@trading/env';
import { createTestMicroservice } from '@trading/jest.env';

const TIMEOUT = 60000;

jest.setTimeout(TIMEOUT);

describe('candles controller', () => {
    let app: INestApplication;
    let candlesRepository: CandlesRepository;

    beforeAll(async () => {
        const module = await Test.createTestingModule({
            imports: [LoggerModule.forRoot(), TypeOrmModule.forRoot(dataSourceOptions), CandlesModule],
        }).compile();

        app = createTestMicroservice(module, appValidationPipe);

        await app.startAllMicroservices();
        await app.listen('50001');

        candlesRepository = app.get(getRepositoryToken(CandlesRepository));
    });

    describe('test flow', () => {
        let candle: CandlesEntity;

        beforeAll(() => {
            candle = candlesRepository.create({
                id: '5454d258-d242-46f7-98ca-61f18b65ec57',
                time: new Date(1660215600000),
                highPrice: '32170000000',
                lowPrice: '32120000000',
                openPrice: '32170000000',
                closePrice: '32150000000',
                volume: '41718000000',
                symbol: 'BTC-USDC',
                resolution: '3h',
                createdAt: new Date('2022-08-12 15:51:14.694'),
            });
        });

        afterAll(async () => {
            await candlesRepository.delete(candle.id);
        });

        describe('getCandles', () => {
            let resultEntity: CandlesEntity;

            beforeAll(async () => {
                resultEntity = await candlesRepository.save(candle);
            });

            it('should return 1 candle', async () => {
                return request(app.getHttpServer())
                    .get('/api/v1/history?symbol=BTC-USDC&resolution=3h')
                    .expect(200)
                    .expect([
                        {
                            time: 1660215600000,
                            high: '32170000000',
                            low: '32120000000',
                            open: '32170000000',
                            close: '32150000000',
                            volume: '41718000000',
                        },
                    ]);
            });

            it('should return empty array', async () => {
                return request(app.getHttpServer())
                    .get('/api/v1/history?symbol=BTC-USDC&resolution=7h')
                    .expect(200)
                    .expect([]);
            });

            it('should return code 400 - bad request', async () => {
                return request(app.getHttpServer()).get('/api/v1/history').expect(400);
            });
        });
    });

    afterAll(async () => {
        await app.close();
    });
});
