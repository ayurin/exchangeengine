/* eslint-disable jest/max-expects */
import { ExceptionModule } from '@app/exception';
import { getRandomArbitrary, getRandomInt } from '@app/utils';
import { HttpStatus } from '@nestjs/common';
import { FastifyAdapter, NestFastifyApplication } from '@nestjs/platform-fastify';
import { Test, TestingModule } from '@nestjs/testing';
import { TypeOrmModule } from '@nestjs/typeorm';
import { LoggerModule } from 'nestjs-pino';

import { API_PREFIX, appValidationPipe, appVersioningOptions, dataSourceOptions } from '@trading/env';
import { TradesModule } from '@trading/trades';
import { PaginatedTradesResponse, TradesResponse } from '@trading/trades/dto';
import { DEFAULT_TRADE_LIMIT, MAX_TRADE_LIMIT, MIN_TRADE_LIMIT } from '@trading/trades/trades.service';

const TIMEOUT = 60000;

jest.setTimeout(TIMEOUT);

const TRADING_TEST_PORT = '50002';

describe('tradeHistoryController', () => {
    const TRADES_ENDPOINT = `http://127.0.0.1:${TRADING_TEST_PORT}/api/v1/trades?`;

    const fetchTrades = (query: Record<string, string>): Promise<TradesResponse[]> => {
        return fetch(TRADES_ENDPOINT + new URLSearchParams(query).toString()).then(async (res) => {
            expect(res.ok).toBe(true);
            const paginated = (await res.json()) as PaginatedTradesResponse;
            return paginated.data;
        });
    };

    let app: NestFastifyApplication;

    beforeAll(async () => {
        const module: TestingModule = await Test.createTestingModule({
            imports: [ExceptionModule, LoggerModule.forRoot(), TypeOrmModule.forRoot(dataSourceOptions), TradesModule],
        }).compile();

        app = module.createNestApplication(new FastifyAdapter());
        app.useGlobalPipes(appValidationPipe);
        app.setGlobalPrefix(API_PREFIX);
        app.enableVersioning(appVersioningOptions);

        await app.listen(TRADING_TEST_PORT as string, '127.0.0.1');
    });

    afterAll(async () => {
        if (app) {
            await app.close();
        }
    });

    it('should return a correct data set', async () => {
        const trades = await fetchTrades({ symbol: 'BNBUSDT' });
        expect(Array.isArray(trades)).toBe(true);

        const [trade] = trades;
        expect(typeof trade).toBe('object');
        expect(trade).not.toBeNull();

        expect(typeof trade.id).toBe('number');
        expect(trade.id).not.toBeNaN();

        expect(typeof trade.price).toBe('string');
        expect(trade.price.split('.')).toHaveLength(2);
        expect(Number(trade.price)).not.toBeNaN();

        expect(typeof trade.quantity).toBe('string');
        expect(trade.quantity.split('.')).toHaveLength(2);
        expect(Number(trade.quantity)).not.toBeNaN();

        expect(typeof trade.quoteQuantity).toBe('string');
        expect(trade.quoteQuantity.split('.')).toHaveLength(2);
        expect(Number(trade.quoteQuantity)).not.toBeNaN();

        expect(typeof trade.asOfDate).toBe('number');
        expect(trade.asOfDate).not.toBeNaN();

        expect(typeof trade.takerBuyFlg).toBe('boolean');
        expect(typeof trade.isBestMatch).toBe('boolean');
    });

    it('should choose a right symbol', async () => {
        const fullBnbUsdt = await fetchTrades({ symbol: 'BNBUSDT' });
        expect(fullBnbUsdt.length).toBeGreaterThan(1);

        const fullBnbUsdc = await fetchTrades({ symbol: 'BNBUSDC' });
        expect(fullBnbUsdc.length).toBeGreaterThan(1);

        const randomSymbol = await fetchTrades({ symbol: Math.random().toString() });
        expect(randomSymbol).toHaveLength(0);
    });

    it('should paginate correctly', async () => {
        const defaultLimit = await fetchTrades({ symbol: 'BNBUSDT' });
        expect(defaultLimit).toHaveLength(DEFAULT_TRADE_LIMIT);

        const someLimit = getRandomInt(MIN_TRADE_LIMIT, MAX_TRADE_LIMIT);
        const strictLimit = await fetchTrades({ symbol: 'BNBUSDT', limit: someLimit.toString() });
        expect(strictLimit).toHaveLength(someLimit);

        const max = await fetchTrades({ symbol: 'BNBUSDT', limit: (MAX_TRADE_LIMIT + 1).toString() });
        expect(max).toHaveLength(MAX_TRADE_LIMIT);

        const min = await fetchTrades({ symbol: 'BNBUSDT', limit: (MIN_TRADE_LIMIT - MAX_TRADE_LIMIT).toString() });
        expect(min).toHaveLength(MIN_TRADE_LIMIT);

        const untrunced = getRandomArbitrary(MIN_TRADE_LIMIT, MAX_TRADE_LIMIT);
        const trunced = await fetchTrades({ symbol: 'BNBUSDT', limit: untrunced.toString() });
        expect(trunced).toHaveLength(Math.trunc(untrunced));
    });

    it('incorrect limit', async () => {
        const resBody = await fetch(
            TRADES_ENDPOINT + new URLSearchParams({ symbol: 'BNBUSDT', limit: 'limit' }).toString(),
        ).then((res) => {
            expect(res.ok).toBe(false);
            expect(res.status).toBe(HttpStatus.BAD_REQUEST);
            return res.json() as Promise<{ message: { message: string[] } }>;
        });

        const errorMessage = resBody.message.message[0];
        expect(errorMessage).toBe('limit must be a number string');
    });

    it('symbol is required', async () => {
        const resBody = await fetch(
            TRADES_ENDPOINT + new URLSearchParams({ limit: Math.random().toString() }).toString(),
        ).then((res) => {
            expect(res.ok).toBe(false);
            expect(res.status).toBe(HttpStatus.BAD_REQUEST);
            return res.json() as Promise<{ message: { message: string[] } }>;
        });

        const errorMessage = resBody.message.message[0];
        expect(errorMessage).toBe('symbol should not be empty');
    });
});
