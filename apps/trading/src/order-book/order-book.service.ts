import { EOrderSide } from '@app/utils';
import { IOrderBookSide } from '@app/ws';
import { Injectable } from '@nestjs/common';
import Big from 'big.js';
import { RedisService } from 'nestjs-redis';

import { OrderBookResponse } from './dto/order-book.response';

@Injectable()
export class OrderBookService {
    protected readonly redis = this.redisService.getClient();

    constructor(private readonly redisService: RedisService) {}

    public async getOrderBook(
        pair: string,
        step: string | undefined,
        limit: number | undefined,
        showZeroVolume: boolean | undefined,
    ): Promise<OrderBookResponse | null> {
        const getter = await Promise.all([
            this.redis.hget('orderBook-time', pair),
            this.redis.hgetall(`orderBook-buy-${pair}`),
            this.redis.hgetall(`orderBook-sell-${pair}`),
        ]);
        const [lastUpdateId, buy, sell] = getter;

        const sortedBuyArray = this.toOrderBookResponse(buy).sort((a, b) =>
            BigInt(a.price) < BigInt(b.price) ? 1 : -1,
        );
        const sortedSellArray = this.toOrderBookResponse(sell).sort((a, b) =>
            BigInt(a.price) > BigInt(b.price) ? 1 : -1,
        );

        return lastUpdateId
            ? ({
                  buy: this.groupedOrderBook(sortedBuyArray, EOrderSide.BUY, step, showZeroVolume),
                  sell: this.groupedOrderBook(sortedSellArray, EOrderSide.SELL, step, showZeroVolume),
              } as OrderBookResponse)
            : null;
    }

    private toOrderBookResponse(sideData: Record<string, string>): IOrderBookSide[] {
        return Object.keys(sideData).map((i) => {
            return { price: i, volume: sideData[i] };
        });
    }

    private groupedOrderBook(
        orderBook: IOrderBookSide[],
        orderSide: EOrderSide,
        step = '0.01',
        showZeroVolume = false,
    ): IOrderBookSide[] {
        const orderBookWithChangedPrecision: IOrderBookSide[] = orderBook.map((e) => {
            return {
                price: this.changePrecision(e.price, orderSide, step),
                volume: e.volume,
            };
        });

        const groupedOrderBook: IOrderBookSide[] = [];
        orderBookWithChangedPrecision.reduce((res: IOrderBookSide, value: IOrderBookSide) => {
            if (!res[value.price]) {
                res[value.price] = { price: value.price, volume: Big(0).toString() };
                groupedOrderBook.push(res[value.price]);
            }
            res[value.price].volume = Big(res[value.price].volume).add(value.volume).toString();
            return res;
        }, {} as IOrderBookSide);

        if (showZeroVolume) {
            return groupedOrderBook.filter((r) => r.volume !== '0');
        }

        return groupedOrderBook;
    }

    private changePrecision(value: string, orderSide: EOrderSide, precision: string): string {
        return orderSide === EOrderSide.SELL
            ? this.roundValue(value, precision, Math.ceil)
            : this.roundValue(value, precision, Math.floor);
    }

    private roundValue(value: string, precision: string, round: (x: number) => number): string {
        const changedValue = Big(value)
            .div(10 ** 8)
            .div(precision)
            .toNumber();

        return Big(round(changedValue))
            .times(precision)
            .times(10 ** 8)
            .toString();
    }
}
