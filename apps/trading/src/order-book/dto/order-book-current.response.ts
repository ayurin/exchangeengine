import { EOrderBookSide } from '@app/utils';
import { ApiProperty } from '@nestjs/swagger';

// ToDo: для теста class-transformer
export class OrderBookCurrentResponse {
    @ApiProperty({ enum: EOrderBookSide, example: EOrderBookSide.ASKS })
    public side: EOrderBookSide;

    @ApiProperty({ example: '431.00000000' })
    public price: string;

    constructor(partial: Partial<OrderBookCurrentResponse>) {
        Object.assign(this, partial);
    }
}

export const ORDER_BOOK_CURRENT_MOCK_DATA = new OrderBookCurrentResponse({
    side: EOrderBookSide.ASKS,
    price: '431.00000000',
});
