import { IOrderBookSide } from '@app/ws';
import { ApiProperty } from '@nestjs/swagger';

export class OrderBookResponse {
    @ApiProperty({ example: [{ price: '10000000', volume: '23450000' }] })
    public buy?: IOrderBookSide[];

    @ApiProperty({ example: [{ price: '20000000', volume: '34560000' }] })
    public sell?: IOrderBookSide[];
}
