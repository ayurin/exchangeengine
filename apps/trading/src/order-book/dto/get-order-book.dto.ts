import { SYMBOL_DELIMITER } from '@app/utils';
import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import { Transform } from 'class-transformer';
import { Contains, IsBoolean, IsNotEmpty, IsNumberString, IsOptional, IsString } from 'class-validator';

export class GetOrderBookDto {
    @ApiProperty({ example: 'BTC-USDC' })
    @IsString()
    @Contains(SYMBOL_DELIMITER)
    @IsNotEmpty()
    public readonly symbol!: string;

    @ApiPropertyOptional({ type: 'number', example: '1', default: '0.01' })
    @IsOptional()
    @IsNumberString()
    public readonly step?: string;

    @ApiPropertyOptional({ type: 'integer', example: 10, default: 100 })
    @IsOptional()
    @IsNumberString()
    public readonly limit?: number;

    @ApiPropertyOptional({ type: 'boolean', example: false, default: false })
    @IsOptional()
    @Transform(({ value }) => JSON.parse(value as string) as boolean)
    @IsBoolean()
    public readonly showZeroVolume?: boolean;
}
