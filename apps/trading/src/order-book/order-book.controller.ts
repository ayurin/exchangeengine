import { Controller, Get, HttpStatus, Query } from '@nestjs/common';
import { ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';

import { GetOrderBookDto } from '@trading/order-book/dto/get-order-book.dto';
import { OrderBookResponse } from '@trading/order-book/dto/order-book.response';

import { OrderBookService } from './order-book.service';

@ApiTags('Order book')
@Controller('order-book')
export class OrderBookController {
    constructor(private readonly orderBookService: OrderBookService) {}

    @Get()
    @ApiOperation({ summary: 'Order book depth' })
    @ApiResponse({ status: HttpStatus.OK, type: OrderBookResponse })
    public getOrderBook(@Query() query: GetOrderBookDto): Promise<OrderBookResponse | null> {
        return this.orderBookService.getOrderBook(query.symbol, query.step, query.limit, query.showZeroVolume);
    }
}
