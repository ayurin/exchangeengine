import { ApiAuth, JwtPayload, TokenAudience, TUserJwtPayload } from '@app/jwt';
import {
    IGetPriceChangeForAssets,
    IMatcherTrade,
    IPriceChangeForAsset,
    IPriceForAsset,
    PaginateApiQuery,
    TRADING_GET_PRICE_CHANGE_FOR_ASSETS,
    TRADING_GET_PRICE_FOR_ASSETS,
    TRADING_NEW_TRADE_PATTERN,
} from '@app/utils';
import { Controller, Get, HttpStatus, Query } from '@nestjs/common';
import { Ctx, MessagePattern, Payload, RmqContext } from '@nestjs/microservices';
import { ApiOperation, ApiQuery, ApiResponse, ApiTags } from '@nestjs/swagger';
import { Channel, Message } from 'amqplib';
import { Paginate, PaginateQuery } from 'nestjs-paginate';

import { PaginatedMyTradesResponse, PaginatedTradesResponse } from '@trading/trades/dto';
import { GetTradesDto } from '@trading/trades/dto/get-trades.dto';

import { TradesService } from './trades.service';
import { RmqWraper } from '@app/utils';

@ApiTags('Trades')
@Controller('trades')
export class TradesController {
    constructor(private readonly tradingService: TradesService) {}

    @Get()
    @ApiOperation({ summary: 'Trade history' })
    @ApiResponse({ status: HttpStatus.OK, type: PaginatedTradesResponse })
    public async getTrades(
        @Paginate() paginateQuery: PaginateQuery,
        @Query() query?: GetTradesDto,
    ): Promise<PaginatedTradesResponse> {
        return this.tradingService.getTrades(paginateQuery, query);
    }

    @Get('my')
    @ApiAuth(TokenAudience.ACCOUNT)
    @ApiOperation({ summary: 'My trades' })
    @ApiQuery({ type: PaginateApiQuery })
    @ApiResponse({ status: HttpStatus.OK, type: PaginatedMyTradesResponse })
    public getMyTrades(
        @Paginate() query: PaginateQuery,
        @JwtPayload() { id }: TUserJwtPayload,
    ): Promise<PaginatedMyTradesResponse> {
        return this.tradingService.getMyTrades(query, id as string);
    }

    @MessagePattern(TRADING_NEW_TRADE_PATTERN)
    @RmqWraper
    public async handleNewTrade(@Payload() data: IMatcherTrade, @Ctx() context: RmqContext): Promise<void> {
        try{
            await this.tradingService.handleNewTrade(data);
        }catch(e){
            console.log(e)
        }
    }

    @MessagePattern(TRADING_GET_PRICE_CHANGE_FOR_ASSETS)
    public async getPriceChangeForAssets(
        @Payload() data: IGetPriceChangeForAssets,
        @Ctx() context: RmqContext,
    ): Promise<IPriceChangeForAsset[]> {
        const channel = context.getChannelRef() as Channel;
        const originalMsg = context.getMessage() as Message;
        channel.ack(originalMsg);
        return this.tradingService.getPriceChangeForAssets(data?.assets, data.fiat);
    }

    @MessagePattern(TRADING_GET_PRICE_FOR_ASSETS)
    public async getPriceForAssets(
        @Payload() data: IGetPriceChangeForAssets,
        @Ctx() context: RmqContext,
    ): Promise<IPriceForAsset[]> {
        const channel = context.getChannelRef() as Channel;
        const originalMsg = context.getMessage() as Message;
        channel.ack(originalMsg);
        return this.tradingService.getPriceForAssets(data?.assets, data.fiat);
    }
}
