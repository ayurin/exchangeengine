import { PaginateApiQuery } from '@app/utils';
import { ApiPropertyOptional } from '@nestjs/swagger';
import { IsOptional, IsString } from 'class-validator';

export class GetTradesDto extends PaginateApiQuery {
    @ApiPropertyOptional()
    @IsString()
    @IsOptional()
    public pair?: string;
}
