import { transformToString, transformToTimestamp } from '@app/utils';
import { ApiProperty } from '@nestjs/swagger';

import { TradesEntity } from '@trading/dao';

export class TradesResponse {
    @ApiProperty({ example: 'b080a30f-a51f-4b9a-94d7-d3c4c16a711b' })
    public readonly id: string;

    @ApiProperty({ example: 'BTC-USDC' })
    public readonly pair: string;

    @ApiProperty({ example: '400000100' })
    public readonly price: string;

    @ApiProperty({ example: '1200000000' })
    public readonly quantity: string;

    @ApiProperty({ example: '48000012' })
    public readonly quoteQuantity: string;

    @ApiProperty({
        description: 'Trade executed timestamp',
        example: 1499865549590,
    })
    public readonly asOfDate: number;

    @ApiProperty({ example: true })
    public readonly takerBuyFlg: boolean;

    @ApiProperty({ example: true })
    public readonly isBestMatch: boolean;

    constructor(trade: TradesEntity) {
        this.id = trade.id;
        this.pair = trade.pair;
        this.price = transformToString(trade.price);
        this.quantity = transformToString(trade.quantity);
        this.quoteQuantity = transformToString(trade.quoteQuantity);
        this.pair = trade.pair;
        this.asOfDate = transformToTimestamp(trade.asOfDate);
        this.takerBuyFlg = trade.takerBuyFlg;
        this.isBestMatch = trade.isBestMatch;
    }
}
