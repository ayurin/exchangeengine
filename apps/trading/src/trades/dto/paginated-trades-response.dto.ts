import { PaginatedApiResponse } from '@app/utils';
import { ApiProperty } from '@nestjs/swagger';

import { TradesResponse } from './trades-response.dto';

export class PaginatedTradesResponse extends PaginatedApiResponse<TradesResponse> {
    @ApiProperty({ type: TradesResponse, isArray: true })
    public readonly data!: TradesResponse[];
}
