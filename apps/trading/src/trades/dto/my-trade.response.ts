import { transformToString, transformToTimestamp } from '@app/utils';
import { ApiProperty } from '@nestjs/swagger';
import { Exclude, Expose } from 'class-transformer';

import { TradesEntity } from '@trading/dao';

@Exclude()
export class MyTradeResponse {
    @Expose()
    @ApiProperty({ example: '100000000' })
    public quantity: string;

    @Expose()
    @ApiProperty({ example: '1660681374436' })
    public asOfDate: number;

    @Expose()
    @ApiProperty({ example: 'ETH-BTC' })
    public pair: string;

    constructor(partial: TradesEntity) {
        this.quantity = transformToString(partial.quantity);
        this.asOfDate = transformToTimestamp(partial.asOfDate);
        this.pair = partial.pair;
    }
}
