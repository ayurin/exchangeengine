import { PaginatedApiResponse } from '@app/utils';
import { ApiProperty } from '@nestjs/swagger';

import { MyTradeResponse } from './my-trade.response';

export class PaginatedMyTradesResponse extends PaginatedApiResponse<MyTradeResponse> {
    @ApiProperty({ type: MyTradeResponse, isArray: true })
    public readonly data!: MyTradeResponse[];
}
