import { PaginatedApiResponse } from '@app/utils';
import { Test } from '@nestjs/testing';
import { SelectQueryBuilder } from 'typeorm';

import { TradesEntity, TradesRepository } from '@trading/dao';
import { MyTradeResponse, PaginatedTradesResponse, TradesResponse } from '@trading/trades/dto';

import { TradesService } from './trades.service';

const trade: TradesEntity = {
    id: '4add4e07-8b8f-4a99-bf13-b347d333d1a2',
    buyOrderId: '10464af4-7210-4bbb-92e6-2fa7ff7bf915',
    sellOrderId: '1e8a37a9-2493-49ed-8486-08baf93fef28',
    price: BigInt(29470000000n),
    quantity: BigInt(7100000n),
    quoteQuantity: BigInt(2092370000n),
    asOfDate: new Date(1661235857045),
    takerBuyFlg: true,
    isBestMatch: true,
    pair: 'BTC-USDT',
    buyerFee: 0n,
    sellerFee: 0n,
    buyerRefunds: 0n,
    buyerFeeRefunds: 0n,
};

const trades = [trade];
const tradeManyAndCount = [trades, trades.length];
const createQueryBuilder = jest.fn(() => ({
    take: jest.fn().mockReturnThis(),
    skip: jest.fn().mockReturnThis(),
    addOrderBy: jest.fn().mockReturnThis(),
    getManyAndCount: jest.fn(() => tradeManyAndCount),
}));
describe('trades service', () => {
    let service: TradesService;
    let tradesRepository: TradesRepository;

    const paginatedQuery = {
        path: 'string',
    };

    beforeAll(async () => {
        const app = await Test.createTestingModule({
            providers: [
                TradesService,
                {
                    provide: TradesRepository,
                    useFactory: () => ({
                        take: jest.fn(() => ({
                            skip: jest.fn().mockReturnThis(),
                            addOrderBy: jest.fn().mockReturnThis(),
                            getManyAndCount: jest.fn(() => tradeManyAndCount),
                        })),
                        createQueryBuilder: jest.fn(() => ({
                            take: jest.fn().mockReturnThis(),
                            skip: jest.fn().mockReturnThis(),
                            addOrderBy: jest.fn().mockReturnThis(),
                            getManyAndCount: jest.fn(() => tradeManyAndCount),
                        })),
                        getMyTradesQueryBuilder: jest.fn(),
                        find: jest.fn(),
                    }),
                },
            ],
        }).compile();

        service = app.get<TradesService>(TradesService);
        tradesRepository = app.get<TradesRepository>(TradesRepository);
    });

    describe('getTrades()', () => {
        describe('return 1 trade', () => {
            let result: PaginatedTradesResponse;
            let findMock: jest.SpyInstance;

            const pair = 'BTC-USDT';
            const limit = 500;

            const expected: TradesResponse = {
                id: '4add4e07-8b8f-4a99-bf13-b347d333d1a2',
                pair: 'BTC-USDT',
                price: '29470000000',
                quantity: '7100000',
                quoteQuantity: '2092370000',
                asOfDate: 1661235857045,
                takerBuyFlg: true,
                isBestMatch: true,
            };

            beforeEach(async () => {
                findMock = jest.spyOn(tradesRepository, 'find').mockResolvedValue([trade]);
                result = await service.getTrades({ limit, searchBy: ['pair'], search: pair, path: '' });
            });

            it('should call find 1 time with params', () => {
                expect(findMock).toHaveBeenCalledTimes(1);
            });

            it('should check contain equal between actual and expected', () => {
                expect(result).toContainEqual(expected);
            });
        });
    });

    describe('getMyTrades()', () => {
        describe('returns user trades', () => {
            let result: PaginatedApiResponse<MyTradeResponse>;
            let getTradesMock: jest.SpyInstance;
            let paginateMock: jest.SpyInstance;

            const user = 'user';

            const expected = [new MyTradeResponse(trade)];

            beforeEach(async () => {
                getTradesMock = jest
                    .spyOn(tradesRepository, 'getMyTradesQueryBuilder')
                    .mockReturnValue(createQueryBuilder as unknown as SelectQueryBuilder<TradesEntity>);

                result = await service.getMyTrades(paginatedQuery, user);
            });

            it('should call getMyTradesQueryBuilder 1 time with params', () => {
                expect(getTradesMock).toHaveBeenCalledTimes(1);
                expect(getTradesMock).toHaveBeenCalledWith(user);
            });

            it('should call paginate 1 time with params', () => {
                expect(paginateMock).toHaveBeenCalledTimes(1);
                expect(paginateMock).toHaveBeenCalledWith(paginatedQuery, createQueryBuilder, {
                    sortableColumns: ['asOfDate', 'pair', 'quantity'],
                    nullSort: 'last',
                    defaultSortBy: [['asOfDate', 'DESC']],
                });
            });

            it('should check equal', () => {
                expect(result.data).toStrictEqual(expected);
            });
        });
    });
});
