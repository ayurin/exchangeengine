/* eslint-disable prettier/prettier, no-nested-ternary */
import { randomUUID } from 'crypto';

import {
    ACCOUNT_SETTLE_ORDER_PATTERN,
    BFFWS_TICKER_UPDATES_PATTERN,
    IIncompleteSettleOrder,
    IMatchedTradeBranch,
    IMatcherTrade,
    IPriceChangeForAsset,
    IPriceForAsset,
    ISettleOrderSide,
    multiplyAmounts,
    RabbitService,
    RmqClientService,
    ZERO_BIGINT,
} from '@app/utils';
import { IBalanceResponse, ITickerUpdates } from '@app/ws';
import { Injectable } from '@nestjs/common';
import { paginate, PaginateQuery } from 'nestjs-paginate';

import { TradesEntity, TradesRepository } from '@trading/dao';
import { OrderService } from '@trading/order/order.service';
import { MyTradeResponse, PaginatedMyTradesResponse, PaginatedTradesResponse, TradesResponse } from '@trading/trades/dto';
import { GetTradesDto } from '@trading/trades/dto/get-trades.dto';

export const MIN_TRADE_LIMIT = 1;
export const DEFAULT_TRADE_LIMIT = 500;
export const MAX_TRADE_LIMIT = 1000;

@Injectable()
export class TradesService {
    public static splitTradeSides(
        trade: IMatcherTrade,
    ): [buyOrder: IMatchedTradeBranch, sellOrder: IMatchedTradeBranch] {
        const buyOrder = trade.taker.buyFlg ? trade.taker : trade.maker;
        const sellOrder = trade.taker.buyFlg ? trade.maker : trade.taker;
        return [buyOrder, sellOrder];
    }

    constructor(
        private tradesRepository: TradesRepository,
        private orderService: OrderService,
        private rmqClientService: RmqClientService,
        private rabbitService: RabbitService,
    ) {}

    public async getPriceChangeForAssets(assets: string[], fiat: string): Promise<IPriceChangeForAsset[]> {
        return this.tradesRepository.getPriceChangeForAssets(assets, fiat);
    }

    public async getPriceForAssets(assets: string[], fiat: string): Promise<IPriceForAsset[]> {
        return this.tradesRepository.getPriceForAssets(assets, fiat);
    }

    public async getTrades(paginateQuery: PaginateQuery, query?: GetTradesDto): Promise<PaginatedTradesResponse> {
        const qb = this.tradesRepository.getTradesQueryBuilder(query);
        return paginate(paginateQuery, qb, {
            select: ['id', 'pair', 'price', 'quantity', 'quoteQuantity', 'asOfDate', 'takerBuyFlg', 'isBestMatch'],
            sortableColumns: [
                'id',
                'pair',
                'price',
                'quantity',
                'quoteQuantity',
                'asOfDate',
                'takerBuyFlg',
                'isBestMatch',
            ],
            nullSort: 'last',
            searchableColumns: [
                'id',
                'pair',
                'price',
                'quantity',
                'quoteQuantity',
                'asOfDate',
                'takerBuyFlg',
                'isBestMatch',
            ],
            defaultSortBy: [['asOfDate', 'DESC']],
        }).then(({ data, links, meta }) => ({
            links,
            meta,
            data: data.map((trade) => new TradesResponse(trade)),
        }));
    }

    public async getMyTrades(paginateQuery: PaginateQuery, customerId: string): Promise<PaginatedMyTradesResponse> {
        const queryBuilder = this.tradesRepository.getMyTradesQueryBuilder(customerId);
        const { data, links, meta } = await paginate(paginateQuery, queryBuilder, {
            sortableColumns: ['asOfDate', 'pair', 'quantity'],
            nullSort: 'last',
            defaultSortBy: [['asOfDate', 'DESC']],
        });

        return { links, meta, data: data.map((item) => new MyTradeResponse(item)) };
    }

    public async handleNewTrade(trade: IMatcherTrade): Promise<void> {
        // const settleData = await this.prepareSettleOrderData(trade);
        // const newTradeTemplate = this.createNewTradeTemplate(trade, settleData);
        // await this.settleOrder(settleData, newTradeTemplate.id);
        // await this.tradesRepository.save(newTradeTemplate);
        

        await this.sendUpdatesToWS(trade);
    }

    private async sendUpdatesToWS(trade: IMatcherTrade): Promise<void> {
        await Promise.all([
            this.orderService.sendOrderUpdatesToBffws(trade.taker.customerId, trade.taker.pair),
            this.orderService.sendOrderUpdatesToBffws(trade.maker.customerId, trade.maker.pair),
            this.orderService.checkOrderCompletedAndSendNotification(
                [trade.taker.id, trade.maker.id],
                trade.matchingTime,
            ),
        ]);
        const priceChange = await this.tradesRepository.getPriceChangeForPair(trade.maker.pair);
        if (priceChange) {
            await this.sendTickerUpdatesToBffws(priceChange);
        }
    }

    private async sendTickerUpdatesToBffws(priceChange: ITickerUpdates): Promise<void> {
        const message = this.rabbitService.buildMessage(priceChange);
        await this.rmqClientService.emit(BFFWS_TICKER_UPDATES_PATTERN, message);
    }

    private createNewTradeTemplate(trade: IMatcherTrade, settleData: IIncompleteSettleOrder): TradesEntity {
        const quantity = BigInt(trade.volume);
        const price = BigInt(trade.taker.price || ZERO_BIGINT);

        const [buyOrder, sellOrder] = TradesService.splitTradeSides(trade);

        return this.tradesRepository.create({
            id: randomUUID(),
            buyOrderId: buyOrder.id,
            sellOrderId: sellOrder.id,
            quantity,
            price,
            quoteQuantity: BigInt(multiplyAmounts(quantity, price)),
            pair: buyOrder.pair,
            takerBuyFlg: trade.taker.buyFlg,
            isBestMatch: false, // хрен его знает как определить
            asOfDate: new Date(trade.matchingTime), // TODO: разобраться с таймзонами
            buyerFee: BigInt(settleData.buyOrder.forFee.amount),
            sellerFee: BigInt(settleData.sellOrder.forFee.amount),
            buyerRefunds: BigInt(settleData.buyOrder.forRefunds?.amount || 0),
            buyerFeeRefunds: BigInt(settleData.buyOrder.forFeeRefunds?.amount || 0),
        });
    }

    private async prepareSettleOrderData(trade: IMatcherTrade): Promise<IIncompleteSettleOrder> {
        const [buyOrderReport, sellOrderReport] = TradesService.splitTradeSides(trade);

        const [buyOrderEntity, sellOrderEntity] = await Promise.all([
            this.orderService.getOrderOrFail(buyOrderReport.id),
            this.orderService.getOrderOrFail(sellOrderReport.id),
        ]);

        const exchangeAmount = trade.taker.price
            ? multiplyAmounts(trade.volume, trade.taker.price)
            : ZERO_BIGINT.toString();
        const buyerAmount = buyOrderEntity.price
            ? multiplyAmounts(trade.volume, BigInt(buyOrderEntity.price))
            : ZERO_BIGINT;

        const buyerFee = multiplyAmounts(
            exchangeAmount,
            trade.taker.buyFlg ? buyOrderEntity.takerFeePercent : buyOrderEntity.makerFeePercent,
        );

        const buyerOrderFee = multiplyAmounts(buyerAmount, buyOrderEntity.takerFeePercent);
        const buyerFeeRefund = String(BigInt(buyerOrderFee) - BigInt(buyerFee));

        const buyOrder: ISettleOrderSide = {
            orderId: buyOrderEntity.id,
            customerId: buyOrderEntity.customerId,
            forExchange: {
                // сколько и чего спишется у customerId
                amount: exchangeAmount,
                currency: buyOrderReport.quote,
            },
            forFee: {
                amount: buyerFee,
                currency: buyOrderReport.quote, // в первой версии комиссия всегда в quote
            },
            forRefunds: {
                amount: trade.taker.buyFlg
                    ? ZERO_BIGINT.toString()
                    : trade.maker.price
                    ? String(BigInt(multiplyAmounts(trade.volume, trade.maker.price)) - BigInt(exchangeAmount))
                    : ZERO_BIGINT.toString(),
                currency: buyOrderReport.quote,
            },
            forFeeRefunds: {
                amount: buyerFeeRefund,
                currency: buyOrderReport.quote, // в первой версии комиссия всегда в quote
            },
        };

        const sellerFee = multiplyAmounts(
            exchangeAmount,
            trade.taker.buyFlg ? sellOrderEntity.makerFeePercent : sellOrderEntity.takerFeePercent,
        );

        const sellOrder: ISettleOrderSide = {
            orderId: sellOrderEntity.id,
            customerId: sellOrderEntity.customerId,
            forExchange: {
                // сколько и чего спишется у customerId
                amount: trade.volume,
                currency: sellOrderReport.base,
            },
            forFee: {
                amount: sellerFee,
                currency: sellOrderReport.quote, // в первой версии комиссия всегда в quote
            },
            // Селлер не возвращает комиссию - она списывается с прибыли
            // Селлеру нечего возвращать из средств
        };

        return {
            buyOrder,
            sellOrder,
        };
    }

    private settleOrder(settleData: IIncompleteSettleOrder, tradeId: string): Promise<IBalanceResponse[]> {
        const message = this.rabbitService.buildMessage({
            ...settleData,
            tradeId,
        });

        return this.rmqClientService.send<IBalanceResponse[], typeof ACCOUNT_SETTLE_ORDER_PATTERN>(
            ACCOUNT_SETTLE_ORDER_PATTERN,
            message,
        );
    }
}
