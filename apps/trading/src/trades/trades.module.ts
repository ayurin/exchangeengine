import { Module } from '@nestjs/common';

import { FeeRepository, OrderRepository, TradesRepository } from '@trading/dao';
import { OrderModule } from '@trading/order/order.module';

import { TradesController } from './trades.controller';
import { TradesService } from './trades.service';

@Module({
    imports: [OrderModule],
    controllers: [TradesController],
    providers: [TradesService, TradesRepository, OrderRepository, FeeRepository],
})
export class TradesModule {}
