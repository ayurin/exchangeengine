// ENVIRONMENT
const ENVIRONMENT = process.env.ENVIRONMENT;

// DATABASE
const DB_HOST = process.env.DB_HOST;
if (!DB_HOST) {
    throw new Error('the DB_HOST environment variable is not defined');
}

const dbPort = process.env.DB_PORT;
if (!dbPort) {
    throw new Error('the DB_PORT environment variable is not defined');
}
const DB_PORT = Number(dbPort);
if (!DB_PORT || DB_PORT < 1000 || DB_PORT % 1 !== 0) {
    throw new Error('the DB_PORT environment variable is not correct');
}

const DB_USER = process.env.DB_USER;
if (!DB_USER) {
    throw new Error('the DB_USER environment variable is not defined');
}

const DB_PASSWORD = process.env.DB_PASSWORD;
if (!DB_PASSWORD) {
    throw new Error('the DB_PASSWORD environment variable is not defined');
}

const DB_NAME = process.env.DB_NAME;
if (!DB_NAME) {
    throw new Error('the DB_NAME environment variable is not defined');
}

const DB_ENABLE_LOGGING = process.env.DB_ENABLE_LOGGING;
const SKIP_MIGRATIONS = process.env.SKIP_MIGRATIONS;
const DB_CONNECTIONS_POOL_SIZE = process.env.DB_CONNECTIONS_POOL_SIZE;
const DB_CONNECTIONS_IDLE_TIMEOUT = process.env.DB_CONNECTIONS_IDLE_TIMEOUT;

export const dbConfig = Object.freeze({
    DB_HOST,
    DB_PORT,
    DB_USER,
    DB_PASSWORD,
    DB_NAME,
    DB_ENABLE_LOGGING,
    SKIP_MIGRATIONS,
    DB_CONNECTIONS_POOL_SIZE,
    DB_CONNECTIONS_IDLE_TIMEOUT,

    ENVIRONMENT,
});
