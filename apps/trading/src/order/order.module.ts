import { Module } from '@nestjs/common';

import { CurrencyPairRepository, FeeRepository, OrderRepository } from '@trading/dao';

import { OrderController } from './order.controller';
import { OrderService } from './order.service';

@Module({
    controllers: [OrderController],
    providers: [OrderService, OrderRepository, FeeRepository, CurrencyPairRepository],
    exports: [OrderService],
})
export class OrderModule {}
