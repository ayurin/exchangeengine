import {
    EOrderSide,
    EOrderType,
    EValidityPeriod,
    transformToString,
    transformToTimestamp,
} from '@app/utils';
import { transformToStringWithNull } from '@app/utils/dist/common';
import { ApiProperty } from '@nestjs/swagger';
import { Exclude, Expose, Transform } from 'class-transformer';

export type OrderStatus = 'canceled' | 'canceling' | 'open' | 'completed';

@Exclude()
export class OrderResponse {
    @ApiProperty({ example: 'b56329c0-613e-41ff-bf81-f83d08b8f020' })
    @Expose()
    public id: string;

    @Transform(({ value }) => transformToTimestamp(value as Date), { toClassOnly: true })
    @ApiProperty({ example: '1660681374436' })
    @Expose()
    public asOfDate: number;

    @ApiProperty({ example: 'BTC-USDC' })
    @Expose()
    public pair: string;

    @ApiProperty({ enum: EOrderSide, example: EOrderSide.SELL })
    @Expose()
    public side: EOrderSide;

    @ApiProperty({ enum: EOrderType, example: EOrderType.LIMIT })
    @Expose()
    public type: EOrderType;

    @ApiProperty({ example: EValidityPeriod.GTC })
    @Expose()
    public expiry: string;

    @ApiProperty({ example: '800000000000' })
    @Expose()
    @Transform(({ value }) => transformToString(value as bigint), { toClassOnly: true })
    public price: string;

    @ApiProperty({ example: '100000000' })
    @Expose()
    @Transform(({ value }) => transformToString(value as bigint), { toClassOnly: true })
    public quantity: string;

    @ApiProperty({ example: '160000000000' })
    @Expose()
    @Transform(({ value }) => transformToString(value as bigint), { toClassOnly: true })
    public fee: string;

    @ApiProperty({ example: '800000000000' })
    @Expose()
    @Transform(({ value }) => transformToStringWithNull(value as bigint), { toClassOnly: true })
    public realizedProfit: string;

    @ApiProperty({ example: '0' })
    @Expose()
    public progress: string;

    @ApiProperty({ example: 'open' })
    @Expose()
    public status: OrderStatus;
}
