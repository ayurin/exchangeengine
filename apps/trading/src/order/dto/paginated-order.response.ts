import { PaginatedResponse } from '@app/utils';
import { ApiProperty } from '@nestjs/swagger';

import { OrderResponse } from './order.response';

export class PaginatedOrderResponse extends PaginatedResponse<OrderResponse> {
    @ApiProperty({ type: OrderResponse, isArray: true })
    public readonly data!: OrderResponse[];
}
