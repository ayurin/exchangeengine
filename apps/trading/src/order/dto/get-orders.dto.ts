import { PaginationQuery, transformToDate, transformToUppercase } from '@app/utils';
import { ApiPropertyOptional } from '@nestjs/swagger';
import { Transform } from 'class-transformer';
import { IsDate, IsIn, IsOptional, IsString, ValidateIf } from 'class-validator';
import { endOfDay, startOfDay, subMonths, subWeeks } from 'date-fns';

type GetOrdersStatus = 'open' | 'history';
const GET_ORDERS_STATUS: GetOrdersStatus[] = ['open', 'history'];

type GetOrdersPeriod = 'day' | 'week' | 'month' | 'diapason';
const GET_ORDERS_PERIOD: GetOrdersPeriod[] = ['day', 'week', 'month', 'diapason'];

export interface IFromToDate {
    dateFrom: Date;
    dateTo: Date;
}

export function getDatesFromPeriodType(
    period: GetOrdersPeriod | null,
    diapasonDateFrom: Date,
    diapasonDateTo: Date,
): IFromToDate {
    if (period === 'diapason' || !period) {
        return { dateFrom: diapasonDateFrom, dateTo: diapasonDateTo };
    }
    const dateTo = endOfDay(new Date());
    let dateFrom = new Date();
    if (period === 'week') {
        dateFrom = subWeeks(dateTo, 1);
    }
    if (period === 'month') {
        dateFrom = subMonths(dateTo, 1);
    }
    return { dateFrom: startOfDay(dateFrom), dateTo };
}

export class GetOrdersDto extends PaginationQuery {
    @ApiPropertyOptional({ enum: GET_ORDERS_STATUS })
    @IsOptional()
    @IsIn(GET_ORDERS_STATUS)
    public status: GetOrdersStatus;

    @ApiPropertyOptional({ enum: GET_ORDERS_PERIOD })
    @IsOptional()
    @IsIn(GET_ORDERS_PERIOD)
    public period: GetOrdersPeriod;

    @ApiPropertyOptional()
    @Transform((params) => transformToUppercase(params.value))
    @IsOptional()
    @IsString()
    public pair: string;

    @ApiPropertyOptional()
    @Transform((params) => transformToDate(params.value, 'start'))
    @ValidateIf((dto: GetOrdersDto) => dto.period === 'diapason')
    @IsDate()
    public dateFrom: Date;

    @ApiPropertyOptional()
    @Transform((params) => transformToDate(params.value, 'end'))
    @ValidateIf((dto: GetOrdersDto) => dto.period === 'diapason')
    @IsDate()
    public dateTo: Date;
}
