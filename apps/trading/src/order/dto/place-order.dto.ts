import { EOrderSide, EOrderType, EValidityPeriod } from '@app/utils';
import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import { IsEnum, IsNotEmpty, IsNumberString, IsOptional, IsString } from 'class-validator';

export class PlaceOrderDto {
    @ApiProperty({ example: 'BTC-USDC' })
    @IsString()
    @IsNotEmpty()
    public readonly pair!: string;

    @ApiProperty({ enum: EOrderType })
    @IsEnum(EOrderType)
    public type: EOrderType;

    @ApiProperty({ example: 'regular' })
    @IsOptional()
    public readonly subtype?: string;

    @ApiProperty({ enum: EValidityPeriod })
    @IsEnum(EValidityPeriod)
    public validityPeriod!: EValidityPeriod;

    @ApiProperty({ enum: EOrderSide })
    @IsEnum(EOrderSide)
    public side!: EOrderSide;

    @ApiPropertyOptional({ example: '100000000' })
    @IsOptional()
    @IsNumberString()
    // @ValidateIf(o => (o.type === EOrderType.MARKET_QUANTITY||o.type === EOrderType.LIMIT||o.type === EOrderType.STOP_LIMIT||o.type === EOrderType.STOP_LIMIT_PRIORITY)&&!o.quantity)
    public readonly quantity!: string;

    @ApiPropertyOptional({ example: '100000000' })
    @IsOptional()
    @IsNumberString()
    public readonly quantityMarketTotal!: string;

    @ApiPropertyOptional({ example: '800000000000' })
    @IsOptional()
    @IsNumberString()
    public readonly price?: string;

    @ApiPropertyOptional({ example: '800000000000' })
    @IsOptional()
    @IsNumberString()
    public readonly stopLimitPrice?: string;
}
function ValidateIf(arg0: (o: any) => boolean) {
    throw new Error('Function not implemented.');
}

