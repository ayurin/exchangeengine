import { randomUUID } from 'crypto';

import {
    ACCOUNT_CANCEL_ORDER_PATTERN,
    ACCOUNT_PLACE_ORDER_PATTERN,
    ACCOUNT_PLACE_ORDER_STORNO_PATTERN,
    ACCOUNT_GET_BALANCE_PATTERN,
    ACCOUNT_GET_CURRENCY_PATTERN,
    BFFWS_SL_ACTIVATE_PATTERN,
    BFFWS_TICK_PATTERN,
    EOrderSide,
    EOrderType,
    IBffwsTick,
    INotificationData,
    IRmqCancelOrder,
    IRmqCancelReport,
    IRmqOrder,
    ITradingSLActivate,
    MATCHER_CANCEL_ORDER_PATTERN,
    MATCHER_PLACE_ORDER_PATTERN,
    NEW_NOTIFICATION_PATTERN,
    OkResponse,
    RabbitService,
    RmqClientService,
    SYMBOL_DELIMITER,
    toOkResponse,
    transformToBigint,
    transformToBigintWithNull,
    ZERO_BIGINT,
} from '@app/utils';
import { IBalanceResponse, IBffWsSlActivation, ITick, NotificationType } from '@app/ws';
import { BadRequestException, HttpException, Injectable, NotImplementedException } from '@nestjs/common';
import { plainToInstance } from 'class-transformer';
import { InjectPinoLogger, PinoLogger } from 'nestjs-pino';
import { RedisService } from 'nestjs-redis';
import { In } from 'typeorm';

import { CurrencyPairRepository, FeeRepository, IOrderForCancel, OrderEntity, OrderRepository } from '@trading/dao';
import { getRmqOrderFromEntity, IAmounts, IOrderRequestData, ISaveOrderData } from '@trading/order';
import { GetOrdersDto, OrderResponse, PaginatedOrderResponse, PlaceOrderDto } from '@trading/order/dto';

interface IAmountsForMarkets extends IAmounts{
    currencyId: string;
}

@Injectable()
export class OrderService {
    protected redis = this.redisService.getClient();

    constructor(
        private orderRepository: OrderRepository,
        private feeRepository: FeeRepository,
        private rabbitService: RabbitService,
        private rmqClientService: RmqClientService,
        private currencyPairRepository: CurrencyPairRepository,
        private redisService: RedisService,
        @InjectPinoLogger(OrderService.name)
        private logger: PinoLogger,
    ) {}
// ----------------------------------------------------------------------------------------
    public async getMyOrders(filters: GetOrdersDto, customerId: string): Promise<PaginatedOrderResponse> {
        const [data, total] = await this.orderRepository.getMyOrders(customerId, filters);
        return { data: plainToInstance(OrderResponse, data), total };
    }
// ----------------------------------------------------------------------------------------
    public getOrderOrFail(orderId: string): Promise<OrderEntity> {
        return this.orderRepository.findOneByOrFail({ id: orderId });
    }
// ----------------------------------------------------------------------------------------
    public async cancelOrder(id: string, customerId: string): Promise<OkResponse> {
        const orderForCancel = await this.orderRepository.getOrderForCancel(id, customerId);
        if (!orderForCancel) {
            throw new BadRequestException('Order not found');
        }
        if (orderForCancel?.cancelStatus !== 'none') {
            throw new BadRequestException('Order has already been cancelled');
        }
        if (BigInt(orderForCancel.amount) === ZERO_BIGINT) {
            throw new BadRequestException('Order has already been completed');
        }
        await this.orderRepository.update(id, { cancelStatus: 'canceling' });
        await this.sendCancelOrdersToMatcher([{ id: orderForCancel.orderId, pair: orderForCancel.pair }]);
        return toOkResponse();
    }
// ----------------------------------------------------------------------------------------
    public async cancelAllOrders(customerId: string): Promise<OkResponse> {
        const orders = await this.orderRepository.getOrdersForCancel(customerId);
        const ids = orders.map((order) => order.id);
        await this.sendCancelOrdersToMatcher(orders);
        await this.orderRepository.update({ id: In(ids) }, { cancelStatus: 'canceling' });
        return toOkResponse();
    }
// ----------------------------------------------------------------------------------------
    public async cancelOrderEvent({ id }: IRmqCancelReport): Promise<void> {
        const asOfDate = Date.now();
        const order = await this.orderRepository.getOrderForCancel(id);
        if (order?.cancelStatus === 'canceling' && BigInt(order.amount) > ZERO_BIGINT) {
            await this.sendCancelOrderTransaction(order);
            await this.orderRepository.update(id, { cancelStatus: 'canceled' });
            await this.sendOrderUpdatesToBffws(order.customerId, order.pair);
            await this.sendNewNotification({
                userId: order.customerId,
                asOfDate,
                showInHistory: true,
                title: 'Cancel order',
                type: NotificationType.ORDER_CANCELED,
                message:
                    'Your order has been canceled, and the analysts have not come up with a text for the notification. The grass is green, the sky is blue.',
            });
        }
    }
// ----------------------------------------------------------------------------------------
    public async sendOrderUpdatesToBffws(userId: string, pair: string): Promise<void> {
        const data: ITick = { asOfDate: Date.now(), updateOrders: true };
        const message = this.rabbitService.buildMessage<IBffwsTick>({ data, userId, pair });
        await this.rmqClientService.emit(BFFWS_TICK_PATTERN, message);
    }
// ----------------------------------------------------------------------------------------
    public async checkOrderCompletedAndSendNotification(orderIds: string[], asOfDate: number): Promise<void> {
        const ordersCompleted = await this.orderRepository.checkOrdersCompleted(orderIds);
        const promises = ordersCompleted.reduce((acc, order) => {
            if (order.completed)
                acc.push(
                    this.sendNewNotification({
                        userId: order.customerId,
                        asOfDate,
                        showInHistory: true,
                        title: 'Fill order',
                        type: NotificationType.ORDER_COMPLETED,
                        message:
                            'Your order has been filled, and the analysts have not come up with a text for the notification. The grass is green, the sky is blue.',
                    }),
                );
            return acc;
        }, [] as Promise<void>[]);
        await Promise.all(promises);
    }
// ----------------------------------------------------------------------------------------
    public async sendNewNotification(data: INotificationData): Promise<void> {
        const message = this.rabbitService.buildMessage<INotificationData>(data);
        await this.rmqClientService.emit(NEW_NOTIFICATION_PATTERN, message);
    }
// ----------------------------------------------------------------------------------------
    private async sendCancelOrderTransaction(order: IOrderForCancel): Promise<IBalanceResponse[]> {
        const message = this.rabbitService.buildMessage({
            orderId: order.orderId,
            customerId: order.customerId,
            currency: order.currency,
            amount: order.amount,
            fee: order.fee,
            feeCurrency: order.feeCurrency,
        });
        return this.rmqClientService.send(ACCOUNT_CANCEL_ORDER_PATTERN, message);
    }
// ----------------------------------------------------------------------------------------
    private async sendCancelOrdersToMatcher(orders: IRmqCancelOrder[]): Promise<void> {
        await Promise.all(
            orders.map((order) => {
                const message = this.rabbitService.buildMessage({
                    id: order.id,
                    pair: order.pair,
                });
                return this.rmqClientService.emit(MATCHER_CANCEL_ORDER_PATTERN, message);
            }),
        );
    }
// ----------------------------------------------------------------------------------------
    public async placeMarketOrder(dto: PlaceOrderDto, customerId: string): Promise<OrderResponse> {
        const { side, type, pair, quantity, quantityMarketTotal } = dto;
        const baseCurrency = pair.split(SYMBOL_DELIMITER)[0];
        const quoteCurrency = pair.split(SYMBOL_DELIMITER)[1];

        if(type===EOrderType.MARKET_QUANTITY&&transformToBigint(quantity)<=0n){
            throw new Error('quantity must be greather than zero for marketQuantity orders.');
        }
        if(type===EOrderType.MARKET_TOTAL&&transformToBigint(quantityMarketTotal)<=0n){
            throw new Error('quantityMarketTotal must be greather than zero for marketTotal orders.');
        }
        const { makerFee, takerFee } = await this.feeRepository.getFeeOrFail(pair);
        const messageCur = this.rabbitService.buildMessage<string>(quoteCurrency.toUpperCase());
        const currency: { id: string } = await this.rmqClientService.send(ACCOUNT_GET_CURRENCY_PATTERN, messageCur);

        const { lockAmount, feeAmount, currencyId } = await this.calculateAmountToLockForMarketOrders(side, type, takerFee, customerId, currency.id)
        const orderId = randomUUID();

        const requestData: IOrderRequestData = {
            customerId,
            orderId,
            currency: side === EOrderSide.BUY ? quoteCurrency : baseCurrency,
            amount: String(lockAmount),
            feeCurrency: quoteCurrency,
            fee: String(feeAmount),
            asOfDate: (new Date().valueOf()).toString()
        };

        const saveOrderData: ISaveOrderData = {
            orderId,
            customerId,
            side,
            pair,
            ...(type===EOrderType.MARKET_QUANTITY?{quantity}:{quantityMarketTotal}),
            makerFee: String(makerFee),
            takerFee: String(takerFee),
            feeCurrency: quoteCurrency,
            feeAmount: String(feeAmount),
            type,
            asOfDate: (new Date().valueOf()).toString()
        };

        // const rmqOrder = getRmqOrderFromEntity(entity);
        // const message = this.rabbitService.buildMessage<IRmqOrder>(rmqOrder);
        // await this.rmqClientService.emit(MATCHER_PLACE_ORDER_PATTERN, message);

        try {
            const newOrder = await this.orderRepository.save({
                id: orderId,
                side: side,
                ...(type===EOrderType.MARKET_QUANTITY?{quantity: transformToBigint(quantity)}:{quantityMarketTotal: transformToBigint(quantityMarketTotal)}),
                executedQuantity: transformToBigint(0),
                makerFeePercent: transformToBigint(makerFee),
                takerFeePercent: transformToBigint(takerFee),
                feeCurrency: quoteCurrency,
                feeAmount: transformToBigint(feeAmount),
                pair: pair,
                customerId: customerId,
                status: 'open',
                type: type,
            });
            return this.sendOrderToMatcher(newOrder);
        } catch (error) {
            // unlock funds and throw an error
            // await this.unlockFunds(requestData);

            // eslint-disable-next-line @typescript-eslint/no-unsafe-argument,@typescript-eslint/no-unsafe-member-access
            throw new HttpException(error.message, error.status);
        }

        throw new NotImplementedException();
    }
// ----------------------------------------------------------------------------------------
    public async placeLimitOrder(dto: PlaceOrderDto, customerId: string): Promise<OrderResponse> {
        const { side, type, pair, price, quantity, stopLimitPrice } = dto;

        if (type === EOrderType.STOP_LIMIT && stopLimitPrice) {
            if (transformToBigint(stopLimitPrice) <= 0n) {
                throw new Error('stopLimitPrice must be greather than zero for stop-limit orders.');
            }
            const lastPrice = await this.redis.hget('last-price', pair);

            if (lastPrice) {
                if (side === EOrderSide.BUY && transformToBigint(stopLimitPrice) > transformToBigint(lastPrice)) {
                    throw new Error('stopLimitPrice must be greather than lastprice for buy orders.');
                }
                if (side === EOrderSide.SELL && transformToBigint(stopLimitPrice) < transformToBigint(lastPrice)) {
                    throw new Error('stopLimitPrice must be less than lastprice for sell orders.');
                }
            }
        }

        const currencyPair = await this.currencyPairRepository.findOne({ where: { pair } });

        if (price && Number(price) < Number(currencyPair?.priceIncrement)) {
            throw new BadRequestException(
                `Order price can't be less than ${currencyPair?.base}/${currencyPair?.quote} pair price increment`,
            );
        }

        if (currencyPair?.maxOrderAmount && Number(quantity) > Number(currencyPair?.maxOrderAmount)) {
            throw new BadRequestException(
                `Order amount can't be more than ${currencyPair?.base}/${currencyPair?.quote} pair max order amount`,
            );
        }

        if (type === EOrderType.LIMIT || type === EOrderType.STOP_LIMIT) {
            return this.placeLimitOrStopLimitOrder(dto, customerId);
        }

        throw new NotImplementedException();
    }
// ----------------------------------------------------------------------------------------


    private async placeLimitOrStopLimitOrder(dto: PlaceOrderDto, customerId: string): Promise<OrderResponse> {
        const { pair, price, quantity, side, stopLimitPrice, type } = dto;
        const baseCurrency = pair.split(SYMBOL_DELIMITER)[0];
        const quoteCurrency = pair.split(SYMBOL_DELIMITER)[1];
        const { makerFee, takerFee } = await this.feeRepository.getFeeOrFail(pair);

        const { lockAmount, feeAmount } = this.calculateAmountToLockForLimitOrders(side, String(price), String(quantity), takerFee);
        const orderId = randomUUID();

        const requestData: IOrderRequestData = {
            customerId,
            orderId,
            currency: side === EOrderSide.BUY ? quoteCurrency : baseCurrency,
            amount: String(lockAmount),
            feeCurrency: quoteCurrency,
            fee: String(feeAmount),
            asOfDate: (new Date().valueOf()).toString()
        };

        const saveOrderData: ISaveOrderData = {
            orderId,
            customerId,
            side,
            pair,
            price: String(price),
            quantity,
            makerFee: String(makerFee),
            takerFee: String(takerFee),
            feeCurrency: quoteCurrency,
            feeAmount: String(feeAmount),
            stopLimitPrice: String(stopLimitPrice || 0),
            type,
            asOfDate: (new Date().valueOf()).toString()
        };

        try {
            // lock funds in account service
            const message = this.rabbitService.buildMessage<IOrderRequestData>(requestData);
            const res = await this.rmqClientService.send(ACCOUNT_PLACE_ORDER_PATTERN, message);
        } catch(e) {
            const message = this.rabbitService.buildMessage<IOrderRequestData>(requestData);
            const e_res = await this.rmqClientService.send(ACCOUNT_PLACE_ORDER_STORNO_PATTERN, message);
            throw new Error(e)
        }
        return this.saveAndSendOrder(saveOrderData, requestData);
    }
// ----------------------------------------------------------------------------------------
    private async saveAndSendOrder(data: ISaveOrderData, requestData: IOrderRequestData): Promise<OrderResponse> {
        try {
            const newOrder = await this.orderRepository.save({
                id: data.orderId,
                side: data.side,
                quantity: transformToBigint(data.quantity),
                quantityMarketTotal: transformToBigint(data.quantityMarketTotal),
                executedQuantity: transformToBigint(0),
                price: transformToBigintWithNull(data.price),
                stopLimitPrice: transformToBigint(data.stopLimitPrice || 0),
                makerFeePercent: transformToBigint(data.makerFee),
                takerFeePercent: transformToBigint(data.takerFee),
                feeCurrency: data.feeCurrency,
                feeAmount: transformToBigint(data.feeAmount),
                pair: data.pair,
                customerId: data.customerId,
                status: 'open',
                type: data.type
            });
            return this.sendOrderToMatcher(newOrder);
        } catch (error) {
            // unlock funds and throw an error
            await this.unlockFunds(requestData);

            // eslint-disable-next-line @typescript-eslint/no-unsafe-argument,@typescript-eslint/no-unsafe-member-access
            throw new HttpException(error.message, error.status);
        }
    }
// ----------------------------------------------------------------------------------------
    private async sendOrderToMatcher(entity: OrderEntity): Promise<OrderResponse> {
        const rmqOrder = getRmqOrderFromEntity(entity);
        const message = this.rabbitService.buildMessage<IRmqOrder>(rmqOrder);
        await this.rmqClientService.emit(MATCHER_PLACE_ORDER_PATTERN, message);
        return plainToInstance(OrderResponse, { ...entity, fee: entity.feeAmount });
    }
// ----------------------------------------------------------------------------------------
    public async slActivationEvent(data: ITradingSLActivate): Promise<void> {
        const messageToMatcher = this.rabbitService.buildMessage<IRmqOrder>(data.slOrder);
        await this.rmqClientService.emit(MATCHER_PLACE_ORDER_PATTERN, messageToMatcher);
        this.logger.info(`Stop-limit order id=${data.slOrder.id} was activated.`);

        const slOrder = await this.getOrderOrFail(data.slOrder.id);

        const messageToBffws = this.rabbitService.buildMessage<IBffWsSlActivation>({
            slOrderId: data.slOrder.id,
            slOrderOwnerId: slOrder.customerId,
            orderTriggerTakerId: data.orderTriggerTakerId,
            orderTriggerMakerId: data.orderTriggerMakerId,
            activateTime: data.matchingTime,
        });
        await this.rmqClientService.emit(BFFWS_SL_ACTIVATE_PATTERN, messageToBffws);
    }
// ----------------------------------------------------------------------------------------
    private async unlockFunds(requestData: IOrderRequestData): Promise<void> {
        const message = this.rabbitService.buildMessage(requestData);
        await this.rmqClientService.send(ACCOUNT_CANCEL_ORDER_PATTERN, message);
    }
// ----------------------------------------------------------------------------------------
    private calculateAmountToLockForLimitOrders(side: EOrderSide, price: string, quantity: string, takerFee: number): IAmounts {
        if (side === EOrderSide.BUY) {
            const lockAmount = (BigInt(price) * BigInt(quantity)) / BigInt(10 ** 8);
            const feeAmount = (lockAmount * BigInt(takerFee)) / BigInt(10 ** 8);
            return { lockAmount, feeAmount };
        }
        return { lockAmount: BigInt(quantity), feeAmount: BigInt(0) };
    }
// ----------------------------------------------------------------------------------------
    private async calculateAmountToLockForMarketOrders(side: EOrderSide, type: EOrderType, takerFee: number, userId: string, currencyId: string): Promise<IAmountsForMarkets> {
        const message = this.rabbitService.buildMessage<any>({ userId });
        const availableBalanceList: any[] = await this.rmqClientService.send(ACCOUNT_GET_BALANCE_PATTERN, message);
        const balance = availableBalanceList.filter(i => i.currencyId === currencyId)

        // if (side === EOrderSide.BUY) {
        //     const lockAmount = (BigInt(price) * BigInt(quantity)) / BigInt(10 ** 8);
        //     const feeAmount = (lockAmount * BigInt(takerFee)) / BigInt(10 ** 8);
        //     return { lockAmount, feeAmount };
        // }
        return { lockAmount: BigInt(0), feeAmount: BigInt(0), currencyId };
    }
}
