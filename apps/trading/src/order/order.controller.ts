import { ApiAuth, JwtPayload, TokenAudience, TUserJwtPayload } from '@app/jwt';
import {
    IdDto,
    IRmqCancelReport,
    ITradingSLActivate,
    OkResponse,
    TRADING_CANCEL_EVENT_PATTERN,
    TRADING_SL_ACTIVATE_PATTERN,
    EOrderType
} from '@app/utils';
import { Body, Controller, Get, HttpStatus, Patch, Post, Query } from '@nestjs/common';
import { Ctx, MessagePattern, Payload, RmqContext } from '@nestjs/microservices';
import { ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { Channel, Message } from 'amqplib';

import { GetOrdersDto, OrderResponse, PaginatedOrderResponse, PlaceOrderDto } from '@trading/order/dto';
import { OrderService } from '@trading/order/order.service';

@ApiTags('Order')
@Controller('order')
export class OrderController {
    constructor(private readonly orderService: OrderService) {}

    @Get('my')
    @ApiAuth(TokenAudience.ACCOUNT)
    @ApiOperation({ summary: 'User orders' })
    @ApiResponse({ status: HttpStatus.OK, type: PaginatedOrderResponse })
    public getMyOrders(
        @Query() query: GetOrdersDto,
        @JwtPayload() { id }: TUserJwtPayload,
    ): Promise<PaginatedOrderResponse> {
        return this.orderService.getMyOrders(query, id as string);
    }

    @Patch('cancel')
    @ApiAuth(TokenAudience.ACCOUNT)
    @ApiOperation({ summary: 'Cancel order' })
    @ApiResponse({ status: HttpStatus.OK, type: OkResponse })
    public cancelOrder(@Body() dto: IdDto, @JwtPayload() { id }: TUserJwtPayload): Promise<OkResponse> {
        return this.orderService.cancelOrder(dto.id, id as string);
    }

    @Patch('cancel-all')
    @ApiAuth(TokenAudience.ACCOUNT)
    @ApiOperation({ summary: 'Cancel all open orders' })
    @ApiResponse({ status: HttpStatus.OK, type: OkResponse })
    public cancelAllOrders(@JwtPayload() { id }: TUserJwtPayload): Promise<OkResponse> {
        return this.orderService.cancelAllOrders(id as string);
    }

    @Post()
    @ApiAuth(TokenAudience.ACCOUNT)
    @ApiOperation({ summary: 'Place Order' })
    @ApiResponse({ status: HttpStatus.OK, type: OrderResponse })
    public placeOrder(@Body() dto: PlaceOrderDto, @JwtPayload() { id }: TUserJwtPayload): Promise<OrderResponse> {
        if(dto.type===EOrderType.MARKET_QUANTITY||dto.type===EOrderType.MARKET_TOTAL){
            return this.orderService.placeMarketOrder(dto, id as string);
        } else {
            return this.orderService.placeLimitOrder(dto, id as string);
        }
    }

    @MessagePattern(TRADING_CANCEL_EVENT_PATTERN)
    public async cancelOrderEvent(@Payload() data: IRmqCancelReport, @Ctx() context: RmqContext): Promise<void> {
        const channel = context.getChannelRef() as Channel;
        const originalMsg = context.getMessage() as Message;
        try {
            await this.orderService.cancelOrderEvent(data);
            channel.ack(originalMsg);
        } catch {
            channel.reject(originalMsg, true);
        }
    }

    @MessagePattern(TRADING_SL_ACTIVATE_PATTERN)
    public async slActivationEvent(@Payload() data: ITradingSLActivate, @Ctx() context: RmqContext): Promise<void> {
        const channel = context.getChannelRef() as Channel;
        const originalMsg = context.getMessage() as Message;
        try {
            await this.orderService.slActivationEvent(data);
            channel.ack(originalMsg);
        } catch {
            channel.reject(originalMsg, true);
        }
    }
}
