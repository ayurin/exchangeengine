import { provideServiceDefaultMock } from '@haqqex-backend/testing';
import { AxiosService, RabbitService, RmqClientService } from '@app/utils';
import { Test, TestingModule } from '@nestjs/testing';
import { PinoLogger } from 'nestjs-pino';
import { RedisService } from 'nestjs-redis';

import { FeeRepository, OrderRepository } from '@trading/dao';

import { OrderService } from './order.service';

describe('orderService', () => {
    let service: OrderService;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            providers: [
                OrderService,
                provideServiceDefaultMock(RedisService),
                provideServiceDefaultMock(OrderRepository),
                provideServiceDefaultMock(FeeRepository),
                provideServiceDefaultMock(PinoLogger),
                provideServiceDefaultMock(AxiosService),
                provideServiceDefaultMock(RabbitService),
                provideServiceDefaultMock(RmqClientService),
            ],
        }).compile();

        service = module.get<OrderService>(OrderService);
    });

    it('should be defined', () => {
        expect(service).toBeDefined();
    });
});
