import {
    EOrderSide,
    EOrderType,
    extractBase,
    extractQuote,
    IRmqOrder,
    transformToString,
    transformToStringWithNull,
} from '@app/utils';

import { OrderEntity } from '@trading/dao';

export const getRmqOrderFromEntity = (entity: OrderEntity): IRmqOrder => ({
    id: entity.id,
    customerId: entity.customerId,
    pair: entity.pair,
    price: transformToStringWithNull(entity.price),
    quantity: transformToStringWithNull(entity.quantity),
    quantityMarketTotal: transformToStringWithNull(entity.quantityMarketTotal),
    stopLimitPrice: transformToStringWithNull(entity.stopLimitPrice),
    buyFlg: entity.side === EOrderSide.BUY,
    base: extractBase(entity.pair),
    quote: extractQuote(entity.pair),
    time: entity.asOfDate.valueOf(),
    totalQuantity: null,
    orderType: entity.type,
});

export interface IAmounts {
    lockAmount: bigint;
    feeAmount: bigint;
}

export interface IOrderRequestData {
    customerId: string;
    orderId: string;
    currency: string;
    amount: string;
    feeCurrency: string;
    fee: string;
    asOfDate: string;
}

export interface ISaveOrderData {
    orderId: string;
    customerId: string;
    side: EOrderSide;
    pair: string;
    price?: string;
    quantity?: string;
    quantityMarketTotal?:string;
    stopLimitPrice?: string;
    makerFee: string;
    takerFee: string;
    feeCurrency: string;
    feeAmount: string;
    type: EOrderType;
    asOfDate: string;
}
