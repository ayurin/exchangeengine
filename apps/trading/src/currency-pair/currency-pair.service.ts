import {
    AddPairBody,
    calculatePairType,
    CurrencyPairResponse,
    ECurrencyPairStatus,
    GetCurrencyPairDto,
    IdResponse,
    OkResponse,
    PaginatedExtendedCurrencyPairResponse,
    PatchPairBody,
    SYMBOL_DELIMITER,
} from '@app/utils';
import { ArrowType } from '@app/ws';
import { ConflictException, Injectable } from '@nestjs/common';
import Big from 'big.js';
import { paginate, PaginateQuery } from 'nestjs-paginate';
import { In, Not } from 'typeorm';

import {
    CandlesRepository,
    CurrencyPairEntity,
    CurrencyPairRepository,
    FavoritePairRepository,
    OrderRepository,
} from '@trading/dao';

import { PRICE_INCREMENT_MULTIPLIERS } from './constants/currency-pair.constants';
import { CurrencyService } from './currency.service';

const calcArrow = (deltaPct24: string): ArrowType => {
    const deltaPct24BigInt = BigInt(deltaPct24);
    if (deltaPct24BigInt > 0n) return 'up';
    if (deltaPct24BigInt < 0n) return 'down';
    return 'no arrow';
};

@Injectable()
export class CurrencyPairService {
    constructor(
        private readonly currencyPairRepository: CurrencyPairRepository,
        private readonly favoritePairRepository: FavoritePairRepository,
        private readonly orderRepository: OrderRepository,
        private readonly currencyService: CurrencyService,
        private readonly candlesRepository: CandlesRepository,
    ) {}

    public async getCurrencyPairs(
        paginatedQuery: PaginateQuery,
        query: GetCurrencyPairDto,
        customerId: string | null,
    ): Promise<PaginatedExtendedCurrencyPairResponse> {
        const favoritePairs = await this.getFavoritePairs(customerId);
        const qb = this.currencyPairRepository.getCurrenciesPairQueryBuilder(query, customerId, favoritePairs);
        const results = await paginate(paginatedQuery, qb, {
            sortableColumns: ['pair', 'base', 'quote', 'type', 'createdAt'],
            nullSort: 'first',
            searchableColumns: ['id', 'pair', 'base', 'quote', 'type', 'createdAt'],
            defaultSortBy: [['pair', 'ASC']],
        });

        const data = await this.addDataToCurrenciesPair(results.data, favoritePairs, paginatedQuery.sortBy);

        return { ...results, data };
    }

    public async addCurrencyPair(newCurrencyReq: AddPairBody): Promise<IdResponse> {
        if (newCurrencyReq.base === newCurrencyReq.quote)
            throw new ConflictException(`Quote and Base should not be equal`);
        const newPairSymbol = newCurrencyReq.base + SYMBOL_DELIMITER + newCurrencyReq.quote;
        const existingPair = await this.currencyPairRepository.findOneBy([
            { pair: newPairSymbol },
            {
                base: newCurrencyReq.quote,
                quote: newCurrencyReq.base,
            },
        ]);
        if (existingPair) {
            throw new ConflictException(`The ${existingPair.pair} pair already exists`);
        }

        const [baseCurrency, quoteCurrency] = await Promise.all([
            this.currencyService.getCurrencyType(newCurrencyReq.base),
            this.currencyService.getCurrencyType(newCurrencyReq.quote),
        ]);

        const newPair = await this.currencyPairRepository.save({
            pair: newPairSymbol,
            base: newCurrencyReq.base,
            quote: newCurrencyReq.quote,
            type: calculatePairType(baseCurrency, quoteCurrency),
            minOrderAmount: newCurrencyReq.minOrderAmount,
            maxOrderAmount: newCurrencyReq.maxOrderAmount,
            amountIncrement: newCurrencyReq.amountIncrement,
            priceIncrement: newCurrencyReq.priceIncrement,
        });

        return new IdResponse(newPair.id.toString());
    }

    public async deleteCurrencyPairList(id: string[]): Promise<OkResponse> {
        const pairs = await this.currencyPairRepository.findBy({ id: In(id) });
        const ordersCount = await this.orderRepository.getOrdersCountByPairs(pairs.map((item) => item.pair));
        const pairsForException: string[] = [];
        const idsForDelete = pairs.reduce((acc, pair) => {
            if (ordersCount.every((item) => item.pair !== pair.pair)) {
                acc.push(pair.id);
            } else {
                pairsForException.push(pair.pair);
            }
            return acc;
        }, [] as string[]);

        if (idsForDelete.length) await this.currencyPairRepository.delete(idsForDelete);
        const exceptionMessage = pairsForException.length
            ? `Pairs ${pairsForException?.join(', ')} already use in orders'`
            : undefined;
        if (!idsForDelete.length && exceptionMessage) {
            throw new ConflictException(exceptionMessage);
        }
        return new OkResponse(exceptionMessage);
    }

    public async setPairListStatus(ids: string[], status: ECurrencyPairStatus): Promise<OkResponse> {
        const pairs = await this.currencyPairRepository.findBy({ id: In(ids) });
        const pairsAlreadyInStatus: string[] = [];
        const idsForUpdate: string[] = pairs.reduce((acc, pair) => {
            if (pair?.status === status) {
                pairsAlreadyInStatus.push(pair.pair);
            } else {
                acc.push(pair.id);
            }
            return acc;
        }, [] as string[]);
        if (idsForUpdate.length) await this.currencyPairRepository.update({ id: In(idsForUpdate) }, { status });
        const exceptionMessage = pairsAlreadyInStatus.length
            ? `Pairs ${pairsAlreadyInStatus.join(', ')} already in status '${status}'`
            : undefined;
        if (!idsForUpdate.length && exceptionMessage) {
            throw new ConflictException(exceptionMessage);
        }
        return new OkResponse(exceptionMessage);
    }

    public async patchCurrencyPair(
        { base: oldBase, quote: oldQuote, ...data }: PatchPairBody,
        id?: string,
    ): Promise<OkResponse> {
        if (!id) throw new ConflictException('Id should not be empty');
        let partial: Partial<CurrencyPairEntity> = { ...data };
        const pair = await this.currencyPairRepository.findOneByOrFail({ id });

        const [base, quote] = [oldBase || pair.base, oldQuote || pair.quote];
        if (base === quote) throw new ConflictException(`Quote and Base should not be equal`);
        // Проверяем, а поменялась ли у нас сама пара
        if (base !== pair.base || quote !== pair.quote) {
            // Ищем пару с другим id в системе, но с такими же base/quote или зеркальными
            const otherPair = await this.currencyPairRepository.findOneBy([
                { id: Not(id), base, quote },
                {
                    id: Not(id),
                    base: quote,
                    quote: base,
                },
            ]);
            if (otherPair) throw new ConflictException(`Pair ${otherPair.pair} already exists`);
            // Если все ок, считаем новый тип пары
            const baseType = (await this.currencyService.getCurrencyType(base)).type;
            const quoteType = (await this.currencyService.getCurrencyType(quote)).type;
            partial = {
                ...partial,
                base,
                quote,
                pair: base + SYMBOL_DELIMITER + quote,
                type: calculatePairType(baseType, quoteType),
            };
        }
        if (Object.values(partial).some((item) => !!item)) {
            await this.currencyPairRepository.update(id, partial);
        }
        return new OkResponse();
    }

    private async addDataToCurrenciesPair(
        pairEntities: CurrencyPairEntity[],
        favoritePairs: string[],
        sortBy: [string, string][] | undefined,
    ): Promise<CurrencyPairResponse[]> {
        const quotes = await this.candlesRepository.getQuotes();
        const sortedField = sortBy && sortBy[0][0];
        const sortedOrder = sortBy && sortBy[0][1];

        const pairs = pairEntities.map((item) => {
            const deltaPct24 = quotes.find((e) => e.symbol === item.pair)?.change || '0';
            const groupingLevels = PRICE_INCREMENT_MULTIPLIERS.map((multiplier) =>
                Big(multiplier)
                    .times(Big(item.priceIncrement))
                    .div(10 ** 8)
                    .toString(),
            );

            return {
                ...item,
                favorite: favoritePairs.includes(item.pair),
                price: quotes.find((e) => e.symbol === item.pair)?.price || '0',
                deltaPct24,
                groupingLevels,
                arrow: calcArrow(deltaPct24),
            };
        });

        if (sortedField === 'price') {
            return sortedOrder === 'ASC'
                ? pairs.sort((a, b) => Number(a.price) - Number(b.price))
                : pairs.sort((a, b) => Number(b.price) - Number(a.price));
        }

        if (sortedField === 'deltaPct24') {
            return sortedOrder === 'ASC'
                ? pairs.sort((a, b) => Number(a.deltaPct24) - Number(b.deltaPct24))
                : pairs.sort((a, b) => Number(b.deltaPct24) - Number(a.deltaPct24));
        }

        return pairs;
    }

    private async getFavoritePairs(customerId: string | null): Promise<string[]> {
        if (!customerId) {
            return Promise.resolve([] as string[]);
        }
        const where = { customerId, favorite: true };
        const favoritePairs = await this.favoritePairRepository.find({
            where,
            select: ['pair'],
            order: { pair: 'ASC' },
        });
        return favoritePairs.map((item) => item.pair);
    }

    public async setFavoritePair(customerId: string, pair: string): Promise<IdResponse> {
        const userPair = await this.favoritePairRepository.findOneBy({ customerId, pair });
        if (userPair) userPair.favorite = !userPair.favorite;
        const result = await this.favoritePairRepository.save(userPair || { customerId, pair, favorite: true });
        return Promise.resolve(new IdResponse(result.id));
    }
}
