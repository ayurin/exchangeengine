import { ACCOUNT_GET_CURRENCY_PATTERN, RabbitService, RmqClientService } from '@app/utils';
import { BadRequestException, Injectable } from '@nestjs/common';

import { ICurrencyType } from '@trading/currency-pair/dto';

@Injectable()
export class CurrencyService {
    constructor(private rabbitService: RabbitService, private rmqClientService: RmqClientService) {}

    public async getCurrencyType(currencyShortName: string): Promise<ICurrencyType> {
        const message = this.rabbitService.buildMessage(currencyShortName);
        const response: ICurrencyType = await this.rmqClientService.send(ACCOUNT_GET_CURRENCY_PATTERN, message);
        if (!response) {
            throw new BadRequestException(`The ${currencyShortName} is not registered at our system`);
        }

        return response;
    }
}
