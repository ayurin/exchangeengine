import {
    ApiAuth,
    JwtOptionalGuard,
    JwtPayload,
    TokenAudience,
    TUserJwtPayload,
    UseAudiences,
} from '@app/jwt';
import {
    AddPairBody,
    GetCurrencyPairDto,
    IdNumListDto,
    IdResponse,
    OkResponse,
    PaginatedExtendedCurrencyPairResponse,
    PatchPairBody,
    SetPairListStatusBody,
    TRADING_ADD_CURRENCY_PAIR_PATTERN,
    TRADING_DELETE_CURRENCY_PAIR_PATTERN,
    TRADING_GET_CURRENCY_PAIRS_PATTERN,
    TRADING_UPDATE_CURRENCY_PAIR_PATTERN,
    TRADING_UPDATE_CURRENCY_PAIR_STATUS_PATTERN,
} from '@app/utils';
import { IdListDto } from '@app/utils/dist/dto';
import { Body, Controller, Delete, Get, HttpStatus, Param, Patch, Post, Query, UseGuards } from '@nestjs/common';
import { Ctx, MessagePattern, Payload, RmqContext } from '@nestjs/microservices';
import { ApiBearerAuth, ApiBody, ApiOperation, ApiParam, ApiResponse, ApiTags } from '@nestjs/swagger';
import { Channel, Message } from 'amqplib';
import { Paginate, PaginateQuery } from 'nestjs-paginate';

import { CurrencyPairService } from '@trading/currency-pair/currency-pair.service';
import { FavoritePairDto } from '@trading/currency-pair/dto';

@ApiTags('Currency pairs')
@Controller('currency-pair')
export class CurrencyPairController {
    constructor(private readonly currencyPairService: CurrencyPairService) {}

    @Get()
    @ApiOperation({ summary: 'Get currency pairs list' })
    @ApiResponse({ status: HttpStatus.OK, type: PaginatedExtendedCurrencyPairResponse })
    @UseGuards(JwtOptionalGuard)
    @UseAudiences(TokenAudience.ACCOUNT)
    @ApiBearerAuth()
    public getCurrencyPairs(
        @Paginate() paginatedQuery: PaginateQuery,
        @Query() query: GetCurrencyPairDto,
        @JwtPayload() payload: TUserJwtPayload,
    ): Promise<PaginatedExtendedCurrencyPairResponse> {
        // eslint-disable-next-line @typescript-eslint/no-unsafe-argument,@typescript-eslint/no-unsafe-member-access
        return this.currencyPairService.getCurrencyPairs(paginatedQuery, query, payload?.id);
    }

    @Post()
    @ApiOperation({ summary: 'Add new currency pair' })
    @ApiBody({ type: AddPairBody })
    @ApiResponse({ status: HttpStatus.OK, type: IdResponse })
    public addCurrencyPair(@Body() body: AddPairBody): Promise<IdResponse> {
        return this.currencyPairService.addCurrencyPair(body);
    }

    @Patch(':id')
    @ApiOperation({ summary: 'Patch currency pair' })
    @ApiBody({ type: PatchPairBody })
    @ApiParam({ name: 'id' })
    @ApiResponse({ status: HttpStatus.OK, type: OkResponse })
    public patchCurrencyPair(@Param('id') id: string, @Body() body: PatchPairBody): Promise<OkResponse> {
        return this.currencyPairService.patchCurrencyPair(body, id);
    }

    @Patch('set-status')
    @ApiOperation({ summary: 'Set currency pair list status' })
    @ApiBody({ type: SetPairListStatusBody })
    @ApiResponse({ status: HttpStatus.OK, type: OkResponse })
    public setPairListStatus(@Body() { status, ids }: SetPairListStatusBody): Promise<OkResponse> {
        return this.currencyPairService.setPairListStatus(ids, status);
    }

    @Patch('favorite')
    @ApiAuth(TokenAudience.ACCOUNT)
    @ApiOperation({ summary: 'Set favorite flag to pair' })
    @ApiResponse({ status: HttpStatus.OK, type: IdResponse })
    public setFavoritePair(
        @Body() { pair }: FavoritePairDto,
        @JwtPayload() { id }: TUserJwtPayload,
    ): Promise<IdResponse> {
        return this.currencyPairService.setFavoritePair(id as string, pair);
    }

    @Delete()
    @ApiOperation({ summary: 'Delete currency pair list' })
    @ApiBody({ type: IdNumListDto })
    @ApiResponse({ status: HttpStatus.OK, type: OkResponse })
    public deleteCurrencyPairList(@Body() { id }: IdListDto): Promise<OkResponse> {
        return this.currencyPairService.deleteCurrencyPairList(id);
    }

    @MessagePattern(TRADING_GET_CURRENCY_PAIRS_PATTERN)
    public async rmqGetCurrencyPairs(
        @Payload() query: GetCurrencyPairDto,
        @Ctx() context: RmqContext,
    ): Promise<PaginatedExtendedCurrencyPairResponse> {
        const channel = context.getChannelRef() as Channel;
        const originalMsg = context.getMessage() as Message;
        channel.ack(originalMsg);

        // TODO: find common desicion for pagination
        // default sorted by created date with descending order
        let sortBy: [string, string][] = [['createdAt', 'DESC']];
        if (query.sortBy) {
            const split = query.sortBy.split(':');
            sortBy = [[split[0], split[1]]];
        }

        let searchBy: string[] | undefined;
        if (query.searchBy) {
            searchBy = [query.searchBy];
        }

        return this.currencyPairService.getCurrencyPairs(
            { ...query, sortBy, searchBy } as unknown as PaginateQuery,
            query,
            null,
        );
    }

    @MessagePattern(TRADING_ADD_CURRENCY_PAIR_PATTERN)
    public async rmqAddCurrencyPair(@Payload() body: AddPairBody, @Ctx() context: RmqContext): Promise<IdResponse> {
        const channel = context.getChannelRef() as Channel;
        const originalMsg = context.getMessage() as Message;
        channel.ack(originalMsg);
        return this.currencyPairService.addCurrencyPair(body);
    }

    @MessagePattern(TRADING_UPDATE_CURRENCY_PAIR_PATTERN)
    public async rmqPatchCurrencyPair(
        @Payload() { id, ...body }: PatchPairBody,
        @Ctx() context: RmqContext,
    ): Promise<OkResponse> {
        const channel = context.getChannelRef() as Channel;
        const originalMsg = context.getMessage() as Message;
        channel.ack(originalMsg);
        return this.currencyPairService.patchCurrencyPair(body, id);
    }

    @MessagePattern(TRADING_UPDATE_CURRENCY_PAIR_STATUS_PATTERN)
    public async rmqSetPairListStatus(
        @Payload() { status, ids }: SetPairListStatusBody,
        @Ctx() context: RmqContext,
    ): Promise<OkResponse> {
        const channel = context.getChannelRef() as Channel;
        const originalMsg = context.getMessage() as Message;
        channel.ack(originalMsg);
        return this.currencyPairService.setPairListStatus(ids, status);
    }

    @MessagePattern(TRADING_DELETE_CURRENCY_PAIR_PATTERN)
    public async rmqDeleteCurrencyPairList(@Payload() id: string[], @Ctx() context: RmqContext): Promise<OkResponse> {
        const channel = context.getChannelRef() as Channel;
        const originalMsg = context.getMessage() as Message;
        channel.ack(originalMsg);
        return this.currencyPairService.deleteCurrencyPairList(id);
    }
}
