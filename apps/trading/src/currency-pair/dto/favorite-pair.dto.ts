import { transformToUppercase } from '@app/utils';
import { ApiPropertyOptional } from '@nestjs/swagger';
import { Transform } from 'class-transformer';
import { IsString } from 'class-validator';

import { IsCurrencyPair } from '@trading/validators';

export class FavoritePairDto {
    @ApiPropertyOptional({ example: 'BTC-USDC' })
    @Transform((params) => transformToUppercase(params.value))
    @IsString()
    @IsCurrencyPair()
    public pair: string;
}
