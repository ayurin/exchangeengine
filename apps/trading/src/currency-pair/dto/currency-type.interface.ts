import { ECurrencyType } from '@app/utils';

export interface ICurrencyType {
    readonly type: ECurrencyType;
}
