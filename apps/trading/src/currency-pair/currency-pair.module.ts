import { Module } from '@nestjs/common';

import { CurrencyPairService } from '@trading/currency-pair/currency-pair.service';
import { CandlesRepository, CurrencyPairRepository, FavoritePairRepository, OrderRepository } from '@trading/dao';
import { CurrencyPairValidatorModule } from '@trading/validators';

import { CurrencyPairController } from './currency-pair.controller';
import { CurrencyService } from './currency.service';

@Module({
    controllers: [CurrencyPairController],
    providers: [
        CurrencyPairService,
        CurrencyService,
        FavoritePairRepository,
        CurrencyPairRepository,
        CandlesRepository,
        OrderRepository,
    ],
    imports: [CurrencyPairValidatorModule],
})
export class CurrencyPairModule {}
