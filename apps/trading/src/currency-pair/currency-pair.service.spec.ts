import { provideRepositoryDefaultMock, provideServiceDefaultMock } from '@haqqex-backend/testing';
import { Test, TestingModule } from '@nestjs/testing';

import { CandlesRepository, CurrencyPairRepository, FavoritePairRepository } from '@trading/dao';

import { CurrencyPairService } from './currency-pair.service';
import { CurrencyService } from './currency.service';

describe('currencyPairService', () => {
    let service: CurrencyPairService;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            providers: [
                CurrencyPairService,
                provideRepositoryDefaultMock(CurrencyPairRepository),
                provideRepositoryDefaultMock(FavoritePairRepository),
                provideServiceDefaultMock(CurrencyService),
                provideRepositoryDefaultMock(CandlesRepository),
            ],
        }).compile();

        service = module.get<CurrencyPairService>(CurrencyPairService);
    });

    it('should be defined', () => {
        expect(service).toBeDefined();
    });
});
