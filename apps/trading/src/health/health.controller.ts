import { ErrorTemplateType } from '@app/exception';
import { Controller, Get, HttpStatus } from '@nestjs/common';
import { ApiExcludeEndpoint, ApiResponse } from '@nestjs/swagger';
import { HealthCheck, HealthCheckResult, HealthCheckService, HttpHealthIndicator } from '@nestjs/terminus';

@Controller('health')
export class HealthController {
    constructor(private readonly health: HealthCheckService, private readonly http: HttpHealthIndicator) {}

    @Get()
    @HealthCheck()
    @ApiExcludeEndpoint()
    public check(): Promise<HealthCheckResult> {
        return this.health.check([() => this.http.pingCheck('basic check trading', 'https://google.com')]);
    }

    @Get('error-type')
    @ApiResponse({ status: HttpStatus.OK, type: ErrorTemplateType })
    public someMethodForErrorType(): ErrorTemplateType {
        return new ErrorTemplateType();
    }
}
