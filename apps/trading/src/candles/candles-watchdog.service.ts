import { Injectable, OnApplicationBootstrap } from '@nestjs/common';
import { PinoLogger } from 'nestjs-pino';
import { EMPTY, interval, lastValueFrom, Subscription } from 'rxjs';
import { catchError, mergeMap } from 'rxjs/operators';

import { CandlesRepository } from '@trading/dao';

const MINUTE = 60 * 1000;

@Injectable()
export class CandlesWatchdogService implements OnApplicationBootstrap {
    constructor(private readonly candlesRepository: CandlesRepository, private readonly logger: PinoLogger) {}

    public onApplicationBootstrap(): Subscription {
        return interval(MINUTE)
            .pipe(
                mergeMap(() => this.run()),
                catchError((error: Error) => {
                    this.logger.error(error.message);
                    return EMPTY;
                }),
            )
            .subscribe();
    }

    private async run(): Promise<void> {
        const response = await this.candlesRepository.query(`SELECT f_refresh_candles(null);`);

        if (Number(response[0].f_refresh_candles) !== 1) {
            this.logger.error(`Can't refresh candles`);
            return lastValueFrom(EMPTY);
        }
    }
}
