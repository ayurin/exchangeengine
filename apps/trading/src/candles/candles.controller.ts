import { Controller, Get, HttpStatus, Query } from '@nestjs/common';
import { ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';

import { CandlesService } from './candles.service';
import { GetHistoryDto } from './dto/get-history.dto';
import { HistoryResponse } from './dto/history-response';

@ApiTags('Candles')
@Controller()
export class CandlesController {
    constructor(private readonly candlesService: CandlesService) {}

    @Get('history')
    @ApiOperation({ summary: 'History' })
    @ApiResponse({ status: HttpStatus.OK, isArray: true, type: HistoryResponse })
    public getHistory(@Query() query: GetHistoryDto): Promise<HistoryResponse[]> {
        return this.candlesService.getHistory(query);
    }
}
