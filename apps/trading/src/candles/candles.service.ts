import { Injectable } from '@nestjs/common';

import { CandlesRepository } from '@trading/dao';

import { GetHistoryDto } from './dto/get-history.dto';
import { HistoryResponse } from './dto/history-response';

@Injectable()
export class CandlesService {
    constructor(private readonly candlesRepository: CandlesRepository) {}

    public async getHistory(dto: GetHistoryDto): Promise<HistoryResponse[]> {
        const candles = await this.candlesRepository.getCandles(
            dto.symbol,
            dto.resolution,
            dto.from,
            dto.to,
            dto.limit,
        );

        return candles.length
            ? candles.map((c) => {
                  return {
                      time: c.time.valueOf(),
                      high: c.highPrice,
                      low: c.lowPrice,
                      open: c.openPrice,
                      close: c.closePrice,
                      volume: c.volume,
                  };
              })
            : [];
    }
}
