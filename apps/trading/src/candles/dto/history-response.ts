import { ApiProperty } from '@nestjs/swagger';

export class HistoryResponse {
    @ApiProperty({ example: 1660215600000 })
    public readonly time: number;

    @ApiProperty({ example: '200000000' })
    public readonly low: string;

    @ApiProperty({ example: '300000000' })
    public readonly high: string;

    @ApiProperty({ example: '298390000' })
    public readonly open: string;

    @ApiProperty({ example: '250000000' })
    public readonly close: string;

    @ApiProperty({ example: '44500000000' })
    public readonly volume: string;
}
