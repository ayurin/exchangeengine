import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import { IsNotEmpty, IsNumberString, IsOptional, IsString } from 'class-validator';

export class GetHistoryDto {
    @ApiProperty({ example: 'BTC-USDC' })
    @IsString()
    @IsNotEmpty()
    public readonly symbol!: string;

    @ApiProperty({ example: '1h' })
    @IsNotEmpty()
    public readonly resolution!: string;

    @ApiProperty({ example: 1664280000 })
    @IsOptional()
    public readonly from?: number;

    @ApiProperty({ example: 1668280000 })
    @IsOptional()
    public readonly to?: number;

    @ApiPropertyOptional({ type: 'integer', example: 10, default: 100 })
    @IsOptional()
    @IsNumberString()
    public readonly limit?: number;
}
