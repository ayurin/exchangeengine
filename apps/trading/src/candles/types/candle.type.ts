export type TCandle = [
    openTime: number, // 1660215600000
    highPrice: string, // '321.70000000'
    lowPrice: string, // '321.20000000'
    openPrice: string, // '321.70000000'
    closePrice: string, // '321.50000000'
    closeTime: number, // 1660219199999
    volume: string, // '417.18000000'
];
