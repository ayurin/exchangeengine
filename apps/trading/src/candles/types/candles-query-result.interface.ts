export interface CandlesQueryResult {
    symbol: string;
    price: string;
    change: string;
}
