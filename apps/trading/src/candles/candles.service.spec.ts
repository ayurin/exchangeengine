import { provideServiceDefaultMock } from '@haqqex-backend/testing';
import { Test } from '@nestjs/testing';

import { CandlesEntity, CandlesRepository } from '@trading/dao';

import { CandlesService } from './candles.service';
import { GetHistoryDto } from './dto/get-history.dto';
import { HistoryResponse } from './dto/history-response';

describe('candles service', () => {
    let service: CandlesService;
    let candlesRepository: CandlesRepository;

    beforeAll(async () => {
        const app = await Test.createTestingModule({
            providers: [CandlesService, provideServiceDefaultMock(CandlesRepository)],
        }).compile();

        service = app.get(CandlesService);
        candlesRepository = app.get(CandlesRepository);
    });

    describe('getHistory()', () => {
        describe('returns 1 candle', () => {
            let getCandlesMock: jest.SpyInstance;
            let result: HistoryResponse[];

            const mockedCandlesData: CandlesEntity = {
                id: '5454d258-d242-46f7-98ca-61f18b65ec57',
                time: new Date(1660215600000),
                highPrice: '32170000000',
                lowPrice: '32120000000',
                openPrice: '32170000000',
                closePrice: '32150000000',
                volume: '41718000000',
                symbol: 'BTC-USDC',
                resolution: '1h',
                createdAt: new Date('2022-08-12 15:51:14.694'),
            };

            const dtoWith1H: GetHistoryDto = {
                symbol: 'BTC-USDC',
                resolution: '1h',
                from: 1660201970,
                to: 1690201970,
                limit: 500,
            };

            const expected: HistoryResponse[] = [
                {
                    time: 1660215600000,
                    high: '32170000000',
                    low: '32120000000',
                    open: '32170000000',
                    close: '32150000000',
                    volume: '41718000000',
                },
            ];

            beforeEach(async () => {
                getCandlesMock = jest.spyOn(candlesRepository, 'getCandles').mockResolvedValue([mockedCandlesData]);
                result = await service.getHistory(dtoWith1H);
            });

            it('should call getCandles 1 time with params', () => {
                expect(getCandlesMock).toHaveBeenCalledTimes(1);
                expect(getCandlesMock).toHaveBeenCalledWith(
                    dtoWith1H.symbol,
                    dtoWith1H.resolution,
                    dtoWith1H.from,
                    dtoWith1H.to,
                    dtoWith1H.limit,
                );
            });

            it('should check strict equal between actual and expected', () => {
                expect(result).toHaveLength(1);
                expect(result).toStrictEqual(expected);
            });
        });

        describe('returns empty array', () => {
            let getCandlesMock: jest.SpyInstance;
            let result: HistoryResponse[];

            const dtoWith1M: GetHistoryDto = {
                symbol: 'BTC-USDC',
                resolution: '1m',
                from: 1660201970,
                to: 1690201970,
                limit: 500,
            };

            beforeEach(async () => {
                getCandlesMock = jest.spyOn(candlesRepository, 'getCandles').mockResolvedValue([]);
                result = await service.getHistory(dtoWith1M);
            });

            it('should call getCandles 1 time with params', () => {
                expect(getCandlesMock).toHaveBeenCalledTimes(1);
                expect(getCandlesMock).toHaveBeenCalledWith(
                    dtoWith1M.symbol,
                    dtoWith1M.resolution,
                    dtoWith1M.from,
                    dtoWith1M.to,
                    dtoWith1M.limit,
                );
            });

            it('should check strict equal between actual and expected', () => {
                expect(result).toStrictEqual([]);
            });
        });
    });
});
