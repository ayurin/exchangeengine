import { Module } from '@nestjs/common';

import { CandlesRepository } from '@trading/dao';

import { CandlesWatchdogService } from './candles-watchdog.service';
import { CandlesController } from './candles.controller';
import { CandlesService } from './candles.service';

@Module({
    controllers: [CandlesController],
    providers: [CandlesService, CandlesRepository, CandlesWatchdogService],
})
export class CandlesModule {}
