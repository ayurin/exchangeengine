import { ExceptionModule } from '@app/exception';
import { RabbitModule, RmqClientModule } from '@app/utils';
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { LoggerModule } from 'nestjs-pino';
import { RedisModule } from 'nestjs-redis';

import { AppController } from '@matcher/app.controller';
import { AppService } from '@matcher/app.service';
import { loggerModuleAsyncParams } from '@matcher/env';
import { HealthModule } from '@matcher/health/health.module';
import { InitModule } from '@matcher/init/init-module';
import { MatcherModule } from '@matcher/matcher/matcher.module';

import { appConfig } from './config';
import { connectionSource } from './ormconfig';

@Module({
    imports: [
        ExceptionModule,
        LoggerModule.forRootAsync(loggerModuleAsyncParams),
        RedisModule.register({ url: appConfig.REDIS_URL }),
        TypeOrmModule.forRoot(connectionSource.options),
        HealthModule,
        InitModule,
        MatcherModule,
        RmqClientModule,
        RabbitModule,
    ],
    controllers: [AppController],
    providers: [AppService],
})
export class AppModule {}
