import { Injectable } from '@nestjs/common';
import { RedisService } from 'nestjs-redis';

@Injectable()
export class AppService {
    protected readonly redis = this.redisService.getClient();

    constructor(private readonly redisService: RedisService) {}

    public async ttt(): Promise<string | null> {
        await this.redis.set('key', 'Redis data!');
        return this.redis.get('key');
    }

    public getHello(): string {
        return 'Hello World!';
    }
}
