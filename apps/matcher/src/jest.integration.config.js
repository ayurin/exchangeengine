module.exports = {
    preset: 'ts-jest',
    testEnvironment: 'node',
    clearMocks: true,
    resetMocks: false,
    testLocationInResults: true,
    testRegex: ['./e2e/'],
    reporters: ['default', 'jest-sonar'],
    moduleNameMapper: {
        '^@matcher/(.*)$': '<rootDir>/$1',
    },
};
