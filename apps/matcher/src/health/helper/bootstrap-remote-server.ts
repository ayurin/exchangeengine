/* eslint-disable import/no-extraneous-dependencies */
import { Server } from 'http';

import fastify, { FastifyRequest, FastifyReply } from 'fastify';
import { getPortPromise } from 'portfinder';

type ThenArg<T> = T extends PromiseLike<infer U> ? U : T;
export type DynamicRemoteServerFn = ThenArg<ReturnType<typeof bootstrapRemoteServer>>;

export async function bootstrapRemoteServer(port?: number) {
    const app = fastify();
    let server: Server;

    if (!port) {
        port = await getPortPromise({
            port: 3000,
            stopPort: 8888,
        });
    }

    function close() {
        server?.close?.();
    }

    function get(endpoint: string, handler: (req: FastifyRequest, res: FastifyReply) => void) {
        app.get(endpoint, handler);

        return {
            start: async () => {
                if (!server) {
                    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
                    // @ts-ignore
                    server = app.listen(port, '0.0.0.0');
                }
            },
        };
    }

    return {
        get,
        close,
        url: `http://0.0.0.0:${port}`,
    };
}
