import { HttpModule } from '@nestjs/axios';
import { Controller, DynamicModule, Get, INestApplication, Type } from '@nestjs/common';
import { FastifyAdapter } from '@nestjs/platform-fastify';
import {
    DiskHealthIndicator,
    HealthCheckResult,
    HealthCheckService,
    HttpHealthIndicator,
    TerminusModule,
    TypeOrmHealthIndicator,
} from '@nestjs/terminus';
import { Test } from '@nestjs/testing';
import { TypeOrmModule } from '@nestjs/typeorm';

type TestingHealthFunc = (props: {
    healthCheck: HealthCheckService;
    http: HttpHealthIndicator;
    disk: DiskHealthIndicator;
    typeorm: TypeOrmHealthIndicator;
}) => Promise<HealthCheckResult>;

function createHealthController(func: TestingHealthFunc) {
    @Controller()
    class HealthController {
        constructor(
            private readonly healthCheck: HealthCheckService,
            private readonly http: HttpHealthIndicator,
            private readonly disk: DiskHealthIndicator,
            private readonly typeorm: TypeOrmHealthIndicator,
        ) {}

        // @ts-ignore
        @Get('health')
        health() {
            return func({
                healthCheck: this.healthCheck,
                http: this.http,
                disk: this.disk,
                typeorm: this.typeorm,
            });
        }
    }

    return HealthController;
}

type PropType<TObj, TProp extends keyof TObj> = TObj[TProp];

export type DynamicHealthEndpointFn = (func: TestingHealthFunc) => {
    start(httpAdapter?: FastifyAdapter): Promise<INestApplication>;
};

export function bootstrapTestingModule() {
    const imports: Array<Type<any> | DynamicModule> = [TerminusModule];

    function setHealthEndpoint(func: TestingHealthFunc) {
        const testingModule = Test.createTestingModule({
            imports,
            controllers: [createHealthController(func)],
        });

        async function start(httpAdapter: FastifyAdapter = new FastifyAdapter()) {
            const moduleRef = await testingModule.compile();

            const app = moduleRef.createNestApplication(httpAdapter);

            await app.init();
            await app.getHttpAdapter().getInstance().ready?.();
            return app;
        }

        return { start };
    }

    function withTypeOrm() {
        imports.push(
            TypeOrmModule.forRoot({
                type: 'mysql',
                host: '0.0.0.0',
                port: 3306,
                username: 'root',
                password: 'root',
                database: 'test',
                keepConnectionAlive: true,
                retryAttempts: 2,
                retryDelay: 1000,
            }),
        );

        return { setHealthEndpoint };
    }

    function withHttp() {
        imports.push(HttpModule);
        return { setHealthEndpoint };
    }

    return {
        withTypeOrm,
        withHttp,
        setHealthEndpoint,
    };
}
