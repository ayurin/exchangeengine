import { Controller, Get } from '@nestjs/common';
import { ApiExcludeController } from '@nestjs/swagger';
import { HealthCheck, HealthCheckResult, HealthCheckService, HttpHealthIndicator } from '@nestjs/terminus';

@Controller('health')
@ApiExcludeController()
export class HealthController {
    constructor(private readonly health: HealthCheckService, private readonly http: HttpHealthIndicator) {}

    @Get()
    @HealthCheck()
    public check(): Promise<HealthCheckResult> {
        return this.health.check([() => this.http.pingCheck('basic check matcher', 'https://docs.nestjs.com')]);
    }
}
