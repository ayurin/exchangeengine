import { NestHybridApplicationOptions, ValidationPipe, VersioningOptions, VersioningType } from '@nestjs/common';
import { DocumentBuilder } from '@nestjs/swagger';

import { appConfig } from './config';
import {LoggerModuleAsyncParams} from "nestjs-pino";
import {ConfigModule, ConfigService} from "@nestjs/config";
import { CORRELATION_ID_HEADER } from '@app/utils';

import {randomUUID} from "crypto";

export const appValidationPipe = new ValidationPipe({
    whitelist: true,
    forbidNonWhitelisted: true,
    forbidUnknownValues: true,
    transform: true,
});

export const appVersioningOptions: VersioningOptions = {
    type: VersioningType.URI,
    defaultVersion: '1',
};

export const API_PREFIX = 'api';

export const swaggerConfig = new DocumentBuilder()
    .setTitle(appConfig.APP_NAME)
    .setDescription(`${appConfig.APP_NAME} API description`)
    .setVersion('1.0')
    .build();

export const hybridMicroserviceOptions: NestHybridApplicationOptions = { inheritAppConfig: true };

export const loggerModuleAsyncParams: LoggerModuleAsyncParams = {
    imports: [ConfigModule],
    useFactory: (configService: ConfigService) => ({
        pinoHttp: {
            genReqId(req, res) {
                let corrId = req.headers[CORRELATION_ID_HEADER];
                if (!corrId) {
                    corrId = randomUUID();
                    req.headers[CORRELATION_ID_HEADER] = corrId;
                }
                res.setHeader(CORRELATION_ID_HEADER, corrId);
                return corrId;
            },
            name: appConfig.APP_NAME,
            level: process.env.LOG_LEVEL || 'info',
            redact: configService.get<string[]>('logger.redacted.fields'),
            transport:
                process.env.NODE_ENV !== 'production'
                    ? {
                        target: 'pino-pretty',

                        options: {
                            colorize: true,
                            translateTime: "yyyy-mm-dd'T'HH:MM:ss.l'Z'",
                            messageFormat: '{correlationId: req.headers.x-correlation-id} [{context}] {msg}',
                            // ignore: 'pid,hostname,context,req,res,responseTime',
                            errorLikeObjectKeys: ['err', 'error'],
                        },
                    }
                    : undefined,
            useLevelLabels: true,
        },
    }),
    inject: [ConfigService],
};
