import { Module, OnApplicationBootstrap } from '@nestjs/common';
import { RedisService } from 'nestjs-redis';

@Module({})
export class InitModule implements OnApplicationBootstrap {
    protected readonly redis = this.redisService.getClient();

    constructor(private readonly redisService: RedisService) {}

    public async onApplicationBootstrap(): Promise<void> {
        console.log(`Initialization...`);
        const bids = await this.redis.zrange('bids', 0, -1);
        console.log(`bidsCount = ${bids.length}`);
        await this.redis.set('bidsCount', bids.length);
        const ascs = await this.redis.zrange('ascs', 0, -1);
        await this.redis.set('ascsCount', ascs.length);
        console.log(`ascsCount = ${ascs.length}`);
        await this.redis.set('orderCount', (ascs.length + bids.length).toString());
        console.log(`orderCount = ${(ascs.length + bids.length).toString()}`);
    }
}
