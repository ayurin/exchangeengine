/* eslint-disable no-await-in-loop, no-constant-condition,@typescript-eslint/naming-convention,camelcase */
import {
    BFFWS_TICK_PATTERN,
    deserializeFromV8Base64,
    EOrderSide,
    EOrderType,
    extractBase,
    extractQuote,
    IBffwsTick,
    IMatcherTrade,
    IRmqCancelOrder,
    ITradingSLActivate,
    promiseDelay,
    RabbitService,
    RmqClientService,
    serializeAnythingToV8Base64,
    TRADING_CANCEL_EVENT_PATTERN,
    TRADING_NEW_TRADE_PATTERN,
    TRADING_SL_ACTIVATE_PATTERN,
    transformToStringWithNull,
    ZERO_BIGINT,
} from '@app/utils';
import { IOrderBook, IOrderBookSide, ITrade } from '@app/ws';
import { Injectable } from '@nestjs/common';
import { InjectPinoLogger, PinoLogger } from 'nestjs-pino';
import { RedisService } from 'nestjs-redis';

// import { stringify } from 'querystring';
import { IProcessedOrder } from './order.pipe';

const DELIMITER = ' - ';

const comparator = (a: string | number, b: string | number): number => a.toString().localeCompare(b.toString(), 'en', { numeric: true });

const generateCancelKey = (data: { id: string; pair: string }): string => `cancel${data.id}${data.pair}`;

interface IOrderBookData {
    side: 'buy' | 'sell';
    operation: 'add' | 'sub';
    price: bigint;
    volume: bigint;
    pair: string;
}

@Injectable()
export class MatcherService {
    protected redis = this.redisService.getClient();

    constructor(
        private redisService: RedisService,
        @InjectPinoLogger(MatcherService.name)
        private logger: PinoLogger,
        private rmqClientService: RmqClientService,
        private rabbitService: RabbitService,
    ) {}

    // --------------------------------------------------------------------------------------------------
    public async poolIsEmpty(takerData: IProcessedOrder, makerRaw: string | null): Promise<boolean> {
        if (!makerRaw) {
            if (takerData.orderType === EOrderType.MARKET_QUANTITY || takerData.orderType === EOrderType.MARKET_TOTAL) {
                return true;
            }

            await this.zaddOrder(takerData);
            await this.sendOrderBookToBffws(
                [
                    {
                        side: takerData.buyFlg ? 'buy' : 'sell',
                        operation: 'add',
                        price: takerData.price,
                        volume: takerData.quantity,
                        pair: takerData.pair,
                    },
                ],
                takerData.pair,
            );
            return true;
        }
        return false;
    }

    // --------------------------------------------------------------------------------------------------
    public async saveSl(takerIn: IProcessedOrder): Promise<boolean> {
        if (
            takerIn.stopLimitPrice != null &&
            takerIn.stopLimitPrice > 0n &&
            (takerIn.orderType === EOrderType.STOP_LIMIT || takerIn.orderType === EOrderType.STOP_LIMIT_PRIORITY)
        ) {
            await this.zaddSLOrder(takerIn);
            return true;
        }
        return false;
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Вопрос.
     * Сейчас метод работает так, что если новый ордер например
     * сразу сматчится с 15 другими из стакана,
     * то мы не получим ack пока матчер не обработает эти 15 существующих ордеров.
     * Это же не то как мы хотим чтобы работало? (ответ: так и хотим)
     * Надо наверно сразу делать ack, а метод tick вызывать без await и вешать на него catch.
     */
    public async tick(takerIn: IProcessedOrder): Promise<void> {
        
        // для ликвидации пустых запросов
        if (!takerIn || Object.keys(takerIn).length === 0) return;

        // добавление в справочник
        const { id, ...other } = takerIn;
        await this.redis.hmset(`orderList${takerIn.pair}`, { [id]: serializeAnythingToV8Base64({ id, ...other }) });

        // если stop-limit, то сохраняем и выходим
        if (await this.saveSl(takerIn)) return;

        // клонирование аргумента
        const takerData: IProcessedOrder = { ...takerIn }; // toFirstOrder(data);

        // обработка тика!!!!!!!!!!!!!
        while (true) {
            // забор мейкера из сортированного массива
            const [makerRaw] = takerData.buyFlg
                ? await this.redis.zpopmin(takerData.pair + EOrderSide.SELL)
                : await this.redis.zpopmax(takerData.pair + EOrderSide.BUY);

            // если мейкеров нет (пустой пул), то добавленяем в пул тейкера для лимитников. Для маркетов ничего не делаем
            if (await this.poolIsEmpty(takerData, makerRaw)) return;

            // десерилизация мейкера
            const [makerCounter, makerDataStr] = makerRaw.split(DELIMITER);
            const makerData = deserializeFromV8Base64<IProcessedOrder>(makerDataStr);

            // если отмена
            const makerCancelKey = generateCancelKey(makerData);
            const takerCancelKey = generateCancelKey(takerData);
            if (await this.redis.get(makerCancelKey)) {
                await this.redis.del(makerCancelKey);
                return;
            }
            if (await this.redis.get(takerCancelKey)) {
                await this.redis.del(takerCancelKey);
                return;
            }

            // tick
            if (
                takerData.pair === makerData.pair &&
                // limit and stop-limit
                (((takerData.orderType === EOrderType.LIMIT ||
                    takerData.orderType === EOrderType.STOP_LIMIT ||
                    takerData.orderType === EOrderType.STOP_LIMIT_PRIORITY) &&
                takerData.buyFlg
                    ? makerData.price <= takerData.price
                    : makerData.price >= takerData.price) ||
                    // markets
                    takerData.orderType === EOrderType.MARKET_QUANTITY ||
                    takerData.orderType === EOrderType.MARKET_TOTAL)
            ) {
                // get last-price
                const rawBeforeClosePrice = await this.redis.hget('last-price', makerData.pair);
                const beforeClosePrice = rawBeforeClosePrice ? BigInt(rawBeforeClosePrice) : 0n;

                this.logger.info(`-- Matcher - Trade --`);
                const makerCompleted =
                    takerData.orderType === EOrderType.MARKET_TOTAL
                        ? takerData.quantityMarketTotal / makerData.price >= makerData.quantity
                        : takerData.quantity >= makerData.quantity;
                const takerCompleted =
                    takerData.orderType === EOrderType.MARKET_TOTAL
                        ? takerData.quantityMarketTotal / makerData.price <= makerData.quantity
                        : makerData.quantity >= takerData.quantity;

                const trade = await this.sendAndGetTrade(takerData, makerData, takerCompleted, makerCompleted);
                await this.redis.hmset('last-price', { [trade.maker.pair]: trade.maker.price });
                await this.redis.hmset('last-price-side', { [trade.maker.pair]: trade.taker.buyFlg ? 'buy' : 'sell' });

                await this.sendOrderBookToBffws(
                    [
                        {
                            side: makerData.buyFlg ? 'buy' : 'sell',
                            operation: 'sub',
                            price: makerData.price,
                            volume: BigInt(trade.volume),
                            pair: takerData.pair,
                        },
                    ],
                    takerData.pair,
                    trade,
                );

                if (makerCompleted) {
                    if (takerData.orderType === EOrderType.MARKET_TOTAL) {
                        takerData.quantityMarketTotal -= makerData.quantity * makerData.price /* /100000000 */;
                        makerData.quantity = ZERO_BIGINT;
                    } else {
                        takerData.quantity -= makerData.quantity;
                        makerData.quantity = ZERO_BIGINT;
                    }

                    const {
                        quantity: quantityOrder,
                        quantityMarketTotal: quantityMarketTotalOther,
                        ...otherTakerData
                    } = deserializeFromV8Base64<IProcessedOrder>(
                        (await this.redis.hget(`orderList${takerData.pair}`, takerData.id)) as string,
                    );
                    await this.redis.hmset(`orderList${takerData.pair}`, {
                        [takerData.id]: serializeAnythingToV8Base64({
                            quantityMarketTotal: takerData.quantityMarketTotal,
                            quantity: takerData.quantity,
                            ...otherTakerData,
                        }),
                    });
                    await this.redis.hdel(`orderList${makerData.pair}`, makerData.id);

                    await this.findSLToActivate(
                        makerData.pair,
                        beforeClosePrice,
                        makerData.price,
                        trade.taker.id,
                        trade.maker.id,
                        trade.matchingTime,
                    );

                    if (
                        takerData.orderType === EOrderType.MARKET_TOTAL
                            ? takerData.quantityMarketTotal === ZERO_BIGINT
                            : takerData.quantity === ZERO_BIGINT
                    ) {
                        return;
                    }
                } else {
                    if (takerData.orderType === EOrderType.MARKET_TOTAL) {
                        makerData.quantity -= takerData.quantityMarketTotal / makerData.price /** 100000000 */;
                        takerData.quantityMarketTotal = ZERO_BIGINT;
                    } else {
                        makerData.quantity -= takerData.quantity;
                        takerData.quantity = ZERO_BIGINT;
                    }

                    await this.zaddOrder(makerData, makerCounter);

                    const {
                        quantity: quantityMarketOrder,
                        quantityMarketTotal: quantityMarketTotalOrder,
                        ...otherMakerData
                    } = deserializeFromV8Base64<IProcessedOrder>(
                        (await this.redis.hget(`orderList${makerData.pair}`, makerData.id)) as string,
                    );
                    await this.redis.hmset(`orderList${makerData.pair}`, {
                        [makerData.id]: serializeAnythingToV8Base64({
                            quantity: makerData.quantity,
                            quantityMarketTotal: quantityMarketTotalOrder,
                            ...otherMakerData,
                        }),
                    });
                    await this.redis.hdel(`orderList${takerData.pair}`, takerData.id);

                    await this.findSLToActivate(
                        makerData.pair,
                        beforeClosePrice,
                        makerData.price,
                        trade.taker.id,
                        trade.maker.id,
                        trade.matchingTime,
                    );
                    return;
                }
            } else {
                if (takerData.orderType !== EOrderType.MARKET_QUANTITY && takerData.orderType !== EOrderType.MARKET_TOTAL) {
                    await this.zaddOrder(takerData);
                    if (makerRaw) await this.zaddOrder(makerData, makerCounter);
                    await this.sendOrderBookToBffws(
                        [
                            {
                                side: takerData.buyFlg ? 'buy' : 'sell',
                                operation: 'add',
                                price: takerData.price,
                                volume: takerData.quantity,
                                pair: takerData.pair,
                            },
                        ],
                        takerData.pair,
                    );
                }
                return;
            }
        }
    }

    public async placeCancel(data: IRmqCancelOrder): Promise<void> {
        if (data?.id && data?.pair) {
            await this.redis.set(generateCancelKey(data), 'to cancel');
            // ToDo: КОСТЫЛИЩЕ - даем возможность провести сделку, если она уже отправилась в trading
            await promiseDelay(5000);
            await this.cancelOrder(data);
        }
    }

    private async cancelOrder(data: { id: string; pair: string }): Promise<void> {
        const cancelReportMessage = this.rabbitService.buildMessage({
            id: data.id,
            pair: data.pair,
        });
        await this.rmqClientService.emit(TRADING_CANCEL_EVENT_PATTERN, cancelReportMessage);
        const raw_order = await this.redis.hget(`orderList${data.pair}`, data.id);
        const orderFromRedis = deserializeFromV8Base64<IProcessedOrder>(raw_order as string);

        if (!(orderFromRedis.stopLimitPrice && orderFromRedis.stopLimitPrice > 0n)) {
            // if not stop-limit
            await this.modifyOrderBook([
                {
                    side: orderFromRedis.buyFlg ? 'buy' : 'sell',
                    operation: 'sub',
                    price: BigInt(orderFromRedis.price),
                    volume: orderFromRedis.quantity,
                    pair: data.pair,
                },
            ]);
        }
        await this.redis.hdel(`orderList${data.pair}`, data.id);
        if (raw_order) {
            await this.redis.zrem(`SL${orderFromRedis.pair}${orderFromRedis.buyFlg ? EOrderSide.BUY : EOrderSide.SELL}`, raw_order);
        }
    }

    private async sendOrderBookToBffws(items: IOrderBookData[], pair: string, trade?: ITrade | IMatcherTrade): Promise<void> {
        const orderBook = await this.modifyOrderBook(items);
        const tickMessage = this.rabbitService.buildMessage<IBffwsTick>({
            pair,
            data: {
                asOfDate: new Date().valueOf(),
                orderBook,
                trade: trade
                    ? ((): ITrade => {
                          const { taker, maker, ...other } = trade;
                          const { price: takerPrice, quantity: takerQuantity, ...otherTaker } = taker;
                          const { price: makerPrice, quantity: makerQuantity, ...otherMaker } = maker;
                          return {
                              taker: {
                                  price: takerPrice as string,
                                  quantity: takerQuantity as string,
                                  ...otherTaker,
                              },
                              maker: {
                                  price: makerPrice as string,
                                  quantity: makerQuantity as string,
                                  ...otherMaker,
                              },
                              ...other,
                          };
                      })()
                    : undefined,
            },
        });

        return this.rmqClientService.emit(BFFWS_TICK_PATTERN, tickMessage);
    }

    private async sendAndGetTrade(
        takerData: IProcessedOrder,
        makerData: IProcessedOrder,
        orderCompleted: boolean,
        contraCompleted: boolean,
    ): Promise<IMatcherTrade> {
        const { stopLimitPrice: _takerStopLimitPrice, price: takerPrice, quantity: takerQuantity, quantityMarketTotal: takerQuantityMarketTotal, ...taker } = takerData;
        const { stopLimitPrice: _makerStopLimitPrice, price: makerPrice, quantity: makerQuantity, quantityMarketTotal: makerQuantityMarketTotal, ...maker } = makerData;
        const trade: IMatcherTrade = {
            taker: {
                price: takerPrice.toString(),
                quantity: takerQuantity.toString(),
                quantityMarketTotal: takerQuantityMarketTotal.toString(),
                completed: orderCompleted,
                ...taker,
            },
            maker: {
                price: makerPrice.toString(),
                quantity: makerQuantity.toString(),
                quantityMarketTotal: makerQuantityMarketTotal.toString(),
                completed: contraCompleted,
                ...maker,
            },
            volume: String(
                takerData.orderType === EOrderType.MARKET_TOTAL
                    ? takerData.quantityMarketTotal / makerData.price >= makerData.quantity
                        ? makerData.quantity
                        : takerData.quantityMarketTotal / makerData.price
                    : takerData.quantity >= makerData.quantity
                    ? makerData.quantity
                    : takerData.quantity,
            ),
            matchingTime: new Date().valueOf(),
        };
        // console.log(trade)
        const tradeMessage = this.rabbitService.buildMessage(trade);
        
        await this.rmqClientService.emit(TRADING_NEW_TRADE_PATTERN, tradeMessage);

        return trade;
    }

    private async modifyOrderBook(items: IOrderBookData[]): Promise<IOrderBook> {
        const res = await Promise.all(
            items.map(async (i) => {
                const { side, operation, price, volume, pair } = i;
                const orderBook = await this.redis.hgetall(`orderBook-${side}-${pair}`);
                const out: IOrderBookSide[] = [];

                if (Object.keys(orderBook).includes(price.toString())) {
                    // exists
                    if (operation === 'sub') {
                        if (BigInt(orderBook[price.toString()]) - volume < 0n) {
                            throw new Error('Subtract error');
                        } else {
                            await this.redis.hmset(`orderBook-${side}-${pair}`, {
                                [price.toString()]: (BigInt(orderBook[price.toString()]) - volume).toString(),
                            });
                            out.push({
                                price: price.toString(),
                                volume: (BigInt(orderBook[price.toString()]) - volume).toString(),
                            });
                        }
                    } else {
                        // orderBook[price.toString()] = (BigInt(orderBook[price.toString()]) + volume).toString();
                        await this.redis.hmset(`orderBook-${side}-${pair}`, {
                            [price.toString()]: (BigInt(orderBook[price.toString()]) + volume).toString(),
                        });
                        out.push({
                            price: price.toString(),
                            volume: (BigInt(orderBook[price.toString()]) + volume).toString(),
                        });
                    }
                } else {
                    // not exists
                    // eslint-disable-next-line no-lonely-if
                    if (operation === 'sub') {
                        throw new Error('Subtract error');
                    } else {
                        await this.redis.hmset(`orderBook-${side}-${pair}`, { [price.toString()]: volume.toString() });
                        out.push({ price: price.toString(), volume: volume.toString() });
                    }
                }
                await this.redis.hmset('orderBook-time', { [pair]: new Date().valueOf() });
                return {
                    [side]: out,
                };
            }),
        );

        return {
            ...res[0],
            ...res[1],
        };
    }

    private async zaddOrder(order: IProcessedOrder, orderCounter?: string | number): Promise<void> {
        const counter = orderCounter || (await this.redis.incr('orderCount'));
        // eslint-disable-next-line no-param-reassign
        await this.redis.zadd(
            order.pair + (order.buyFlg ? EOrderSide.BUY : EOrderSide.SELL),
            order.price.toString(), // Number(order.price)
            `${counter}${DELIMITER}${serializeAnythingToV8Base64(order)}`,
        );
        // return;
    }

    private async zaddSLOrder(order: IProcessedOrder, orderCounter?: string | number): Promise<void> {
        const counter = orderCounter || (await this.redis.incr('orderCount')); // !!!!!!!!!!!
        // eslint-disable-next-line no-param-reassign
        if (order.stopLimitPrice) {
            await this.redis.zadd(
                `SL${order.pair}${order.buyFlg ? EOrderSide.BUY : EOrderSide.SELL}`,
                order.stopLimitPrice.toString(), // !.toString()
                `${counter}${DELIMITER}${serializeAnythingToV8Base64(order)}`,
            );
        } else {
            throw new Error('stopLimitPrice is null');
        }
    }

    private async findSLToActivate(
        pair: string,
        beforeClosePrice: bigint,
        close_price: bigint,
        takerId: string,
        makerId: string,
        matchingTime: number,
    ): Promise<void> {
        const cl = Number(close_price);
        const bcl = Number(beforeClosePrice);

        const buySlPromise = this.redis.zrange(
            `SL${pair}${EOrderSide.BUY}`,
            ...([cl, bcl].sort((a, b) => a - b) as [number, number]),
            'BYSCORE',
        );
        const sellSlPromise = this.redis.zrange(
            `SL${pair}${EOrderSide.SELL}`,
            ...([cl, bcl].sort((a, b) => a - b) as [number, number]),
            'BYSCORE',
        );
        const [buySl, sellSl] = await Promise.all([buySlPromise, sellSlPromise]);
        const combine = [...buySl, ...sellSl].sort(comparator);

        for (const i of combine) {
            const [counter, dataStr] = i.split(DELIMITER);
            const slOrder = deserializeFromV8Base64<IProcessedOrder>(dataStr);
            await this.activateSLOrder(slOrder, close_price, takerId, makerId, matchingTime);
        }
    }

    private async activateSLOrder(
        inSl: IProcessedOrder,
        close_price: bigint,
        takerId: string,
        makerId: string,
        matchingTime: number,
    ): Promise<void> {
        const { stopLimitPrice, quantityMarketTotal, pair, price, quantity, buyFlg, base, quote, ...other } = inSl;
        if (stopLimitPrice && close_price) {
            const orderMessage = this.rabbitService.buildMessage<ITradingSLActivate>({
                slOrder: {
                    stopLimitPrice: null,
                    pair,
                    price: transformToStringWithNull(price),
                    quantity: transformToStringWithNull(quantity),
                    quantityMarketTotal: transformToStringWithNull(quantityMarketTotal),
                    buyFlg,
                    base: extractBase(pair),
                    quote: extractQuote(pair),
                    ...other,
                },
                matchingTime,
                orderTriggerMakerId: makerId,
                orderTriggerTakerId: takerId,
            });

            await this.rmqClientService.emit(TRADING_SL_ACTIVATE_PATTERN, orderMessage);
        } else {
            throw new Error('stopLimitPrice and close_price should be is not null');
        }
    }
}
