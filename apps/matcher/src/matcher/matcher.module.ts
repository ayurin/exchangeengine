import { Module } from '@nestjs/common';

import { MatcherController } from './matcher.controller';
import { MatcherService } from './matcher.service';
import { OrderPipe } from './order.pipe';

@Module({
    providers: [MatcherService, OrderPipe],
    controllers: [MatcherController],
})
export class MatcherModule {}
