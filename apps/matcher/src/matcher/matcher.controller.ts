/* eslint-disable @typescript-eslint/no-unsafe-assignment,@typescript-eslint/no-unsafe-call,@typescript-eslint/no-unsafe-member-access */
import { IRmqCancelOrder, MATCHER_CANCEL_ORDER_PATTERN, MATCHER_PLACE_ORDER_PATTERN, RmqWraper } from '@app/utils';
import { Controller } from '@nestjs/common';
import { Ctx, MessagePattern, Payload, RmqContext } from '@nestjs/microservices';
import { Channel, Message } from 'amqplib';

import { MatcherService } from './matcher.service';
import { IProcessedOrder, OrderPipe } from './order.pipe';

@Controller()
export class MatcherController {
    constructor(private readonly matcherService: MatcherService) {}

    // for test
    @MessagePattern({ cmd: 'greeting' })
    public testConsume(@Payload() data: string, @Ctx() context: RmqContext): string {
        const channel = context.getChannelRef() as Channel;
        const originalMsg = context.getMessage() as Message;
        channel.ack(originalMsg);
        return `xxx-${data}`;
    }

    // for Prod
    @MessagePattern(MATCHER_PLACE_ORDER_PATTERN)
    @RmqWraper
    public async tick(@Payload(OrderPipe) data: IProcessedOrder, @Ctx() _context: RmqContext): Promise<void> {
        this.matcherService.tick(data);
    }

    @MessagePattern(MATCHER_CANCEL_ORDER_PATTERN)
    @RmqWraper
    public async cancel(@Payload() data: IRmqCancelOrder, @Ctx() _context: RmqContext): Promise<void> {
        this.matcherService.placeCancel(data);
    }
}
