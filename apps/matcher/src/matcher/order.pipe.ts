import { IRmqOrder, ZERO_BIGINT } from '@app/utils';
import { Injectable, PipeTransform } from '@nestjs/common';

export interface IProcessedOrder extends Omit<IRmqOrder, 'price' | 'quantity' | 'stopLimitPrice' | 'quantityMarketTotal'> {
    price: bigint;
    quantity: bigint;
    quantityMarketTotal: bigint;
    stopLimitPrice: bigint | null;
}

@Injectable()
export class OrderPipe implements PipeTransform {
    public transform(value: IRmqOrder): IProcessedOrder {
        return {
            ...value,
            price: value.price ? BigInt(value.price) : ZERO_BIGINT,
            quantity: value.quantity ? BigInt(value.quantity) : ZERO_BIGINT,
            quantityMarketTotal: value.quantityMarketTotal ? BigInt(value.quantityMarketTotal) : ZERO_BIGINT,
            stopLimitPrice: value.stopLimitPrice ? BigInt(value.stopLimitPrice) : ZERO_BIGINT,
        } as IProcessedOrder;
    }
}
