import { Injectable } from '@nestjs/common';
import { DataSource, Repository } from 'typeorm';

import { RefreshTokenEntity } from './entities/refresh-token.entity';

@Injectable()
export class RefreshTokenRepository extends Repository<RefreshTokenEntity> {
    constructor(private readonly dataSource: DataSource) {
        super(dataSource.getRepository(RefreshTokenEntity).target, dataSource.manager, dataSource.createQueryRunner());
    }
}
