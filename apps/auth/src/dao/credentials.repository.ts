import { Injectable } from '@nestjs/common';
import { DataSource, Repository } from 'typeorm';

import { CredentialsEntity } from '@auth/dao/entities';

@Injectable()
export class CredentialsRepository extends Repository<CredentialsEntity> {
    constructor(private readonly dataSource: DataSource) {
        super(dataSource.getRepository(CredentialsEntity).target, dataSource.manager, dataSource.createQueryRunner());
    }
}
