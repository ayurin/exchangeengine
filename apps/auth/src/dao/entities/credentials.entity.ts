import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

const TABLE_NAME = 'credentials';

@Entity(TABLE_NAME)
export class CredentialsEntity {
    public static readonly TABLE_NAME = TABLE_NAME;

    @PrimaryGeneratedColumn('uuid')
    public id!: string;

    @Column({ nullable: false, unique: true })
    public login!: string;

    @Column({ nullable: false })
    public passwordHash!: string;

    @Column({ nullable: false })
    public salt!: string;
}
