import { Column, CreateDateColumn, Entity, PrimaryGeneratedColumn, UpdateDateColumn } from 'typeorm';

export const TABLE_NAME = 'refresh_tokens';

@Entity(TABLE_NAME)
export class RefreshTokenEntity {
    public static readonly TABLE_NAME = TABLE_NAME;

    @PrimaryGeneratedColumn('uuid')
    public id!: string;

    @Column()
    public token!: string;

    @Column({ type: 'uuid' })
    public userId!: string;

    @Column({ type: 'boolean' })
    public isRevoked!: boolean;

    @CreateDateColumn({ type: 'timestamp' })
    public createdAt!: Date;

    @UpdateDateColumn({ type: 'timestamp' })
    public updatedAt!: Date;

    public update(partial: Partial<RefreshTokenEntity>): RefreshTokenEntity {
        Object.assign(this, partial);
        return this;
    }
}
