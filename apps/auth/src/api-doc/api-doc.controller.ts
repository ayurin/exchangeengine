import { Controller, Get } from '@nestjs/common';
import { ApiExcludeController } from '@nestjs/swagger';
import { OpenAPIObject } from '@nestjs/swagger/dist/interfaces';

@Controller('api-doc')
@ApiExcludeController()
export class ApiDocController {
    @Get()
    public getApiDocs(): Promise<OpenAPIObject> {
        // eslint-disable-next-line @typescript-eslint/no-unsafe-return, global-require
        return require('../swagger.json');
    }
}
