import { Module } from '@nestjs/common';

import { ApiDocController } from '@auth/api-doc/api-doc.controller';

@Module({
    controllers: [ApiDocController],
})
export class ApiDocModule {}
