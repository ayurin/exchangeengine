import crypto = require('crypto');
import * as process from 'process';

import { dbConfig } from './db.config';

// HTTP
const APP_NAME = process.env.APP_NAME;
if (!APP_NAME) {
    throw new Error('the APP_NAME environment variable is not defined');
}

const appPort = process.env.APP_PORT;
if (!appPort) {
    throw new Error('the APP_PORT environment variable is not defined');
}
const APP_PORT = Number(appPort);
if (!APP_PORT || APP_PORT % 1 !== 0) {
    throw new Error('the APP_PORT environment variable is not correct');
}

// AUTHORIZATION
const authSalt = process.env.AUTH_STATIC_SALT;
if (!authSalt) {
    throw new Error('the AUTH_STATIC_SALT environment variable is not defined');
}

const privateKey = process.env.AUTH_PRIVATE_KEY;
if (!privateKey) {
    throw new Error('the AUTH_PRIVATE_KEY environment variable is not defined');
}
let AUTH_PRIVATE_KEY: string;
try {
    AUTH_PRIVATE_KEY = Buffer.from(privateKey, 'base64url').toString();
} catch {
    throw new Error('the AUTH_PRIVATE_KEY environment is not correct base64url encoded string');
}

const pubKeyObject = crypto.createPublicKey({
    key: AUTH_PRIVATE_KEY,
    format: 'pem',
});

const AUTH_PUBLIC_KEY = pubKeyObject.export({
    format: 'pem',
    type: 'spki',
}) as string;

const JWT_ACCESS_EXPIRES = process.env.JWT_ACCESS_EXPIRES;
if (!JWT_ACCESS_EXPIRES) {
    throw new Error('the JWT_ACCESS_EXPIRES environment variable is not defined');
}

const JWT_REFRESH_EXPIRES = process.env.JWT_REFRESH_EXPIRES;
if (!JWT_REFRESH_EXPIRES) {
    throw new Error('the JWT_REFRESH_EXPIRES environment variable is not defined');
}

// RMQ
const RMQ_HOST = process.env.RMQ_HOST;
if (!RMQ_HOST) {
    throw new Error('the RMQ_HOST environment variable is not defined');
}

const rmqPot = process.env.RMQ_PORT;
if (!rmqPot) {
    throw new Error('the RMQ_PORT environment variable is not defined');
}
const RMQ_PORT = Number(rmqPot);
if (!RMQ_PORT || RMQ_PORT <= 1000 || RMQ_PORT % 1 !== 0) {
    throw new Error('the RMQ_PORT environment variable is not correct');
}

const RMQ_USER = process.env.RMQ_USER;
if (!RMQ_USER) {
    throw new Error('the RMQ_USER environment variable is not defined');
}

const RMQ_PASSWORD = process.env.RMQ_PASSWORD;
if (!RMQ_PASSWORD) {
    throw new Error('the RMQ_PASSWORD environment variable is not defined');
}
const API_PREFIX = process.env.API_PREFIX;
if (!API_PREFIX) {
    throw new Error('the API_PREFIX environment variable is not defined');
}

const RMQ_URL = `amqp://${RMQ_USER}:${RMQ_PASSWORD}@${RMQ_HOST}:${RMQ_PORT}`;

const GOOGLE_RECAPTCHA_SECRET_KEY = process.env.GOOGLE_RECAPTCHA_SECRET_KEY;
if (!GOOGLE_RECAPTCHA_SECRET_KEY) {
    throw new Error('the GOOGLE_RECAPTCHA_SECRET_KEY environment variable is not defined');
}

const SKIP_RECAPTCHA = process.env.SKIP_RECAPTCHA === '1';

const VAULT_ENDPOINT_URL = process.env.VAULT_ENDPOINT_URL;
if (!VAULT_ENDPOINT_URL) {
    throw new Error('the VAULT_ENDPOINT_URL environment variable is not defined');
}

const VAULT_PORT = process.env.VAULT_PORT;
if (!VAULT_PORT) {
    throw new Error('the VAULT_PORT environment variable is not defined');
}

const VAULT_API_TOKEN = process.env.VAULT_API_TOKEN;
if (!VAULT_API_TOKEN) {
    throw new Error('the VAULT_API_TOKEN environment variable is not defined');
}

const VAULT_API_VERSION = process.env.VAULT_API_VERSION ?? 'v1';

const VAULT_TOTP_PATH = process.env.VAULT_TOTP_PATH ?? 'totp';

const VAULT_CREATE_TOTP_ENGINE = process.env.VAULT_CREATE_TOTP_ENGINE === 'true';

const REDIS_HOST = process.env.REDIS_HOST;
if (!REDIS_HOST) {
    throw new Error('the REDIS_HOST environment variable is not defined');
}

const REDIS_PORT = process.env.REDIS_PORT;
if (!REDIS_PORT) {
    throw new Error('the REDIS_PORT environment variable is not defined');
}

const REDIS_URL = `redis://${REDIS_HOST}:${REDIS_PORT}`;

export const appConfig = Object.freeze({
    APP_NAME,
    APP_PORT,

    ...dbConfig,

    RMQ_URL,

    AUTH_STATIC_SALT: Buffer.from(authSalt),
    AUTH_PRIVATE_KEY,
    AUTH_PUBLIC_KEY,
    JWT_ACCESS_EXPIRES,
    JWT_REFRESH_EXPIRES,
    API_PREFIX,
    GOOGLE_RECAPTCHA_SECRET_KEY,
    SKIP_RECAPTCHA,

    VAULT_ENDPOINT_URL,
    VAULT_PORT,
    VAULT_API_TOKEN,
    VAULT_API_VERSION,
    VAULT_TOTP_PATH,
    VAULT_CREATE_TOTP_ENGINE,

    REDIS_URL,
});
