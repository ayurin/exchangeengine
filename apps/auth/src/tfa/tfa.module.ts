import { Module } from '@nestjs/common';

import { RateLimitModule } from '@auth/rate-limit';
import { UserService } from '@auth/user';
import { VaultModule } from '@auth/vault';

import { TfaController } from './tfa.controller';
import { TfaService } from './tfa.service';

@Module({
    imports: [RateLimitModule, VaultModule],
    controllers: [TfaController],
    providers: [TfaService, UserService],
})
export class TfaModule {}
