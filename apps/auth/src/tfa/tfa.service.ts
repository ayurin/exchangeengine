import { IGenerateTfaSecretResponse, RmqClientService } from '@app/utils';
import { RabbitService } from '@app/utils';
import { ELockReason, ELockTime } from '@app/ws';
import { HttpStatus, Injectable } from '@nestjs/common';

import { RateLimitService } from '@auth/rate-limit';
import { UserService } from '@auth/user';
import { VaultService } from '@auth/vault';

interface IVaultError {
    response: { statusCode: number; body: { errors: string[] } };
}

@Injectable()
export class TfaService {
    constructor(
        private readonly vaultService: VaultService,
        private readonly rmqClientService: RmqClientService,
        private readonly rabbitService: RabbitService,
        private readonly rateLimitService: RateLimitService,
        private readonly userService: UserService,
    ) {}

    public async tfaAdd(options: { userId: string; email: string }): Promise<IGenerateTfaSecretResponse> {
        try {
            const barcode = await this.vaultService.generateTotpSecret(options);
            return { status: HttpStatus.OK, barcode: `data:image/png;base64,${barcode}` };
        } catch (e) {
            const parsedError = this.parseError(e as IVaultError);
            if (parsedError) {
                return parsedError;
            }

            const { message = 'Unknown error', status = HttpStatus.INTERNAL_SERVER_ERROR } = e as {
                message: string;
                status: number;
            };

            return { status, message };
        }
    }

    public async disableTfa(userId: string): Promise<{ status: HttpStatus; message?: string }> {
        try {
            await this.vaultService.deleteTotp(userId);
            return { status: HttpStatus.OK };
        } catch (e) {
            const parsedError = this.parseError(e as IVaultError);
            if (parsedError) {
                return parsedError;
            }

            const { message = 'Unknown error', status = HttpStatus.INTERNAL_SERVER_ERROR } = e as {
                message: string;
                status: number;
            };

            return { status, message };
        }
    }

    public async checkTfaCode(options: {
        userId: string;
        code: string;
        isConfirmation: boolean;
    }): Promise<any> {
        const user = await this.userService.getUser(options.userId);
        if (!user.tfaEnable && !options.isConfirmation) {
            return { status: HttpStatus.OK, isValid: true };
        }

        const canContinue = await this.rateLimitService.checkCount(user.id, 'tfa');
        if (!canContinue) {
            await this.userService.temporaryBlockUser(user, {
                lockReason: ELockReason.TOO_MANY_TFA_REQUEST,
                lockTime: ELockTime.TEN_MINUTES,
            });
            return { status: HttpStatus.TOO_MANY_REQUESTS, message: 'Too many request for check 2fa', isValid: false };
        }

        try {
            const isValid = await this.vaultService.totpCodeIsValid(options);
            if (isValid) {
                await this.rateLimitService.clearCount(user.id, 'tfa');
            }

            return {
                status: HttpStatus.OK,
                isValid,
            };
        } catch (e) {
            const parsedError = this.parseError(e as IVaultError);
            if (parsedError) {
                return parsedError;
            }

            const { message = 'Unknown error', status = HttpStatus.INTERNAL_SERVER_ERROR } = e as {
                message: string;
                status: number;
            };

            return {
                message,
                status,
            };
        }
    }

    private parseError(error?: IVaultError): {
        status: number;
        message: string;
    } | null {
        if (!error) {
            return null;
        }

        const { response } = error;
        if (!response) {
            return null;
        }

        return { status: response.statusCode, message: response.body?.errors?.join(', ') };
    }
}
