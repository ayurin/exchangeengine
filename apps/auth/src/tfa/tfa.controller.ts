import {
    AUTH_CHECK_TFA_CODE_PATTERN,
    AUTH_DELETE_TFA_SECRET_PATTERN,
    AUTH_GENERATE_TFA_SECRET_PATTERN,
    IGenerateTfaSecretPayload,
    IGenerateTfaSecretResponse,
} from '@app/utils';
import { Controller, HttpStatus } from '@nestjs/common';
import { Ctx, MessagePattern, Payload, RmqContext } from '@nestjs/microservices';
import { Channel, Message } from 'amqplib';

import { TfaService } from './tfa.service';

@Controller()
export class TfaController {
    constructor(private readonly tfaService: TfaService) {}

    @MessagePattern(AUTH_GENERATE_TFA_SECRET_PATTERN)
    public tfaGenerate(
        @Payload() data: IGenerateTfaSecretPayload,
        @Ctx() context: RmqContext,
    ): Promise<IGenerateTfaSecretResponse> {
        this.ack(context);
        return this.tfaService.tfaAdd(data);
    }

    @MessagePattern(AUTH_DELETE_TFA_SECRET_PATTERN)
    public deleteTfaSecret(
        @Payload() data: { userId: string },
        @Ctx() context: RmqContext,
    ): Promise<{ status: HttpStatus; message?: string }> {
        this.ack(context);
        return this.tfaService.disableTfa(data.userId);
    }

    @MessagePattern(AUTH_CHECK_TFA_CODE_PATTERN)
    public checkCodeIsValid(
        @Payload() data: { code: string; userId: string; isConfirmation: boolean },
        @Ctx() context: RmqContext,
    ): Promise<any> {
        this.ack(context);
        return this.tfaService.checkTfaCode(data);
    }

    private ack(ctx: RmqContext): void {
        const channel = ctx.getChannelRef() as Channel;
        const originalMsg = ctx.getMessage() as Message;
        channel.ack(originalMsg);
    }
}
