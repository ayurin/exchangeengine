import { JWTOptions } from '@app/jwt';
import { NestHybridApplicationOptions, ValidationPipe, VersioningOptions, VersioningType } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { DocumentBuilder } from '@nestjs/swagger';
import { LoggerModuleAsyncParams } from 'nestjs-pino';
import { DataSourceOptions } from 'typeorm';
import { v4 as uuidv4 } from 'uuid';

import { appConfig } from './config';
import { connectionSource } from './ormconfig';

export const dataSourceOptions: DataSourceOptions = connectionSource.options;

export const appValidationPipe = new ValidationPipe({
    whitelist: true,
    forbidNonWhitelisted: true,
    forbidUnknownValues: true,
    transform: true,
});

export const appVersioningOptions: VersioningOptions = {
    type: VersioningType.URI,
    defaultVersion: '1',
};

export const API_PREFIX = appConfig.API_PREFIX;

export const swaggerConfig = new DocumentBuilder()
    .setTitle(appConfig.APP_NAME)
    .setDescription(`${appConfig.APP_NAME} API description`)
    .setVersion('1.0')
    .addTag(`${appConfig.APP_NAME}`)
    .addBearerAuth()
    .build();

export const hybridMicroserviceOptions: NestHybridApplicationOptions = { inheritAppConfig: true };

export const loggerModuleAsyncParams: LoggerModuleAsyncParams = {
    imports: [ConfigModule],
    useFactory: (configService: ConfigService) => ({
        pinoHttp: {
            genReqId(req, res) {
                let corrId = req.headers['x-correlation-id'] as string;
                if (!corrId) {
                    // eslint-disable-next-line @typescript-eslint/no-unsafe-call
                    corrId = uuidv4() as string;
                    req.headers['x-correlation-id'] = corrId;
                }
                res.setHeader('x-correlation-id', corrId);
                return corrId;
            },
            name: 'Auth',
            level: process.env.LOG_LEVEL || 'info',
            redact: configService.get<string[]>('logger.redacted.fields'),
            transport:
                process.env.NODE_ENV !== 'production'
                    ? {
                          target: 'pino-pretty',

                          options: {
                              colorize: true,
                              translateTime: "yyyy-mm-dd'T'HH:MM:ss.l'Z'",
                              messageFormat: '{correlationId: req.headers.x-correlation-id} [{context}] {msg}',
                              // ignore: 'pid,hostname,context,req,res,responseTime',
                              errorLikeObjectKeys: ['err', 'error'],
                          },
                      }
                    : undefined,
            useLevelLabels: true,
        },
    }),
    inject: [ConfigService],
};

export const jwtModuleOptions: JWTOptions = {
    publicKey: appConfig.AUTH_PUBLIC_KEY,
    jwtAccessExpires: appConfig.JWT_ACCESS_EXPIRES,
    jwtRefreshExpires: appConfig.JWT_REFRESH_EXPIRES,
};
