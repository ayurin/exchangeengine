import { ExceptionModule } from '@app/exception';
import { RmqClientModule } from '@app/utils';
import { RabbitModule } from '@app/utils';
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { LoggerModule } from 'nestjs-pino';

import { ApiDocModule } from '@auth/api-doc/api-doc.module';
import { AuthModule } from '@auth/auth';
import { dataSourceOptions, loggerModuleAsyncParams } from '@auth/env';
import { HealthModule } from '@auth/health';
import { RefreshTokenModule } from '@auth/refresh-token';
import { TfaModule } from '@auth/tfa';

@Module({
    imports: [
        RmqClientModule,
        RabbitModule,
        // ExceptionModule,
        LoggerModule.forRootAsync(loggerModuleAsyncParams),
        TypeOrmModule.forRoot(dataSourceOptions),
        AuthModule,
        HealthModule,
        ApiDocModule,
        RefreshTokenModule,
        TfaModule,
    ],
})
export class AppModule {}
