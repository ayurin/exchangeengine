export interface IVaultDataResponse<ResponseData = unknown> {
    data: ResponseData;
}

export type TGenerateTotpVaultRes = IVaultDataResponse<{ barcode: string }>;

export type TCheckTotpCodeVaultRes = IVaultDataResponse<{ valid: boolean }>;
