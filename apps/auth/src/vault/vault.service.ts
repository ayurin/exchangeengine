import { Injectable, OnModuleInit } from '@nestjs/common';
import Vault from 'node-vault';

import { appConfig } from '@auth/config';

import { TGenerateTotpVaultRes } from './types';

@Injectable()
export class VaultService implements OnModuleInit {
    private vaultClient: Vault.client;

    private readonly totpPath = appConfig.VAULT_TOTP_PATH;

    constructor() {
        this.vaultClient = Vault({
            apiVersion: appConfig.VAULT_API_VERSION,
            endpoint: `http://${appConfig.VAULT_ENDPOINT_URL}:${appConfig.VAULT_PORT}`,
            token: appConfig.VAULT_API_TOKEN,
        });
    }

    public async onModuleInit(): Promise<void> {
        if (!appConfig.VAULT_CREATE_TOTP_ENGINE) {
            return;
        }

        const path = `sys/mounts/${this.totpPath}`;

        try {
            await this.vaultClient.read(path);
        } catch (e) {
            if ((e as { message: string }).message.includes('No secret engine mount')) {
                await this.vaultClient.write(path, { type: 'totp' }).catch(console.error);
            }
        }
    }

    public async generateTotpSecret(options: { userId: string; email: string }): Promise<string> {
        const { userId, email } = options;

        const { data } = (await this.vaultClient.write(`${this.totpPath}/keys/${userId}`, {
            issuer: 'Haqqex',
            generate: true,
            account_name: email,
        })) as TGenerateTotpVaultRes;

        return data.barcode;
    }

    public async totpCodeIsValid(options: { userId: string; code: string }): Promise<boolean> {
        const { userId, code } = options;

        const { data } = (await this.vaultClient.write(`${this.totpPath}/code/${userId}`, { code })) as {
            data: { valid: boolean };
        };

        return data.valid;
    }

    public async deleteTotp(userId: string): Promise<void> {
        await this.vaultClient.delete(`${this.totpPath}/keys/${userId}`);
    }
}
