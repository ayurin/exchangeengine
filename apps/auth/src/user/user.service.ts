import {
    ACCOUNT_GET_USER_PATTERN,
    ACCOUNT_LOCK_USER_PATTERN,
    IGetUser,
    NOTIFICATION_EMAIL_PATTERN,
    RmqClientService,
} from '@app/utils';
import { RabbitService } from '@app/utils';
import { ELockReason, ELockTime, IUser } from '@app/ws';
import { Injectable } from '@nestjs/common';

@Injectable()
export class UserService {
    constructor(private readonly rabbitService: RabbitService, private readonly rmqClientService: RmqClientService) {}

    public async temporaryBlockUser(
        user: IUser,
        options: { lockReason: ELockReason; lockTime: ELockTime },
    ): Promise<void> {
        const notifyMessage = this.rabbitService.buildMessage({
            to: user.email,
            payload: { text: 'You are blocked for 10 minutes', subject: 'Block' },
        });

        const lockUserMessage = this.rabbitService.buildMessage({
            userId: user.id,
            ...options,
        });

        await Promise.all([
            this.rmqClientService.emit(ACCOUNT_LOCK_USER_PATTERN, lockUserMessage),
            this.rmqClientService.emit(NOTIFICATION_EMAIL_PATTERN, notifyMessage),
        ]);
    }

    public async getUser(userId: string): Promise<IUser> {
        const message = this.rabbitService.buildMessage<IGetUser>({ userId });
        return this.rmqClientService.send(ACCOUNT_GET_USER_PATTERN, message);
    }
}
