import { MigrationInterface, QueryRunner } from 'typeorm';

import { HashService } from '@auth/auth/services';
import { CredentialsEntity } from '@auth/dao/entities';

const SEED_PASSWORD = 'qwer1234';

const credentialsSeeds = [
    {
        id: '00000000-0000-0000-0000-000000000001',
        login: 'al-kindi@haqqex.com',
    },
    {
        id: '00000000-0000-0000-0000-000000000002',
        login: 'ibn-khordadbeh@haqqex.com',
    },
    {
        id: '00000000-0000-0000-0000-000000000003',
        login: 'hunayn@haqqex.com',
    },
    {
        id: '00000000-0000-0000-0000-000000000004',
        login: 'abu@haqqex.com',
    },
    {
        id: '00000000-0000-0000-0000-000000000005',
        login: 'al-khalil@haqqex.com',
    },
    {
        id: '00000000-0000-0000-0000-000000000006',
        login: 'najm@haqqex.com',
    },
    {
        id: '00000000-0000-0000-0000-000000000007',
        login: 'solomon@haqqex.com',
    },
];

export class CredentialsSeeds1661461749857 implements MigrationInterface {
    public async up(qr: QueryRunner): Promise<void> {
        let insertSql = `INSERT INTO "${CredentialsEntity.TABLE_NAME}" (id, login, password_hash, salt) VALUES \n`;

        const h = new HashService();
        const passes = await Promise.all(credentialsSeeds.map(() => h.hashPassword(SEED_PASSWORD)));

        insertSql += credentialsSeeds
            .map((c, i) => `('${c.id}', '${c.login}', '${passes[i][0]}', '${passes[i][1]}')`)
            .join(',\n');

        await qr.query(insertSql);
    }

    public async down(qr: QueryRunner): Promise<void> {
        await qr.query(`
            DELETE FROM "${CredentialsEntity.TABLE_NAME}"
            WHERE id IN (${credentialsSeeds.map((c) => `'${c.id}'`).join(', ')})
        `);
    }
}
