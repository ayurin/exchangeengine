import { TKeyBuilder } from './types';

export const TEN_MINUTES_IN_SECONDS = 60 * 10;

const tfaKeyBuilder = (userId: string): string => `tfa_count:${userId}:user_id`;
const loginKeyBuilder = (userId: string): string => `login_count:${userId}:user_id`;

export const keyBuilderByType: TKeyBuilder = {
    tfa: tfaKeyBuilder,
    login: loginKeyBuilder,
};
