export type TCheckType = 'tfa' | 'login';

export type TKeyBuilder = Record<TCheckType, (v: string) => string>;
