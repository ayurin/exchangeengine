import { Injectable } from '@nestjs/common';
import Redis from 'ioredis';

import { appConfig } from '@auth/config';

import { TCheckType } from './types';
import { keyBuilderByType, TEN_MINUTES_IN_SECONDS } from './utils';

@Injectable()
export class RateLimitService {
    private readonly client: Redis;

    constructor() {
        this.client = new Redis(appConfig.REDIS_URL);
    }

    public async checkCount(userId: string, type: TCheckType): Promise<boolean> {
        const key = keyBuilderByType[type](userId);

        const result = await this.client.incr(key);
        if (result === 1) {
            await this.client.expire(key, TEN_MINUTES_IN_SECONDS);
        }

        return result <= 10;
    }

    public async clearCount(userId: string, type: TCheckType): Promise<void> {
        const key = keyBuilderByType[type](userId);
        await this.client.del(key);
    }
}
