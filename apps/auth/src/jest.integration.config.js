module.exports = {
    preset: 'ts-jest',
    testEnvironment: 'node',
    clearMocks: true,
    resetMocks: false,
    testLocationInResults: true,
    testRegex: ['./e2e/'],
    reporters: ['default', 'jest-sonar'],
    moduleNameMapper: {
        // eslint-disable-next-line @typescript-eslint/naming-convention
        '^@auth/(.*)$': '<rootDir>/$1',
    },
};
