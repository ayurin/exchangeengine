import { generateRandomString } from '@app/utils';
import { Injectable } from '@nestjs/common';
import argon2 = require('argon2');

import { appConfig } from '@auth/config';

const ARGON_DELIMITER = '$';

export type THasedPassword = [hash: string, salt: string];

@Injectable()
export class HashService {
    private static readonly argonPrefix$ = argon2
        .hash(`It doesn't matter`, { salt: appConfig.AUTH_STATIC_SALT })
        .then((result) => {
            const [empty, libName, version, options /* , part1, part2 */] = result.split(ARGON_DELIMITER);
            return [empty, libName, version, options].join(ARGON_DELIMITER);
        });

    public async hashPassword(passwordToHash: string): Promise<THasedPassword> {
        const dynamicSalt = generateRandomString();
        const result = await argon2.hash(passwordToHash, {
            salt: Buffer.concat([appConfig.AUTH_STATIC_SALT, Buffer.from(dynamicSalt)]),
        });
        const [, , , , part1, part2] = result.split(ARGON_DELIMITER);
        const hash = [part1, part2].join(ARGON_DELIMITER);
        return [hash, dynamicSalt];
    }

    public async verifyPassword(userPasswordInput: string, passwordHash: string, userSalt: string): Promise<boolean> {
        const argonPrefix = await HashService.argonPrefix$;
        const fullSalt = Buffer.concat([appConfig.AUTH_STATIC_SALT, Buffer.from(userSalt)]);
        const fullHash = [argonPrefix, passwordHash].join(ARGON_DELIMITER);

        return argon2.verify(fullHash, userPasswordInput, { salt: fullSalt });
    }
}
