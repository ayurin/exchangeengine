import { setTimeout as asyncTimeout } from 'timers/promises';

import { IRequestPayload, JwtService, TokenAudience } from '@app/jwt';
import { RmqClientService, RmqFail, RmqResult, RmqSuccess } from '@app/utils';
import { getRandomInt, RabbitService } from '@app/utils';
import { ELockReason, ELockTime, IUser } from '@app/ws';
import { HttpException, HttpStatus, Injectable, NotFoundException, UnauthorizedException } from '@nestjs/common';

import { HashService } from '@auth/auth/services/hash.service';
import { appConfig } from '@auth/config';
import { CredentialsRepository } from '@auth/dao';
import { CredentialsEntity } from '@auth/dao/entities';
import { RateLimitService } from '@auth/rate-limit';
import { RefreshTokenService } from '@auth/refresh-token';
import { UserService } from '@auth/user';

import { LoginResponse, RenewResponse } from '../dto';

@Injectable()
export class AuthService {
    constructor(
        private readonly credentialsRepository: CredentialsRepository,
        private readonly hashService: HashService,
        private readonly jwtService: JwtService,
        private readonly refreshTokenService: RefreshTokenService,
        private readonly rabbitService: RabbitService,
        private readonly rmqClientService: RmqClientService,
        private readonly rateLimitService: RateLimitService,
        private readonly userService: UserService,
    ) {
        this.jwtService.setOptions({
            privateKey: appConfig.AUTH_PRIVATE_KEY,
        });
    }

    public async login(login: string, password: string): Promise<LoginResponse> {
        // why?
        const sleepPeriod = getRandomInt(1, 1000);
        await asyncTimeout(sleepPeriod);

        const userCredentials = await this.credentialsRepository.findOneBy({ login });
        if (!userCredentials) {
            throw new UnauthorizedException();
        }

        const user = await this.userService.getUser(userCredentials.id);

        this.checkUserIsBlocked(user);

        const isValidPassword = await this.hashService.verifyPassword(
            password,
            userCredentials.passwordHash,
            userCredentials.salt,
        );
        if (!isValidPassword) {
            const canContinue = await this.rateLimitService.checkCount(userCredentials.id, 'login');
            if (!canContinue) {
                await this.userService.temporaryBlockUser(user, {
                    lockReason: ELockReason.TOO_MANY_LOGIN_REQUEST,
                    lockTime: ELockTime.TEN_MINUTES,
                });
                throw new HttpException('Too many request for login', HttpStatus.TOO_MANY_REQUESTS);
            }

            throw new UnauthorizedException();
        }

        await this.rateLimitService.clearCount(user.id, 'login');
        return this.generateTokens(userCredentials.id);
    }

    private checkUserIsBlocked(user: IUser): void {
        if (user.blocked) {
            throw new UnauthorizedException('Your account is blocked, contact technical support');
        }
    }

    public async checkLoginExisting(login: string): Promise<boolean> {
        const existingUser = await this.credentialsRepository.findOne({ where: { login } });
        return existingUser !== null;
    }

    public async registration(userId: string, login: string, password: string): Promise<RmqResult> {
        const existingUser = await this.credentialsRepository.findOne({ where: { login } });
        if (existingUser !== null) {
            return new RmqFail('this login already taken');
        }

        const [passwordHash, salt] = await this.hashService.hashPassword(password);

        await this.credentialsRepository.save({
            id: userId,
            login,
            passwordHash,
            salt,
        });

        return new RmqSuccess();
    }

    public async continueRegistration(userId: string, login: string, password: string): Promise<RmqResult> {
        const [existingUser, [passwordHash, salt]] = await Promise.all([
            this.credentialsRepository.findOne({ where: { login } }),
            this.hashService.hashPassword(password),
        ]);

        if (existingUser !== null) {
            await this.credentialsRepository.update(existingUser.id, { passwordHash, salt });
            return new RmqSuccess();
        }

        await this.credentialsRepository.save({
            id: userId,
            login,
            passwordHash,
            salt,
        });

        return new RmqSuccess();
    }

    public async refresh(refreshToken: string): Promise<RenewResponse> {
        const result = await this.refreshTokenService.refresh(refreshToken);

        if (!result) {
            throw new NotFoundException('Refresh token not found');
        }

        const [token, refreshTokenEntity] = result;

        const user = await this.findUserById(refreshTokenEntity.userId);
        if (user === null) {
            throw new UnauthorizedException();
        }

        const { accessToken } = await this.generateTokens(user.id, token);

        return { accessToken };
    }

    public async logout(id: string): Promise<void> {
        await this.refreshTokenService.revokeAllTokens(id);
    }

    public async generateTokens(userId: string, refresh?: string): Promise<LoginResponse> {
        const payloadPattern: IRequestPayload = {
            id: userId,
            aud: TokenAudience.ACCOUNT,
        };

        const [accessToken, refreshToken] = await Promise.all([
            await this.jwtService.generateToken(payloadPattern),
            refresh ? Promise.resolve(refresh) : this.generateRefreshToken(userId),
        ]);

        return { accessToken, refreshToken };
    }

    private findUserById(userId: string): Promise<CredentialsEntity | null> {
        return this.credentialsRepository.findOne({ where: { id: userId } });
    }

    private async generateRefreshToken(userId: string): Promise<string> {
        const [token] = await this.refreshTokenService.generate(userId);

        return token;
    }
}
