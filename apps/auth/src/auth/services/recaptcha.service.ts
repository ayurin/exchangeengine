import { HttpService } from '@nestjs/axios';
import { Injectable } from '@nestjs/common';
import { firstValueFrom } from 'rxjs';

import { appConfig } from '@auth/config';

interface IRecaptchaResponse {
    success?: boolean;
}

@Injectable()
export class RecaptchaService {
    constructor(private readonly httpService: HttpService) {}

    public async validate(token?: string): Promise<boolean> {
        if (appConfig.SKIP_RECAPTCHA) {
            return true;
        }

        if (!token) {
            return false;
        }

        const res = await firstValueFrom(
            this.httpService.request<IRecaptchaResponse>({
                url: 'https://www.google.com/recaptcha/api/siteverify',
                params: {
                    secret: appConfig.GOOGLE_RECAPTCHA_SECRET_KEY,
                    response: token,
                },
            }),
        );

        return Boolean(res.data?.success);
    }
}
