import { ApiAuth, JwtPayload, JwtService, TokenAudience, TUserJwtPayload } from '@app/jwt';
import {
    AUTH_CHECK_LOGIN_EXISTING_PATTERN,
    AUTH_CONTINUE_REGISTRATION_PATTERN,
    AUTH_REGISTRATION_PATTERN,
    Exists,
    IAuthRegistrationData,
    ILoginExistingRequest,
    RmqResult,
} from '@app/utils';
import { OkResponse } from '@app/utils';
import { Body, Controller, Get, Header, HttpCode, HttpStatus, Post } from '@nestjs/common';
import { MessagePattern, Payload } from '@nestjs/microservices';
import { ApiBody, ApiOkResponse, ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';

import { AuthService, RecaptchaService } from '@auth/auth/services';
import { appConfig } from '@auth/config';

import {
    CaptchaVerificationRequest,
    CaptchaVerificationResponse,
    LoginBody,
    LoginResponse,
    RefreshAccessBody,
    RenewResponse,
} from './dto';

@ApiTags('authorisation')
@Controller()
export class AuthController {
    constructor(
        private authService: AuthService,
        private jwtService: JwtService,
        private recaptchaService: RecaptchaService,
    ) {
        this.jwtService.setOptions({
            privateKey: appConfig.AUTH_PRIVATE_KEY,
        });
    }

    @Get('public_key')
    @Header('Content-Type', 'plain/text')
    @ApiOperation({ summary: 'Authorization public key sharing' })
    @ApiOkResponse({ content: { 'plain/text': { example: appConfig.AUTH_PUBLIC_KEY.toString() } } }) // eslint-disable-line @typescript-eslint/naming-convention
    public getPublicKey(): string {
        return appConfig.AUTH_PUBLIC_KEY.toString();
    }

    @MessagePattern(AUTH_CHECK_LOGIN_EXISTING_PATTERN)
    public async checkLoginExisting(@Payload() data: ILoginExistingRequest): Promise<Exists> {
        const exists = await this.authService.checkLoginExisting(data.login);
        return new Exists(exists);
    }

    @MessagePattern(AUTH_REGISTRATION_PATTERN)
    public registration(@Payload() data: IAuthRegistrationData): Promise<RmqResult> {
        return this.authService.registration(data.userId, data.login, data.password);
    }

    @MessagePattern(AUTH_CONTINUE_REGISTRATION_PATTERN)
    public async continueRegistration(@Payload() data: IAuthRegistrationData): Promise<RmqResult> {
        return this.authService.continueRegistration(data.userId, data.login, data.password);
    }

    @Post('login')
    @HttpCode(HttpStatus.OK)
    @ApiOperation({ summary: 'Authorize user' })
    @ApiBody({ type: LoginBody })
    @ApiResponse({ status: HttpStatus.OK, type: LoginResponse })
    public async login(@Body() { login, password }: LoginBody): Promise<LoginResponse> {
        return this.authService.login(login, password);
    }

    @Post('renew_authorisation')
    @HttpCode(HttpStatus.OK)
    @ApiOperation({ summary: 'Renew authorisation' })
    @ApiBody({ type: RefreshAccessBody })
    @ApiResponse({ status: HttpStatus.OK, type: RenewResponse })
    public async renewAuthorisation(@Body() body: RefreshAccessBody): Promise<RenewResponse> {
        return this.authService.refresh(body.refreshToken);
    }

    @Post('recaptcha_verification')
    @HttpCode(HttpStatus.OK)
    @ApiOperation({ summary: 'Recaptcha verification' })
    @ApiBody({ type: CaptchaVerificationRequest })
    @ApiResponse({ status: HttpStatus.OK, type: CaptchaVerificationResponse })
    public async captchaVerification(@Body() body: CaptchaVerificationRequest): Promise<CaptchaVerificationResponse> {
        const isValidCaptcha = await this.recaptchaService.validate(body.token);
        return new CaptchaVerificationResponse(isValidCaptcha);
    }

    @Post('logout')
    @ApiAuth(TokenAudience.ACCOUNT)
    @ApiOperation({ summary: 'Logout user' })
    @ApiResponse({ status: HttpStatus.OK })
    public async logout(@JwtPayload() { id }: TUserJwtPayload): Promise<OkResponse> {
        await this.authService.logout(id);
        return new OkResponse();
    }
}
