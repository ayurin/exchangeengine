import { JwtModule } from '@app/jwt';
import { HttpModule } from '@nestjs/axios';
import { Module } from '@nestjs/common';

import { AuthService, HashService, RecaptchaService } from '@auth/auth/services';
import { CredentialsRepository } from '@auth/dao';
import { jwtModuleOptions } from '@auth/env';
import { RateLimitModule } from '@auth/rate-limit';
import { RefreshTokenModule } from '@auth/refresh-token';
import { UserService } from '@auth/user';

import { AuthController } from './auth.controller';

@Module({
    imports: [RateLimitModule, HttpModule, JwtModule.forRoot(jwtModuleOptions), RefreshTokenModule],
    controllers: [AuthController],
    providers: [AuthService, RecaptchaService, HashService, CredentialsRepository, UserService],
    exports: [UserService],
})
export class AuthModule {}
