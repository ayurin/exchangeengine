import { ApiProperty } from '@nestjs/swagger';
import { IsEmail, IsNotEmpty, IsString } from 'class-validator';

export class LoginBody {
    @ApiProperty({ example: 'saladin@haqqex.com' })
    @IsEmail()
    @IsNotEmpty()
    public readonly login!: string;

    @ApiProperty()
    @IsString()
    @IsNotEmpty()
    public readonly password!: string;
}
