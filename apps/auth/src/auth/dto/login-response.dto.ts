/* eslint-disable max-classes-per-file */
import { ApiProperty } from '@nestjs/swagger';

export class RenewResponse {
    @ApiProperty()
    public readonly accessToken: string;

    constructor(accessToken: string) {
        this.accessToken = accessToken;
    }
}

export class LoginResponse extends RenewResponse {
    @ApiProperty()
    public readonly refreshToken: string;

    constructor(accessToken: string, refreshToken: string) {
        super(accessToken);
        this.refreshToken = refreshToken;
    }
}
