import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString } from 'class-validator';

export class RefreshAccessBody {
    @ApiProperty()
    @IsString()
    @IsNotEmpty()
    public readonly refreshToken!: string;
}
