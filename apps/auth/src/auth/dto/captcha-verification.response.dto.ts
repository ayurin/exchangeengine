import { ApiProperty } from '@nestjs/swagger';

export class CaptchaVerificationResponse {
    @ApiProperty()
    public readonly result: boolean;

    constructor(result: boolean) {
        this.result = result;
    }
}
