import { MigrationInterface, QueryRunner } from 'typeorm';

import { CredentialsEntity } from '@auth/dao/entities';

export class Credentials1660639724453 implements MigrationInterface {
    public async up(qr: QueryRunner): Promise<void> {
        await qr.query(`
        CREATE TABLE "${CredentialsEntity.TABLE_NAME}" (
            "id" uuid NOT NULL DEFAULT uuid_generate_v4(),
            "login" character varying UNIQUE NOT NULL,
            "password_hash" character varying NOT NULL,
            "salt" character varying NOT NULL,
            CONSTRAINT "AUTH_PRIMARY_KEY" PRIMARY KEY ("id")
        )`);
    }

    public async down(qr: QueryRunner): Promise<void> {
        await qr.query(`DROP TABLE "${CredentialsEntity.TABLE_NAME}"`);
    }
}
