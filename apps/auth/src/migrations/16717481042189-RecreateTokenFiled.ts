import { MigrationInterface, QueryRunner } from 'typeorm';

import { RefreshTokenEntity } from '@auth/dao/entities/refresh-token.entity';

export class RecreateTokenFiled16717481042189 implements MigrationInterface {
    public async up(qr: QueryRunner): Promise<void> {
        await qr.query(`DROP INDEX "IDX_AUTH_REFRESH_TOKEN"`);
        await qr.query(`ALTER TABLE ${RefreshTokenEntity.TABLE_NAME} DROP COLUMN "token"`);
        await qr.query(
            `ALTER TABLE ${RefreshTokenEntity.TABLE_NAME} ADD "token" character varying NOT NULL DEFAULT ''`,
        );
        await qr.query(`CREATE INDEX "IDX_AUTH_REFRESH_TOKEN" ON ${RefreshTokenEntity.TABLE_NAME}("token");`);
    }

    public async down(qr: QueryRunner): Promise<void> {
        await qr.query(`DROP INDEX "IDX_AUTH_REFRESH_TOKEN"`);
        await qr.query(`ALTER TABLE ${RefreshTokenEntity.TABLE_NAME} DROP COLUMN "token"`);
        await qr.query(
            `ALTER TABLE ${RefreshTokenEntity.TABLE_NAME} ADD "token" character varying(40) NOT NULL DEFAULT ''`,
        );
        await qr.query(`CREATE INDEX "IDX_AUTH_REFRESH_TOKEN" ON ${RefreshTokenEntity.TABLE_NAME}("token");`);
    }
}
