import { MigrationInterface, QueryRunner } from 'typeorm';

import { RefreshTokenEntity } from '@auth/dao/entities/refresh-token.entity';

export class RefreshTokensTable1670840956231 implements MigrationInterface {
    public async up(qr: QueryRunner): Promise<void> {
        await qr.query(`
            CREATE TABLE ${RefreshTokenEntity.TABLE_NAME} (
                "id" uuid NOT NULL DEFAULT uuid_generate_v4(),
                "token" character varying(40) NOT NULL,
                "user_id" uuid NOT NULL,
                "is_revoked" boolean DEFAULT false,
                "created_at" TIMESTAMP NOT NULL DEFAULT now(), 
                "updated_at" TIMESTAMP NOT NULL DEFAULT now()
            );
        `);

        await qr.query(`CREATE INDEX "IDX_AUTH_REFRESH_TOKEN" ON ${RefreshTokenEntity.TABLE_NAME}("token");`);
    }

    public async down(qr: QueryRunner): Promise<void> {
        await qr.query(`DROP INDEX "IDX_AUTH_REFRESH_TOKEN"`);
        await qr.query(`DROP TABLE "${RefreshTokenEntity.TABLE_NAME}"`);
    }
}
