import { tfStringToNum } from '@app/utils';
import { ApiProperty } from '@nestjs/swagger';
import { Transform } from 'class-transformer';
import { IsNumber, IsOptional, Min } from 'class-validator';

export class PaginationDto {
    @ApiProperty({ example: 10, required: false })
    @Transform(({ value }) => tfStringToNum(value), { toClassOnly: true })
    @IsOptional()
    @IsNumber({ maxDecimalPlaces: 0 })
    @Min(1)
    public pageSize?: number;

    @ApiProperty({ example: 1, required: false })
    @Transform(({ value }) => tfStringToNum(value), { toClassOnly: true })
    @IsOptional()
    @IsNumber({ maxDecimalPlaces: 0 })
    @Min(1)
    public pageNumber?: number;

    public total?: number;
}
