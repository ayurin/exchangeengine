import { ApiProperty } from '@nestjs/swagger';

export class IdResponse {
    @ApiProperty({ example: 'ecc12d97-5347-488e-bead-b86945ec3f48' })
    public readonly id: string;

    constructor(id: string) {
        this.id = id;
    }
}
