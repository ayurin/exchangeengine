import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsUUID } from 'class-validator';

export class IdDto {
    @ApiProperty({ example: 'ecc12d97-5347-488e-bead-b86945ec3f48' })
    @IsUUID(4)
    @IsNotEmpty()
    public id: string;
}
