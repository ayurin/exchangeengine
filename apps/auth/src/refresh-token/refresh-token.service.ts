import * as crypto from 'crypto';

import { Injectable } from '@nestjs/common';
import moment from 'moment';
import ms from 'ms';

import { appConfig } from '@auth/config';
import { RefreshTokenEntity } from '@auth/dao/entities/refresh-token.entity';
import { RefreshTokenRepository } from '@auth/dao/refresh-token.repository';

import { ALGORITHM, REFRESH_TOKEN_LENGTH } from './constants/refresh-token.constants';
import { IRefreshToken } from './interfaces/refresh-token.interface';

type TRefresh = [string, RefreshTokenEntity];

@Injectable()
export class RefreshTokenService {
    constructor(private readonly refreshTokenRepository: RefreshTokenRepository) {}

    public async refresh(token: string): Promise<TRefresh | null> {
        const refreshTokenEntity = await this.findToken(token);

        if (!refreshTokenEntity) {
            return null;
        }

        const tokenValidationResult = this.validateToken(refreshTokenEntity);

        if (!tokenValidationResult) {
            return null;
        }

        return this.create(refreshTokenEntity.userId, refreshTokenEntity.id);
    }

    public async generate(userId: string): Promise<TRefresh> {
        return this.create(userId);
    }

    public async revokeAllTokens(userId: string): Promise<void> {
        await this.refreshTokenRepository.update({ userId, isRevoked: false }, { isRevoked: true });
    }

    private async findToken(refreshToken: string): Promise<RefreshTokenEntity | null> {
        const token = this.createHash(refreshToken);
        return this.refreshTokenRepository.findOne({ where: { token } });
    }

    private async create(userId: string, tokenId?: string): Promise<TRefresh> {
        const { token, hash } = this.generateRandomString(REFRESH_TOKEN_LENGTH);

        const baseTokenParams = {
            token: hash,
            userId,
            isRevoked: false,
            updatedAt: new Date(),
        };
        const tokenParams = tokenId ? { id: tokenId, ...baseTokenParams } : { ...baseTokenParams };

        const refreshTokenEntity = await this.refreshTokenRepository.save(tokenParams);
        return [token, refreshTokenEntity];
    }

    private generateRandomString(numberOfCharacters: number): IRefreshToken {
        const token = crypto.randomBytes(numberOfCharacters / 2).toString('hex');
        const hash = this.createHash(token);
        return { token, hash };
    }

    private validateToken(token: RefreshTokenEntity): boolean {
        const isExpired = moment(token.updatedAt)
            .add(ms(appConfig.JWT_REFRESH_EXPIRES) as moment.DurationInputArg1, 'ms')
            .isBefore(moment());
        return !token.isRevoked && !isExpired;
    }

    private createHash(refreshToken: string): string {
        return crypto.createHash(ALGORITHM).update(refreshToken).digest('hex');
    }
}
