export interface IRefreshToken {
    token: string;
    hash: string;
}
