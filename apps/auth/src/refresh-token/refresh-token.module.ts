import { Module } from '@nestjs/common';

import { RefreshTokenRepository } from '@auth/dao/refresh-token.repository';
import { RefreshTokenService } from '@auth/refresh-token/refresh-token.service';

@Module({
    providers: [RefreshTokenService, RefreshTokenRepository],
    exports: [RefreshTokenService],
})
export class RefreshTokenModule {}
