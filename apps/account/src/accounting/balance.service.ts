import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { DataSource } from 'typeorm';
import { BA_AVAILABLE } from '@account/transaction';
import { IGetBalanceResponse } from '@app/utils';

interface IAccountItem {
    account_number: string;
    currency_id?: string;
}

interface IAccountBalance extends IAccountItem {
    balance: number;
}

@Injectable()
export class BalanceService {
    constructor(protected dataSource: DataSource) {}
    // ----------------------------------------------------------------------------------------
    private getSqlForAccountList(ba_number: string, userid: string): string {
        return `select ua.account_number,
									 		 ua.currency_id
								from account.user_accounts ua
								inner join account.balance_accounts ba on ua.balance_account_number=ba.ba_number
								where balance_account_number ='${ba_number}'
								  and user_id='${userid}'`;
    }
    // ----------------------------------------------------------------------------------------
    public async getAvailableBalance(accountItem: IAccountItem): Promise<IAccountBalance> {
        const queryRunner = this.dataSource.createQueryRunner();
        const { balance } = (
            await queryRunner.query(`select coalesce(sum(case when credit_account_number='${accountItem.account_number}'
																															 and substring('${accountItem.account_number}' from 1 for 1)='C' then amount
																															when debit_account_number='${accountItem.account_number}'
																															 and substring('${accountItem.account_number}' from 1 for 1)='D' then amount
																															else -1*amount end),0) as balance
																		 from account.entries
																		 where credit_account_number='${accountItem.account_number}'
																		    or debit_account_number='${accountItem.account_number}'`)
        )[0];
        return {
            ...accountItem,
            balance,
        };
    }
    // ----------------------------------------------------------------------------------------
    public async getAvailableBalanceList(userid: string): Promise<IGetBalanceResponse[]> {
        const queryRunner = this.dataSource.createQueryRunner();
        const accountList = await queryRunner.query(this.getSqlForAccountList(BA_AVAILABLE, userid));
        const balanceList = await Promise.all(accountList.map(i => this.getAvailableBalance(i)));
        return balanceList;
    }

    public async exceptionWhenInsufficientFunds(account_number: string, amount: bigint): Promise<void> {
        const res = await this.getAvailableBalance({account_number})
        if(res.balance<amount){
            console.log('Insufficient funds at '+account_number+'. Need '+amount.toString())
            throw new HttpException('Insufficient funds', HttpStatus.NOT_ACCEPTABLE);
        }
    }
    // ----------------------------------------------------------------------------------------
}
