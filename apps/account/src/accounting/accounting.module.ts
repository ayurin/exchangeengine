import { Module } from '@nestjs/common';
import { AccountingService } from './accounting.service';
import { AccountingController } from './accounting.controller';
import { BalanceService } from './balance.service';

@Module({
  providers: [AccountingService, BalanceService],
  controllers: [AccountingController],
  exports: [AccountingService, BalanceService]
})
export class AccountingModule {}
