import { Controller, Get, Param, Query } from '@nestjs/common';
import { Ctx, MessagePattern, Payload, RmqContext } from '@nestjs/microservices';
import { AccountingService, EntryType } from './accounting.service';
import { RmqWraper } from '@app/utils';
// import { TopupFromFaucetDto } from './dto/topup-from-faucet.dto';
import { FromOrderDto } from './dto/from-order.dto';
import { ACCOUNT_PLACE_ORDER_PATTERN, ACCOUNT_PLACE_ORDER_STORNO_PATTERN, ACCOUNT_GET_BALANCE_PATTERN, IGetBalanceResponse, ACCOUNT_SETTLE_ORDER_PATTERN, ISettleOrder } from '@app/utils';
import { BalanceService } from './balance.service';
import { GetBalanceDto } from './dto/get-balance.dto';

@Controller()
export class AccountingController {
    constructor(
        private readonly accountingService: AccountingService,
        private readonly balanceService: BalanceService,
    ) {}

    // @Get('bal')
    // public async balance(@Query() params: any):Promise<any> {
    //   console.log(params)
    //   return this.balanceService.getAvailableBalanceList(params.userid)
    // }

    // @MessagePattern()
    // @RmqWraper
    // public async deposit(@Payload() data: TopupFromFaucetDto, @Ctx() _context: RmqContext): Promise<void> {
    //     this.accountingService.deposit(data);
    // }

    // @MessagePattern(ACCOUNT_GET_BALANCE_PATTERN)
    // @RmqWraper
    // public async availableBalance(@Payload() data: GetBalanceDto, @Ctx() _context: RmqContext): Promise<IGetBalanceResponse> {
    //     return this.balanceService.getAvailableBalanceList(data.userId);
    // }

    @MessagePattern(ACCOUNT_PLACE_ORDER_PATTERN)
    @RmqWraper
    public async placeOrder(@Payload() data: FromOrderDto, @Ctx() _context: RmqContext): Promise<boolean> {
        return this.accountingService.placeOrder(data, EntryType.DIRECT);
    }

    @MessagePattern(ACCOUNT_PLACE_ORDER_STORNO_PATTERN)
    @RmqWraper
    public async placeOrderStorno(@Payload() data: FromOrderDto, @Ctx() _context: RmqContext): Promise<boolean> {
        return this.accountingService.placeOrder(data, EntryType.STORNO);
    }

    // @MessagePattern(ACCOUNT_SETTLE_ORDER_PATTERN)
    // @RmqWraper
    // public async settle(@Payload() data: any, @Ctx() context: RmqContext): Promise<void> {
    // 	return this.accountingService.deal(data, EntryType.DIRECT);
    // }

    // @MessagePattern()
    // public async cancelOrder(@Payload() data: any, @Ctx() context: RmqContext): Promise<void> {
    // 		return await warper(this.accountingService.xxx(data), context)
    // }
    // @MessagePattern()
    // public async trade(@Payload() data: any, @Ctx() context: RmqContext): Promise<void> {
    // 		return await warper(this.accountingService.xxx(data), context)
    // }
    // @MessagePattern()
    // public async deposit(@Payload() data: any, @Ctx() context: RmqContext): Promise<void> {
    // 		return await warper(this.accountingService.xxx(data), context)
    // }
    // @MessagePattern()
    // public async withdrawalApplication(@Payload() data: any, @Ctx() context: RmqContext): Promise<void> {
    // 		return await warper(this.accountingService.xxx(data), context)
    // }
    // @MessagePattern()
    // public async withdrawal(@Payload() data: any, @Ctx() context: RmqContext): Promise<void> {
    // 		return await warper(this.accountingService.xxx(data), context)
    // }
}
