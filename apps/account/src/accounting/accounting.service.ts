import { Injectable } from '@nestjs/common';
import { SQLRaWraper } from '@app/utils';
import { DataSource, QueryRunner } from 'typeorm';
import { randomUUID } from 'crypto';
import { createAccountIfNotExists } from './helpers';
import { TopupFromFaucetDto } from './dto/topup-from-faucet.dto';
import { FromOrderDto } from './dto/from-order.dto';
import { SettleOrderDto } from './dto/settle-order.dto';
import { DealDto } from './dto/deal.dto';
import { BalanceService } from './balance.service';

const toString = (data) =>
    JSON.stringify(data, (_key, value) => (typeof value === 'bigint' ? value.toString() : value));

export enum EntryType {
    DIRECT,
    STORNO,
}

// For Platform
export const PLATFORM_ID = '00000000-0000-0000-0000-000000000000';
export const BA_CASH = 'D1600';
export const BA_BROKER_FEE = 'C6110';

// For Customer
export const BA_AVAILABLE = 'C2600';
export const BA_LOCKED = 'C2601';

//
export const CreateAccountIfNotExists = (_target: Object, _key: String, descriptor: any) => {
    const originalMethod = descriptor.value;
    descriptor.value = async function (...args: any[]): Promise<any> {
        const [data, queryRunner] = args;
        await createAccountIfNotExists(
            {
                customerId: data.customerId,
                currency_code: data.currency.toUpperCase(),
            },
            queryRunner,
        );
        const res = await originalMethod.apply(this, args);
        return res;
    };
    return descriptor;
};

@Injectable()
export class AccountingService {
    constructor(protected dataSource: DataSource, private balanceService: BalanceService) {}
    // -------------------------------------------------------------------------------------------------------------------
    @SQLRaWraper
    public async deposit(data: TopupFromFaucetDto, queryRunner?: QueryRunner): Promise<void> {
        await createAccountIfNotExists(
            { customerId: data.customerId, currency_code: data.currency.toUpperCase() },
            queryRunner,
        );
        const oper_uuid4 = randomUUID();
        await queryRunner!.query(`insert into operations(type,
                                                        status,
                                                        user_ids,
                                                        as_of_date,
                                                        id,
                                                        metadata)
                                                 values('topupFromFaucet',
                                                        'confirmed',
                                                        array['${data.customerId}']::uuid[],
                                                        to_timestamp(${data.as_of_date}),
                                                        '${oper_uuid4}',
                                                        '${toString(data)}'::json)`);
        const accountNumDeb = `${BA_CASH}x${data.currency.toUpperCase()}x${PLATFORM_ID}`;
        const accountNumCred = `${BA_AVAILABLE}x${data.currency.toUpperCase()}x${data.customerId}`;
        await queryRunner!.query(`insert into entries(debit_account_id,
                                                        debit_account_number,
                                                        credit_account_id,
                                                        credit_account_number,
                                                        amount,
                                                        currency,
                                                        as_of_date,
                                                        operation_id,
                                                        metadata)
                                                 select deb.id as debit_account_id,
                                                        deb.account_number as debit_account_number,
                                                        cred.id as credit_account_id,
                                                        cred.account_number as credit_account_number,
                                                        ${data.amount} as amount,
                                                        deb.currency_cd as currency,
                                                        now() as as_of_date,
                                                        '${oper_uuid4}',
                                                        '${toString(data)}'::json
                                                 from user_accounts deb
                                                 cross join user_accounts cred
                                                 where deb.account_number='${accountNumDeb}'
                                                   and cred.account_number='${accountNumCred}'`);
    }
    // -------------------------------------------------------------------------------------------------------------------
    @SQLRaWraper
    public async withdraw(data: any, queryRunner?: QueryRunner): Promise<void> {
        await createAccountIfNotExists(
            { customerId: data.customerId, currency_code: data.currency.toUpperCase() },
            queryRunner,
        );
        const accountNumCred = `${BA_CASH}x${data.currency.toUpperCase()}x${PLATFORM_ID}`;
        const accountNumDeb = `${BA_AVAILABLE}x${data.currency.toUpperCase()}x${data.customerId}`;
        const oper_uuid4 = randomUUID();
        await this.balanceService.exceptionWhenInsufficientFunds(accountNumDeb, data.amount)

        await queryRunner!.query(`insert into operations(type,
                                                    status,
                                                    user_ids,
                                                    as_of_date,
                                                    id,
                                                    metadata)
                                             values('withdraw',
                                                    'confirmed',
                                                    array['${data.customerId}']::uuid[],
                                                    to_timestamp(${data.as_of_date}),
                                                    '${oper_uuid4}',
                                                    '${toString(data)}'::json)`);

        await queryRunner!.query(`insert into entries(debit_account_id,
                                                    debit_account_number,
                                                    credit_account_id,
                                                    credit_account_number,
                                                    amount,
                                                    currency,
                                                    as_of_date,
                                                    operation_id,
                                                    metadata)
                                             select deb.id as debit_account_id,
                                                    deb.account_number as debit_account_number,
                                                    cred.id as credit_account_id,
                                                    cred.account_number as credit_account_number,
                                                    ${data.amount} as amount,
                                                    deb.currency_cd as currency,
                                                    now() as as_of_date,
                                                    '${oper_uuid4}',
                                                    '${toString(data)}'::json
                                             from user_accounts deb
                                             cross join user_accounts cred
                                             where deb.account_number='${accountNumDeb}'
                                               and cred.account_number='${accountNumCred}'`);
    }
    // -------------------------------------------------------------------------------------------------------------------
    @SQLRaWraper
    public async placeOrder(data: FromOrderDto, entryType: EntryType, queryRunner?: QueryRunner): Promise<boolean> {
        await createAccountIfNotExists(
            { customerId: data.customerId, currency_code: data.currency.toUpperCase() },
            queryRunner,
        );

        let accountNumDeb, accountNumCred, fee_accountNumDeb, fee_accountNumFeeCred;
        if (entryType === EntryType.DIRECT) {
            accountNumDeb = `${BA_AVAILABLE}x${data.currency.toUpperCase()}x${data.customerId}`;
            accountNumCred = `${BA_LOCKED}x${data.feeCurrency.toUpperCase()}x${data.customerId}`;
            fee_accountNumDeb = `${BA_AVAILABLE}x${data.feeCurrency.toUpperCase()}x${data.customerId}`;
            fee_accountNumFeeCred = `${BA_LOCKED}x${data.feeCurrency.toUpperCase()}x${data.customerId}`;
        } else {
            accountNumCred = `${BA_AVAILABLE}x${data.currency.toUpperCase()}x${data.customerId}`;
            accountNumDeb = `${BA_LOCKED}x${data.feeCurrency.toUpperCase()}x${data.customerId}`;
            fee_accountNumFeeCred = `${BA_AVAILABLE}x${data.feeCurrency.toUpperCase()}x${data.customerId}`;
            fee_accountNumDeb = `${BA_LOCKED}x${data.feeCurrency.toUpperCase()}x${data.customerId}`;
        }

        await this.balanceService.exceptionWhenInsufficientFunds(accountNumDeb, BigInt(data.amount)+BigInt(data.fee))

        const oper_uuid4 = randomUUID();
        await queryRunner!.query(`insert into operations(type,
                                                        status,
                                                        user_ids,
                                                        as_of_date,
                                                        id,
                                                        metadata)
                                                 values('placeOrder',
                                                        '${entryType === EntryType.DIRECT ? 'confirmed' : 'storno'}',
                                                        array['${data.customerId}']::uuid[],
                                                        to_timestamp(${data.asOfDate}),
                                                        '${oper_uuid4}',
                                                        '${toString(data)}'::json)`);
        
        await queryRunner!.query(`insert into entries(debit_account_id,
                                                        debit_account_number,
                                                        credit_account_id,
                                                        credit_account_number,
                                                        amount,
                                                        currency,
                                                        as_of_date,
                                                        operation_id,
                                                        metadata)
                                                 select deb.id as debit_account_id,
                                                        deb.account_number as debit_account_number,
                                                        cred.id as credit_account_id,
                                                        cred.account_number as credit_account_number,
                                                        ${data.amount} as amount,
                                                        '${data.currency}' as currency,
                                                        now() as as_of_date,
                                                        '${oper_uuid4}'::uuid,
                                                        '${toString(data)}'::json
                                                 from user_accounts deb
                                                 cross join user_accounts cred
                                                 where deb.account_number='${accountNumDeb}'
                                                   and cred.account_number='${accountNumCred}'
                                                 union all
                                                 select deb.id as debit_account_id,
                                                        deb.account_number as debit_account_number,
                                                        cred.id as credit_account_id,
                                                        cred.account_number as credit_account_number,
                                                        ${data.fee} as amount,
                                                        '${data.feeCurrency}' as currency,
                                                        now() as as_of_date,
                                                        '${oper_uuid4}'::uuid,
                                                        '${toString(data)}'::json
                                                 from user_accounts deb
                                                 cross join user_accounts cred
                                                 where deb.account_number='${fee_accountNumDeb}'
                                                   and cred.account_number='${fee_accountNumFeeCred}'`);
        return true;
    }
    // -------------------------------------------------------------------------------------------------------------------
    @SQLRaWraper
    public async cancelOrder(data: FromOrderDto, queryRunner?: QueryRunner): Promise<void> {
        const oper_uuid4 = randomUUID();
        await queryRunner!.query(`insert into operations(type,
                                                        status,
                                                        user_ids,
                                                        as_of_date,
                                                        id,
                                                        metadata)
                                                 values('cancelOrder',
                                                        'confirmed',
                                                        array['${oper_uuid4}']::uuid[],
                                                        to_timestamp(${data.asOfDate}),
                                                        '${oper_uuid4}',
                                                        '${toString(data)}'::json)`);

        const accountNumDeb = `${BA_AVAILABLE}x${data.currency.toUpperCase()}x${data.customerId}`;
        const accountNumCred = `${BA_LOCKED}x${data.feeCurrency.toUpperCase()}x${data.customerId}`;
        const fee_accountNumDeb = `${BA_AVAILABLE}x${data.feeCurrency.toUpperCase()}x${data.customerId}`;
        const fee_accountNumFeeCred = `${BA_LOCKED}x${data.feeCurrency.toUpperCase()}x${data.customerId}`;
        await queryRunner!.query(`insert into entries(debit_account_id,
                                                        debit_account_number,
                                                        credit_account_id,
                                                        credit_account_number,
                                                        amount,
                                                        currency,
                                                        as_of_date,
                                                        operation_id,
                                                        metadata)
                                                 select deb.id as debit_account_id,
                                                        deb.account_number as debit_account_number,
                                                        cred.id as credit_account_id,
                                                        cred.account_number as credit_account_number,
                                                        ${data.amount} as amount,
                                                        ba1.currency_cd as currency,
                                                        now() as as_of_date,
                                                        '${oper_uuid4}',
                                                        '${toString(data)}'::json
                                                 from user_accounts deb
                                                 cross join user_accounts cred
                                                 where deb.account_number='${accountNumDeb}'
                                                   and cred.account_number='${accountNumCred}'
                                                 union all
                                                 select deb.id as debit_account_id,
                                                        deb.account_number as debit_account_number,
                                                        cred.id as credit_account_id,
                                                        cred.account_number as credit_account_number,
                                                        ${data.fee} as amount,
                                                        ba1.currency_cd as currency,
                                                        now() as as_of_date,
                                                        '${oper_uuid4}',
                                                        '${toString(data)}'::json
                                                 from user_accounts deb
                                                 cross join user_accounts cred
                                                 where deb.account_number='${fee_accountNumDeb}'
                                                   and cred.account_number='${fee_accountNumFeeCred}'`);
    }
    // -------------------------------------------------------------------------------------------------------------------
//     @SQLRaWraper
//     public async deal(data: DealDto, queryRunner?: QueryRunner): Promise<void> {
//         const oper_uuid4 = randomUUID();
//         await queryRunner!.query(`insert into operations(type,
//                                                         status,
//                                                         user_ids,
//                                                         as_of_date,
//                                                         id,
//                                                         metadata)
//                                                  values('settleOrder',
//                                                         'confirmed',
//                                                         array['${data.taker.customerId}', '${data.maker.customerId}']::uuid[],
//                                                         to_timestamp(${data.as_of_date}),
//                                                         '${oper_uuid4}',
//                                                         '${toString(data)}'::json)`);

//         const taker = `${BA_LOCKED}x${data.currency.shortName.toUpperCase()}x${data.customerId}`

//         // const accountNumDeb = `${BA_AVAILABLE}x${data.currency.shortName.toUpperCase()}x${data.customerId}`;
//         // const accountNumCred = `${BA_LOCKED}x${data.feeCurrency.shortName.toUpperCase()}x${data.customerId}`;
//         // const fee_accountNumDeb = `${BA_AVAILABLE}x${data.feeCurrency.shortName.toUpperCase()}x${data.customerId}`;
//         // const fee_accountNumFeeCred = `${BA_LOCKED}x${data.feeCurrency.shortName.toUpperCase()}x${data.customerId}`;

//         `${BA_LOCKED}x${data.buyOrder.}x${data.buyOrder.customerId}`

//         await queryRunner!.query(`insert into entries(debit_account_id,
//                                                         debit_account_number,
//                                                         credit_account_id,
//                                                         credit_account_number,
//                                                         amount,
//                                                         currency,
//                                                         as_of_date,
//                                                         operation_id,
//                                                         metadata)
//                                                  select deb.id as debit_account_id,
//                                                         deb.account_number as debit_account_number,
//                                                         cred.id as credit_account_id,
//                                                         cred.account_number as credit_account_number,
//                                                         ${data.amount} as amount,
//                                                         ba1.currency_cd as currency,
//                                                         now() as as_of_date,
//                                                         '${oper_uuid4}',
//                                                         '${toString(data)}'::json
//                                                  from user_accounts deb
//                                                  cross join user_accounts cred
//                                                  where deb.account_number='${accountNumDeb}'
//                                                    and cred.account_number='${accountNumCred}'
//                                                  union all
//                                                  select deb.id as debit_account_id,
//                                                         deb.account_number as debit_account_number,
//                                                         cred.id as credit_account_id,
//                                                         cred.account_number as credit_account_number,
//                                                         ${data.fee} as amount,
//                                                         ba1.currency_cd as currency,
//                                                         now() as as_of_date,
//                                                         '${oper_uuid4}',
//                                                         '${toString(data)}'::json
//                                                  from user_accounts deb
//                                                  cross join user_accounts cred
//                                                  where deb.account_number='${fee_accountNumDeb}'
//                                                    and cred.account_number='${fee_accountNumFeeCred}'`);
//     }
    // -------------------------------------------------------------------------------------------------------------------
}
