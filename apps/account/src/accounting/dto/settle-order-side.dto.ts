import { Type } from 'class-transformer';
import { IsOptional, IsUUID, ValidateNested } from 'class-validator';

import { CurrencyAmountDto } from '@account/transaction/dto';
import { IsUser } from '@account/validators';

export class SettleOrderSideDto {
    constructor(partial: Partial<SettleOrderSideDto>) {
        Object.assign(this, partial);
    }

    @IsUUID()
    public orderId: string;

    @IsUser()
    public customerId: string;

    @IsOptional()
    @ValidateNested()
    @Type(() => CurrencyAmountDto)
    public forExchange: CurrencyAmountDto;

    @IsOptional()
    @ValidateNested()
    @Type(() => CurrencyAmountDto)
    public forFee: CurrencyAmountDto;

    @IsOptional()
    @ValidateNested()
    @Type(() => CurrencyAmountDto)
    public forRefunds?: CurrencyAmountDto;

    @IsOptional()
    @ValidateNested()
    @Type(() => CurrencyAmountDto)
    public forFeeRefunds?: CurrencyAmountDto;
}
