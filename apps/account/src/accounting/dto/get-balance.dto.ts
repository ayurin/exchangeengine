import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsUUID } from 'class-validator';

export class GetBalanceDto {
		@ApiProperty({ example: '079aaaf7-4f56-4b0a-ad91-2699f8e3f8e3' })
		@IsNotEmpty()
		@IsUUID()
    public userId: string;
}
