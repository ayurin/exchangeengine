import { EOrderType } from "@app/utils";
import { ApiProperty } from "@nestjs/swagger";
import { Type } from "class-transformer";
import { IsNotEmpty, IsNumberString, IsUUID, ValidateNested } from "class-validator";

export class OrderDto {
	@ApiProperty({ example: '079aaaf7-4f56-4b0a-ad91-2699f8e3f8e3' })
	@IsNotEmpty()
	@IsUUID()
	public orderId!: string;

	@ApiProperty({ example: '079aaaf7-4f56-4b0a-ad91-2699f8e3f8e3' })
	@IsNotEmpty()
	@IsUUID()
	public customerId!: string;

	@IsNotEmpty()
	public pair!: string;

	@IsNumberString()
	@IsNotEmpty()
	public quantity!: number;

	@ApiProperty({ example: ['buy', 'sell'] })
	@IsNotEmpty()
	public side!: 'sell'|'buy';

	@ApiProperty({ example: EOrderType })
	@IsNotEmpty()
	public type!: EOrderType;
}

export class DealDto {
	@ApiProperty({ example: '079aaaf7-4f56-4b0a-ad91-2699f8e3f8e3' })
	@IsUUID()
	public tradeId!: string;

	@ApiProperty({ example: '1672724076' })
	@IsNumberString()
	@IsNotEmpty()
	public as_of_date!: number;

	@ApiProperty({ example: '10000000' })
	@IsNotEmpty()
	public volume!: number;

	@ApiProperty({ example: 'ETC/ETH' })
	@IsNotEmpty()
	public pair!: string;

	@ApiProperty({ example: OrderDto })
	@ValidateNested()
	@Type(() => OrderDto)
	public taker: OrderDto;

	@ApiProperty({ example: OrderDto })
	@ValidateNested()
	@Type(() => OrderDto)
	public maker: OrderDto;


}
