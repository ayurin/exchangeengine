import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsNumberString, IsString } from 'class-validator';

import { CurrencyEntity } from '@account/dao';
import { IsUser } from '@account/validators';

export class TopupFromFaucetDto {
    @ApiProperty({ example: '02e2d713-0c30-44a3-aeb8-3ec849384d0e' })
    @IsUser()
    public customerId: string;

    @ApiProperty({ example: 'BTC', type: 'string' })
    @IsString()
    @IsNotEmpty()
    // CurrencyPipe string => CurrencyEntity
    public currency: string;

    @ApiProperty({ example: (10_0000_0000).toString() })
    @IsNumberString()
    @IsNotEmpty()
    public amount: string;

    @ApiProperty({ example: '1672724076' })
    // @IsNumberString()
    @IsNotEmpty()
    public as_of_date: number;
}
