import { IsString } from 'class-validator';

export class CurrencyAmountDto {
    constructor(partial: Partial<CurrencyAmountDto>) {
        Object.assign(this, partial);
    }

    @IsString()
    public currency: string;

    @IsString()
    public amount: string;
}
