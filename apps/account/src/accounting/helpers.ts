import { QueryRunner } from "typeorm"
import { BA_AVAILABLE, BA_LOCKED, BA_CASH, BA_BROKER_FEE, PLATFORM_ID} from './accounting.service'

interface IAccountsList {
	baNumber: string;
	customerId: string;
	currency_code: string;
	accountName?: string;
	accountNumber: string;
}
interface IArgs {
	customerId: string;
	currency_code: string;
}

export const createAccountIfNotExists = async (data: IArgs, queryRunner?: QueryRunner): Promise<void> => {
	const accList: IAccountsList[] = [
		{
			baNumber: BA_BROKER_FEE,
			customerId: PLATFORM_ID,
			currency_code: data.currency_code,
			accountNumber: `${BA_BROKER_FEE.toUpperCase()}x${data.currency_code.toUpperCase()}x${PLATFORM_ID}`,
		},
		{
			baNumber: BA_CASH,
			customerId: PLATFORM_ID,
			currency_code: data.currency_code,
			accountNumber: `${BA_CASH .toUpperCase()}x${data.currency_code.toUpperCase()}x${PLATFORM_ID}`,
		},
		{
			baNumber: BA_AVAILABLE,
			customerId: data.customerId,
			currency_code: data.currency_code,
			accountNumber: `${BA_AVAILABLE.toUpperCase()}x${data.currency_code.toUpperCase()}x${data.customerId}`,
		},
		{
			baNumber: BA_LOCKED,
			customerId: data.customerId,
			currency_code: data.currency_code,
			accountNumber: `${BA_LOCKED.toUpperCase()}x${data.currency_code.toUpperCase()}x${data.customerId}`,
		},
	]
	await Promise.all(accList.map(i => queryRunner!.query(`insert into user_accounts(account_number,
																															balance_account_number,
																															currency_id,
																															currency_cd,
																															account_name,
																															user_id,
																															debit_flg)
																													select '${i.accountNumber}',
																																'${i.baNumber}',
																																c.id,
																																c.short_name,
																																'${i.accountName||i.baNumber}',
																																'${i.customerId}',
																																case when substring('${i.baNumber.toUpperCase()}' from 1 for 1)='D'
																																		 then true else false end
																													from currencies c
																													where c.short_name ='${i.currency_code.toUpperCase()}'
																														and not exists (select 1
																																			from user_accounts
																																			where account_number='${i.accountNumber}')`)))
}
