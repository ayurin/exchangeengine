import { ACCOUNT_REGISTRATION_USER_PATTERN } from '@app/utils';
import { Body, ConflictException, Controller, HttpStatus, Post } from '@nestjs/common';
import { Ctx, MessagePattern, Payload, RmqContext } from '@nestjs/microservices';
import { ApiBody, ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { Channel, Message } from 'amqplib';

import { IdResponse } from '@account/dto';
import { RegistrationBody } from '@account/registration/dto';
import { RegistrationService } from '@account/registration/registration.service';

@Controller('registration')
@ApiTags('registration')
export class RegistrationController {
    constructor(private registrationService: RegistrationService) {}

    @Post()
    @ApiOperation({ summary: 'Register new user' })
    @ApiBody({ type: RegistrationBody })
    @ApiResponse({ status: HttpStatus.OK, type: IdResponse })
    public async createUser(@Body() body: RegistrationBody): Promise<IdResponse> {
        const newUserId = await this.registrationService.registerUser(body);
        if (newUserId === null) {
            throw new ConflictException('Login already taken');
        }
        return new IdResponse(newUserId);
    }

    @MessagePattern(ACCOUNT_REGISTRATION_USER_PATTERN)
    public async rmqCreateUser(@Payload() body: RegistrationBody, @Ctx() context: RmqContext): Promise<IdResponse> {
        const channel = context.getChannelRef() as Channel;
        const originalMsg = context.getMessage() as Message;
        channel.ack(originalMsg);
        const newUserId = await this.registrationService.registerUser(body);
        if (newUserId === null) {
            throw new ConflictException('Login already taken');
        }
        return new IdResponse(newUserId);
    }
}
