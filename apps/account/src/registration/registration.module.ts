import { Module } from '@nestjs/common';

import { UserRepository } from '@account/dao';
import { RegistrationRmqClient } from '@account/registration/registration-rmq.client';

import { RegistrationController } from './registration.controller';
import { RegistrationService } from './registration.service';

@Module({
    controllers: [RegistrationController],
    providers: [RegistrationService, UserRepository, RegistrationRmqClient],
})
export class RegistrationModule {}
