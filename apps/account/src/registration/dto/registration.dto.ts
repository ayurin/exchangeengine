// import { IRegistrationBody } from '@app/utils';
import { ApiProperty } from '@nestjs/swagger';
import { IsEmail, IsNotEmpty, IsString } from 'class-validator';

export class RegistrationBody /*implements IRegistrationBody*/ {
    @ApiProperty({ example: 'Yusuf ibn Ayyub ibn Shadi' })
    @IsString()
    @IsNotEmpty()
    public readonly fullName!: string;

    @ApiProperty({ example: 'Saladin' })
    @IsString()
    @IsNotEmpty()
    public readonly shortName!: string;

    @ApiProperty({ example: 'saladin@haqqex.com' })
    @IsEmail()
    @IsNotEmpty()
    public readonly email!: string;

    @ApiProperty()
    @IsString()
    @IsNotEmpty()
    public readonly password!: string;
}
