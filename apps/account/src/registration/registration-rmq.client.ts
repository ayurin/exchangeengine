import {
    AUTH_CHECK_LOGIN_EXISTING_PATTERN,
    AUTH_CONTINUE_REGISTRATION_PATTERN,
    AUTH_REGISTRATION_PATTERN,
    Exists,
    RmqClientService,
    RmqResult,
} from '@app/utils';
import { RabbitService } from '@app/utils';
import { Injectable } from '@nestjs/common';

@Injectable()
export class RegistrationRmqClient {
    constructor(private rmqClientService: RmqClientService, private rabbitService: RabbitService) {}

    public checkLoginExisting(login: string): Promise<Exists> {
        const message = this.rabbitService.buildMessage({ login });
        return this.rmqClientService.send(AUTH_CHECK_LOGIN_EXISTING_PATTERN, message);
    }

    public sendRegistrationDataToAuth(userId: string, login: string, password: string): Promise<RmqResult> {
        const message = this.rabbitService.buildMessage({
            userId,
            login,
            password,
        });
        return this.rmqClientService.send(AUTH_REGISTRATION_PATTERN, message);
    }

    public sendContinueRegistrationToAuth(userId: string, login: string, password: string): Promise<RmqResult> {
        const message = this.rabbitService.buildMessage({
            userId,
            login,
            password,
        });
        return this.rmqClientService.send(AUTH_CONTINUE_REGISTRATION_PATTERN, message);
    }
}
