import { Injectable } from '@nestjs/common';

import { EUserStatus, UserRepository } from '@account/dao';
import { RegistrationRmqClient } from '@account/registration/registration-rmq.client';

@Injectable()
export class RegistrationService {
    constructor(
        private readonly userRepository: UserRepository,
        private readonly registrationRmqClient: RegistrationRmqClient,
    ) {}

    public async registerUser({
        fullName,
        shortName,
        email,
        password,
    }: {
        fullName: string;
        shortName: string;
        email: string;
        password: string;
    }): Promise<string | null> {
        const existingUser = await this.userRepository.findOne({ where: { email } });

        if (existingUser === null) {
            return this.startNewRegistration(fullName, shortName, email, password);
        }

        if (existingUser.status !== EUserStatus.CREATED) {
            return null;
        }

        return this.continueRegistration(existingUser.id, email, password);
    }

    private async startNewRegistration(
        fullName: string,
        shortName: string,
        email: string,
        password: string,
    ): Promise<string | null> {
        const loginExisting = await this.registrationRmqClient.checkLoginExisting(email);
        if (loginExisting.exists) {
            return null;
        }

        const newUser = await this.userRepository.save({ fullName, shortName, email });
        const userId = newUser.id;

        const result = await this.registrationRmqClient.sendRegistrationDataToAuth(userId, email, password);
        if (!result.success) {
            throw new Error(result.message);
        }

        await this.userRepository.update(userId, { status: EUserStatus.REGISTERED });

        return userId;
    }

    private async continueRegistration(userId: string, login: string, password: string): Promise<string | null> {
        const result = await this.registrationRmqClient.sendContinueRegistrationToAuth(userId, login, password);
        if (!result.success) {
            throw new Error(result.message);
        }

        await this.userRepository.update(userId, { status: EUserStatus.REGISTERED });
        return userId;
    }
}
