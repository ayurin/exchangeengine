import { AxiosModule } from '@app/utils';
import { Module } from '@nestjs/common';

import { NetworkCurrencyRepository, UserAddressRepository } from '@account/dao';

import { AddressesController } from './addresses.controller';
import { AddressesService } from './addresses.service';

@Module({
    imports: [AxiosModule],
    controllers: [AddressesController],
    providers: [AddressesService, NetworkCurrencyRepository, UserAddressRepository],
})
export class AddressesModule {}
