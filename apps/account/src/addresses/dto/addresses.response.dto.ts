import { ApiProperty } from '@nestjs/swagger';

import { TAddressResult } from '../addresses.service';

export class AddressesResponseDto {
    @ApiProperty()
    public currencyId: string;

    @ApiProperty()
    public address: string;

    @ApiProperty()
    public addressId: string;

    @ApiProperty()
    public networkId: string;

    constructor(item: TAddressResult) {
        this.address = item.address;
        this.addressId = item.addressId;
        this.currencyId = item.networkCurrency?.currencyId;
        this.networkId = item.networkCurrency?.networkId;
    }
}

export class OneAddressesResponseDto {
    @ApiProperty({ type: AddressesResponseDto })
    public result: AddressesResponseDto;

    constructor(item: TAddressResult) {
        this.result = new AddressesResponseDto(item);
    }
}

export class BoolResponse {
    @ApiProperty({ type: 'boolean', example: true })
    public result: boolean;

    constructor(result = true) {
        this.result = result;
    }
}
