import { ApiProperty } from '@nestjs/swagger';
import { IsEnum, IsNotEmpty, IsString, IsUUID } from 'class-validator';

import { EBlockchains } from '@account/network/network.service';

export class AddressesQueryRequestDto {
    @ApiProperty()
    @IsUUID()
    @IsNotEmpty()
    public networkId: string;

    @ApiProperty()
    @IsUUID()
    @IsNotEmpty()
    public currencyId: string;
}

export class ValidateAddressesPostRequestDto {
    @ApiProperty({ type: 'string', required: true, example: 'bc1qzqh3nqsrgt37kqxzzqpqn8hy39fh29fjrc9j4r' })
    @IsString()
    @IsNotEmpty()
    public address: string;

    @ApiProperty({ type: 'string', required: true, example: EBlockchains.Bitcoin, enum: Object.values(EBlockchains) })
    @IsEnum(EBlockchains)
    @IsNotEmpty()
    public network: EBlockchains;
}
