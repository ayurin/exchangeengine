import { join } from 'path';

import { AxiosService } from '@app/utils';
import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { bech32 } from 'bech32';
import * as addressValidator from 'multicoin-address-validator';

import { appConfig } from '@account/config';
import { NetworkCurrencyEntity, NetworkCurrencyRepository, UserAddressEntity, UserAddressRepository } from '@account/dao';
import { EBlockchains } from '@account/network/network.service';

import { AddressesQueryRequestDto } from './dto';

export type TAddressResult = Omit<UserAddressEntity, 'update'> & { address: string };
@Injectable()
export class AddressesService {
    constructor(
        private readonly axiosService: AxiosService,
        private readonly networkCurrencyRepository: NetworkCurrencyRepository,
        private readonly userAddressRepository: UserAddressRepository,
    ) {}

    public async ensureAddress(userId: string, params: AddressesQueryRequestDto): Promise<TAddressResult> {
        const networkCurrency = await this.networkCurrencyRepository.findOne({
            where: params,
            relations: ['currency', 'network'],
        });

        if (!networkCurrency) {
            throw new HttpException('Unavailable network for currency', HttpStatus.BAD_REQUEST);
        }

        const userAddress = await this.userAddressRepository.findWithRelations({
            networkCurrencyId: networkCurrency.id,
            userId,
        });

        if (userAddress?.addressId) {
            const result = await this.request<{ address: string }>({
                pathname: `address/${userAddress.addressId}`,
            });

            return { ...userAddress, address: result?.address };
        }

        const { address, addressId } = await this.createAddress(networkCurrency);

        await this.userAddressRepository.save(
            new UserAddressEntity({
                networkCurrencyId: networkCurrency.id,
                userId,
                addressId,
            }),
        );

        const newUserAddress = await this.userAddressRepository.findWithRelations({
            networkCurrencyId: networkCurrency.id,
            userId,
        });

        if (!newUserAddress) {
            throw new HttpException('Address not found', HttpStatus.NOT_FOUND);
        }

        return { ...newUserAddress, address };
    }

    public validate(options: { address: string; network: EBlockchains }): boolean {
        const { address, network } = options;

        switch (network) {
            case EBlockchains.Bitcoin:
            case EBlockchains.Ethereum:
            case EBlockchains.Solana:
            case EBlockchains.Polkadot:
            case EBlockchains.Tron:
            case EBlockchains.Cardano:
            case EBlockchains.Dogecoin:
            case EBlockchains.Neo:
            case EBlockchains.Xrp:
            case EBlockchains.Litecoin:
                return addressValidator.validate(address, network);
            case EBlockchains.Cosmos:
                return this.validateCosmos(address);
            case EBlockchains.Islamicoin:
                return this.validateIslamCoin(address);
            default:
                throw new HttpException(`${network} is not allowed now for validate`, HttpStatus.BAD_REQUEST);
        }
    }

    private validateCosmos(address: string): boolean {
        const res = bech32.decodeUnsafe(address);
        return !!(res?.words.length && res?.prefix === EBlockchains.Cosmos);
    }

    private validateIslamCoin(address: string): boolean {
        return addressValidator.validate(address, EBlockchains.Ethereum);
    }

    private async createAddress(
        networkCurrency: NetworkCurrencyEntity,
    ): Promise<{ address: string; addressId: string }> {
        const { currency, network } = networkCurrency;

        const createAddressResult = await this.request<{ id: string }>({
            data: { currency: currency.shortName, blockchain: network.name },
            method: 'post',
            pathname: 'address/generate-transit',
        });

        if (!createAddressResult?.id) {
            throw new HttpException('Address is not created', HttpStatus.SERVICE_UNAVAILABLE);
        }

        const { id: addressId } = createAddressResult;

        const { address } = await this.request<{ address: string }>({
            pathname: `address/${addressId}`,
        });

        return {
            address,
            addressId,
        };
    }

    private request<T = unknown>(options: {
        pathname?: string;
        data?: unknown;
        params?: unknown;
        method?: 'get' | 'post' | 'put' | 'patch' | 'delete';
    }): Promise<T> {
        const { method = 'get', pathname = '', data, params } = options;

        /* @ts-ignore */
        return this.axiosService.request<T>({
            url: `http://${join(appConfig.INTERNAL_WALLET_DOMAIN, 'v1', pathname)}`,
            method,
            data,
            params,
        });
    }
}
