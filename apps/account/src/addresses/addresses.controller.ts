import { ApiAuth, JwtPayload, TokenAudience, TUserJwtPayload } from '@app/jwt';
import { Body, Controller, Get, HttpStatus, Post, Query } from '@nestjs/common';
import { ApiBadRequestResponse, ApiBody, ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';

import { AddressesService } from './addresses.service';
import {
    AddressesQueryRequestDto,
    BoolResponse,
    OneAddressesResponseDto,
    ValidateAddressesPostRequestDto,
} from './dto';

@ApiTags('address')
@Controller('address')
export class AddressesController {
    constructor(private readonly addressService: AddressesService) {}

    @Get()
    @ApiAuth(TokenAudience.ACCOUNT)
    @ApiResponse({ type: OneAddressesResponseDto, status: HttpStatus.OK })
    public async ensureAddress(
        @Query() query: AddressesQueryRequestDto,
        @JwtPayload() { id: userId }: TUserJwtPayload,
    ): Promise<OneAddressesResponseDto> {
        const result = await this.addressService.ensureAddress(userId as string, query);
        return new OneAddressesResponseDto(result);
    }

    @Post('validator')
    @ApiOperation({ summary: 'Validate crypto address' })
    @ApiBody({ type: ValidateAddressesPostRequestDto })
    @ApiResponse({ type: BoolResponse, status: HttpStatus.OK })
    @ApiBadRequestResponse()
    public validateAddress(@Body() body: ValidateAddressesPostRequestDto): BoolResponse {
        const res = this.addressService.validate(body);
        return new BoolResponse(res);
    }
}
