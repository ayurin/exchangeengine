import { MigrationInterface, QueryRunner } from 'typeorm';

export class addColumnUser1673551178775 implements MigrationInterface {
    name = 'addColumnUser1673551178775';

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "users" ADD "block_expires" TIMESTAMP`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "users" DROP COLUMN "block_expires"`);
    }
}
