import { MigrationInterface, QueryRunner } from 'typeorm';

export class OperationType1672758472473 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE operation_type (
            "type" varchar NOT NULL,
            CONSTRAINT operation_type_pk PRIMARY KEY ("type")
        );`);

        await queryRunner.query(`INSERT INTO operation_type ("type") VALUES('topupFromFaucet');`);
        await queryRunner.query(`INSERT INTO operation_type ("type") VALUES('placeOrder');`);
        await queryRunner.query(`INSERT INTO operation_type ("type") VALUES('cancelOrder');`);
        await queryRunner.query(`INSERT INTO operation_type ("type") VALUES('settleOrder');`);
        await queryRunner.query(`INSERT INTO operation_type ("type") VALUES('withdraw');`);

        await queryRunner.query(`ALTER TABLE operations RENAME COLUMN "type" TO old_type;`);

        await queryRunner.query(`ALTER TABLE operations ADD "type" varchar;`);

        await queryRunner.query(`ALTER TABLE operations ADD CONSTRAINT operations_type_fk FOREIGN KEY ("type")
            REFERENCES operation_type("type") ON DELETE RESTRICT ON UPDATE CASCADE;`);

        await queryRunner.query(
            `UPDATE "operations" SET "type" = o.old_type FROM (SELECT id, old_type FROM "operations") as o WHERE operations.id = o.id;`,
        );

        await queryRunner.query(`ALTER TABLE operations ALTER COLUMN "type" SET NOT NULL;`);

        await queryRunner.query(`ALTER TABLE operations DROP COLUMN "old_type";`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE operations RENAME COLUMN "type" TO old_type;`);

        await queryRunner.query(`ALTER TABLE operations ADD "type" varchar;`);

        await queryRunner.query(
            `UPDATE "operations" SET "type" = o.old_type FROM (SELECT id, old_type FROM "operations") as o WHERE operations.id = o.id;`,
        );

        await queryRunner.query(`ALTER TABLE operations ALTER COLUMN "type" SET NOT NULL;`);

        await queryRunner.query(`ALTER TABLE operations DROP COLUMN "old_type";`);

        await queryRunner.query(`DROP TABLE operation_type;`);
    }
}
