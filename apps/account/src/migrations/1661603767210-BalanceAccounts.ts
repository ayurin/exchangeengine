import { MigrationInterface, QueryRunner } from 'typeorm';

import { BalanceAccountEntity } from '@account/dao';

const data: Partial<BalanceAccountEntity>[] = [
    {
        baNumber: 'D1600',
        description: 'debit accounts with available balance',
        debitFlg: true,
    },
    {
        baNumber: 'C2600',
        description: 'credit accounts with available balance',
        debitFlg: false,
    },
    {
        baNumber: 'C2601',
        description: 'credit accounts with locked by orders balance',
        debitFlg: false,
    },
];

export class BalanceAccounts1661603767210 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "${BalanceAccountEntity.TABLE_NAME}" (
            "id" uuid NOT NULL DEFAULT uuid_generate_v4(),
            "ba_number" character varying NOT NULL,
            "description" character varying,
            "is_debit" boolean NOT NULL,
            CONSTRAINT "PK_BALANCE_ACCOUNTS" PRIMARY KEY ("id"),
            CONSTRAINT "UQ_BALANCE_ACCOUNTS_BA_NUMBER" UNIQUE ("ba_number")
        )`);

        const query = data.map(
            (entity) => `
            INSERT INTO "${BalanceAccountEntity.TABLE_NAME}"
            (id, ba_number, description, is_debit)
            VALUES (uuid_generate_v4(), '${entity.baNumber}', '${entity.description}', '${entity.debitFlg}');
            `,
        );

        await queryRunner.query(query.join('\n'));
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`DROP TABLE "${BalanceAccountEntity.TABLE_NAME}"`);
    }
}
