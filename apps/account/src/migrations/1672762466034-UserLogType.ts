import { MigrationInterface, QueryRunner } from 'typeorm';

export class UserLogType1672762466034 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE user_log_type (
            "type" varchar NOT NULL,
            CONSTRAINT user_log_type_pk PRIMARY KEY ("type")
        );`);

        await queryRunner.query(`INSERT INTO user_log_type ("type") VALUES('lock');`);
        await queryRunner.query(`INSERT INTO user_log_type ("type") VALUES('unlock');`);

        await queryRunner.query(`ALTER TABLE user_log RENAME COLUMN "type" TO old_type;`);

        await queryRunner.query(`ALTER TABLE user_log ADD "type" varchar;`);

        await queryRunner.query(`ALTER TABLE user_log ADD CONSTRAINT user_log_type_fk FOREIGN KEY ("type")
            REFERENCES user_log_type("type") ON DELETE RESTRICT ON UPDATE CASCADE;`);

        await queryRunner.query(
            `UPDATE "user_log" SET "type" = ul.old_type FROM (SELECT id, old_type FROM "user_log") as ul WHERE user_log.id = ul.id;`,
        );

        await queryRunner.query(`ALTER TABLE user_log ALTER COLUMN "type" SET NOT NULL;`);

        await queryRunner.query(`ALTER TABLE user_log DROP COLUMN "old_type";`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE user_log RENAME COLUMN "type" TO old_type;`);

        await queryRunner.query(`ALTER TABLE user_log ADD "type" varchar;`);

        await queryRunner.query(
            `UPDATE "user_log" SET "type" = ul.old_type FROM (SELECT id, old_type FROM "user_log") as ul WHERE user_log.id = ul.id;`,
        );

        await queryRunner.query(`ALTER TABLE user_log ALTER COLUMN "type" SET NOT NULL;`);

        await queryRunner.query(`ALTER TABLE user_log DROP COLUMN "old_type";`);

        await queryRunner.query(`DROP TABLE user_log_type;`);
    }
}
