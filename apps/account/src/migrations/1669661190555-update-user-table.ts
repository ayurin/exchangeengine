import { MigrationInterface, QueryRunner } from 'typeorm';

export class updateUserTable1669661190555 implements MigrationInterface {
    name = 'updateUserTable1669661190555';

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "users"
        ADD "kyc" boolean NOT NULL DEFAULT false`);
        await queryRunner.query(`ALTER TABLE "users"
        ADD "tfa_enable" boolean NOT NULL DEFAULT false`);
        await queryRunner.query(`ALTER TABLE "users"
        ADD "tax_number" character varying`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "users"
        DROP COLUMN "tax_number"`);
        await queryRunner.query(`ALTER TABLE "users"
        DROP COLUMN "tfa_enable"`);
        await queryRunner.query(`ALTER TABLE "users"
        DROP COLUMN "kyc"`);
    }
}
