import { MigrationInterface, QueryRunner } from 'typeorm';

export class networkCurrenctTable1670952555036 implements MigrationInterface {
    name = 'networkCurrenctTable1670952555036';

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "networks"
        DROP CONSTRAINT "FK_89eb2c5a50f1fea7891f28d07f9"`);
        await queryRunner.query(
            `CREATE TABLE "currency-network"
       (
           "network_id"  uuid              NOT NULL,
           "currency_id" uuid              NOT NULL,
           "status"      character varying NOT NULL DEFAULT 'enable',
           "created_at"  TIMESTAMP         NOT NULL DEFAULT now(),
           "updated_at"  TIMESTAMP         NOT NULL DEFAULT now(),
           "deleted_at"  TIMESTAMP,
           CONSTRAINT "currency_network_status_check" CHECK (status in ('enable', 'disable', 'maintenance')),
           CONSTRAINT "PK_3ab6c983347ed977c561f138371" PRIMARY KEY ("network_id", "currency_id")
       )`,
        );
        await queryRunner.query(`ALTER TABLE "networks"
        DROP COLUMN "currency_id"`);
        await queryRunner.query(
            `ALTER TABLE "currency-network"
          ADD CONSTRAINT "FK_bab6537227005c7fe0c1512be29" FOREIGN KEY ("currency_id") REFERENCES "currencies" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
        );
        await queryRunner.query(
            `ALTER TABLE "currency-network"
          ADD CONSTRAINT "FK_5fe7cd003f897dfb0f2edc3b05e" FOREIGN KEY ("network_id") REFERENCES "networks" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
        );
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "currency-network"
        DROP CONSTRAINT "FK_5fe7cd003f897dfb0f2edc3b05e"`);
        await queryRunner.query(`ALTER TABLE "currency-network"
        DROP CONSTRAINT "FK_bab6537227005c7fe0c1512be29"`);
        await queryRunner.query(`ALTER TABLE "networks"
        ADD "currency_id" uuid NOT NULL`);
        await queryRunner.query(`DROP TABLE "currency-network"`);
        await queryRunner.query(
            `ALTER TABLE "networks"
          ADD CONSTRAINT "FK_89eb2c5a50f1fea7891f28d07f9" FOREIGN KEY ("currency_id") REFERENCES "currencies" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
        );
    }
}
