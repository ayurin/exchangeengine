import { MigrationInterface, QueryRunner } from 'typeorm';

import { BalanceAccountEntity } from '@account/dao';

export class AlterBalanceAccounts1661875351332 implements MigrationInterface {
    public async up(qr: QueryRunner): Promise<void> {
        await qr.query(`ALTER TABLE "${BalanceAccountEntity.TABLE_NAME}" RENAME COLUMN "is_debit" TO "debit_flg"`);
        await qr.query(
            `ALTER TABLE "${BalanceAccountEntity.TABLE_NAME}" ADD "lock_flg" boolean NOT NULL DEFAULT false`,
        );
        await qr.query(`UPDATE "${BalanceAccountEntity.TABLE_NAME}" SET lock_flg = true where ba_number = 'C2601'`);
        await qr.query(
            `UPDATE "${BalanceAccountEntity.TABLE_NAME}" SET lock_flg = false where ba_number = 'D1600' or ba_number = 'C2600'`,
        );
    }

    public async down(qr: QueryRunner): Promise<void> {
        await qr.query(`ALTER TABLE "${BalanceAccountEntity.TABLE_NAME}" RENAME COLUMN "debit_flg" TO "is_debit"`);
        await qr.query(`ALTER TABLE "${BalanceAccountEntity.TABLE_NAME}" DROP COLUMN "lock_flg"`);
    }
}
