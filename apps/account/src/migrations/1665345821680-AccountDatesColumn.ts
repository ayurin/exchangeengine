import { MigrationInterface, QueryRunner } from 'typeorm';

export class AccountDatesColumn1665345821680 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(
            `ALTER TABLE "user_accounts" ADD "created" TIMESTAMP NOT NULL DEFAULT ('now'::text)::timestamp(6) with time zone`,
        );
        await queryRunner.query(
            `ALTER TABLE "user_accounts" ADD "updated" TIMESTAMP NOT NULL DEFAULT ('now'::text)::timestamp(6) with time zone`,
        );
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "user_accounts" DROP COLUMN "updated"`);
        await queryRunner.query(`ALTER TABLE "user_accounts" DROP COLUMN "created"`);
    }
}
