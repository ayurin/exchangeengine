import { MigrationInterface, QueryRunner } from 'typeorm';

import { UserAccountEntity } from '@account/dao';

export class UserAccount1661616233596 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "${UserAccountEntity.TABLE_NAME}" (
            "id" uuid NOT NULL DEFAULT uuid_generate_v4(),
            "account_number" character varying NOT NULL,
            "balance_account_number" character varying NOT NULL,
            "currency_id" uuid NOT NULL,
            "currency_cd" character varying NOT NULL,
            "account_name" character varying NOT NULL,
            "user_id" uuid NOT NULL,
            "debit_flg" boolean NOT NULL,
            "balance" bigint NOT NULL DEFAULT 0,
            CONSTRAINT "PK_125e915cf23ad1cfb43815ce59b" PRIMARY KEY ("id")
        )`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`DROP TABLE "${UserAccountEntity.TABLE_NAME}"`);
    }
}
