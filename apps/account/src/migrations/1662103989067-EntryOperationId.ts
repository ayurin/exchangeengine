import { MigrationInterface, QueryRunner } from 'typeorm';

import { EntryEntity, OperationEntity, UserAccountEntity } from '@account/dao';

export class EntryOperationId1662103989067 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(
            `TRUNCATE "${EntryEntity.TABLE_NAME}", "${OperationEntity.TABLE_NAME}", "${UserAccountEntity.TABLE_NAME}" `,
        );
        await queryRunner.query(
            `ALTER TABLE "${EntryEntity.TABLE_NAME}" RENAME COLUMN "operation_ids" TO "operation_id"`,
        );
        await queryRunner.query(`ALTER TABLE "${EntryEntity.TABLE_NAME}" DROP COLUMN "operation_id"`);
        await queryRunner.query(`ALTER TABLE "${EntryEntity.TABLE_NAME}" ADD "operation_id" uuid`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "${EntryEntity.TABLE_NAME}" DROP COLUMN "operation_id"`);
        await queryRunner.query(
            `ALTER TABLE "${EntryEntity.TABLE_NAME}" ADD "operation_id" uuid array NOT NULL DEFAULT '{}'`,
        );
    }
}
