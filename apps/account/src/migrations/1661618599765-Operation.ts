import { MigrationInterface, QueryRunner } from 'typeorm';

import { OperationEntity } from '@account/dao';

export class Operation1661618599765 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "${OperationEntity.TABLE_NAME}" (
            "id" uuid NOT NULL DEFAULT uuid_generate_v4(),
            "type" character varying NOT NULL,
            "status" character varying NOT NULL,
            "user_ids" uuid array NOT NULL DEFAULT '{}',
            "metadata" text NOT NULL,
            "as_of_date" TIMESTAMP NOT NULL DEFAULT now(),
            CONSTRAINT "operation_type_check" CHECK (type in ('topupFromFaucet')),
            CONSTRAINT "operation_status_check" CHECK (status in ('new', 'pending', 'completed')),
            CONSTRAINT "PK_7b62d84d6f9912b975987165856" PRIMARY KEY ("id")
        )`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`DROP TABLE "${OperationEntity.TABLE_NAME}"`);
    }
}
