import { MigrationInterface, QueryRunner } from 'typeorm';

import { EUserStatus, UserEntity } from '@account/dao';

export class UsersEmail1661461749855 implements MigrationInterface {
    public async up(qr: QueryRunner): Promise<void> {
        await qr.query(`
            ALTER TABLE "${UserEntity.TABLE_NAME}"
            ADD COLUMN "email" character varying  NOT NULL,
            ADD COLUMN "status" character varying  NOT NULL  DEFAULT '${EUserStatus.CREATED}';
        `);
    }

    public async down(qr: QueryRunner): Promise<void> {
        await qr.query(`
            ALTER TABLE "${UserEntity.TABLE_NAME}"
            DROP COLUMN "email",
            DROP COLUMN "status";
        `);
    }
}
