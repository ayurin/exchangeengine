import { MigrationInterface, QueryRunner } from 'typeorm';

import { CurrencyEntity } from '@account/dao';

const data: Partial<CurrencyEntity>[] = [
    {
        shortName: 'USDC',
        isoCode: '',
        fullName: 'USD Coin',
        decimals: BigInt(6),
        active: true,
    },
    {
        shortName: 'ETH',
        isoCode: 'ETH',
        fullName: 'Ethereum',
        decimals: BigInt(18),
        active: true,
    },
    {
        shortName: 'BTC',
        isoCode: 'XBT',
        fullName: 'Bitcoin',
        decimals: BigInt(8),
        active: true,
    },
    {
        shortName: 'USD',
        isoCode: 'USD',
        fullName: 'United States dollar',
        decimals: BigInt(2),
        active: true,
    },
    {
        shortName: 'AED',
        isoCode: 'AED',
        fullName: 'United Arab Emirates dirham',
        decimals: BigInt(2),
        active: true,
    },
];

export class Currencies1661352685137 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "${CurrencyEntity.TABLE_NAME}" (
            "id" uuid NOT NULL DEFAULT uuid_generate_v4(),
            "short_name" character varying NOT NULL,
            "iso_code" character varying,
            "full_name" character varying NOT NULL,
            "decimals" bigint NOT NULL,
            "active" boolean NOT NULL DEFAULT true,
            CONSTRAINT "PK_d528c54860c4182db13548e08c4" PRIMARY KEY ("id"),
            CONSTRAINT "UQ_07f76f4cb11c0bbc7ebcfcf67f6" UNIQUE ("short_name")
        )`);

        const query = data.map(
            (entity) => `
            INSERT INTO "${CurrencyEntity.TABLE_NAME}"
            (id, short_name, iso_code, full_name, decimals, active)
            VALUES(uuid_generate_v4(), '${entity.shortName}', '${entity.isoCode}', '${entity.fullName}', ${entity.decimals}, ${entity.active});
            `,
        );

        await queryRunner.query(query.join('\n'));
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`DROP TABLE "${CurrencyEntity.TABLE_NAME}"`);
    }
}
