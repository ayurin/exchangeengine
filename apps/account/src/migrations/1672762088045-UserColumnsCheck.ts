import { MigrationInterface, QueryRunner } from 'typeorm';

export class UserColumnsCheck1672762088045 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(
            `ALTER TABLE "users" ADD CONSTRAINT "user_status_check" CHECK (status in ('CREATED', 'REGISTERED'))`,
        );
        await queryRunner.query(
            `ALTER TABLE "users" ADD CONSTRAINT "user_kyc_status_check" CHECK (kyc_status in ('BASIC', 'STANDARD', 'PRO', 'UNVERIFIED'))`,
        );
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "users" DROP CONSTRAINT "user_kyc_status_check"`);
        await queryRunner.query(`ALTER TABLE "users" DROP CONSTRAINT "user_status_check"`);
    }
}
