import { MigrationInterface, QueryRunner } from 'typeorm';

export class UuidExtensionCreate1660050461200 implements MigrationInterface {
    public async up(qr: QueryRunner): Promise<void> {
        await qr.query(`CREATE EXTENSION IF NOT EXISTS "uuid-ossp";`);
    }

    public async down(qr: QueryRunner): Promise<void> {
        await qr.query(`DROP EXTENSION  "uuid-ossp";`);
    }
}
