import { ECurrencyType } from '@app/utils';
import { MigrationInterface, QueryRunner } from 'typeorm';

import { CurrencyEntity } from '@account/dao';

export class CurrencyType1665518025189 implements MigrationInterface {
    public async up(qr: QueryRunner): Promise<void> {
        await qr.query(`
            ALTER TABLE "${CurrencyEntity.TABLE_NAME}"
            ADD COLUMN "type" character varying;

            UPDATE "${CurrencyEntity.TABLE_NAME}"
            SET "type" = '${ECurrencyType.STABLE}'
            WHERE "short_name" = 'USDC';

            UPDATE "${CurrencyEntity.TABLE_NAME}"
            SET "type" = '${ECurrencyType.CRYPTO}'
            WHERE "short_name" = 'ETH';

            UPDATE "${CurrencyEntity.TABLE_NAME}"
            SET "type" = '${ECurrencyType.CRYPTO}'
            WHERE "short_name" = 'BTC';

            UPDATE "${CurrencyEntity.TABLE_NAME}"
            SET "type" = '${ECurrencyType.FIAT}'
            WHERE "short_name" = 'USD';

            UPDATE "${CurrencyEntity.TABLE_NAME}"
            SET "type" = '${ECurrencyType.FIAT}'
            WHERE "short_name" = 'AED';

            ALTER TABLE "${CurrencyEntity.TABLE_NAME}"
            ALTER COLUMN "type" SET NOT NULL;
        `);
    }

    public async down(qr: QueryRunner): Promise<void> {
        await qr.query(`
            ALTER TABLE "${CurrencyEntity.TABLE_NAME}"
            DROP COLUMN "type";
        `);
    }
}
