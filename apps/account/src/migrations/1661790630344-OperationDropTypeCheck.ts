import { MigrationInterface, QueryRunner } from 'typeorm';

import { OperationEntity } from '@account/dao';

export class OperationDropTypeCheck1661790630344 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "${OperationEntity.TABLE_NAME}" DROP CONSTRAINT "operation_type_check";`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(
            `ALTER TABLE "${OperationEntity.TABLE_NAME}" ADD CONSTRAINT "operation_type_check" CHECK (type in ('topupFromFaucet', 'placeOrder', 'cancelOrder'))`,
        );
    }
}
