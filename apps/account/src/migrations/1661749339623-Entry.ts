import { MigrationInterface, QueryRunner } from 'typeorm';

import { EntryEntity } from '@account/dao';

export class Entry1661749339623 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "${EntryEntity.TABLE_NAME}" (
            "id" uuid NOT NULL DEFAULT uuid_generate_v4(),
            "debit_account_id" uuid NOT NULL,
            "debit_account_number" character varying NOT NULL,
            "credit_account_id" uuid NOT NULL,
            "credit_account_number" character varying NOT NULL,
            "amount" bigint NOT NULL,
            "currency" character varying NOT NULL,
            "operation_ids" uuid array NOT NULL DEFAULT '{}',
            "metadata" json NOT NULL,
            "as_of_date" TIMESTAMP NOT NULL DEFAULT now(),
            CONSTRAINT "PK_ENTRY" PRIMARY KEY ("id")
        )`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`DROP TABLE "${EntryEntity.TABLE_NAME}"`);
    }
}
