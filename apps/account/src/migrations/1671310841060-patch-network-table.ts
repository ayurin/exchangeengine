import { MigrationInterface, QueryRunner } from 'typeorm';

export class patchNetworkTable1671310841060 implements MigrationInterface {
    name = 'patchNetworkTable1671310841060';

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "currency-network"
        ADD "id" uuid NOT NULL DEFAULT uuid_generate_v4()`);

        await queryRunner.query(`ALTER TABLE "currency-network"
        DROP CONSTRAINT "PK_3ab6c983347ed977c561f138371"`);

        await queryRunner.query(`ALTER TABLE "currency-network"
        ADD CONSTRAINT "PK_0f142d70f5d037a520f4eef2a55" PRIMARY KEY ("id")`);

        await queryRunner.query(`ALTER TABLE "currency-network"
        ADD CONSTRAINT "UQ_3ab6c983347ed977c561f138371" UNIQUE ("network_id", "currency_id")`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "currency-network"
        DROP CONSTRAINT "UQ_3ab6c983347ed977c561f138371"`);

        await queryRunner.query(`ALTER TABLE "currency-network"
        DROP CONSTRAINT "PK_0f142d70f5d037a520f4eef2a55"`);

        await queryRunner.query(`ALTER TABLE "currency-network"
        ADD CONSTRAINT "PK_3ab6c983347ed977c561f138371" PRIMARY KEY ("network_id", "currency_id")`);

        await queryRunner.query(`ALTER TABLE "currency-network"
        DROP COLUMN "id"`);
    }
}
