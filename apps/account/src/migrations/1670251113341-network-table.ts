import { MigrationInterface, QueryRunner } from 'typeorm';

export class networkTable1670251113341 implements MigrationInterface {
    name = 'networkTable1670251113341';

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(
            `CREATE TABLE "networks"
       (
           "id"           uuid              NOT NULL DEFAULT uuid_generate_v4(),
           "name"         character varying NOT NULL,
           "native_token" character varying NOT NULL,
           "currency_id"  uuid              NOT NULL,
           "created_at"   TIMESTAMP         NOT NULL DEFAULT now(),
           "updated_at"   TIMESTAMP         NOT NULL DEFAULT now(),
           "deleted_at"   TIMESTAMP,
           CONSTRAINT "PK_61b1ee921bf79550d9d4742b9f7" PRIMARY KEY ("id")
       )`,
        );
        await queryRunner.query(
            `ALTER TABLE "networks"
          ADD CONSTRAINT "FK_89eb2c5a50f1fea7891f28d07f9" FOREIGN KEY ("currency_id") REFERENCES "currencies" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
        );
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "networks"
        DROP CONSTRAINT "FK_89eb2c5a50f1fea7891f28d07f9"`);
        await queryRunner.query(`DROP TABLE "networks"`);
    }
}
