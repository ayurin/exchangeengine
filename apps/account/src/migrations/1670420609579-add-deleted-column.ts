import { MigrationInterface, QueryRunner } from 'typeorm';

export class addDeletedColumn1670420609579 implements MigrationInterface {
    name = 'addDeletedColumn1670420609579';

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "users" ADD "deleted_at" TIMESTAMP`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "users" DROP COLUMN "deleted_at"`);
    }
}
