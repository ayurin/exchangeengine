import { MigrationInterface, QueryRunner } from 'typeorm';

export class kysStatus1671791751781 implements MigrationInterface {
    name = 'kysStatus1671791751781';

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "users" RENAME COLUMN "kyc" TO "kyc_status"`);
        await queryRunner.query(`ALTER TABLE "users" DROP COLUMN "kyc_status"`);
        await queryRunner.query(`ALTER TABLE "users" ADD "kyc_status" character varying NOT NULL DEFAULT 'UNVERIFIED'`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "users" DROP COLUMN "kyc_status"`);
        await queryRunner.query(`ALTER TABLE "users" ADD "kyc_status" boolean NOT NULL DEFAULT false`);
        await queryRunner.query(`ALTER TABLE "users" RENAME COLUMN "kyc_status" TO "kyc"`);
    }
}
