import { MigrationInterface, QueryRunner } from 'typeorm';

import { OperationEntity } from '@account/dao';

export class OperationStatus1668618190547 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(
            `ALTER TABLE "${OperationEntity.TABLE_NAME}" DROP CONSTRAINT "operation_status_check";`,
        );
        await queryRunner.query(
            `UPDATE "${OperationEntity.TABLE_NAME}" SET status = 'confirmed' where status = 'completed'`,
        );
        await queryRunner.query(
            `ALTER TABLE "${OperationEntity.TABLE_NAME}" ADD CONSTRAINT "operation_status_check" CHECK (status in ('active', 'rejected', 'confirmed'))`,
        );
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(
            `ALTER TABLE "${OperationEntity.TABLE_NAME}" DROP CONSTRAINT "operation_status_check";`,
        );
        await queryRunner.query(
            `UPDATE "${OperationEntity.TABLE_NAME}" SET status = 'completed' where status = 'confirmed'`,
        );
        await queryRunner.query(
            `ALTER TABLE "${OperationEntity.TABLE_NAME}" ADD CONSTRAINT "operation_status_check" CHECK (status in ('new', 'pending', 'completed'))`,
        );
    }
}
