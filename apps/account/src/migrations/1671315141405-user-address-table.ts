import { MigrationInterface, QueryRunner } from 'typeorm';

export class userAddressTable1671315141405 implements MigrationInterface {
    name = 'userAddressTable1671315141405';

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(
            `CREATE TABLE "user_addresses"
       (
           "id"                  uuid      NOT NULL DEFAULT uuid_generate_v4(),
           "user_id"             uuid      NOT NULL,
           "address_id"          uuid      NOT NULL,
           "network_currency_id" uuid      NOT NULL,
           "created_at"          TIMESTAMP NOT NULL DEFAULT now(),
           "updated_at"          TIMESTAMP NOT NULL DEFAULT now(),
           "deleted_at"          TIMESTAMP,
           CONSTRAINT "PK_8abbeb5e3239ff7877088ffc25b" PRIMARY KEY ("id")
       )`,
        );
        await queryRunner.query(
            `ALTER TABLE "user_addresses"
          ADD CONSTRAINT "FK_d780bf729d559368b6bf43e2c8b" FOREIGN KEY ("network_currency_id") REFERENCES "currency-network" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
        );
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "user_addresses"
        DROP CONSTRAINT "FK_d780bf729d559368b6bf43e2c8b"`);
        await queryRunner.query(`DROP TABLE "user_addresses"`);
    }
}
