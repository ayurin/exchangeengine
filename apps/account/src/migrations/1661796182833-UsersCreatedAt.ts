import { MigrationInterface, QueryRunner } from 'typeorm';

import { UserEntity } from '@account/dao';

export class UsersCreatedAt1661796182833 implements MigrationInterface {
    public async up(qr: QueryRunner): Promise<void> {
        await qr.query(`ALTER TABLE "${UserEntity.TABLE_NAME}" ADD "created_at" TIMESTAMP NOT NULL DEFAULT now()`);
    }

    public async down(qr: QueryRunner): Promise<void> {
        await qr.query(`ALTER TABLE "${UserEntity.TABLE_NAME}" DROP COLUMN "created_at"`);
    }
}
