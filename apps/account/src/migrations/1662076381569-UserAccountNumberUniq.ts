import { MigrationInterface, QueryRunner } from 'typeorm';

export class UserAccountNumberUniq1662076381569 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(
            `ALTER TABLE "user_accounts" ADD CONSTRAINT "UQ_79e2754d6ced8efb4e53f646c2f" UNIQUE ("account_number")`,
        );
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "user_accounts" DROP CONSTRAINT "UQ_79e2754d6ced8efb4e53f646c2f"`);
    }
}
