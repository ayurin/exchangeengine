import { MigrationInterface, QueryRunner } from 'typeorm';

export class uniqueWhere1671030674798 implements MigrationInterface {
    name = 'uniqueWhere1671030674798';

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(
            `CREATE UNIQUE INDEX "IDX_9a37ed8cf6d695cede9d958a86" ON "networks" ("native_token") WHERE "deleted_at" IS NULL`,
        );
        await queryRunner.query(
            `CREATE UNIQUE INDEX "IDX_e6863a86d184d2dee9889b08d8" ON "networks" ("name") WHERE "deleted_at" IS NULL`,
        );
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`DROP INDEX "postgres"."IDX_e6863a86d184d2dee9889b08d8"`);
        await queryRunner.query(`DROP INDEX "postgres"."IDX_9a37ed8cf6d695cede9d958a86"`);
    }
}
