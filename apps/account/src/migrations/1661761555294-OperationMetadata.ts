import { MigrationInterface, QueryRunner } from 'typeorm';

export class OperationMetadata1661761555294 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "operations" DROP COLUMN "metadata"`);
        await queryRunner.query(`ALTER TABLE "operations" ADD "metadata" json NOT NULL`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "operations" DROP COLUMN "metadata"`);
        await queryRunner.query(`ALTER TABLE "operations" ADD "metadata" text NOT NULL`);
    }
}
