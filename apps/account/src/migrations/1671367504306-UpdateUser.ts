import { MigrationInterface, QueryRunner } from 'typeorm';

export class UpdateUser1671367504306 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "users" ADD "blocked" boolean NOT NULL DEFAULT false`);
        await queryRunner.query(`UPDATE users set status = 'REGISTERED', blocked = true where status = 'BLOCKED'`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "users" DROP COLUMN "blocked"`);
    }
}
