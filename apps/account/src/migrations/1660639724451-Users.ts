import { MigrationInterface, QueryRunner } from 'typeorm';

import { UserEntity } from '@account/dao';

export class Users1660639724451 implements MigrationInterface {
    public async up(qr: QueryRunner): Promise<void> {
        await qr.query(`
        CREATE TABLE "${UserEntity.TABLE_NAME}" (
            "id" uuid NOT NULL DEFAULT uuid_generate_v4(),
            "full_name" character varying NOT NULL,
            "short_name" character varying NOT NULL,
            CONSTRAINT "ACCOUNTS_PRIMARY_KEY" PRIMARY KEY ("id")
        )`);
    }

    public async down(qr: QueryRunner): Promise<void> {
        await qr.query(`DROP TABLE "${UserEntity.TABLE_NAME}"`);
    }
}
