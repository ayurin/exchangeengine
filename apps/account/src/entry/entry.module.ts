import { Module } from '@nestjs/common';

import { EntryRepository } from '@account/dao/entry.repository';

import { EntryController } from './entry.controller';
import { EntryService } from './entry.service';

@Module({
    controllers: [EntryController],
    providers: [EntryService, EntryRepository],
})
export class EntryModule {}
