import { Test, TestingModule } from '@nestjs/testing';
import { plainToInstance } from 'class-transformer';
import { Paginated, PaginateQuery } from 'nestjs-paginate';

import { EntryEntity } from '@account/dao';
import { EntryRepository } from '@account/dao/entry.repository';

import { EntryResponse } from './dto/entry.response';
import { EntryService } from './entry.service';

const entry: EntryEntity = {
    id: '83e67c7c-d130-456e-9c35-e2e1c13d5920',
    debitAccountId: '8e2eb1dc-2884-11ed-a261-0242ac120002',
    debitAccountNumber: 'D1600xUSDCx8e2eb1dc288411eda2610242ac120002x00',
    creditAccountId: '64f14bab-9433-4a8a-bc6f-d15f0473e556',
    creditAccountNumber: 'C2600xUSDCx64f14bab94334a8abc6fd15f0473e556x00',
    amount: '23400000000',
    currency: 'USDC',
    operationId: '52db78f6-ec41-46ab-84ec-6049f5ce8eea',
    metadata: {
        comment: 'new entry',
    },
    asOfDate: new Date(),
};

const entries: EntryEntity[] = [entry];
const entriesResponse: EntryResponse[] = plainToInstance(EntryResponse, entries);
const entryManyAndCount = [entries, entries.length];

describe('balance account service', () => {
    let service: EntryService;
    let entryRepository: EntryRepository;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            providers: [
                EntryService,
                {
                    provide: EntryRepository,
                    useFactory: () => ({
                        take: jest.fn(() => ({
                            skip: jest.fn().mockReturnThis(),
                            addOrderBy: jest.fn().mockReturnThis(),
                            getManyAndCount: jest.fn(() => entryManyAndCount),
                        })),
                        createQueryBuilder: jest.fn(() => ({
                            take: jest.fn().mockReturnThis(),
                            skip: jest.fn().mockReturnThis(),
                            addOrderBy: jest.fn().mockReturnThis(),
                            getManyAndCount: jest.fn(() => entryManyAndCount),
                        })),
                        getOneByIdOrFail: jest.fn(),
                    }),
                },
            ],
        }).compile();

        service = module.get<EntryService>(EntryService);
        entryRepository = module.get(EntryRepository);
    });

    describe('getEntries()', () => {
        describe('returns paginated list of entries', () => {
            let result: Paginated<EntryResponse>;

            const query: PaginateQuery = {
                page: undefined,
                limit: undefined,
                sortBy: undefined,
                search: undefined,
                searchBy: undefined,
                filter: undefined,
                path: '',
            };

            const expected: Paginated<EntryResponse> = {
                data: entriesResponse,
                meta: {
                    itemsPerPage: 20,
                    totalItems: 1,
                    currentPage: 1,
                    totalPages: 1,
                    sortBy: [['asOfDate', 'DESC']],
                    filter: undefined,
                },
                links: {
                    current: `${query.path}?page=1&limit=20&sortBy=asOfDate:DESC`,
                    first: undefined,
                    last: undefined,
                    next: undefined,
                    previous: undefined,
                },
            } as Paginated<EntryResponse>;

            beforeEach(async () => {
                result = await service.getEntries(query);
            });

            it('should check equal between result and expected', () => {
                expect(result).toEqual(expected);
                expect(result.data).toStrictEqual(expected.data);
            });
        });

        describe('getEntry()', () => {
            describe('returns paginated list of entries', () => {
                let result: EntryResponse;
                let repositoryMock: jest.SpyInstance;

                const expected: EntryResponse = plainToInstance(EntryResponse, entry);

                beforeEach(async () => {
                    repositoryMock = jest.spyOn(entryRepository, 'getOneByIdOrFail').mockResolvedValue(entry);
                    result = await service.getEntry(entry.id);
                });

                it('should call getOneByIdOrFail', () => {
                    expect(repositoryMock).toHaveBeenCalledTimes(1);
                    expect(repositoryMock).toHaveBeenCalledWith(entry.id);
                });

                it('should check equal between result and expected', () => {
                    expect(result).toStrictEqual(expected);
                });
            });
        });
    });
});
