import { Controller, Get, HttpStatus, Param } from '@nestjs/common';
import { ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { Paginate, Paginated, PaginateQuery } from 'nestjs-paginate';

import { EntryService } from '@account/entry/entry.service';

import { EntryResponse, PaginatedEntryResponse } from './dto/entry.response';

@ApiTags('entries')
@Controller('entries')
export class EntryController {
    constructor(private readonly entryService: EntryService) {}

    @Get()
    @ApiOperation({ summary: 'Get list of entries' })
    @ApiResponse({ status: HttpStatus.OK, type: PaginatedEntryResponse })
    public getEntries(@Paginate() query: PaginateQuery): Promise<Paginated<EntryResponse>> {
        return this.entryService.getEntries(query);
    }

    @Get(':entryId')
    @ApiOperation({ summary: 'Get details of entry' })
    @ApiResponse({ status: HttpStatus.OK, type: EntryResponse })
    public getEntry(@Param('entryId') id: string): Promise<EntryResponse> {
        return this.entryService.getEntry(id);
    }
}
