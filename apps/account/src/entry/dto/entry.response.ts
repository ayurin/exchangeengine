// eslint-disable-next-line max-classes-per-file
import { PaginatedApiResponse } from '@app/utils';
import { ApiProperty } from '@nestjs/swagger';
import { Transform } from 'class-transformer';

import { IEntryMetadata } from '@account/dao';

export class EntryResponse {
    @ApiProperty({ example: '83e67c7c-d130-456e-9c35-e2e1c13d5920' })
    public id: string;

    @ApiProperty({ example: '8e2eb1dc-2884-11ed-a261-0242ac120002' })
    public debitAccountId: string;

    @ApiProperty({ example: 'D1600xUSDCx8e2eb1dc288411eda2610242ac120002x00' })
    public debitAccountNumber: string;

    @ApiProperty({ example: '64f14bab-9433-4a8a-bc6f-d15f0473e556' })
    public creditAccountId: string;

    @ApiProperty({ example: 'C2600xUSDCx64f14bab94334a8abc6fd15f0473e556x00' })
    public creditAccountNumber: string;

    @ApiProperty({ example: '23400000000' })
    public amount: string;

    @ApiProperty({ example: 'USDC' })
    public currency: string;

    @ApiProperty({ example: '52db78f6-ec41-46ab-84ec-6049f5ce8eea' })
    public operationId: string;

    @ApiProperty({ example: { comment: 'new entry' } })
    public metadata: IEntryMetadata;

    @ApiProperty({ example: 1660302000324 })
    @Transform(({ value }) => (value as Date).valueOf(), { toClassOnly: true })
    public asOfDate: number;
}

export class PaginatedEntryResponse extends PaginatedApiResponse<EntryResponse> {
    @ApiProperty({ type: EntryResponse, isArray: true })
    public readonly data!: EntryResponse[];
}
