import { Injectable } from '@nestjs/common';
import { plainToInstance } from 'class-transformer';
import { paginate, Paginated, PaginateQuery } from 'nestjs-paginate';

import { EntryRepository } from '@account/dao/entry.repository';

import { EntryResponse } from './dto/entry.response';

@Injectable()
export class EntryService {
    constructor(private readonly entryRepository: EntryRepository) {}

    public async getEntries(query: PaginateQuery): Promise<Paginated<EntryResponse>> {
        const paginatedEntries = await paginate(query, this.entryRepository, {
            sortableColumns: ['amount', 'currency', 'creditAccountId', 'debitAccountId'],
            nullSort: 'last',
            searchableColumns: [
                'id',
                'creditAccountId',
                'creditAccountNumber',
                'debitAccountId',
                'debitAccountNumber',
                'currency',
            ],
            defaultSortBy: [['asOfDate', 'DESC']],
        });

        return {
            data: plainToInstance(EntryResponse, paginatedEntries.data),
            meta: paginatedEntries.meta,
            links: paginatedEntries.links,
        };
    }

    public async getEntry(entryId: string): Promise<EntryResponse> {
        const entity = await this.entryRepository.getOneByIdOrFail(entryId);
        return plainToInstance(EntryResponse, entity);
    }
}
