import { provideServiceDefaultMock } from '@haqqex-backend/testing';
import { Test, TestingModule } from '@nestjs/testing';

import { CurrencyService } from '@account/currency/currency.service';

import { CurrencyController } from '../currency/currency.controller';

describe('currencyController', () => {
    let controller: CurrencyController;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            controllers: [CurrencyController],
            providers: [provideServiceDefaultMock(CurrencyService)],
        }).compile();

        controller = module.get<CurrencyController>(CurrencyController);
    });

    it('should be defined', () => {
        expect(controller).toBeDefined();
    });
});
