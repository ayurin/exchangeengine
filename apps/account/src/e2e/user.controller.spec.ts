import { ExceptionModule } from '@haqqex-backend/exception';
import { HttpStatus, Module } from '@nestjs/common';
import { FastifyAdapter, NestFastifyApplication } from '@nestjs/platform-fastify';
import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken, TypeOrmModule } from '@nestjs/typeorm';
import { isUUID } from 'class-validator';
import { LoggerModule } from 'nestjs-pino';
import { In, Repository } from 'typeorm';

import { appConfig } from '@account/config';
import { UserEntity } from '@account/dao';
import { IdResponse } from '@account/dto';
import { API_PREFIX, appValidationPipe, appVersioningOptions, dataSourceOptions } from '@account/env';
import { RegistrationRmqClient } from '@account/registration/registration-rmq.client';
import { UserController } from '@account/user/user.controller';
import { UserService } from '@account/user/user.service';

import { AuthRmqClientMoch } from './moch';

describe('userController', () => {
    let app: NestFastifyApplication;
    let userRepository: Repository<UserEntity>;

    const REGISTRATION_URL = `http://localhost:${appConfig.APP_PORT}/api/v1/registration`;
    const jsonHeaders = {
        // eslint-disable-next-line @typescript-eslint/naming-convention
        'Content-Type': 'application/json',
    };

    beforeAll(async () => {
        @Module({
            imports: [TypeOrmModule.forFeature([UserEntity])],
            controllers: [UserController],
            providers: [
                UserService,
                {
                    provide: RegistrationRmqClient,
                    useClass: AuthRmqClientMoch,
                },
            ],
        })
        class UserModuleMoch {}

        const module: TestingModule = await Test.createTestingModule({
            imports: [
                ExceptionModule,
                LoggerModule.forRoot(),
                TypeOrmModule.forRoot(dataSourceOptions),
                UserModuleMoch,
            ],
        }).compile();

        app = module.createNestApplication(new FastifyAdapter());
        app.useGlobalPipes(appValidationPipe);
        app.setGlobalPrefix(API_PREFIX);
        app.enableVersioning(appVersioningOptions);

        userRepository = module.get(getRepositoryToken(UserEntity));

        await app.listen(appConfig.APP_PORT, '0.0.0.0');
    });

    afterAll(async () => {
        if (userRepository) {
            await userRepository.delete({ email: In(['saladin@haqqex.com', 'saladin@google.com']) });
        }
        if (app) {
            await app.close();
        }
    });

    it('email validation', async () => {
        const resBody = await fetch(REGISTRATION_URL, {
            method: 'POST',
            headers: jsonHeaders,
            body: JSON.stringify({
                fullName: 'Yusuf ibn Ayyub ibn Shadi',
                shortName: 'Saladin',
                email: 'not a email',
                password: 'veryhardtoguessit',
            }),
        }).then((res) => {
            expect(res.ok).toBe(false);
            expect(res.status).toBe(HttpStatus.BAD_REQUEST);
            return res.json() as Promise<{ message: { message: string[] } }>;
        });

        const errorMessage = resBody.message.message[0];
        expect(errorMessage).toBe('email must be an email');
    });

    it('registration success', async () => {
        const { id } = await fetch(REGISTRATION_URL, {
            method: 'POST',
            headers: jsonHeaders,
            body: JSON.stringify({
                fullName: 'Yusuf ibn Ayyub ibn Shadi',
                shortName: 'Saladin',
                email: 'saladin@haqqex.com',
                password: 'veryhardtoguessit',
            }),
        }).then((res) => {
            expect(res.ok).toBe(true);
            return res.json() as Promise<IdResponse>;
        });

        expect(isUUID(id)).toBe(true);
    });

    it('login already taken', async () => {
        const duplicationRegistrationData = JSON.stringify({
            fullName: 'Yusuf ibn Ayyub ibn Shadi',
            shortName: 'Saladin',
            email: 'saladin@google.com',
            password: 'veryhardtoguessit',
        });

        const res1 = await fetch(REGISTRATION_URL, {
            method: 'POST',
            headers: jsonHeaders,
            body: duplicationRegistrationData,
        });
        expect(res1.ok).toBe(true);

        const res2 = await fetch(REGISTRATION_URL, {
            method: 'POST',
            headers: jsonHeaders,
            body: duplicationRegistrationData,
        });

        expect(res2.ok).toBe(false);
        expect(res2.status).toBe(HttpStatus.CONFLICT);
    });
});
