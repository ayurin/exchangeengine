import { randomUUID } from 'crypto';

import { Injectable } from '@nestjs/common';

import { EUserStatus, UserEntity } from '@account/dao';

@Injectable()
export class UserRepositoryMoch {
    private static readonly STORE: UserEntity[] = [];

    public find(errorMessage?: string): Promise<UserEntity[]> {
        this.throwError(errorMessage);
        return Promise.resolve(UserRepositoryMoch.STORE);
    }

    public findOne({ where }: { where: Partial<UserEntity> }, errorMessage?: string): Promise<UserEntity | null> {
        this.throwError(errorMessage);
        const user = UserRepositoryMoch.STORE.find((u) => Object.keys(where).every((key) => u[key] === where[key]));
        return Promise.resolve(user || null);
    }

    public async save(
        { id, fullName, shortName, email, status }: Partial<UserEntity>,
        errorMessage?: string,
    ): Promise<UserEntity> {
        this.throwError(errorMessage);

        if (!fullName || !shortName || !email) {
            throw new Error('fullName, shortName, email are required');
        }

        // eslint-disable-next-line no-param-reassign
        status = status || EUserStatus.CREATED;

        let newUser = new UserEntity();
        let existingUser: UserEntity | null = null;

        if (id) {
            existingUser = await this.findOne({ where: { id } });
            if (existingUser !== null) {
                newUser = existingUser;
            }
        } else {
            // eslint-disable-next-line no-param-reassign
            id = randomUUID();
        }

        newUser.id = id;
        newUser.fullName = fullName;
        newUser.shortName = shortName;
        newUser.email = email;
        newUser.status = status;

        if (existingUser === null) {
            UserRepositoryMoch.STORE.push(newUser);
        }

        return newUser;
    }

    public async update(userId: string, updateData: Partial<UserEntity>, errorMessage?: string): Promise<void> {
        this.throwError(errorMessage);

        const user = await this.findOne({ where: { id: userId } });
        if (user === null) {
            throw new Error(`[UserRepository Moch] user not found`);
        }

        Object.keys(updateData).forEach((key) => {
            if (key === 'id') {
                throw new Error(`[UserRepository Moch] you can't update an user's ID`);
            }
            // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
            user[key] = updateData[key];
        });
    }

    public delete(userId: string, errorMessage?: string): Promise<void> {
        this.throwError(errorMessage);

        const userIndex = UserRepositoryMoch.STORE.findIndex((u) => u.id === userId);
        if (userIndex === -1) {
            throw new Error(`[UserRepository Moch] you trying to deleta a non existent user`);
        }
        UserRepositoryMoch.STORE.splice(userIndex, 1);
        return Promise.resolve();
    }

    private throwError(errorMessage?: string): void {
        if (errorMessage) {
            throw new Error(`[UserRepository Moch] ${errorMessage}`);
        }
    }
}
