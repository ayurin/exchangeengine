import { Exists, RmqFail, RmqResult, RmqSuccess } from '@app/utils';
import { Injectable } from '@nestjs/common';

interface IMochCredential {
    id: string;
    login: string;
    passwordHash: string;
    salt: string;
}

@Injectable()
export class AuthRmqClientMoch {
    private static readonly STORE: IMochCredential[] = [];

    public checkLoginExisting(login: string): Promise<Exists> {
        this.beforeHook();
        const cred = AuthRmqClientMoch.STORE.find((c) => c.login === login);
        const exists = cred !== undefined;
        return Promise.resolve(new Exists(exists));
    }

    public async sendRegistrationDataToAuth(userId: string, login: string, password: string): Promise<RmqResult> {
        this.beforeHook();

        const existingUser = AuthRmqClientMoch.STORE.find((c) => c.login === login);
        if (existingUser !== undefined) {
            return new RmqFail('this login already taken');
        }

        // TODO: fix it
        const [passwordHash, salt] = [password, 'salt'];

        AuthRmqClientMoch.STORE.push({
            id: userId,
            login,
            passwordHash,
            salt,
        });

        return new RmqSuccess();
    }

    public async sendContinueRegistrationToAuth(userId: string, login: string, password: string): Promise<RmqResult> {
        this.beforeHook();

        // TODO: fix it
        const [passwordHash, salt] = [password, 'salt'];

        const existingUser = AuthRmqClientMoch.STORE.find((c) => c.login === login);

        if (existingUser !== undefined) {
            existingUser.passwordHash = passwordHash;
            existingUser.salt = salt;
            return new RmqSuccess();
        }

        AuthRmqClientMoch.STORE.push({
            id: userId,
            login,
            passwordHash,
            salt,
        });

        return new RmqSuccess();
    }

    protected beforeHook(): void {}
}
