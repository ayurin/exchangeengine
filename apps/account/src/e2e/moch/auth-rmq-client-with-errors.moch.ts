import { Injectable } from '@nestjs/common';

import { AuthRmqClientMoch } from './auth-rmq-client.moch';

@Injectable()
export class AuthRmqClientMochWithErrors extends AuthRmqClientMoch {
    private static counter = 1;

    protected override beforeHook(): void {
        // eslint-disable-next-line no-plusplus
        AuthRmqClientMochWithErrors.counter++;
        if (AuthRmqClientMochWithErrors.counter % 2 !== 0) {
            throw new Error(`[AuthRmqClientMochWithErrors] it's error time`);
        }
        return super.beforeHook();
    }
}
