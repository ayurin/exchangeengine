import { provideServiceDefaultMock } from '@haqqex-backend/testing';
import { Test, TestingModule } from '@nestjs/testing';

import { CurrencyRepository } from '@account/dao';
import { CurrencyPipe } from '@account/pipes';
import { TransactionService } from '@account/transaction/services/transaction.service';

import { TransactionController } from '../transaction/transaction.controller';

describe('transactionController', () => {
    let controller: TransactionController;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            controllers: [TransactionController],
            providers: [
                provideServiceDefaultMock(TransactionService),
                provideServiceDefaultMock(CurrencyPipe),
                provideServiceDefaultMock(CurrencyRepository),
            ],
        }).compile();

        controller = module.get<TransactionController>(TransactionController);
    });

    it('should be defined', () => {
        expect(controller).toBeDefined();
    });
});
