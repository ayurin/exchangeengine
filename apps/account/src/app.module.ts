import { ExceptionModule } from '@app/exception';
import { RmqClientModule } from '@app/utils';
import { RabbitModule } from '@app/utils';
import { Module } from '@nestjs/common';
import { ScheduleModule } from '@nestjs/schedule';
import { TypeOrmModule } from '@nestjs/typeorm';
import { LoggerModule } from 'nestjs-pino';

import { AddressesModule } from '@account/addresses';
import { AdminModule } from '@account/admin/admin.module';
import { ApiDocModule } from '@account/api-doc/api-doc.module';
import { BalanceAccountModule } from '@account/balance-account';
import { BalancesModule } from '@account/balances';
import { CurrencyModule } from '@account/currency';
import { EntryModule } from '@account/entry';
import { dataSourceOptions, loggerModuleAsyncParams } from '@account/env';
import { HealthModule } from '@account/health';
import { NetworkModule } from '@account/network';
import { NetworkCurrencyModule } from '@account/network-currency';
import { PortfolioModule } from '@account/portfolio';
import { RegistrationModule } from '@account/registration';
import { SettingsModule } from '@account/settings/settings.module';
import { TfaModule } from '@account/tfa';
import { TransactionModule } from '@account/transaction';
import { UserModule } from '@account/user';
import { AccountingModule } from './accounting/accounting.module';

@Module({
    imports: [
        RabbitModule,
        RmqClientModule,
        ExceptionModule,
        LoggerModule.forRootAsync(loggerModuleAsyncParams),
        TypeOrmModule.forRoot(dataSourceOptions),
        ScheduleModule.forRoot(),
        AdminModule,
        UserModule,
        HealthModule,
        CurrencyModule,
        BalanceAccountModule,
        TransactionModule,
        EntryModule,
        ApiDocModule,
        BalancesModule,
        PortfolioModule,
        SettingsModule,
        NetworkModule,
        NetworkCurrencyModule,
        RegistrationModule,
        AddressesModule,
        TfaModule,
        AccountingModule,
    ],
})
export class AppModule {}
