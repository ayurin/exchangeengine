import { dbConfig } from './db.config';

// HTTP
const APP_NAME = process.env.APP_NAME;
if (!APP_NAME) {
    throw new Error('the APP_NAME environment variable is not defined');
}

const appPort = process.env.APP_PORT;
if (!appPort) {
    throw new Error('the APP_PORT environment variable is not defined');
}
const APP_PORT = Number(appPort);
if (!APP_PORT || APP_PORT % 1 !== 0) {
    throw new Error('the APP_PORT environment variable is not correct');
}

// RMQ
const RMQ_HOST = process.env.RMQ_HOST;
if (!RMQ_HOST) {
    throw new Error('the RMQ_HOST environment variable is not defined');
}

const rmqPot = process.env.RMQ_PORT;
if (!rmqPot) {
    throw new Error('the RMQ_PORT environment variable is not defined');
}
const RMQ_PORT = Number(rmqPot);
if (!RMQ_PORT || RMQ_PORT <= 1000 || RMQ_PORT % 1 !== 0) {
    throw new Error('the RMQ_PORT environment variable is not correct');
}

const RMQ_USER = process.env.RMQ_USER;
if (!RMQ_USER) {
    throw new Error('the RMQ_USER environment variable is not defined');
}

const RMQ_PASSWORD = process.env.RMQ_PASSWORD;
if (!RMQ_PASSWORD) {
    throw new Error('the RMQ_PASSWORD environment variable is not defined');
}

const RMQ_URL = `amqp://${RMQ_USER}:${RMQ_PASSWORD}@${RMQ_HOST}:${RMQ_PORT}`;

const API_PREFIX = process.env.API_PREFIX;
if (!API_PREFIX) {
    throw new Error('the API_PREFIX environment variable is not defined');
}

const INTERNAL_WALLET_DOMAIN = process.env.INTERNAL_WALLET_DOMAIN;
if (!INTERNAL_WALLET_DOMAIN) {
    throw new Error('INTERNAL_WALLET_DOMAIN environment variable is not defined');
}

export const appConfig = Object.freeze({
    APP_NAME,
    APP_PORT,
    ...dbConfig,
    RMQ_URL,
    API_PREFIX,
    INTERNAL_WALLET_DOMAIN,
});
