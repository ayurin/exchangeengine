import { Injectable } from '@nestjs/common';

import { NetworkEntity } from '@account/dao';
import { NetworkRepository } from '@account/dao/network.repository';
import {
    NetworkPatchRequestDto,
    NetworkPostRequestDto,
    NetworkQueryRequestDto,
} from '@account/network/dto/network.request.dto';

// was copied from wallet service
export enum EBlockchains {
    Bitcoin = 'bitcoin',
    Ethereum = 'ethereum',
    Polkadot = 'polkadot',
    Solana = 'solana',
    Cosmos = 'cosmos',
    Tron = 'tron',
    Neo = 'neo',
    Cardano = 'cardano',
    Dogecoin = 'dogecoin',
    Xrp = 'xrp',
    Litecoin = 'litecoin',
    Islamicoin = 'islamicoin',
}

@Injectable()
export class NetworkService {
    constructor(private readonly networkRepository: NetworkRepository) {}

    public finManyAndCount(params: NetworkQueryRequestDto): Promise<[NetworkEntity[], number]> {
        return this.networkRepository.findNetworksWithPagination(params);
    }

    public getOneById(id: string): Promise<NetworkEntity> {
        return this.networkRepository.getOneByIdOrFail(id);
    }

    public async createNetwork(payload: NetworkPostRequestDto): Promise<NetworkEntity> {
        return this.networkRepository.save(new NetworkEntity(payload));
    }

    public async updateNetwork(id: string, payload: NetworkPatchRequestDto): Promise<NetworkEntity> {
        const network = await this.networkRepository.getOneByIdOrFail(id);

        network.update(payload);
        await this.networkRepository.save(network);
        return network;
    }

    public async deleteNetwork(id: string): Promise<boolean> {
        const network = await this.networkRepository.getOneByIdOrFail(id);

        const res = await this.networkRepository.softDelete(network.id);
        return (res.affected ?? 0) > 0;
    }
}
