import { ApiAuth, TokenAudience } from '@app/jwt';
import {
    ACCOUNT_CREATE_NETWORK_PATTERN,
    ACCOUNT_DELETE_NETWORK_PATTERN,
    ACCOUNT_GET_NETWORK_PATTERN,
    ACCOUNT_GET_NETWORKS_PATTERN,
    ACCOUNT_UPDATE_NETWORK_PATTERN,
} from '@app/utils';
import { OkResponse } from '@app/utils';
import { Body, Controller, Delete, Get, HttpStatus, Param, Patch, Post, Query } from '@nestjs/common';
import { Ctx, MessagePattern, Payload, RmqContext } from '@nestjs/microservices';
import { ApiBody, ApiResponse, ApiTags } from '@nestjs/swagger';
import { Channel, Message } from 'amqplib';

import {
    NetworkPatchRequestDto,
    NetworkPostRequestDto,
    NetworkQueryRequestDto,
} from '@account/network/dto/network.request.dto';
import { ManyNetworksResponseDto, OneNetworkResponseDto } from '@account/network/dto/network.response.dto';
import { NetworkService } from '@account/network/network.service';

@ApiTags('networks')
@Controller('networks')
export class NetworkController {
    constructor(private readonly networkService: NetworkService) {}

    @Get()
    @ApiAuth(TokenAudience.ADMIN, TokenAudience.ACCOUNT)
    @ApiResponse({ type: ManyNetworksResponseDto, status: HttpStatus.OK })
    public async getManyNetworks(@Query() query: NetworkQueryRequestDto): Promise<ManyNetworksResponseDto> {
        const [networks, count] = await this.networkService.finManyAndCount(query);
        return new ManyNetworksResponseDto(networks, count, query);
    }

    @Get(':id')
    @ApiAuth(TokenAudience.ADMIN, TokenAudience.ACCOUNT)
    @ApiResponse({ type: OneNetworkResponseDto, status: HttpStatus.OK })
    public async getOneById(@Param('id') id: string): Promise<OneNetworkResponseDto> {
        const network = await this.networkService.getOneById(id);
        return new OneNetworkResponseDto(network);
    }

    @Post()
    @ApiAuth(TokenAudience.ADMIN)
    @ApiBody({ type: NetworkPostRequestDto })
    @ApiResponse({ type: OneNetworkResponseDto, status: HttpStatus.CREATED })
    public async createNetwork(@Body() body: NetworkPostRequestDto): Promise<OneNetworkResponseDto> {
        const network = await this.networkService.createNetwork(body);
        return new OneNetworkResponseDto(network);
    }

    @Patch(':id')
    @ApiAuth(TokenAudience.ADMIN)
    @ApiBody({ type: NetworkPatchRequestDto })
    @ApiResponse({ type: OneNetworkResponseDto, status: HttpStatus.OK })
    public async updateNetwork(
        @Param('id') id: string,
        @Body() body: NetworkPatchRequestDto,
    ): Promise<OneNetworkResponseDto> {
        const network = await this.networkService.updateNetwork(id, body);
        return new OneNetworkResponseDto(network);
    }

    @Delete(':id')
    @ApiAuth(TokenAudience.ADMIN)
    @ApiResponse({ type: OkResponse, status: HttpStatus.OK })
    public async deleteNetwork(@Param('id') id: string): Promise<OkResponse> {
        await this.networkService.deleteNetwork(id);
        return new OkResponse('Network wad deleted');
    }

    @MessagePattern(ACCOUNT_GET_NETWORKS_PATTERN)
    public async rmqGetManyNetworks(
        @Payload() query: NetworkQueryRequestDto,
        @Ctx() context: RmqContext,
    ): Promise<ManyNetworksResponseDto> {
        const channel = context.getChannelRef() as Channel;
        const originalMsg = context.getMessage() as Message;
        channel.ack(originalMsg);
        const [networks, count] = await this.networkService.finManyAndCount(query);
        return new ManyNetworksResponseDto(networks, count, query);
    }

    @MessagePattern(ACCOUNT_GET_NETWORK_PATTERN)
    public async rmqGetOneById(@Payload() id: string, @Ctx() context: RmqContext): Promise<OneNetworkResponseDto> {
        const channel = context.getChannelRef() as Channel;
        const originalMsg = context.getMessage() as Message;
        channel.ack(originalMsg);
        const network = await this.networkService.getOneById(id);
        return new OneNetworkResponseDto(network);
    }

    @MessagePattern(ACCOUNT_CREATE_NETWORK_PATTERN)
    public async rmqCreateNetwork(
        @Payload() body: NetworkPostRequestDto,
        @Ctx() context: RmqContext,
    ): Promise<OneNetworkResponseDto> {
        const channel = context.getChannelRef() as Channel;
        const originalMsg = context.getMessage() as Message;
        channel.ack(originalMsg);
        const network = await this.networkService.createNetwork(body);
        return new OneNetworkResponseDto(network);
    }

    @MessagePattern(ACCOUNT_UPDATE_NETWORK_PATTERN)
    public async rmqUpdateNetwork(
        @Payload() { id, ...body }: NetworkPatchRequestDto & { id: string },
        @Ctx() context: RmqContext,
    ): Promise<OneNetworkResponseDto> {
        const channel = context.getChannelRef() as Channel;
        const originalMsg = context.getMessage() as Message;
        channel.ack(originalMsg);
        const network = await this.networkService.updateNetwork(id, body);
        return new OneNetworkResponseDto(network);
    }

    @MessagePattern(ACCOUNT_DELETE_NETWORK_PATTERN)
    public async rmqDeleteNetwork(@Payload() id: string, @Ctx() context: RmqContext): Promise<OkResponse> {
        const channel = context.getChannelRef() as Channel;
        const originalMsg = context.getMessage() as Message;
        channel.ack(originalMsg);
        await this.networkService.deleteNetwork(id);
        return new OkResponse('Network wad deleted');
    }
}
