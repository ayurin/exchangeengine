// eslint-disable-next-line max-classes-per-file
// import { ICreateNetworkBody, INetworkQuery, IPatchNetworkBody } from '@app/utils';
import { PaginationQuery } from '@app/utils';
import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import { IsEnum, IsNotEmpty, IsOptional, IsString } from 'class-validator';

import { EBlockchains } from '@account/network/network.service';

export class NetworkQueryRequestDto extends PaginationQuery //implements INetworkQuery 
{
    @ApiPropertyOptional()
    @IsString()
    @IsOptional()
    public search?: string;

    @ApiPropertyOptional()
    @IsString()
    @IsOptional()
    public currency?: string;
}

export class NetworkPostRequestDto //implements ICreateNetworkBody
 {
    @ApiProperty({ example: EBlockchains.Bitcoin })
    @IsEnum(EBlockchains)
    @IsNotEmpty()
    public name: string;

    @ApiProperty()
    @IsString()
    @IsNotEmpty()
    public nativeToken: string;
}

export class NetworkPatchRequestDto // implements Omit<IPatchNetworkBody, 'id'>
{
    @ApiPropertyOptional()
    @IsEnum(EBlockchains)
    @IsOptional()
    public name: string;

    @ApiPropertyOptional()
    @IsString()
    @IsOptional()
    public nativeToken: string;
}
