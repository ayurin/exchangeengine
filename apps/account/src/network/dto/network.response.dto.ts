import { PaginatedResponse, PaginationQuery } from '@app/utils';
import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';

import { NetworkEntity } from '@account/dao';

export interface INetworkAdditionalData {
    arrivalTime?: string;
    feeCoinValue?: string;
    feeFiatValue?: string;
}

export class NetworkResponseDto {
    @ApiProperty({ example: '69167015-A216-4DF5-93BD-B702CF6FA804' })
    public id: string;

    @ApiProperty({ example: 'ERC20' })
    public name: string;

    @ApiProperty({ example: 'Ethereum' })
    public nativeToken: string;

    @ApiPropertyOptional()
    public arrivalTime?: string;

    @ApiPropertyOptional()
    public feeCoinValue?: string;

    @ApiPropertyOptional()
    public feeFiatValue?: string;

    @ApiProperty()
    public createdAt: Date;

    @ApiProperty()
    public updatedAt: Date;

    constructor(item?: NetworkEntity, data?: INetworkAdditionalData) {
        if (!item) {
            return;
        }

        this.id = item.id;
        this.name = item.name;
        this.nativeToken = item.nativeToken;
        this.createdAt = item.createdAt;
        this.updatedAt = item.updatedAt;
        this.arrivalTime = data?.arrivalTime;
        this.feeCoinValue = data?.feeCoinValue;
        this.feeFiatValue = data?.feeFiatValue;
    }
}

export class OneNetworkResponseDto {
    @ApiProperty({ type: NetworkResponseDto })
    public data: NetworkResponseDto;

    constructor(item: NetworkEntity) {
        this.data = new NetworkResponseDto(item);
    }
}

export class ManyNetworksResponseDto extends PaginatedResponse<NetworkResponseDto> {
    @ApiProperty({ type: NetworkResponseDto, isArray: true })
    public data: Array<NetworkResponseDto>;

    constructor(items: NetworkEntity[], total: number, paginationQuery: PaginationQuery) {
        super(
            items.map(
                (item) =>
                    new NetworkResponseDto(item, {
                        arrivalTime: '40 min',
                        feeCoinValue: '20000',
                        feeFiatValue: '337000000',
                    }),
            ),
            total,
            paginationQuery,
        );
    }
}
