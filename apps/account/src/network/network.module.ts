import { Module } from '@nestjs/common';

import { NetworkRepository } from '@account/dao/network.repository';

import { NetworkController } from './network.controller';
import { NetworkService } from './network.service';

@Module({
    controllers: [NetworkController],
    providers: [NetworkService, NetworkRepository],
})
export class NetworkModule {}
