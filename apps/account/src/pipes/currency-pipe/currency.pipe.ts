/* eslint-disable no-param-reassign */
import { Injectable, PipeTransform } from '@nestjs/common';

import { CurrencyEntity, CurrencyRepository } from '@account/dao';

interface ICurrency {
    currency: string | CurrencyEntity;
    feeCurrency: string | CurrencyEntity;
}

@Injectable()
export class CurrencyPipe implements PipeTransform {
    constructor(private currencyRepository: CurrencyRepository) {}

    public async transform(value: ICurrency): Promise<ICurrency> {
        if (typeof value?.currency === 'string') {
            value.currency = await this.currencyRepository.getOneByCdOrFail(value.currency);
        }
        if (typeof value?.feeCurrency === 'string') {
            value.feeCurrency = await this.currencyRepository.getOneByCdOrFail(value.feeCurrency);
        }
        return Promise.resolve(value);
    }
}
