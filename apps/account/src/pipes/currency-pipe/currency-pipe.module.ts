import { Module } from '@nestjs/common';

import { CurrencyRepository } from '@account/dao';
import { CurrencyPipe } from '@account/pipes/currency-pipe/currency.pipe';

@Module({
    providers: [CurrencyPipe, CurrencyRepository],
    exports: [CurrencyPipe, CurrencyRepository],
})
export class CurrencyPipeModule {}
