import { CurrencyEntity } from '@account/dao';
import { ENTRY_CONFIG, EntryConfigType, IEntrySideData, IEntrySidesData } from '@account/transaction';

export interface ITopupFromFaucetMeta {
    currency: string;
    amount: string;
}

export function getEntrySidesData(
    method: EntryConfigType,
    amount: string,
    { id, shortName }: CurrencyEntity,
    debitCustomerId: string,
    creditCustomerId = debitCustomerId,
): IEntrySidesData {
    const { debit, credit } = ENTRY_CONFIG[method];
    return {
        debit: { ...debit, customerId: debitCustomerId } as IEntrySideData,
        credit: { ...credit, customerId: creditCustomerId } as IEntrySideData,
        amount,
        currencyId: id,
        currencyCd: shortName,
    };
}
