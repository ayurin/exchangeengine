import {
    ACCOUNT_CANCEL_ORDER_PATTERN,
    ACCOUNT_PLACE_ORDER_PATTERN,
    ACCOUNT_SETTLE_ORDER_PATTERN,
} from '@app/utils';
import { Body, Controller, HttpStatus, Post } from '@nestjs/common';
import { Ctx, MessagePattern, Payload, RmqContext } from '@nestjs/microservices';
import { ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { Channel, Message } from 'amqplib';

import { BalanceResponse } from '@account/dto';
import { CurrencyPipe } from '@account/pipes';
import { SettleOrderDto, TopupFromFaucetDto } from '@account/transaction/dto';
import { FromOrderDto } from '@account/transaction/dto/from-order.dto';
import { TransactionService } from '@account/transaction/services/transaction.service';

@ApiTags('transaction')
@Controller('transaction')
export class TransactionController {
    constructor(private readonly transactionService: TransactionService) {}

    @Post('topup-from-faucet')
    @ApiOperation({ summary: 'Account topup balance of customer using faucet' })
    // @ApiResponse({ status: HttpStatus.OK, type: BalanceResponse, isArray: true })
    public topupFromFaucet(@Body(CurrencyPipe) data: TopupFromFaucetDto): Promise<void> {
        return this.transactionService.topupFromFaucet(data);
    }

    @Post('place-order')
    @ApiOperation({ summary: 'Account lock of funds for the new order' })
    @ApiResponse({ status: HttpStatus.OK, type: BalanceResponse, isArray: true })
    public placeOrder(@Body(CurrencyPipe) data: FromOrderDto): Promise<BalanceResponse[]> {
        return this.transactionService.placeOrder(data);
    }

    @Post('cancel-order')
    @ApiOperation({ summary: 'Account unlock funds locked in order' })
    @ApiResponse({ status: HttpStatus.OK, type: BalanceResponse, isArray: true })
    public cancelOrder(@Body(CurrencyPipe) data: FromOrderDto): Promise<BalanceResponse[]> {
        return this.transactionService.cancelOrder(data);
    }

    @Post('settle-order')
    @ApiOperation({ summary: 'Account settle trade for match event' })
    @ApiResponse({ status: HttpStatus.OK, type: BalanceResponse, isArray: true })
    public settleOrder(@Body() data: SettleOrderDto): Promise<BalanceResponse[]> {
        return this.transactionService.settleOrder(data);
    }

    @MessagePattern(ACCOUNT_PLACE_ORDER_PATTERN)
    public async rmqPlaceOrder(
        @Payload(CurrencyPipe) data: FromOrderDto,
        @Ctx() context: RmqContext,
    ): Promise<BalanceResponse[]> {
        const channel = context.getChannelRef() as Channel;
        const originalMsg = context.getMessage() as Message;
        channel.ack(originalMsg);
        return this.transactionService.placeOrder(data);
    }

    @MessagePattern(ACCOUNT_CANCEL_ORDER_PATTERN)
    public async rmqCancelOrder(
        @Payload(CurrencyPipe) data: FromOrderDto,
        @Ctx() context: RmqContext,
    ): Promise<BalanceResponse[]> {
        const channel = context.getChannelRef() as Channel;
        const originalMsg = context.getMessage() as Message;
        channel.ack(originalMsg);
        return this.transactionService.cancelOrder(data);
    }

    @MessagePattern(ACCOUNT_SETTLE_ORDER_PATTERN)
    public async rmqSettleOrder(
        @Payload() data: SettleOrderDto,
        @Ctx() context: RmqContext,
    ): Promise<BalanceResponse[]> {
        const channel = context.getChannelRef() as Channel;
        const originalMsg = context.getMessage() as Message;
        channel.ack(originalMsg);
        return this.transactionService.settleOrder(data);
    }
}
