import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsNumberString, IsOptional, IsString, IsUUID } from 'class-validator';

import { CurrencyEntity } from '@account/dao';
import { IsUser } from '@account/validators';

export class FromOrderDto {
    @ApiProperty({ example: '02e2d713-0c30-44a3-aeb8-3ec849384d0e' })
    @IsUser()
    public customerId: string;

    @ApiProperty({ example: '079aaaf7-4f56-4b0a-ad91-2699f8e3f8e3' })
    @IsUUID()
    @IsNotEmpty()
    public orderId: string;

    @ApiProperty({ example: 'BTC' })
    @IsString()
    @IsNotEmpty()
    // CurrencyPipe string => CurrencyEntity
    public currency: CurrencyEntity;

    @ApiProperty({ example: (1_7500_0000).toString() })
    @IsNumberString()
    @IsNotEmpty()
    public amount: string;

    @ApiProperty({ example: 'USDC' })
    @IsOptional()
    @IsString()
    // CurrencyPipe string => CurrencyEntity
    public feeCurrency: CurrencyEntity;

    @ApiProperty({ example: (1_580_000).toString() })
    @IsOptional()
    @IsNumberString()
    public fee: string;
}
