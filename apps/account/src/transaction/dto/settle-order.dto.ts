import { randomUUID } from 'crypto';

import { ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { IsOptional, IsUUID, ValidateNested } from 'class-validator';

import { CurrencyAmountDto } from '@account/transaction/dto/currency-amount.dto';
import { SettleOrderSideDto } from '@account/transaction/dto/settle-order-side.dto';

const customerId1 = '02e2d713-0c30-44a3-aeb8-3ec849384d0e';
const customerId2 = 'ca742a5e-3954-4a09-958e-307dc0c38e76';

export const firstOrder = new SettleOrderSideDto({
    orderId: randomUUID(),
    customerId: customerId1,
    forExchange: new CurrencyAmountDto({
        currency: 'USDC',
        amount: (1550_000_000).toString(),
    }),
    forFee: new CurrencyAmountDto({
        currency: 'USDC',
        amount: (1_550_000).toString(),
    }),
    forRefunds: new CurrencyAmountDto({
        currency: 'USDC',
        amount: (1_550_000).toString(),
    }),
});

export const secondOrder = new SettleOrderSideDto({
    orderId: randomUUID(),
    customerId: customerId2,
    forExchange: new CurrencyAmountDto({
        currency: 'ETH',
        amount: (1_000_000_000_000_000_000).toString(),
    }),
    forFee: new CurrencyAmountDto({
        currency: 'USDC',
        amount: (3_100_000).toString(),
    }),
});

export class SettleOrderDto {
    @ApiProperty({ example: '079aaaf7-4f56-4b0a-ad91-2699f8e3f8e3' })
    @IsUUID()
    public tradeId: string;

    @ApiProperty({ example: firstOrder })
    @IsOptional()
    @ValidateNested()
    @Type(() => SettleOrderSideDto)
    public buyOrder: SettleOrderSideDto;

    @ApiProperty({ example: secondOrder })
    @IsOptional()
    @ValidateNested()
    @Type(() => SettleOrderSideDto)
    public sellOrder: SettleOrderSideDto;
}
