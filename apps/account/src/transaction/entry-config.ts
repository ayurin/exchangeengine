import { ChangeBalanceType } from '@account/utils';

export type EntryConfigType = 'emit' | 'lock' | 'unlockOrExchange' | 'feeFromLock' | 'feeFromUnlock';

export const BA_AVAILABLE = 'C2600';
export const BA_LOCKED = 'C2601';
export const BA_BROKER_FEE = 'C6110';

export interface IEntrySideData {
    customerId: string;
    changeBalanceType: ChangeBalanceType;
    baNumber: string;
    debitFlg: boolean;
    checkNullBalance: boolean;
}

export interface IEntrySidesData {
    debit: IEntrySideData;
    credit: IEntrySideData;
    amount: string;
    currencyId: string;
    currencyCd: string;
}

export const ENTRY_CONFIG: Record<EntryConfigType, Partial<IEntrySidesData>> = {
    emit: {
        debit: {
            customerId: '',
            changeBalanceType: 'inc',
            baNumber: 'D1600',
            debitFlg: true,
            checkNullBalance: false,
        },
        credit: {
            customerId: '',
            changeBalanceType: 'inc',
            baNumber: BA_AVAILABLE,
            debitFlg: false,
            checkNullBalance: false,
        },
    },
    lock: {
        debit: {
            customerId: '',
            changeBalanceType: 'dec',
            baNumber: BA_AVAILABLE,
            debitFlg: false,
            checkNullBalance: true,
        },
        credit: {
            customerId: '',
            changeBalanceType: 'inc',
            baNumber: BA_LOCKED,
            debitFlg: false,
            checkNullBalance: false,
        },
    },
    unlockOrExchange: {
        debit: {
            customerId: '',
            changeBalanceType: 'dec',
            baNumber: BA_LOCKED,
            debitFlg: false,
            checkNullBalance: true,
        },
        credit: {
            customerId: '',
            changeBalanceType: 'inc',
            baNumber: BA_AVAILABLE,
            debitFlg: false,
            checkNullBalance: false,
        },
    },
    feeFromLock: {
        debit: {
            customerId: '',
            changeBalanceType: 'dec',
            baNumber: BA_LOCKED,
            debitFlg: false,
            checkNullBalance: true,
        },
        credit: {
            customerId: '',
            changeBalanceType: 'inc',
            baNumber: BA_BROKER_FEE,
            debitFlg: false,
            checkNullBalance: false,
        },
    },
    feeFromUnlock: {
        debit: {
            customerId: '',
            changeBalanceType: 'dec',
            baNumber: BA_AVAILABLE,
            debitFlg: false,
            checkNullBalance: true,
        },
        credit: {
            customerId: '',
            changeBalanceType: 'inc',
            baNumber: BA_BROKER_FEE,
            debitFlg: false,
            checkNullBalance: false,
        },
    },
};
