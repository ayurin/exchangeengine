import { Module } from '@nestjs/common';

import { CurrencyRepository } from '@account/dao';
import { CurrencyPipeModule } from '@account/pipes/currency-pipe';
import { IsUserValidatorModule } from '@account/validators';

import { DbTransactionService } from './services/db-transaction.service';
import { TransactionService } from './services/transaction.service';
import { TransactionController } from './transaction.controller';
import { AccountingModule } from '../accounting/accounting.module'

@Module({
    controllers: [TransactionController],
    providers: [TransactionService, DbTransactionService, CurrencyRepository],
    imports: [IsUserValidatorModule, CurrencyPipeModule, AccountingModule],
})
export class TransactionModule {}
