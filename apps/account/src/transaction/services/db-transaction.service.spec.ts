import { mockDataSource } from '@haqqex-backend/testing';
import { Test, TestingModule } from '@nestjs/testing';
import { DataSource } from 'typeorm';

import { OperationType } from '@account/dao';
import { DbTransactionService, IEntrySidesData } from '@account/transaction';

const entrySidesList: IEntrySidesData[] = [
    {
        debit: {
            customerId: '00000000-0000-0000-0000-000000000000',
            changeBalanceType: 'inc',
            baNumber: 'D1600',
            debitFlg: true,
            checkNullBalance: false,
        },
        credit: {
            customerId: '02e2d713-0c30-44a3-aeb8-3ec849384d0e',
            changeBalanceType: 'inc',
            baNumber: 'C2600',
            debitFlg: false,
            checkNullBalance: false,
        },
        amount: '1000000000',
        currencyId: 'bb6305c7-3a0d-49f7-89a2-6589d50643f6',
        currencyCd: 'BTC',
    },
];
const method: OperationType = 'topupFromFaucet';
const customerIds: string[] = ['02e2d713-0c30-44a3-aeb8-3ec849384d0e'];
const operationMetadata = { currency: 'BTC', amount: '1000000000' };
const metadata = {};

describe('dbTransactionService', () => {
    let service: DbTransactionService;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            providers: [
                DbTransactionService,
                {
                    provide: DataSource,
                    useValue: mockDataSource,
                },
            ],
        }).compile();

        service = module.get(DbTransactionService);
    });

    describe('transaction()', () => {
        describe('returns void', () => {
            let result: void;
            const expected = void 0 as void;

            beforeEach(async () => {
                jest.mock('', () => jest.fn());
                // result = await service.transaction(entrySidesList, method, customerIds, operationMetadata, metadata);
                result = await Promise.resolve();
            });

            it('should check equal between result and expected', () => {
                expect(result).toBe(expected);
            });
        });
    });
});
