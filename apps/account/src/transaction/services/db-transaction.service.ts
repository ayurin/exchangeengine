import { ZERO_BIGINT } from '@app/utils';
import { Injectable } from '@nestjs/common';
import { DataSource, EntityManager } from 'typeorm';

import { EntryEntity, OperationEntity, OperationStatus, OperationType, UserAccountEntity } from '@account/dao';
import { IEntrySideData, IEntrySidesData } from '@account/transaction';
import { BROKER_MOCK_ID, changeBalance, getAccountName, getAccountNumber, getBalanceAccountNumber } from '@account/utils';

interface IEntrySides {
    debit: UserAccountEntity;
    credit: UserAccountEntity;
    amount: string;
    currency: string;
}

interface IEntriesAndAccounts {
    entries: EntryEntity[];
    accounts: UserAccountEntity[];
}

function filterAccount(account: UserAccountEntity, accounts: UserAccountEntity[]): boolean {
    return (
        account.userId !== BROKER_MOCK_ID &&
        !accounts.find((item) => item.updated > account.updated && item.id === account.id)
    );
}

@Injectable()
export class DbTransactionService {
    constructor(private dataSource: DataSource) {}

    public async transaction(
        entrySidesList: IEntrySidesData[],
        method: OperationType,
        customerIds: string[],
        operationMetadata: unknown,
        metadata = {},
    ): Promise<UserAccountEntity[]> {
        const queryRunner = this.dataSource.createQueryRunner();
        const manager = queryRunner.manager;
        try {
            await queryRunner.connect();
            await queryRunner.startTransaction();
            let operation = this.prepareOperation(manager, method, customerIds, operationMetadata);
            operation = await queryRunner.manager.save(operation);
            const entrySides: IEntrySides[] = [];
            for (const item of entrySidesList) {
                entrySides.push(await this.updateBalanceAndGetSides(manager, item));
            }
            const { entries, accounts } = this.getEntriesAndBalances(manager, entrySides, operation.id, metadata);
            await queryRunner.manager.save(entries);
            await queryRunner.commitTransaction();
            await queryRunner.release();
            return accounts;
        } catch (err) {
            await queryRunner.rollbackTransaction();
            await queryRunner.release();
            throw err;
        }
    }

    private getEntriesAndBalances(
        manager: EntityManager,
        entrySides: IEntrySides[],
        operationId: string,
        metadata = {},
    ): IEntriesAndAccounts {
        const accounts: UserAccountEntity[] = [];
        const entries = entrySides.map(({ debit, credit, amount, currency }) => {
            accounts.push(...[debit, credit]);
            return this.prepareEntry(manager, debit, credit, amount, currency, operationId, metadata);
        });
        return { accounts: accounts.filter((account) => filterAccount(account, accounts)), entries };
    }

    private prepareOperation(
        manager: EntityManager,
        type: OperationType,
        customerIds: string[],
        metadata: unknown,
        status: OperationStatus = 'confirmed',
    ): OperationEntity {
        return manager.create(OperationEntity, { type, userIds: customerIds, metadata, status });
    }

    private async updateBalanceAndGetSides(
        manager: EntityManager,
        { debit, credit, amount, currencyId, currencyCd }: IEntrySidesData,
    ): Promise<IEntrySides> {
        const debitSide = await this.updateBalanceAndGetSide(manager, debit, currencyId, currencyCd, amount);
        const creditSide = await this.updateBalanceAndGetSide(manager, credit, currencyId, currencyCd, amount);
        return { debit: debitSide, credit: creditSide, amount, currency: currencyCd };
    }

    private async updateBalanceAndGetSide(
        manager: EntityManager,
        { customerId, baNumber, changeBalanceType, debitFlg, checkNullBalance }: IEntrySideData,
        currencyId: string,
        currencyCd: string,
        amount: string,
    ): Promise<UserAccountEntity> {
        const updated = new Date();
        let balance: bigint;
        let customerAccount: UserAccountEntity | null;
        const balanceAccountNumber = getBalanceAccountNumber(baNumber, currencyCd);
        customerAccount = await this.getCustomerAccount(manager, customerId, balanceAccountNumber);
        if (customerAccount) {
            balance = changeBalance(changeBalanceType, customerAccount.balance, amount, checkNullBalance);
            await manager.update(UserAccountEntity, customerAccount.id, { balance, updated });
        } else {
            balance = changeBalance(changeBalanceType, ZERO_BIGINT, amount, checkNullBalance);
            customerAccount = await this.saveNewCustomerAccount(
                manager,
                customerId,
                debitFlg,
                currencyId,
                currencyCd,
                balanceAccountNumber,
                balance,
            );
        }

        return { ...customerAccount, balance, updated };
    }

    private getCustomerAccount(
        manager: EntityManager,
        customerId: string,
        balanceAccountNumber: string,
    ): Promise<UserAccountEntity | null> {
        return manager.findOne(UserAccountEntity, {
            where: { userId: customerId, balanceAccountNumber },
            lock: { mode: 'pessimistic_write_or_fail' },
        });
    }

    private saveNewCustomerAccount(
        manager: EntityManager,
        customerId: string,
        debitFlg: boolean,
        currencyId: string,
        currencyCd: string,
        balanceAccountNumber: string,
        balance: bigint,
    ): Promise<UserAccountEntity> {
        const entity = manager.create(UserAccountEntity, {
            accountName: getAccountName(currencyCd, debitFlg),
            accountNumber: getAccountNumber(balanceAccountNumber, customerId),
            balanceAccountNumber,
            currencyCd,
            currencyId,
            debitFlg,
            userId: customerId,
            balance,
        });
        return manager.save(entity);
    }

    private prepareEntry(
        manager: EntityManager,
        debitData: UserAccountEntity,
        creditData: UserAccountEntity,
        amount: string,
        currency: string,
        operationId: string,
        metadata = {},
    ): EntryEntity {
        return manager.create(EntryEntity, {
            debitAccountId: debitData.id,
            debitAccountNumber: debitData.accountNumber,
            creditAccountId: creditData.id,
            creditAccountNumber: creditData.accountNumber,
            amount,
            currency,
            metadata,
            operationId,
        });
    }
}
