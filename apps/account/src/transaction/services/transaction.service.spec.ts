import { provideServiceDefaultMock } from '@haqqex-backend/testing';
import { ECurrencyType } from '@app/utils';
import { Test, TestingModule } from '@nestjs/testing';
import { PinoLogger } from 'nestjs-pino';

import { CurrencyEntity, CurrencyRepository } from '@account/dao';
import { BalanceResponse, OkResponse } from '@account/dto';
import { DbTransactionService, TransactionService } from '@account/transaction';
import { SettleOrderDto, TopupFromFaucetDto } from '@account/transaction/dto';

const customerId = '02e2d713-0c30-44a3-aeb8-3ec849384d0e';
const btcCurrency: CurrencyEntity = {
    id: 'bb6305c7-3a0d-49f7-89a2-6589d50643f6',
    shortName: 'BTC',
    isoCode: 'XBT',
    fullName: 'Bitcoin',
    decimals: 8n,
    active: true,
    type: ECurrencyType.CRYPTO,
};
const usdcCurrency: CurrencyEntity = {
    id: '91354be4-a368-4dcf-9dc2-103e6e380b72',
    shortName: 'USDC',
    isoCode: '',
    fullName: 'USD Coin',
    decimals: 6n,
    active: true,
    type: ECurrencyType.STABLE_OR_FIAT,
};
const topupFromFaucetData: TopupFromFaucetDto = {
    currency: btcCurrency,
    customerId,
    amount: '1000000000',
};

const fromOrderData = {
    currency: btcCurrency,
    customerId,
    amount: '175000000',
    orderId: '079aaaf7-4f56-4b0a-ad91-2699f8e3f8e3',
    fee: '1580000',
    feeCurrency: usdcCurrency,
};

const settleOrderData: SettleOrderDto = {
    tradeId: '079aaaf7-4f56-4b0a-ad91-2699f8e3f8e3',
    buyOrder: {
        orderId: 'ce28634c-c082-457d-803d-afd1c075cb29',
        customerId: '02e2d713-0c30-44a3-aeb8-3ec849384d0e',
        forExchange: { currency: 'USDC', amount: '1550000000' },
        forFee: { currency: 'USDC', amount: '1550000' },
        forRefunds: { currency: 'USDC', amount: '1550000' },
    },
    sellOrder: {
        orderId: '8c34f4c3-1087-48ae-9e44-3e5089717aad',
        customerId: 'c7920e53-8f2e-4f95-87dd-a023e6294876',
        forExchange: {
            currency: 'ETH',
            amount: '1000000000000000000',
        },
        forFee: { currency: 'USDC', amount: '3100000' },
    },
};

describe('devModeService', () => {
    let service: TransactionService;
    let currencyRepository: CurrencyRepository;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            providers: [
                TransactionService,
                provideServiceDefaultMock(PinoLogger),
                provideServiceDefaultMock(CurrencyRepository),
                provideServiceDefaultMock(DbTransactionService),
            ],
        }).compile();

        service = module.get<TransactionService>(TransactionService);
        currencyRepository = module.get(CurrencyRepository);
    });

    it('should be defined', () => {
        expect(service).toBeDefined();
    });

    describe('topupFromFaucet()', () => {
        describe('returns OkResponse', () => {
            let result: BalanceResponse[];
            const expected = new OkResponse();

            beforeEach(async () => {
                result = await service.topupFromFaucet(topupFromFaucetData);
            });

            it('should check equal between result and expected', () => {
                expect(result).toStrictEqual(expected);
            });
        });
    });

    describe('placeOrder()', () => {
        describe('returns OkResponse', () => {
            let result: BalanceResponse[];
            const expected = new OkResponse();

            beforeEach(async () => {
                result = await service.placeOrder(fromOrderData);
            });

            it('should check equal between result and expected', () => {
                expect(result).toStrictEqual(expected);
            });
        });
    });

    describe('cancelOrder()', () => {
        describe('returns OkResponse', () => {
            let result: BalanceResponse[];
            const expected = new OkResponse();

            beforeEach(async () => {
                result = await service.cancelOrder(fromOrderData);
            });

            it('should check equal between result and expected', () => {
                expect(result).toStrictEqual(expected);
            });
        });
    });

    describe('settleOrder()', () => {
        describe('returns OkResponse', () => {
            let repositoryMock: jest.SpyInstance;
            let result: BalanceResponse[];
            const expected = new OkResponse();

            beforeEach(async () => {
                repositoryMock = jest.spyOn(currencyRepository, 'getOneByCdOrFail').mockResolvedValue(usdcCurrency);

                result = await service.settleOrder(settleOrderData);
            });

            it('should call getOneByCdOrFail', () => {
                expect(repositoryMock).toHaveBeenCalledTimes(5);
                expect(repositoryMock).toHaveBeenCalledWith(settleOrderData.sellOrder.forExchange.currency);
            });

            it('should check equal between result and expected', () => {
                expect(result).toStrictEqual(expected);
            });
        });
    });
});
