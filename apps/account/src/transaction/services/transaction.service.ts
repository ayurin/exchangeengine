/* eslint-disable @typescript-eslint/no-unsafe-return,no-console,jest/unbound-method,@typescript-eslint/unbound-method */
import { Injectable } from '@nestjs/common';
import { plainToInstance } from 'class-transformer';
import { PinoLogger } from 'nestjs-pino';

import { CurrencyRepository } from '@account/dao';
import { BalanceResponse } from '@account/dto';
import { EntryConfigType, getEntrySidesData, IEntrySidesData, ITopupFromFaucetMeta } from '@account/transaction';
import {
    CurrencyAmountDto,
    FromOrderDto,
    SettleOrderDto,
    SettleOrderSideDto,
    TopupFromFaucetDto,
} from '@account/transaction/dto';
import { DbTransactionService } from '@account/transaction/services/db-transaction.service';
import { BROKER_MOCK_ID } from '@account/utils';
import { AccountingService } from '@account/accounting/accounting.service';

@Injectable()
export class TransactionService {
    constructor(
        private logger: PinoLogger,
        private dbTransactionService: DbTransactionService,
        private currencyRepository: CurrencyRepository,
        private accountingBalance: AccountingService
    ) {}

    public async topupFromFaucet(data: TopupFromFaucetDto): Promise<void> {
        const { currency, customerId, amount } = data;
        try {
            // const operationMetadata: ITopupFromFaucetMeta = { currency: currency.shortName, amount };
            // const emitData = getEntrySidesData('emit', amount, currency, BROKER_MOCK_ID, customerId);
            // const userAccounts = await this.dbTransactionService.transaction(
            //     [emitData],
            //     'topupFromFaucet',
            //     [customerId],
            //     operationMetadata,
            // );
            await this.accountingBalance.deposit({
                customerId, 
                currency: currency.shortName,
                amount, 
                as_of_date: new Date().valueOf()
            })
            // return plainToInstance(BalanceResponse, userAccounts);
        } catch (err) {
            this.logger.error(err);
            throw err;
        }
    }

    public async placeOrder(data: FromOrderDto): Promise<BalanceResponse[]> {
        const { currency, customerId, amount, orderId, fee, feeCurrency } = data;
        try {
            let operationMetadata = { currency: currency.shortName, amount, orderId };
            const lockData = getEntrySidesData('lock', amount, currency, customerId);
            const entriesData = [lockData];
            if (fee && feeCurrency) {
                operationMetadata = Object.assign(operationMetadata, { fee, feeCurrency: feeCurrency.shortName });
                const lockFeeData = getEntrySidesData('lock', fee, feeCurrency, customerId);
                entriesData.push(lockFeeData);
            }
            const userAccounts = await this.dbTransactionService.transaction(
                entriesData,
                'placeOrder',
                [customerId],
                operationMetadata,
            );
            return plainToInstance(BalanceResponse, userAccounts);
        } catch (err) {
            this.logger.error(err);
            throw err;
        }
    }

    public async cancelOrder(data: FromOrderDto): Promise<BalanceResponse[]> {
        const { currency, customerId, amount, orderId, fee, feeCurrency } = data;
        try {
            let operationMetadata = { currency: currency.shortName, amount, orderId };
            const lockData = getEntrySidesData('unlockOrExchange', amount, currency, customerId);
            const entriesData = [lockData];
            if (fee && feeCurrency) {
                operationMetadata = Object.assign(operationMetadata, { fee, feeCurrency: feeCurrency.shortName });
                const lockFeeData = getEntrySidesData('unlockOrExchange', fee, feeCurrency, customerId);
                entriesData.push(lockFeeData);
            }
            const userAccounts = await this.dbTransactionService.transaction(
                entriesData,
                'cancelOrder',
                [customerId],
                operationMetadata,
            );
            return plainToInstance(BalanceResponse, userAccounts);
        } catch (err) {
            this.logger.error(err);
            throw err;
        }
    }

    public async settleOrder(data: SettleOrderDto): Promise<BalanceResponse[]> {
        const { tradeId, buyOrder, sellOrder } = data;
        try {
            const operationMetadata = { tradeId, buyOrderId: buyOrder.orderId, sellOrderId: sellOrder.orderId };
            const buyEntries = await this.getEntriesForSettleOrder(buyOrder, sellOrder.customerId, true);
            const sellEntries = await this.getEntriesForSettleOrder(sellOrder, buyOrder.customerId, false);
            const entries = buyEntries.concat(sellEntries);
            const userAccounts = await this.dbTransactionService.transaction(
                entries,
                'settleOrder',
                [buyOrder.customerId, sellOrder.customerId],
                operationMetadata,
            );
            return plainToInstance(BalanceResponse, userAccounts);
        } catch (err) {
            this.logger.error(err);
            throw err;
        }
    }

    private async getEntriesForSettleOrder(
        { customerId, forExchange, forFee, forRefunds, forFeeRefunds }: SettleOrderSideDto,
        toCustomerId: string,
        buyer: boolean,
    ): Promise<IEntrySidesData[]> {
        const forExchangeEntry = await this.getEntrySideDataFromStrCurrency(
            'unlockOrExchange',
            forExchange,
            customerId,
            toCustomerId,
        );
        const forFeeEntry = await this.getEntrySideDataFromStrCurrency(
            buyer ? 'feeFromLock' : 'feeFromUnlock',
            forFee,
            customerId,
            BROKER_MOCK_ID,
        );
        const entries = [forExchangeEntry, forFeeEntry];
        if (forRefunds) {
            const forRefundsEntry = await this.getEntrySideDataFromStrCurrency(
                'unlockOrExchange',
                forRefunds,
                customerId,
            );
            entries.push(forRefundsEntry);
        }
        if (forFeeRefunds) {
            const forFeeRefundsEntry = await this.getEntrySideDataFromStrCurrency(
                'unlockOrExchange',
                forFeeRefunds,
                customerId,
            );
            entries.push(forFeeRefundsEntry);
        }
        return entries;
    }

    private async getEntrySideDataFromStrCurrency(
        method: EntryConfigType,
        { currency, amount }: CurrencyAmountDto,
        fromCustomerId: string,
        toCustomerId: string = fromCustomerId,
    ): Promise<IEntrySidesData> {
        const currencyEntity = await this.currencyRepository.getOneByCdOrFail(currency);

        return getEntrySidesData(method, amount, currencyEntity, fromCustomerId, toCustomerId);
    }
}
