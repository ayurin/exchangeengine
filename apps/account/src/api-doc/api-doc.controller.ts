import { Controller, Get } from '@nestjs/common';
import { ApiExcludeController } from '@nestjs/swagger';
import { OpenAPIObject } from '@nestjs/swagger/dist/interfaces';

@Controller('api-doc')
@ApiExcludeController()
export class ApiDocController {
    @Get()
    public getCurrencyPairs(): Promise<OpenAPIObject> {
        const apiDoc = require('../swagger.json');
        return apiDoc;
    }
}
