import { Module } from '@nestjs/common';

import { ApiDocController } from '@account/api-doc/api-doc.controller';

@Module({
    controllers: [ApiDocController],
})
export class ApiDocModule {}
