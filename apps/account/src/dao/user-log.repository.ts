import { Injectable } from '@nestjs/common';
import { DataSource, Repository } from 'typeorm';

import { UserLogEntity } from '@account/dao/entities';

@Injectable()
export class UserLogRepository extends Repository<UserLogEntity> {
    constructor(private dataSource: DataSource) {
        super(dataSource.getRepository(UserLogEntity).target, dataSource.manager, dataSource.createQueryRunner());
    }
}
