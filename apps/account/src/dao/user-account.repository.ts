import { ECurrencyType } from '@app/utils';
import { Injectable } from '@nestjs/common';
import { DataSource, FindOperator, In, Like, Repository } from 'typeorm';

import { GetMyBalancesDto } from '@account/balances/dto';
import { CurrencyEntity, UserAccountEntity } from '@account/dao/entities';
import { BA_AVAILABLE, BA_LOCKED } from '@account/transaction';
import { ACCOUNT_DELIMITER } from '@account/utils';

const ACCOUNT_AVAILABLE = Like(`${BA_AVAILABLE}${ACCOUNT_DELIMITER}%`);
const ACCOUNT_LOCK = Like(`${BA_LOCKED}${ACCOUNT_DELIMITER}%`);

const accountNumberFilter = (lock: boolean): FindOperator<string> => (lock ? ACCOUNT_LOCK : ACCOUNT_AVAILABLE);

export interface IAssetBalance {
    balance: string;
    asset: string;
    description: string;
    type: ECurrencyType;
}

@Injectable()
export class UserAccountRepository extends Repository<UserAccountEntity> {
    private userAccountRepository = this.dataSource.getRepository(UserAccountEntity);

    constructor(private dataSource: DataSource) {
        super(dataSource.getRepository(UserAccountEntity).target, dataSource.manager, dataSource.createQueryRunner());
    }

    public getAccountsForMyBalances(
        query: GetMyBalancesDto,
        userId: string,
        lock: boolean,
    ): Promise<UserAccountEntity[]> {
        const qb = this.userAccountRepository.createQueryBuilder();
        qb.where({ userId });
        qb.andWhere({ accountNumber: accountNumberFilter(lock) });
        if (!query.withNull) {
            qb.andWhere('balance > 0');
        }
        if (query.currency?.length) {
            qb.andWhere({ currencyCd: In(query.currency) });
        }
        return qb.getMany();
    }

    public async getAccountsForMyAssets(userId: string, lock: boolean): Promise<IAssetBalance[]> {
        const qb = this.userAccountRepository.createQueryBuilder('a');
        qb.leftJoinAndSelect(CurrencyEntity, 'c', 'c.id = a.currencyId');
        qb.where({ userId });
        qb.andWhere({ accountNumber: accountNumberFilter(lock) });
        qb.select('a.currency_cd', 'asset');
        qb.addSelect('a.balance', 'balance');
        qb.addSelect('c.type', 'type');
        qb.addSelect('c.fullName', 'description');
        return qb.getRawMany<IAssetBalance>();
    }

    public getAccountForMyBalance(
        currencyCd: string,
        userId: string,
        lock: boolean,
    ): Promise<UserAccountEntity | null> {
        return this.userAccountRepository.findOneBy({
            currencyCd,
            userId,
            accountNumber: lock ? ACCOUNT_LOCK : ACCOUNT_AVAILABLE,
        });
    }

    public getSummaryUserBalance(userId: string): Promise<{ currency: string; balance: string }[]> {
        return this.dataSource.query(`select currency_cd as currency, SUM(coalesce(balance, 0))::text as balance
            from user_accounts ua
            where user_id = '${userId}'
            group by ua.currency_cd;`) as Promise<{ currency: string; balance: string }[]>;
    }
}
