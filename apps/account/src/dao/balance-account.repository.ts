import { valueOrFail } from '@app/utils';
import { Injectable } from '@nestjs/common';
import { DataSource, Like, Repository } from 'typeorm';

import { BalanceAccountEntity } from '@account/dao/entities';
import { BA_LOCK_SUFFIX, BA_SUFFIX } from '@account/utils';

@Injectable()
export class BalanceAccountRepository extends Repository<BalanceAccountEntity> {
    private balanceAccountRepository = this.dataSource.getRepository(BalanceAccountEntity);

    constructor(private dataSource: DataSource) {
        super(
            dataSource.getRepository(BalanceAccountEntity).target,
            dataSource.manager,
            dataSource.createQueryRunner(),
        );
    }

    public async getBalanceAccountForSave(debitFlg: boolean, forLock: boolean): Promise<BalanceAccountEntity> {
        const balanceAccount = await this.balanceAccountRepository.findOneBy({
            debitFlg,
            baNumber: Like(`%${forLock ? BA_LOCK_SUFFIX : BA_SUFFIX}`),
        });
        return valueOrFail(balanceAccount, 'BalanceAccount');
    }
}
