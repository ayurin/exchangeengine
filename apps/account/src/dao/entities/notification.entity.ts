import { NotificationType } from '@app/ws';
import { Column, CreateDateColumn, Entity, PrimaryGeneratedColumn, UpdateDateColumn } from 'typeorm';

const TABLE_NAME = 'notifications';

@Entity(TABLE_NAME)
export class NotificationEntity {
    public static readonly TABLE_NAME = TABLE_NAME;

    @PrimaryGeneratedColumn('uuid')
    public id!: string;

    @Column('uuid')
    public userId!: string;

    @Column({ nullable: true })
    public title: string;

    @Column({ nullable: false })
    public message: string;

    @Column({ nullable: false })
    public type: NotificationType;

    @Column({ nullable: false, default: false })
    public readStatus: boolean;

    @Column({ nullable: false })
    public showInHistory: boolean;

    @Column({ type: 'timestamp' })
    public asOfDate: Date;

    @CreateDateColumn({ type: 'timestamp', default: () => 'CURRENT_TIMESTAMP(6)' })
    public created: Date;

    @UpdateDateColumn({ type: 'timestamp', default: () => 'CURRENT_TIMESTAMP(6)' })
    public updated: Date;
}
