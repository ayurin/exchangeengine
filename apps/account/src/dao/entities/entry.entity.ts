import { Column, CreateDateColumn, Entity, PrimaryGeneratedColumn } from 'typeorm';

const TABLE_NAME = 'entries';

export interface IEntryMetadata {
    comment: string;
}

@Entity(TABLE_NAME)
export class EntryEntity {
    public static readonly TABLE_NAME = TABLE_NAME;

    @PrimaryGeneratedColumn('uuid')
    public id: string;

    @Column({ type: 'uuid', nullable: false })
    public debitAccountId: string;

    @Column({ nullable: false })
    public debitAccountNumber: string;

    @Column({ type: 'uuid', nullable: false })
    public creditAccountId: string;

    @Column({ nullable: false })
    public creditAccountNumber: string;

    @Column({ nullable: false, type: 'numeric', default: 0 })
    public amount: string;

    @Column({ nullable: false })
    public currency: string;

    @Column({ type: 'uuid', nullable: true })
    public operationId?: string;

    @Column({ type: 'json', nullable: false })
    public metadata?: IEntryMetadata;

    @CreateDateColumn({ type: 'timestamp', nullable: false })
    public asOfDate: Date;
}
