import { checkColumnIn } from '@app/utils';
import { Check, Column, CreateDateColumn, DeleteDateColumn, Entity, PrimaryGeneratedColumn } from 'typeorm';

const TABLE_NAME = 'users';

export enum EUserStatus {
    CREATED = 'CREATED',
    REGISTERED = 'REGISTERED',
    // to be continued...
}

export enum EKycStatus {
    Basic = 'BASIC',
    Standard = 'STANDARD',
    Pro = 'PRO',
    Unverified = 'UNVERIFIED',
}

@Entity(TABLE_NAME)
export class UserEntity {
    public static readonly TABLE_NAME = TABLE_NAME;

    @PrimaryGeneratedColumn('uuid')
    public id!: string;

    @Column({ nullable: false })
    public fullName!: string;

    @Column({ nullable: false })
    public shortName!: string;

    @Column({ nullable: false })
    public email!: string;

    @Check('user_status_check', checkColumnIn('status', Object.values(EUserStatus)))
    @Column({ type: 'varchar', default: EUserStatus.CREATED, nullable: false })
    public status!: EUserStatus;

    @Check('user_kyc_status_check', checkColumnIn('kyc_status', Object.values(EKycStatus)))
    @Column({ type: 'varchar', default: EKycStatus.Unverified, nullable: false })
    public kycStatus: EKycStatus;

    @Column({ type: 'boolean', default: false, nullable: false })
    public tfaEnable: boolean;

    @Column({ type: 'boolean', default: false, nullable: false })
    public blocked: boolean;

    @Column({ type: 'timestamp', nullable: true })
    public blockExpires?: Date | null;

    @Column({ type: 'varchar', nullable: true })
    public taxNumber: string;

    @CreateDateColumn({ type: 'timestamp' })
    public createdAt!: Date;

    @DeleteDateColumn({ type: 'timestamp' })
    public deletedAt?: Date;

    public update(partial: Partial<UserEntity>): UserEntity {
        Object.assign(this, partial);
        return this;
    }
}
