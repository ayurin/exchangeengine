import { Column, CreateDateColumn, Entity, PrimaryGeneratedColumn, UpdateDateColumn } from 'typeorm';

const TABLE_NAME = 'user_accounts';

@Entity(TABLE_NAME)
export class UserAccountEntity {
    public static readonly TABLE_NAME = TABLE_NAME;

    @PrimaryGeneratedColumn('uuid')
    public id!: string;

    @Column({ nullable: false, unique: true })
    public accountNumber!: string;

    @Column({ nullable: false })
    public balanceAccountNumber!: string;

    @Column({ type: 'uuid', nullable: false })
    public currencyId!: string;

    @Column({ nullable: false })
    public currencyCd!: string;

    @Column({ nullable: false })
    public accountName!: string;

    @Column('uuid')
    public userId!: string;

    @Column({ nullable: false })
    public debitFlg!: boolean;

    @Column({ type: 'bigint', default: BigInt(0), transformer: { to: String, from: BigInt } })
    public balance!: bigint;

    @CreateDateColumn({ type: 'timestamp', default: () => 'CURRENT_TIMESTAMP(6)' })
    public created: Date;

    @UpdateDateColumn({ type: 'timestamp', default: () => 'CURRENT_TIMESTAMP(6)' })
    public updated: Date;
}
