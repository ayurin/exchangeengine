import { ELockTime } from '@app/ws';
import { Column, CreateDateColumn, Entity, PrimaryGeneratedColumn, UpdateDateColumn } from 'typeorm';

const TABLE_NAME = 'user_log';

// При добавлении новых типов, нужно добавить значение в таблицу user_log_type
export type UserLogType = 'lock' | 'unlock';

@Entity(TABLE_NAME)
export class UserLogEntity {
    public static readonly TABLE_NAME = TABLE_NAME;

    @PrimaryGeneratedColumn('uuid')
    public id: string;

    @Column('uuid')
    public userId: string;

    @Column({ nullable: false })
    public type: UserLogType;

    @Column({ nullable: true })
    public reason?: string;

    @Column({ nullable: true })
    public options?: ELockTime;

    @CreateDateColumn({ type: 'timestamp', default: () => 'CURRENT_TIMESTAMP(6)' })
    public created: Date;

    @UpdateDateColumn({ type: 'timestamp', default: () => 'CURRENT_TIMESTAMP(6)' })
    public updated: Date;
}
