import {
    Column,
    CreateDateColumn,
    DeleteDateColumn,
    Entity,
    JoinColumn,
    ManyToOne,
    PrimaryGeneratedColumn,
    UpdateDateColumn,
} from 'typeorm';

import { NetworkCurrencyEntity } from '@account/dao';

const TABLE_NAME = 'user_addresses';

@Entity(TABLE_NAME)
export class UserAddressEntity {
    public static readonly TABLE_NAME = TABLE_NAME;

    @PrimaryGeneratedColumn('uuid')
    public id: string;

    @Column({ type: 'uuid', nullable: false })
    public userId: string;

    @Column({ type: 'uuid', nullable: false })
    public addressId: string;

    @Column({ type: 'uuid', nullable: false })
    public networkCurrencyId: string;

    @ManyToOne(() => NetworkCurrencyEntity)
    @JoinColumn({ name: 'network_currency_id', referencedColumnName: 'id' })
    public networkCurrency: NetworkCurrencyEntity;

    @CreateDateColumn({ type: 'timestamp' })
    public createdAt: Date;

    @UpdateDateColumn({ type: 'timestamp' })
    public updatedAt: Date;

    @DeleteDateColumn({ type: 'timestamp' })
    public deletedAt: Date;

    constructor(partial: Partial<UserAddressEntity>) {
        this.update(partial);
    }

    public update(partial: Partial<UserAddressEntity>): UserAddressEntity {
        Object.assign(this, partial);
        return this;
    }
}
