import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

const TABLE_NAME = 'balance_accounts';

@Entity(TABLE_NAME)
export class BalanceAccountEntity {
    public static readonly TABLE_NAME = TABLE_NAME;

    @PrimaryGeneratedColumn('uuid')
    public id: string;

    @Column({ nullable: false, unique: true })
    public baNumber: string;

    @Column({ nullable: true })
    public description?: string;

    @Column({ nullable: false })
    public debitFlg: boolean;

    @Column({ nullable: false })
    public lockFlg: boolean;
}
