import { ECurrencyType } from '@app/utils';
import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';

import { NetworkCurrencyEntity } from '@account/dao';

const TABLE_NAME = 'currencies';

@Entity(TABLE_NAME)
export class CurrencyEntity {
    public static readonly TABLE_NAME = TABLE_NAME;

    @PrimaryGeneratedColumn('uuid')
    public id!: string;

    @Column({ nullable: false, unique: true })
    public shortName!: string;

    @Column({ nullable: true })
    public isoCode?: string;

    @Column({ nullable: false })
    public fullName!: string;

    @Column({ nullable: false, type: 'bigint', transformer: { to: String, from: BigInt } })
    public decimals!: bigint;

    @Column({ default: true })
    public active!: boolean;

    @Column({ type: 'character varying' })
    public type!: ECurrencyType;

    @OneToMany(() => NetworkCurrencyEntity, (n) => n.currency)
    public networkCurrency?: NetworkCurrencyEntity[];
}
