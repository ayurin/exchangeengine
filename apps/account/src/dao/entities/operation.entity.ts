import { checkColumnIn } from '@app/utils';
import { Check, Column, CreateDateColumn, Entity, PrimaryGeneratedColumn } from 'typeorm';

import { ITopupFromFaucetMeta } from '@account/transaction';

const TABLE_NAME = 'operations';

// При добавлении новых типов, нужно добавить значение в таблицу operation_type
export type OperationType = 'topupFromFaucet' | 'placeOrder' | 'cancelOrder' | 'settleOrder' | 'withdraw';
export type OperationStatus = 'active' | 'rejected' | 'confirmed';
export const OPERATION_STATUSES: OperationStatus[] = ['active', 'rejected', 'confirmed'];

@Entity(TABLE_NAME)
export class OperationEntity {
    public static readonly TABLE_NAME = TABLE_NAME;

    @PrimaryGeneratedColumn('uuid')
    public id!: string;

    @Column({ nullable: false })
    public type!: OperationType;

    // может юзать enum? тогда постгря сама повесит констреинт и нам с енамам легче будет работать
    @Check('operation_status_check', checkColumnIn('status', OPERATION_STATUSES))
    @Column({ nullable: false })
    public status!: OperationStatus;

    @Column({ type: 'uuid', array: true, nullable: false, default: [] })
    public userIds!: string[];

    @Column({ nullable: false, type: 'json' })
    public metadata!: ITopupFromFaucetMeta | unknown;

    @CreateDateColumn({ type: 'timestamp' })
    public asOfDate!: Date;

    public get transactionData(): { asset: string; amount: string } {
        if (this.type === 'topupFromFaucet') {
            const meta = this.metadata as ITopupFromFaucetMeta;
            return { asset: meta.currency, amount: meta.amount };
        }

        return { asset: '', amount: '' };
    }
}
