import { ENetworkCurrencyStatus } from '@app/utils';
import { checkColumnIn } from '@app/utils';
import {
    Check,
    Column,
    CreateDateColumn,
    DeleteDateColumn,
    Entity,
    JoinColumn,
    ManyToOne,
    OneToMany,
    PrimaryGeneratedColumn,
    Unique,
    UpdateDateColumn,
} from 'typeorm';

import { CurrencyEntity, NetworkEntity, UserAddressEntity } from '@account/dao';

const TABLE_NAME = 'currency-network';

@Entity(TABLE_NAME)
@Unique(['networkId', 'currencyId'])
export class NetworkCurrencyEntity {
    public static readonly TABLE_NAME = TABLE_NAME;

    @PrimaryGeneratedColumn('uuid')
    public id: string;

    @Column({ type: 'uuid', nullable: false })
    public networkId: string;

    @Column({ type: 'uuid', nullable: false })
    public currencyId: string;

    @ManyToOne(() => CurrencyEntity, (c) => c.networkCurrency)
    @JoinColumn({ name: 'currency_id', referencedColumnName: 'id' })
    public currency: CurrencyEntity;

    @ManyToOne(() => NetworkEntity, (n) => n.networkCurrency)
    @JoinColumn({ name: 'network_id', referencedColumnName: 'id' })
    public network: NetworkEntity;

    @OneToMany(() => UserAddressEntity, (t) => t.networkCurrency)
    public userAddresses?: UserAddressEntity[];

    @Check('currency_network_status_check', checkColumnIn('status', Object.values(ENetworkCurrencyStatus)))
    @Column({ type: 'varchar', nullable: false, default: ENetworkCurrencyStatus.Enable })
    public status: ENetworkCurrencyStatus;

    @CreateDateColumn()
    public createdAt: Date;

    @UpdateDateColumn()
    public updatedAt: Date;

    @DeleteDateColumn()
    public deletedAt: Date;

    constructor(partial: Partial<NetworkCurrencyEntity>) {
        this.update(partial);
    }

    public update(partial: Partial<NetworkCurrencyEntity>): NetworkCurrencyEntity {
        Object.assign(this, partial);
        return this;
    }
}
