import {
    Column,
    CreateDateColumn,
    DeleteDateColumn,
    Entity,
    PrimaryGeneratedColumn,
    UpdateDateColumn,
    OneToMany,
    Index,
} from 'typeorm';

import { NetworkCurrencyEntity } from '@account/dao';

const TABLE_NAME = 'networks';

@Entity(TABLE_NAME)
@Index(['name'], { where: '"deleted_at" IS NULL', unique: true })
@Index(['nativeToken'], { where: '"deleted_at" IS NULL', unique: true })
export class NetworkEntity {
    public static readonly TABLE_NAME = TABLE_NAME;

    @PrimaryGeneratedColumn('uuid')
    public id!: string;

    @Column({ type: 'varchar', nullable: false })
    public name: string;

    @Column({ type: 'varchar', nullable: false })
    public nativeToken: string;

    @OneToMany(() => NetworkCurrencyEntity, (n) => n.network)
    public networkCurrency: NetworkCurrencyEntity[];

    @CreateDateColumn({ type: 'timestamp' })
    public createdAt: Date;

    @UpdateDateColumn({ type: 'timestamp' })
    public updatedAt: Date;

    @DeleteDateColumn({ type: 'timestamp' })
    public deletedAt?: string;

    constructor(partial: Partial<NetworkEntity>) {
        this.update(partial);
    }

    public update(partial: Partial<NetworkEntity>): NetworkEntity {
        Object.assign(this, partial);
        return this;
    }
}
