import { NetworkCurrencyQueryDto, valueOrFail } from '@app/utils';
import { Injectable } from '@nestjs/common';
import { Brackets, DataSource, Repository } from 'typeorm';

import { NetworkCurrencyEntity } from '@account/dao/entities';

@Injectable()
export class NetworkCurrencyRepository extends Repository<NetworkCurrencyEntity> {
    private readonly repository = this.dataSource.getRepository(NetworkCurrencyEntity);

    constructor(private dataSource: DataSource) {
        super(
            dataSource.getRepository(NetworkCurrencyEntity).target,
            dataSource.manager,
            dataSource.createQueryRunner(),
        );
    }

    public async findWithPagination(
        params: NetworkCurrencyQueryDto,
    ): Promise<[NetworkCurrencyEntity[], number]> {
        const {
            currency,
            network,
            paginationOptions: { take, skip },
        } = params;
        const qb = this.repository.createQueryBuilder('nc');

        qb.innerJoinAndSelect('nc.currency', 'c');
        qb.innerJoinAndSelect('nc.network', 'n');

        if (currency) {
            const cValue = currency.toLowerCase();

            qb.andWhere(
                new Brackets((iqb) => {
                    iqb.orWhere('lower(c.id::text) = :cValue', { cValue });
                    iqb.orWhere('lower(c.shortName) = :cValue', { cValue });
                }),
            );
        }

        if (network) {
            const nValue = network.toLowerCase();

            qb.andWhere(
                new Brackets((iqb) => {
                    iqb.orWhere('lower(n.id::text) = :nValue', { nValue });
                    iqb.orWhere('lower(n.name) = :nValue', { nValue });
                }),
            );
        }

        qb.skip(skip);
        qb.take(take);

        return qb.getManyAndCount();
    }

    public async findOneWithRelations(params: {
        currencyId: string;
        networkId: string;
    }): Promise<NetworkCurrencyEntity> {
        return valueOrFail(
            await this.repository.findOne({ where: params, relations: ['currency', 'network'] }),
            'NetworkCurrency',
        );
    }
}
