import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { Brackets, DataSource, In, Repository } from 'typeorm';

import { DepositRequestDto } from '@app/utils';
import { OperationEntity, OperationType } from '@account/dao/entities';
import { GetRecentTransactionDto, recentTxPeriodTransform, recentTxTypeTransform } from '@account/portfolio/dto';

const recentTxTypes: OperationType[] = ['topupFromFaucet'];
@Injectable()
export class OperationRepository extends Repository<OperationEntity> {
    private readonly availableOrderBy = ['asOfDate'];

    constructor(private dataSource: DataSource) {
        super(dataSource.getRepository(OperationEntity).target, dataSource.manager, dataSource.createQueryRunner());
    }

    public async getOperationsForRecentTransaction(params: {
        userId?: string;
        filters: GetRecentTransactionDto & DepositRequestDto;
    }): Promise<[OperationEntity[], number]> {
        const { userId, filters } = params;
        const {
            search,
            type,
            period,
            status,
            paginationOptions: { skip, take, orderBy, orderSQL },
        } = filters;

        const qb = this.createQueryBuilder('o');

        if (userId) {
            qb.where(`'${userId}' = ANY(o.user_ids)`);
        }

        if (type) {
            qb.andWhere({ type: recentTxTypeTransform.from(type) });
        } else {
            qb.andWhere({ type: In(recentTxTypes) });
        }

        if (status) {
            qb.andWhere({ status });
        }

        if (period) {
            qb.andWhere(`o.as_of_date >= now()::date - interval '${recentTxPeriodTransform(period)} day'`);
        }

        if (search) {
            /*
             * если есть параметр search, то ищем like'ом по
             * id, user_ids, users.full_name, metadata, currency
             * */
            const searchValue = `%${search}%`.toLowerCase();

            qb.innerJoin('users', 'u', 'u.id = ANY(o.user_ids)');
            qb.andWhere(
                new Brackets((iqb) => {
                    iqb.orWhere(`lower(array_to_string(o.user_ids, ' ')) like :searchValue`, { searchValue });
                    iqb.orWhere(`lower(o.id::text) like :searchValue`, { searchValue });
                    iqb.orWhere(`lower(o.metadata->>'currency') like :searchValue`, { searchValue });
                    iqb.orWhere('lower(u.full_name) like :searchValue', { searchValue });
                }),
            );
        }

        if (orderBy) {
            this.checkAvailableOrderBy(orderBy);
            qb.orderBy(`o.${orderBy}`, orderSQL);
        }

        qb.skip(skip);
        qb.take(take);

        return qb.getManyAndCount();
    }

    private checkAvailableOrderBy(value: string): void | never {
        if (this.availableOrderBy.includes(value)) {
            return;
        }

        throw new HttpException(`${value} not available for order by`, HttpStatus.BAD_REQUEST);
    }
}
