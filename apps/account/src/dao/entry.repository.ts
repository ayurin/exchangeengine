import { valueOrFail } from '@app/utils';
import { Injectable } from '@nestjs/common';
import { DataSource, Repository } from 'typeorm';

import { EntryEntity } from '@account/dao/entities';

@Injectable()
export class EntryRepository extends Repository<EntryEntity> {
    private entryRepository = this.dataSource.getRepository(EntryEntity);

    constructor(private dataSource: DataSource) {
        super(dataSource.getRepository(EntryEntity).target, dataSource.manager, dataSource.createQueryRunner());
    }

    public async getOneByIdOrFail(id: string): Promise<EntryEntity> {
        const entity = await this.entryRepository.findOneBy({ id });

        return valueOrFail(entity, 'Entry');
    }
}
