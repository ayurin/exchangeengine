import { checkExists, valueOrFail } from '@app/utils';
import { Injectable } from '@nestjs/common';
import { instanceToPlain } from 'class-transformer';
import { DataSource, Not, Repository } from 'typeorm';

import { GetCurrenciesQuery, NewCurrencyDto, UpdateCurrencyDto } from '@account/currency/dto';
import { CurrencyEntity, NetworkCurrencyEntity, NetworkEntity } from '@account/dao/entities';
import { getSkipTakeOptions } from '@account/utils';

@Injectable()
export class CurrencyRepository extends Repository<CurrencyEntity> {
    private currencyRepository = this.dataSource.getRepository(CurrencyEntity);

    constructor(private dataSource: DataSource) {
        super(dataSource.getRepository(CurrencyEntity).target, dataSource.manager, dataSource.createQueryRunner());
    }

    public async getWithPagination({
        network,
        networkStatus,
        search,
        type,
        pageNumber,
        pageSize,
    }: GetCurrenciesQuery): Promise<[CurrencyEntity[], number]> {
        const { skip, take } = getSkipTakeOptions({ pageNumber, pageSize });
        const qb = this.currencyRepository.createQueryBuilder('c');

        qb.skip(skip);
        qb.take(take);

        if (networkStatus || network) {
            qb.innerJoin(NetworkCurrencyEntity, 'cn', 'cn.currencyId = c.id');
            qb.innerJoin(NetworkEntity, 'n', 'cn.network_id = n.id');
        }

        if (networkStatus) qb.andWhere(`cn.status = '${networkStatus}'`);

        if (network) qb.andWhere(`n.name = '${network}'`);

        if (search) qb.andWhere(`upper(c.short_name) like upper('${search}%')`);

        if (type) qb.andWhere({ type });

        return qb.getManyAndCount();
    }

    public async getOneByIdOrFail(id: string): Promise<CurrencyEntity> {
        const entity = await this.currencyRepository.findOneBy({ id });

        return valueOrFail(entity, 'Currency');
    }

    public async getOneByNameOrFail(shortName: string): Promise<CurrencyEntity> {
        const entity = await this.currencyRepository.findOneBy({ shortName });

        return valueOrFail(entity, 'Currency');
    }

    public async getOneByCdOrFail(shortName: string): Promise<CurrencyEntity> {
        const entity = await this.currencyRepository.findOneBy({ shortName });

        return valueOrFail(entity, 'Currency');
    }

    public async add(data: NewCurrencyDto): Promise<CurrencyEntity> {
        const existEntity = await this.currencyRepository.findOneBy({ shortName: data.shortName });

        checkExists(existEntity, 'Currency', 'shortName');

        const entity = this.currencyRepository.create(instanceToPlain(data));
        return this.currencyRepository.save(entity);
    }

    public async updateWithCheck(id: string, data: UpdateCurrencyDto): Promise<void> {
        await this.getOneByIdOrFail(id);
        const existNameEntity = await this.currencyRepository.findOneBy({ shortName: data.shortName, id: Not(id) });
        checkExists(existNameEntity, 'Currency', 'shortName');

        await this.currencyRepository.update(id, instanceToPlain(data));
    }
}
