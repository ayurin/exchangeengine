import { valueOrFail } from '@app/utils';
import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { Brackets, DataSource, Repository } from 'typeorm';

import { NetworkEntity } from '@account/dao/entities';
import { NetworkQueryRequestDto } from '@account/network/dto/network.request.dto';

@Injectable()
export class NetworkRepository extends Repository<NetworkEntity> {
    private networkRepository = this.dataSource.getRepository(NetworkEntity);

    private readonly availableOrderByKeys = ['createdAt', 'name', 'nativeToken'];

    constructor(private dataSource: DataSource) {
        super(dataSource.getRepository(NetworkEntity).target, dataSource.manager, dataSource.createQueryRunner());
    }

    public async getOneByIdOrFail(id: string): Promise<NetworkEntity> {
        const entity = await this.networkRepository.findOneBy({ id });
        return valueOrFail(entity, 'Network');
    }

    public findNetworksWithPagination(params: NetworkQueryRequestDto): Promise<[NetworkEntity[], number]> {
        const {
            search,
            currency,
            paginationOptions: { skip, take, orderBy, orderSQL },
        } = params;

        const qb = this.networkRepository.createQueryBuilder('n');

        if (currency || search) {
            qb.leftJoin('n.networkCurrency', 'nc');
            qb.leftJoin('nc.currency', 'c');
        }

        if (currency) {
            qb.andWhere('lower(c.short_name) = :currency', { currency: currency.toLowerCase() });
        }

        if (search) {
            const searchValue = `%${search}%`.toLowerCase();

            qb.andWhere(
                new Brackets((iqb) => {
                    iqb.orWhere('lower(n.id::text) like :searchValue', { searchValue });
                    iqb.orWhere('lower(n.name) like :searchValue', { searchValue });
                    iqb.orWhere('lower(n.native_token) like :searchValue', { searchValue });
                    iqb.orWhere('lower(c.short_name) like :searchValue', { searchValue });
                    iqb.orWhere('lower(c.full_name) like :searchValue', { searchValue });
                }),
            );
        }

        if (orderBy) {
            this.checkOrderBy(orderBy);
            qb.orderBy(`n.${orderBy}`, orderSQL);
        }

        qb.skip(skip);
        qb.take(take);

        return qb.getManyAndCount();
    }

    private checkOrderBy(orderByKey?: string): void {
        if (!orderByKey) {
            return;
        }

        if (this.availableOrderByKeys.includes(orderByKey)) {
            return;
        }

        throw new HttpException(`${orderByKey} not available for order by`, HttpStatus.BAD_REQUEST);
    }
}
