import { Injectable } from '@nestjs/common';
import { DataSource, Repository } from 'typeorm';

import { UserAddressEntity } from '@account/dao/entities';

@Injectable()
export class UserAddressRepository extends Repository<UserAddressEntity> {
    private readonly repository = this.dataSource.getRepository(UserAddressEntity);

    constructor(private dataSource: DataSource) {
        super(dataSource.getRepository(UserAddressEntity).target, dataSource.manager, dataSource.createQueryRunner());
    }

    public findWithRelations(params: { networkCurrencyId: string; userId: string }): Promise<UserAddressEntity | null> {
        return this.repository.findOne({
            where: params,
            relations: ['networkCurrency', 'networkCurrency.network', 'networkCurrency.currency'],
        });
    }
}
