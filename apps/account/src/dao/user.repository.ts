import { Injectable } from '@nestjs/common';
import { InjectPinoLogger, PinoLogger } from 'nestjs-pino';
import { DataSource, Repository } from 'typeorm';
import { QueryDeepPartialEntity } from 'typeorm/query-builder/QueryPartialEntity';

import { UserEntity, UserLogEntity } from '@account/dao/entities';

@Injectable()
export class UserRepository extends Repository<UserEntity> {
    constructor(
        private dataSource: DataSource,
        @InjectPinoLogger(UserRepository.name)
        private logger: PinoLogger,
    ) {
        super(dataSource.getRepository(UserEntity).target, dataSource.manager, dataSource.createQueryRunner());
    }

    public async updateUserWithLog(
        id: string,
        updates: QueryDeepPartialEntity<UserEntity>,
        log: Partial<UserLogEntity>,
    ): Promise<void> {
        // слишком усложнили транзакцию, можно гораздо проще описывать

        // const queryRunner = this.dataSource.createQueryRunner();
        // const manager = queryRunner.manager;
        // try {
        //     await queryRunner.connect();
        //     await queryRunner.startTransaction();
        //     await manager.update(UserEntity, id, updates);
        //     await manager.save(UserLogEntity, log);
        //     await queryRunner.commitTransaction();
        //     await queryRunner.release();
        // } catch (err) {
        //     await queryRunner.rollbackTransaction();
        //     await queryRunner.release();
        //     this.logger.error(err);
        //     throw err;
        // }

        return this.dataSource
            .transaction(async (manager) => {
                await Promise.all([manager.update(UserEntity, id, updates), manager.save(UserLogEntity, log)]);
            })
            .catch((err) => {
                // если мы будем включать typeorm логер, то она сама все будет логировать, включая ошибки и медленные запросы
                this.logger.error(err);
                throw err;
            });
    }
}
