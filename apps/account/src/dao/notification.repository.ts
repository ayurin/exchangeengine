import { Injectable } from '@nestjs/common';
import { DataSource, Repository } from 'typeorm';

import { NotificationEntity } from '@account/dao/entities';

@Injectable()
export class NotificationRepository extends Repository<NotificationEntity> {
    constructor(private dataSource: DataSource) {
        super(dataSource.getRepository(NotificationEntity).target, dataSource.manager, dataSource.createQueryRunner());
    }
}
