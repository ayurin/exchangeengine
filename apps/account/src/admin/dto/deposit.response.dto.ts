import { PaginatedResponse } from '@app/utils';
import { ApiProperty } from '@nestjs/swagger';

import { RecentTransactionResponse } from '@account/portfolio/dto';

export class DepositResponse extends RecentTransactionResponse {
    @ApiProperty({ example: 'CF177FB1-3DCB-4EF9-910E-F9E8B15D650E' })
    public customerId: string;

    @ApiProperty({ example: 'John Snow' })
    public customerName?: string;

    @ApiProperty({ example: new Date() })
    public createdAt: Date;
}

export class PaginatedDepositResponse extends PaginatedResponse<DepositResponse> {
    @ApiProperty({ type: DepositResponse, isArray: true })
    public data: DepositResponse[];
}
