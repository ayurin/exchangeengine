import { ACCOUNT_GET_DEPOSITS_PATTERN, DepositRequestDto, PaginatedDepositResponse } from '@app/utils';
import { Controller, Get, HttpStatus, Query } from '@nestjs/common';
import { Ctx, MessagePattern, Payload, RmqContext } from '@nestjs/microservices';
import { ApiBearerAuth, ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { Channel, Message } from 'amqplib';

import { PortfolioService } from '@account/portfolio/portfolio.service';

@ApiTags('admin')
@Controller()
export class AdminController {
    constructor(private readonly portfolioService: PortfolioService) {}

    @ApiBearerAuth()
    @Get('deposits')
    // @UseAudiences(TokenAudience.ADMIN)
    // @UseGuards(JwtGuard)
    @ApiOperation({ summary: 'Get deposits' })
    @ApiResponse({ status: HttpStatus.OK, type: PaginatedDepositResponse })
    public getDeposits(@Query() filters: DepositRequestDto): Promise<PaginatedDepositResponse> {
        return this.portfolioService.getDeposits({ filters });
    }

    @MessagePattern(ACCOUNT_GET_DEPOSITS_PATTERN)
    public async rmqGetDeposits(
        @Payload() filters: DepositRequestDto,
        @Ctx() context: RmqContext,
    ): Promise<PaginatedDepositResponse> {
        const channel = context.getChannelRef() as Channel;
        const originalMsg = context.getMessage() as Message;
        channel.ack(originalMsg);
        return this.portfolioService.getDeposits({ filters });
    }
}
