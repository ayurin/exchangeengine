import { Module } from '@nestjs/common';

import { PortfolioModule } from '@account/portfolio';

import { AdminController } from './admin.controller';

@Module({
    imports: [PortfolioModule],
    controllers: [AdminController],
})
export class AdminModule {}
