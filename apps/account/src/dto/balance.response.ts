import { transformToTimestamp } from '@app/utils';
import { IBalanceResponse } from '@app/ws';
import { ApiProperty } from '@nestjs/swagger';
import { Exclude, Expose, Transform } from 'class-transformer';

import { UserAccountEntity } from '@account/dao';

function transformToCurrency(obj: unknown): string | null {
    return (obj as UserAccountEntity)?.currencyCd;
}

@Exclude()
export class BalanceResponse implements IBalanceResponse {
    @Expose()
    @ApiProperty({ example: 'ca742a5e-3954-4a09-958e-307dc0c38e76' })
    public userId: string;

    @Expose()
    @Transform(({ obj }) => transformToCurrency(obj))
    @ApiProperty({ example: 'BTC' })
    public currency: string;

    @Expose()
    @Transform(({ value }) => String(value))
    @ApiProperty({ example: '2000000000' })
    public balance: string;

    @Expose()
    @ApiProperty({ example: 'C2600xBTCxca742a5e-3954-4a09-958e-307dc0c38e76x00' })
    public accountNumber: string;

    @Expose()
    @Transform(({ value }) => transformToTimestamp(value as Date))
    @ApiProperty({ example: '1665348894245' })
    public updated: number;
}
