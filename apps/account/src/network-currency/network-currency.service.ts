import { Injectable } from '@nestjs/common';

import { NetworkCurrencyEntity, NetworkCurrencyRepository } from '@account/dao';
import { NetworkCurrencyPostDto, NetworkCurrencyQueryDto } from '@app/utils';

@Injectable()
export class NetworkCurrencyService {
    constructor(private readonly repository: NetworkCurrencyRepository) {}

    public async upsert(payload: NetworkCurrencyPostDto): Promise<NetworkCurrencyEntity> {
        const entity = new NetworkCurrencyEntity(payload);
        await this.repository.save(entity);

        return this.repository.findOneWithRelations({ currencyId: entity.currencyId, networkId: entity.networkId });
    }

    public async findWithPagination(
        params: NetworkCurrencyQueryDto,
    ): Promise<[NetworkCurrencyEntity[], number]> {
        return this.repository.findWithPagination(params);
    }
}
