// eslint-disable-next-line max-classes-per-file
import { ENetworkCurrencyStatus } from '@app/utils';
import { PaginatedResponse, PaginationQuery } from '@app/utils';
import { ApiProperty } from '@nestjs/swagger';

import { CurrencyResponse } from '@account/currency/dto';
import { NetworkCurrencyEntity } from '@account/dao/entities/network-currency.entity';
import { NetworkResponseDto } from '@account/network/dto/network.response.dto';

class NetworkCurrencyDto {
    @ApiProperty({ example: ENetworkCurrencyStatus.Maintenance, enum: Object.values(ENetworkCurrencyStatus) })
    public status: ENetworkCurrencyStatus;

    @ApiProperty()
    public createdAt: Date;

    @ApiProperty()
    public updatedAt: Date;

    @ApiProperty({ type: CurrencyResponse })
    public currency: CurrencyResponse;

    @ApiProperty({ type: NetworkResponseDto })
    public network: NetworkResponseDto;

    constructor(item: NetworkCurrencyEntity) {
        this.status = item.status;
        this.createdAt = item.createdAt;
        this.updatedAt = item.updatedAt;
        this.currency = new CurrencyResponse(item.currency);
        this.network = new NetworkResponseDto(item.network);
    }
}

export class OneNetworkCurrencyResponseDto {
    @ApiProperty({ type: NetworkCurrencyDto })
    public data: NetworkCurrencyDto;

    constructor(item: NetworkCurrencyEntity) {
        this.data = new NetworkCurrencyDto(item);
    }
}

export class ManyNetworkCurrencyResponseDto extends PaginatedResponse<NetworkCurrencyDto> {
    @ApiProperty({ type: NetworkCurrencyDto })
    public data: NetworkCurrencyDto[];

    constructor(items: NetworkCurrencyEntity[], total: number, query: PaginationQuery) {
        super(
            items.map((item) => new NetworkCurrencyDto(item)),
            total,
            query,
        );
    }
}
