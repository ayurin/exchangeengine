import {
    ACCOUNT_GET_NETWORK_CURRENCIES_PATTERN,
    ACCOUNT_PUT_NETWORK_CURRENCY_PATTERN,
    NetworkCurrencyPostDto,
    NetworkCurrencyQueryDto,
} from '@app/utils';
import { Body, Controller, Get, HttpStatus, Put, Query } from '@nestjs/common';
import { Ctx, MessagePattern, Payload, RmqContext } from '@nestjs/microservices';
import { ApiBody, ApiResponse, ApiTags } from '@nestjs/swagger';
import { Channel, Message } from 'amqplib';

import {
    ManyNetworkCurrencyResponseDto,
    OneNetworkCurrencyResponseDto,
} from '@account/network-currency/dto/network-currency.response.dto';

import { NetworkCurrencyPostRequestDto, NetworkCurrencyQueryRequestDto } from './dto';
import { NetworkCurrencyService } from './network-currency.service';

@ApiTags('network-currency')
@Controller('network-currency')
export class NetworkCurrencyController {
    constructor(private readonly networkCurrencyService: NetworkCurrencyService) {}

    @Put()
    // @ApiAuth(TokenAudience.ADMIN)
    @ApiBody({ type: NetworkCurrencyPostRequestDto })
    @ApiResponse({ type: OneNetworkCurrencyResponseDto, status: HttpStatus.CREATED })
    public async post(@Body() body: NetworkCurrencyPostDto): Promise<OneNetworkCurrencyResponseDto> {
        const result = await this.networkCurrencyService.upsert(body);
        return new OneNetworkCurrencyResponseDto(result);
    }

    @Get()
    // @ApiAuth(TokenAudience.ADMIN)
    @ApiResponse({ type: ManyNetworkCurrencyResponseDto, status: HttpStatus.CREATED })
    public async findAll(@Query() query: NetworkCurrencyQueryDto): Promise<ManyNetworkCurrencyResponseDto> {
        const [results, total] = await this.networkCurrencyService.findWithPagination(query);
        return new ManyNetworkCurrencyResponseDto(results, total, query);
    }

    @MessagePattern(ACCOUNT_PUT_NETWORK_CURRENCY_PATTERN)
    public async rmqPost(
        @Payload() body: NetworkCurrencyPostDto,
        @Ctx() context: RmqContext,
    ): Promise<OneNetworkCurrencyResponseDto> {
        const channel = context.getChannelRef() as Channel;
        const originalMsg = context.getMessage() as Message;
        channel.ack(originalMsg);
        const result = await this.networkCurrencyService.upsert(body);
        return new OneNetworkCurrencyResponseDto(result);
    }

    @MessagePattern(ACCOUNT_GET_NETWORK_CURRENCIES_PATTERN)
    public async rmqFindAll(
        @Payload() query: NetworkCurrencyQueryDto,
        @Ctx() context: RmqContext,
    ): Promise<ManyNetworkCurrencyResponseDto> {
        const channel = context.getChannelRef() as Channel;
        const originalMsg = context.getMessage() as Message;
        channel.ack(originalMsg);
        const [results, total] = await this.networkCurrencyService.findWithPagination(query);
        return new ManyNetworkCurrencyResponseDto(results, total, query);
    }
}
