import { Module } from '@nestjs/common';

import { NetworkCurrencyRepository } from '@account/dao';

import { NetworkCurrencyController } from './network-currency.controller';
import { NetworkCurrencyService } from './network-currency.service';

@Module({
    providers: [NetworkCurrencyService, NetworkCurrencyRepository],
    controllers: [NetworkCurrencyController],
})
export class NetworkCurrencyModule {}
