process.env.DOTENV_CONFIG_PATH = '../.env.test.dist';

module.exports = {
    preset: 'ts-jest',
    testEnvironment: 'node',
    clearMocks: true,
    resetMocks: false,
    testLocationInResults: true,
    testRegex: ['./e2e/(.*).controller(.*)'],
    reporters: ['default', 'jest-sonar'],
    moduleNameMapper: {
        // eslint-disable-next-line @typescript-eslint/naming-convention
        '^@account/(.*)$': '<rootDir>/$1',
    },
    setupFiles: ['dotenv/config'],
};
