import { randomUUID } from 'crypto';

import { provideServiceDefaultMock } from '@haqqex-backend/testing';
import { ECurrencyType } from '@app/utils';
import { Test, TestingModule } from '@nestjs/testing';
import { plainToInstance } from 'class-transformer';

import { CurrencyEntity, CurrencyRepository } from '@account/dao';
import { OkResponse } from '@account/dto';

import { CurrencyService } from './currency.service';
import { CurrencyResponse, NewCurrencyDto, UpdateCurrencyDto } from './dto';

const usdcCurrency: CurrencyEntity = {
    id: randomUUID(),
    shortName: 'USDC',
    isoCode: '',
    fullName: 'USD Coin',
    decimals: BigInt(6),
    active: true,
    type: ECurrencyType.STABLE_OR_FIAT,
};

const ethCurrency: CurrencyEntity = {
    id: randomUUID(),
    shortName: 'ETH',
    isoCode: 'ETH',
    fullName: 'Ethereum',
    decimals: BigInt(18),
    active: true,
    type: ECurrencyType.CRYPTO,
};

describe('currency service', () => {
    let service: CurrencyService;
    let currencyRepository: CurrencyRepository;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            providers: [CurrencyService, provideServiceDefaultMock(CurrencyRepository)],
        }).compile();

        service = module.get<CurrencyService>(CurrencyService);
        currencyRepository = module.get(CurrencyRepository);
    });

    describe('getCurrency()', () => {
        describe('returns one entry by id', () => {
            let result: CurrencyResponse;
            let repositoryMock: jest.SpyInstance;

            const expected: CurrencyResponse = plainToInstance(CurrencyResponse, ethCurrency);

            beforeEach(async () => {
                repositoryMock = jest.spyOn(currencyRepository, 'getOneByIdOrFail').mockResolvedValue(ethCurrency);
                result = await service.getCurrency(ethCurrency.id);
            });

            it('should call getOneByIdOrFail', () => {
                expect(repositoryMock).toHaveBeenCalledTimes(1);
                expect(repositoryMock).toHaveBeenCalledWith(ethCurrency.id);
            });

            it('should check equal between result and expected', () => {
                expect(result).toStrictEqual(expected);
            });
        });
    });

    describe('addCurrency()', () => {
        describe('add new entry', () => {
            let result: OkResponse;
            let repositoryMock: jest.SpyInstance;

            const dto: NewCurrencyDto = {
                decimals: String(ethCurrency.decimals),
                fullName: ethCurrency.fullName,
                isoCode: ethCurrency.isoCode as string,
                shortName: ethCurrency.shortName,
                type: ethCurrency.type,
            };

            const expected: OkResponse = new OkResponse();

            beforeEach(async () => {
                repositoryMock = jest.spyOn(currencyRepository, 'add').mockResolvedValue(ethCurrency);
                result = await service.addCurrency(dto);
            });

            it('should call add', () => {
                expect(repositoryMock).toHaveBeenCalledTimes(1);
                expect(repositoryMock).toHaveBeenCalledWith(dto);
            });

            it('should check equal between result and expected', () => {
                expect(result).toStrictEqual(expected);
            });
        });
    });

    describe('updateCurrency()', () => {
        describe('update entry', () => {
            let result: OkResponse;
            let repositoryMock: jest.SpyInstance;

            const id = ethCurrency.id;

            const dto: UpdateCurrencyDto = {
                decimals: String(ethCurrency.decimals),
                fullName: ethCurrency.fullName,
                isoCode: ethCurrency.isoCode as string,
                shortName: ethCurrency.shortName,
                type: ethCurrency.type,
            };

            const expected: OkResponse = new OkResponse();

            beforeEach(async () => {
                repositoryMock = jest.spyOn(currencyRepository, 'updateWithCheck').mockResolvedValue();
                result = await service.updateCurrency(id, dto);
            });

            it('should call updateWithCheck', () => {
                expect(repositoryMock).toHaveBeenCalledTimes(1);
                expect(repositoryMock).toHaveBeenCalledWith(id, dto);
            });

            it('should check equal between result and expected', () => {
                expect(result).toStrictEqual(expected);
            });
        });
    });
});
