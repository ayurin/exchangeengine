import { ENetworkCurrencyStatus } from '@app/utils';
import { ECurrencyType, tfStringToNum } from '@app/utils';
import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import { Transform } from 'class-transformer';
import { IsBoolean, IsEnum, IsNumber, IsOptional, IsString, Min } from 'class-validator';

export class GetCurrenciesQuery {
    @ApiProperty({ example: 10, required: false })
    @Transform(({ value }) => tfStringToNum(value), { toClassOnly: true })
    @IsOptional()
    @IsNumber({ maxDecimalPlaces: 0 })
    @Min(1)
    public pageSize?: number;

    @ApiProperty({ example: 1, required: false })
    @Transform(({ value }) => tfStringToNum(value), { toClassOnly: true })
    @IsOptional()
    @IsNumber({ maxDecimalPlaces: 0 })
    @Min(1)
    public pageNumber?: number;

    @ApiPropertyOptional({ example: 'btc' })
    @IsOptional()
    @IsString()
    public search?: string;

    @ApiPropertyOptional({ type: 'boolean' })
    @Transform(({ value }) => value === 'true')
    @IsOptional()
    @IsBoolean()
    public withBalance?: boolean;

    @ApiPropertyOptional({ example: 'btc' })
    @IsOptional()
    @IsString()
    public network?: string;

    @ApiPropertyOptional({ enum: ENetworkCurrencyStatus })
    @IsOptional()
    @IsEnum(ENetworkCurrencyStatus)
    public networkStatus?: ENetworkCurrencyStatus;

    @ApiPropertyOptional({
        enum: ECurrencyType,
        description: `${ECurrencyType.CRYPTO} = crypto, ${ECurrencyType.FIAT} - stable coin or fiat`,
    })
    @IsOptional()
    @IsEnum(ECurrencyType)
    public type?: ECurrencyType;
}
