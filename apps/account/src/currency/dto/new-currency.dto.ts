import { ECurrencyType } from '@app/utils';
import { ApiProperty } from '@nestjs/swagger';
import { Transform } from 'class-transformer';
import { IsEnum, IsNotEmpty, IsNumberString, IsString } from 'class-validator';

export class NewCurrencyDto {
    @ApiProperty({ example: '8' })
    @IsNumberString()
    @IsNotEmpty()
    @Transform(({ value }) => BigInt(value as string), { toPlainOnly: true })
    public decimals: string;

    @ApiProperty({ example: 'Bitcoin' })
    @IsString()
    @IsNotEmpty()
    public fullName: string;

    @ApiProperty({ example: 'BTC' })
    @IsString()
    @IsNotEmpty()
    public isoCode: string;

    @ApiProperty({ example: 'BTC' })
    @IsString()
    @IsNotEmpty()
    public shortName: string;

    @ApiProperty({
        enum: ECurrencyType,
        description: `${ECurrencyType.CRYPTO} = crypto, ${ECurrencyType.FIAT} - stable coin or fiat`,
    })
    @IsEnum(ECurrencyType)
    @IsNotEmpty()
    public type: ECurrencyType;
}
