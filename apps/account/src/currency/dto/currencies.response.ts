import { ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';

import { CurrencyResponse } from '@account/currency/dto/currency.response';
import { PaginationDto } from '@account/dto';

export class CurrenciesResponse extends PaginationDto {
    @ApiProperty({ type: CurrencyResponse, isArray: true })
    @Type(() => CurrencyResponse)
    public list: CurrencyResponse[];
}
