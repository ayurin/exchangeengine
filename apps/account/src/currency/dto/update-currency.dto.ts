import { ECurrencyType } from '@app/utils';
import { ApiProperty } from '@nestjs/swagger';
import { Transform } from 'class-transformer';
import { IsEnum, IsNumberString, IsOptional, IsString } from 'class-validator';

import { NewCurrencyDto } from '@account/currency/dto/new-currency.dto';

export class UpdateCurrencyDto implements NewCurrencyDto {
    @ApiProperty({ example: '8' })
    @IsNumberString()
    @IsOptional()
    @Transform(({ value }) => BigInt(value as string), { toPlainOnly: true })
    public decimals: string;

    @ApiProperty({ example: 'Bitcoin' })
    @IsString()
    @IsOptional()
    public fullName: string;

    @ApiProperty({ example: 'BTC' })
    @IsString()
    @IsOptional()
    public isoCode: string;

    @ApiProperty({ example: 'BTC' })
    @IsString()
    @IsOptional()
    public shortName: string;

    @ApiProperty({
        enum: ECurrencyType,
        description: `${ECurrencyType.CRYPTO} = crypto, ${ECurrencyType.FIAT} - stable coin or fiat`,
    })
    @IsEnum(ECurrencyType)
    @IsOptional()
    public type: ECurrencyType;
}
