import { IPriceForAsset, RmqClientService, TRADING_GET_PRICE_FOR_ASSETS } from '@app/utils';
import { RabbitService, ZERO_BIGINT } from '@app/utils';
import { Injectable } from '@nestjs/common';
import { plainToInstance } from 'class-transformer';

import {
    CurrenciesResponse,
    CurrencyResponse,
    GetCurrenciesQuery,
    NewCurrencyDto,
    UpdateCurrencyDto,
} from '@account/currency/dto';
import { CurrencyEntity, CurrencyRepository, UserAccountRepository } from '@account/dao';
import { OkResponse } from '@account/dto';
import { FIAT } from '@account/utils';

type CurrencyWithBalanceData = CurrencyEntity & { coinEstimateValue?: string; fiatEstimateValue?: string };

@Injectable()
export class CurrencyService {
    constructor(
        private currencyRepository: CurrencyRepository,
        private rmqClient: RmqClientService,
        private rabbitService: RabbitService,
        private userAccountRepository: UserAccountRepository,
    ) {}

    public async getCurrencies(params: GetCurrenciesQuery, userId?: string): Promise<CurrenciesResponse> {
        const [entities, total] = await this.currencyRepository.getWithPagination(params);
        let list = entities.slice();
        if (params.withBalance === true && userId) {
            list = await this.setBalanceDataToCurrency(userId, entities);
        }
        return plainToInstance(CurrenciesResponse, { list, ...params, total });
    }

    private async setBalanceDataToCurrency(
        userId: string,
        entities: CurrencyEntity[],
    ): Promise<CurrencyWithBalanceData[]> {
        const accounts = await this.userAccountRepository.getSummaryUserBalance(userId);
        const getPriceMessage = this.rabbitService.buildMessage({
            assets: accounts.map((account) => account.currency),
            fiat: FIAT,
        });
        const currencyPrices: IPriceForAsset[] = await this.rmqClient.send(
            TRADING_GET_PRICE_FOR_ASSETS,
            getPriceMessage,
        );
        return entities.map((entity) => {
            const account = accounts.find((item) => item.currency === entity.shortName);
            const currencyPrice = account ? currencyPrices.find((item) => item.asset === account.currency) : undefined;
            const fiatEstimateValue =
                account && currencyPrice
                    ? (BigInt(account.balance) * BigInt(currencyPrice?.price)) / BigInt(10 ** 8)
                    : ZERO_BIGINT;
            return {
                ...entity,
                coinEstimateValue: account?.balance?.toString() ?? '0',
                fiatEstimateValue: fiatEstimateValue.toString(),
            };
        });
    }

    public async getCurrency(currencyId: string): Promise<CurrencyResponse> {
        const entity = await this.currencyRepository.getOneByIdOrFail(currencyId);
        return plainToInstance(CurrencyResponse, entity);
    }

    public async getCurrencyByNameOrFail(shortName: string): Promise<CurrencyResponse> {
        const entity = await this.currencyRepository.getOneByNameOrFail(shortName);
        return plainToInstance(CurrencyResponse, entity);
    }

    public async getCurrencyByName(shortName: string): Promise<CurrencyResponse> {
        const entity = await this.currencyRepository.findOneBy({ shortName });
        return plainToInstance(CurrencyResponse, entity);
    }

    public async addCurrency(data: NewCurrencyDto): Promise<OkResponse> {
        await this.currencyRepository.add(data);
        return new OkResponse();
    }

    public async updateCurrency(id: string, data: UpdateCurrencyDto): Promise<OkResponse> {
        await this.currencyRepository.updateWithCheck(id, data);
        return new OkResponse();
    }
}
