import { Module } from '@nestjs/common';

import { CurrencyRepository, UserAccountRepository } from '@account/dao';

import { CurrencyController } from './currency.controller';
import { CurrencyService } from './currency.service';

@Module({
    controllers: [CurrencyController],
    providers: [CurrencyService, CurrencyRepository, UserAccountRepository],
})
export class CurrencyModule {}
