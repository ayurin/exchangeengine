import { JwtOptionalGuard, JwtPayload, TokenAudience, TUserJwtPayload, UseAudiences } from '@app/jwt';
import { ACCOUNT_GET_CURRENCY_PATTERN } from '@app/utils';
import { Body, Controller, Get, HttpStatus, Param, Patch, Post, Query, UseGuards } from '@nestjs/common';
import { Ctx, MessagePattern, Payload, RmqContext } from '@nestjs/microservices';
import { ApiBearerAuth, ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { Channel, Message } from 'amqplib';

import { CurrencyService } from '@account/currency/currency.service';
import {
    CurrenciesResponse,
    CurrencyResponse,
    GetCurrenciesQuery,
    NewCurrencyDto,
    UpdateCurrencyDto,
} from '@account/currency/dto';
import { OkResponse } from '@account/dto';
import { RmqWraper } from '@app/utils';

@ApiTags('currencies')
@Controller('currencies')
export class CurrencyController {
    constructor(private readonly currencyService: CurrencyService) {}

    @ApiBearerAuth()
    @Get()
    @UseGuards(JwtOptionalGuard)
    @UseAudiences(TokenAudience.ACCOUNT)
    @ApiOperation({ summary: 'Get list of currencies' })
    @ApiResponse({ status: HttpStatus.OK, type: CurrenciesResponse })
    public getCurrencies(
        @Query() query: GetCurrenciesQuery,
        @JwtPayload() payload?: TUserJwtPayload,
    ): Promise<CurrenciesResponse> {
        return this.currencyService.getCurrencies(query, payload?.id);
    }

    @Get(':currencyId')
    @ApiOperation({ summary: 'Get details of currency' })
    @ApiResponse({ status: HttpStatus.OK, type: CurrencyResponse })
    public getCurrency(@Param('currencyId') id: string): Promise<CurrencyResponse> {
        return this.currencyService.getCurrency(id);
    }

    @Get('name/:shortName')
    @ApiOperation({ summary: 'Get details of currency' })
    @ApiResponse({ status: HttpStatus.OK, type: CurrencyResponse })
    public getCurrencyByName(@Param('shortName') shortName: string): Promise<CurrencyResponse> {
        return this.currencyService.getCurrencyByNameOrFail(shortName);
    }

    @Post()
    @ApiOperation({ summary: 'Add new currency' })
    @ApiResponse({ status: HttpStatus.OK, type: OkResponse })
    public addCurrency(@Body() data: NewCurrencyDto): Promise<OkResponse> {
        return this.currencyService.addCurrency(data);
    }

    @Patch(':currencyId')
    @ApiOperation({ summary: 'Update currency' })
    @ApiResponse({ status: HttpStatus.OK, type: OkResponse })
    public updateCurrency(@Param('currencyId') id: string, @Body() data: UpdateCurrencyDto): Promise<OkResponse> {
        return this.currencyService.updateCurrency(id, data);
    }

    @MessagePattern(ACCOUNT_GET_CURRENCY_PATTERN)
    @RmqWraper
    public async rmqGetCurrencyByName(
        @Payload() shortName: string,
        @Ctx() context: RmqContext,
    ): Promise<CurrencyResponse> {
        return this.currencyService.getCurrencyByName(shortName);
    }
}
