import { OkResponse } from '@app/utils';
import { Controller, HttpStatus, Post } from '@nestjs/common';
import { ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';

import { SettingsService } from './settings.service';

@ApiTags('settings')
@Controller('settings')
export class SettingsController {
    constructor(private readonly settingsService: SettingsService) {}

    // TODO: 24/10/2022 - Protect with administrator guard
    @Post('/clear-data')
    @ApiOperation({ summary: 'Clear operations, entries and user-accounts tables' })
    @ApiResponse({ status: HttpStatus.OK, type: OkResponse })
    public clearData(): Promise<OkResponse> {
        return this.settingsService.clearData();
    }
}
