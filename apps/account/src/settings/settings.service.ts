import { OkResponse } from '@app/utils';
import { Injectable } from '@nestjs/common';
import { DataSource } from 'typeorm';

import { EntryEntity, OperationEntity, UserAccountEntity } from '@account/dao';

@Injectable()
export class SettingsService {
    constructor(private readonly dataSource: DataSource) {}

    public async clearData(): Promise<OkResponse> {
        this.dataSource.query(`TRUNCATE TABLE "${OperationEntity.TABLE_NAME}"`);
        this.dataSource.query(`TRUNCATE TABLE "${EntryEntity.TABLE_NAME}"`);
        this.dataSource.query(`TRUNCATE TABLE "${UserAccountEntity.TABLE_NAME}"`);

        return new OkResponse();
    }
}
