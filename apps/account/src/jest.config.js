module.exports = {
    preset: 'ts-jest',
    testEnvironment: 'node',
    clearMocks: true,
    coverageDirectory: 'coverage',
    collectCoverageFrom: ['./**/*.ts', '!./*.ts'],
    coveragePathIgnorePatterns: [
        './node_modules/',
        '/asset/',
        '/constants/',
        '(.*).constants.ts',
        '/dto/',
        '/dao/',
        '(.*).module.ts',
        '(.*).controller.ts',
        '(.*).spec.ts',
        '(.*).moch.ts',
        '(.*).index.ts',
        '/health/helper/',
        '/migrations/',
        '/migrations-spec/',
    ],
    errorOnDeprecated: true,
    moduleFileExtensions: ['js', 'ts'],
    resetMocks: false,
    testLocationInResults: true,
    testMatch: ['**/?(*.)+(spec|test).[tj]s?(x)'],
    testPathIgnorePatterns: ['./node_modules/'],
    reporters: ['default', 'jest-junit', 'jest-sonar'],
    moduleNameMapper: {
        // eslint-disable-next-line @typescript-eslint/naming-convention
        '^@account/(.*)$': '<rootDir>/$1',
    },
};
