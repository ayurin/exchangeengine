/* @ts-ignore */
import { ApiAuth, JwtPayload, TFA_HEADER, TokenAudience, TUserJwtPayload, UseTfaProtect } from '@app/jwt';
import { OkResponse } from '@app/utils';
import { Controller, HttpStatus, Post, Headers, Delete } from '@nestjs/common';
import { ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';

import { BarcodeDtoResponse } from '@account/tfa/dto';

import { TfaService } from './tfa.service';

@ApiTags('tfa')
@Controller('tfa')
export class TfaController {
    constructor(private readonly tfaService: TfaService) {}

    @Post('enable')
    @ApiAuth(TokenAudience.ACCOUNT)
    @ApiOperation({ summary: 'Enable tfa for user' })
    @ApiResponse({ type: BarcodeDtoResponse, status: HttpStatus.CREATED })
    public async enableTfa(@JwtPayload() { id }: TUserJwtPayload): Promise<BarcodeDtoResponse> {
        const result = await this.tfaService.generateTfaSecret(id as string);
        return new BarcodeDtoResponse({ barcode: result });
    }

    @Post('confirm')
    @ApiAuth(TokenAudience.ACCOUNT)
    @ApiOperation({ summary: 'Confirm tfa for user (request must be executed after enable tfa)' })
    @ApiResponse({ type: OkResponse })
    public async confirmTfaEnable(
        @Headers(TFA_HEADER) code: string,
        @JwtPayload() { id }: TUserJwtPayload,
    ): Promise<OkResponse> {
        await this.tfaService.confirmTfaEnable(id as string, code);
        return new OkResponse();
    }

    @Delete()
    @ApiAuth(TokenAudience.ACCOUNT)
    // @UseTfaProtect()
    @ApiOperation({ summary: 'Delete tfa protect' })
    @ApiResponse({ type: OkResponse })
    public async removeTfa(@JwtPayload() { id }: TUserJwtPayload): Promise<OkResponse> {
        await this.tfaService.removeTfa(id as string);
        return new OkResponse();
    }
}
