import { ApiProperty } from '@nestjs/swagger';

export class BarcodeDtoResponse {
    @ApiProperty({ description: 'uri data' })
    public barcode: string;

    constructor(item: { barcode: string }) {
        this.barcode = item.barcode;
    }
}
