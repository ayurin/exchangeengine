import {
    AUTH_CHECK_TFA_CODE_PATTERN,
    AUTH_DELETE_TFA_SECRET_PATTERN,
    AUTH_GENERATE_TFA_SECRET_PATTERN,
    /* @ts-ignore */
    ICheckTfaCodeResponse,
    IGenerateTfaSecretResponse,
    RmqClientService,
} from '@app/utils';
import { RabbitService, valueOrFail } from '@app/utils';
import { HttpException, HttpStatus, Injectable } from '@nestjs/common';

import { UserRepository } from '@account/dao';

@Injectable()
export class TfaService {
    constructor(
        private readonly rmqClientService: RmqClientService,
        private readonly rabbitService: RabbitService,
        private readonly userRepository: UserRepository,
    ) {}

    public async generateTfaSecret(userId: string): Promise<string> {
        const user = valueOrFail(await this.userRepository.findOneBy({ id: userId }), 'User');
        if (user.tfaEnable) {
            throw new HttpException('Tfa is enable', HttpStatus.CONFLICT);
        }

        const payload = this.rabbitService.buildMessage({ userId: user.id, email: user.email });
        const { status, message, barcode }: IGenerateTfaSecretResponse = await this.rmqClientService.send(
            AUTH_GENERATE_TFA_SECRET_PATTERN,
            payload,
        );

        if (barcode) {
            return barcode;
        }

        throw new HttpException(message ?? 'Unknown error', status || HttpStatus.INTERNAL_SERVER_ERROR);
    }

    public async confirmTfaEnable(userId: string, code: string): Promise<void> {
        const user = valueOrFail(await this.userRepository.findOneBy({ id: userId }), 'User');
        if (user.tfaEnable) {
            throw new HttpException('Tfa is enable', HttpStatus.CONFLICT);
        }

        const payload = this.rabbitService.buildMessage({ userId: user.id, code, isConfirmation: true });
        const { status, message, isValid }: ICheckTfaCodeResponse = await this.rmqClientService.send(
            AUTH_CHECK_TFA_CODE_PATTERN,
            /* @ts-ignore */
            payload,
        );

        if (status !== HttpStatus.OK) {
            throw new HttpException(message ?? 'Unknown error', status ?? HttpStatus.INTERNAL_SERVER_ERROR);
        }

        if (!isValid) {
            throw new HttpException('Tfa code is not valid', HttpStatus.FORBIDDEN);
        }

        user.tfaEnable = true;
        await this.userRepository.save(user);
    }

    public async removeTfa(userId: string): Promise<void> {
        const user = valueOrFail(await this.userRepository.findOneBy({ id: userId }), 'User');
        if (!user.tfaEnable) {
            throw new HttpException('Tfa is not enable', HttpStatus.CONFLICT);
        }

        const message = this.rabbitService.buildMessage({ userId });
        await this.rmqClientService.send(AUTH_DELETE_TFA_SECRET_PATTERN, message);

        user.tfaEnable = false;
        await this.userRepository.save(user);
    }
}
