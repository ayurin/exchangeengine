import { Module } from '@nestjs/common';

import { UserRepository } from '@account/dao';

import { TfaController } from './tfa.controller';
import { TfaService } from './tfa.service';

@Module({
    controllers: [TfaController],
    providers: [TfaService, UserRepository],
    exports: [TfaService],
})
export class TfaModule {}
