import { MigrationInterface, QueryRunner } from 'typeorm';

import { EUserStatus, UserEntity } from '@account/dao';

const usersSeeds = [
    {
        id: '00000000-0000-0000-0000-000000000001',
        fullName: 'Abu Yusuf Ya qub ibn Ishaq as-Sabbah al-Kindi',
        shortName: 'Al-Kindi',
        email: 'al-kindi@haqqex.com',
    },
    {
        id: '00000000-0000-0000-0000-000000000002',
        fullName: 'Abu al-Qasim Ubaydallah ibn Abdallah ibn Khordadbeh',
        shortName: 'Ibn Khordadbeh',
        email: 'ibn-khordadbeh@haqqex.com',
    },
    {
        id: '00000000-0000-0000-0000-000000000003',
        fullName: 'Hunayn ibn Ishaq al-Ibadi',
        shortName: 'Hunayn',
        email: 'hunayn@haqqex.com',
    },
    {
        id: '00000000-0000-0000-0000-000000000004',
        fullName: 'Muhammad ibn Muhammad ibn Yahya ibn Isma il ibn al-Abbas al-Buzjani',
        shortName: 'Abu',
        email: 'abu@haqqex.com',
    },
    {
        id: '00000000-0000-0000-0000-000000000005',
        fullName: 'Abu Abd ar-Rahman al-Khalil ibn Ahmad ibn Amr ibn Tammam al-Farahidi al-Azdi al-Yahmadi',
        shortName: 'Al-Khalil',
        email: 'al-khalil@haqqex.com',
    },
    {
        id: '00000000-0000-0000-0000-000000000006',
        fullName: 'Najm al-Din Alī ibn Umar al-Qazwini al-Katibi',
        shortName: 'Najm',
        email: 'najm@haqqex.com',
    },
    {
        id: '00000000-0000-0000-0000-000000000007',
        fullName: 'Solomon ibn Gabirol or Solomon ben Judah',
        shortName: 'Solomon',
        email: 'solomon@haqqex.com',
    },
];

export class UsersSeeds1661461749856 implements MigrationInterface {
    public async up(qr: QueryRunner): Promise<void> {
        let insertSql = `INSERT INTO "${UserEntity.TABLE_NAME}" (id, full_name, short_name, email, status) VALUES \n`;

        insertSql += usersSeeds
            .map((u) => `('${u.id}', '${u.fullName}', '${u.shortName}', '${u.email}', '${EUserStatus.REGISTERED}')`)
            .join(',\n');

        await qr.query(insertSql);
    }

    public async down(qr: QueryRunner): Promise<void> {
        await qr.query(
            `DELETE FROM "${UserEntity.TABLE_NAME}" WHERE id IN (${usersSeeds.map((u) => `'${u.id}'`).join(', ')})`,
        );
    }
}
