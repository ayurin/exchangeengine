import { Server } from 'http';

import fastify, { FastifyRequest, FastifyReply } from 'fastify';
import { getPortPromise } from 'portfinder';

interface IGetResponse {
    start: () => Promise<void>;
}

interface IBootstrapRemoteServerResponse {
    get: (endpoint: string, handler: (req: FastifyRequest, res: FastifyReply) => Promise<void>) => IGetResponse;
    close: () => void;
    url: string;
}

export async function bootstrapRemoteServer(appPort?: number): Promise<IBootstrapRemoteServerResponse> {
    const app = fastify();
    let server: Server;
    let port = appPort;

    if (!port) {
        port = await getPortPromise({
            port: 3000,
            stopPort: 8888,
        });
    }

    function close(): void {
        server?.close?.();
    }

    function get(endpoint: string, handler: (req: FastifyRequest, res: FastifyReply) => Promise<void>): IGetResponse {
        app.get(endpoint, handler);

        return {
            start: async () => {
                if (!server) {
                    await app.listen({ port: Number(port), host: '0.0.0.0' });
                    server = app.server;
                }
            },
        };
    }

    return {
        get,
        close,
        url: `http://0.0.0.0:${port}`,
    };
}

type ThenArg<T> = T extends PromiseLike<infer U> ? U : T;
export type DynamicRemoteServerFn = ThenArg<ReturnType<typeof bootstrapRemoteServer>>;
