import { HttpModule } from '@nestjs/axios';
import { Controller, DynamicModule, Get, INestApplication, Type } from '@nestjs/common';
import { FastifyAdapter } from '@nestjs/platform-fastify';
import {
    DiskHealthIndicator,
    HealthCheckResult,
    HealthCheckService,
    HttpHealthIndicator,
    TerminusModule,
    TypeOrmHealthIndicator,
} from '@nestjs/terminus';
import { Test } from '@nestjs/testing';
import { TypeOrmModule } from '@nestjs/typeorm';

type TestingHealthFunc = (props: {
    healthCheck: HealthCheckService;
    http: HttpHealthIndicator;
    disk: DiskHealthIndicator;
    typeorm: TypeOrmHealthIndicator;
}) => Promise<HealthCheckResult>;

interface ISetHealthEnpointResponse {
    start: (httpAdapter: FastifyAdapter) => Promise<INestApplication>;
}

interface IWithTypeOrmResponse {
    setHealthEndpoint: (func: TestingHealthFunc) => ISetHealthEnpointResponse;
}

interface IWithHttpResponse {
    setHealthEndpoint: (func: TestingHealthFunc) => ISetHealthEnpointResponse;
}

interface IBootstrapTestingModuleResponse {
    withTypeOrm: () => IWithTypeOrmResponse;
    withHttp: () => IWithHttpResponse;
    setHealthEndpoint: (func: TestingHealthFunc) => ISetHealthEnpointResponse;
}

// eslint-disable-next-line @typescript-eslint/no-explicit-any
function createHealthController(func: TestingHealthFunc): Type<any> {
    @Controller()
    class HealthController {
        constructor(
            private readonly healthCheck: HealthCheckService,
            private readonly http: HttpHealthIndicator,
            private readonly disk: DiskHealthIndicator,
            private readonly typeorm: TypeOrmHealthIndicator,
        ) {}

        @Get('health')
        public health(): Promise<HealthCheckResult> {
            return func({
                healthCheck: this.healthCheck,
                http: this.http,
                disk: this.disk,
                typeorm: this.typeorm,
            });
        }
    }

    return HealthController;
}

export type DynamicHealthEndpointFn = (func: TestingHealthFunc) => {
    start(httpAdapter?: FastifyAdapter): Promise<INestApplication>;
};

export function bootstrapTestingModule(): IBootstrapTestingModuleResponse {
    const imports: Array<Type<TerminusModule> | DynamicModule> = [TerminusModule];

    function setHealthEndpoint(func: TestingHealthFunc): ISetHealthEnpointResponse {
        const testingModule = Test.createTestingModule({
            imports,
            controllers: [createHealthController(func)],
        });

        async function start(httpAdapter: FastifyAdapter = new FastifyAdapter()): Promise<INestApplication> {
            const moduleRef = await testingModule.compile();

            const app = moduleRef.createNestApplication(httpAdapter);

            await app.init();
            // eslint-disable-next-line @typescript-eslint/no-unsafe-member-access, @typescript-eslint/no-unsafe-call
            await app.getHttpAdapter().getInstance().ready?.();
            return app;
        }

        return { start };
    }

    function withTypeOrm(): IWithTypeOrmResponse {
        imports.push(
            TypeOrmModule.forRoot({
                type: 'mysql',
                host: '0.0.0.0',
                port: 3306,
                username: 'root',
                password: 'root',
                database: 'test',
                keepConnectionAlive: true,
                retryAttempts: 2,
                retryDelay: 1000,
            }),
        );

        return { setHealthEndpoint };
    }

    function withHttp(): IWithHttpResponse {
        imports.push(HttpModule);
        return { setHealthEndpoint };
    }

    return {
        withTypeOrm,
        withHttp,
        setHealthEndpoint,
    };
}
