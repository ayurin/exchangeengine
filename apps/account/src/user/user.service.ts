import { BFFWS_PROFILE_PATTERN, LockUserBody, RmqClientService } from '@app/utils';
import { RabbitService, valueOrFail } from '@app/utils';
import { ELockTime, IBffwsProfile, IUser } from '@app/ws';
import { ConflictException, Injectable } from '@nestjs/common';
import { RmqRecord } from '@nestjs/microservices';
import Big from 'big.js';
import moment from 'moment';
import { paginate, PaginateQuery } from 'nestjs-paginate';

import { BalancesService, EConvertTo } from '@account/balances/balances.service';
import { UserEntity, UserRepository } from '@account/dao';
import { OkResponse } from '@account/dto';
import { PaginatedUserResponse, UserResponse } from '@account/user/dto';

const lockTimeMap: Record<ELockTime, () => Date | null> = {
    [ELockTime.FOREVER]: () => null,
    [ELockTime.MONTH]: () => moment().add(1, 'month').toDate(),
    [ELockTime.DAY]: () => moment().add(1, 'day').toDate(),
    [ELockTime.TEN_MINUTES]: () => moment().add(10, 'minutes').toDate(),
};

const getLockExpireDate = (type: ELockTime): Date | null => {
    const action = lockTimeMap[type];
    return action ? action() : null;
};

@Injectable()
export class UserService {
    constructor(
        private readonly userRepository: UserRepository,
        private rabbitService: RabbitService,
        private rmqClientService: RmqClientService,
        private balancesService: BalancesService,
    ) {}

    public async getUsers(query: PaginateQuery): Promise<PaginatedUserResponse> {
        const keysOfEntity: Array<keyof UserEntity> = [
            'id',
            'fullName',
            'shortName',
            'email',
            'status',
            'kycStatus',
            'tfaEnable',
            'taxNumber',
        ];

        const results = await paginate(query, this.userRepository, {
            select: keysOfEntity,
            sortableColumns: keysOfEntity,
            searchableColumns: keysOfEntity,
            nullSort: 'last',
            defaultSortBy: [['createdAt', 'DESC']],
        });

        const data = await this.addAssetsValueToUsers(results.data);

        return { ...results, data };
    }

    public getUser(id: string): Promise<UserEntity | null> {
        return this.userRepository.findOne({ where: { id } });
    }

    public getUserOrFail(id: string): Promise<UserEntity> {
        return this.userRepository.findOneByOrFail({ id });
    }

    public async updateUser(userId: string, partialUser: Partial<UserEntity>): Promise<void> {
        const user = valueOrFail(await this.userRepository.findOneBy({ id: userId }), 'User');
        user.update(partialUser);
        await this.userRepository.save(user);
    }

    public async deleteUser(userId: string): Promise<void> {
        const user = valueOrFail(await this.userRepository.findOneBy({ id: userId }), 'User');
        await this.userRepository.softDelete(user.id);
    }

    public async batchSoftDelete(userIds: string[]): Promise<void> {
        await this.userRepository.softDelete(userIds);
    }

    public async lockUserByAdmin(id: string, { lockReason, lockTime }: LockUserBody): Promise<OkResponse> {
        const user = await this.userRepository.findOneByOrFail({ id });
        const blocked = true;
        if (user.blocked) throw new ConflictException('User already has been blocked');

        await this.userRepository.updateUserWithLog(
            id,
            { blocked, blockExpires: getLockExpireDate(lockTime) },
            { type: 'lock', userId: id, reason: lockReason, options: lockTime },
        );

        await this.sendUserUpdatesToBffws(id, { blocked, lockReason, lockTime });
        return new OkResponse();
    }

    public async unlockUserByAdmin(id: string): Promise<OkResponse> {
        const user = await this.userRepository.findOneByOrFail({ id });
        const blocked = false;
        if (!user.blocked) throw new ConflictException('User was not blocked');

        await this.userRepository.updateUserWithLog(
            id,
            { blocked, blockExpires: null },
            { type: 'unlock', userId: id },
        );
        await this.sendUserUpdatesToBffws(id, { blocked });
        return new OkResponse();
    }

    private async sendUserUpdatesToBffws(id: string, partial: Partial<IUser>): Promise<void> {
        const message: RmqRecord<IBffwsProfile> = this.rabbitService.buildMessage({ id, partial });
        await this.rmqClientService.emit(BFFWS_PROFILE_PATTERN, message);
    }

    private async addAssetsValueToUsers(data: UserEntity[]): Promise<UserResponse[]> {
        return Promise.all(
            data.map(async (user) => {
                const balances = await this.balancesService.getMyBalances({ convertTo: EConvertTo.Usd }, user.id);
                const sum = balances.reduce((partialSum, a) => Big(partialSum).add(a.balance), Big(0));

                return {
                    ...user,
                    assetsValue: sum.toString(),
                } as UserResponse;
            }),
        );
    }
}
