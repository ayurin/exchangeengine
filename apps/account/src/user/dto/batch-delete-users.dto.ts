import { ApiProperty } from '@nestjs/swagger';
import { ArrayNotEmpty, IsArray, IsString } from 'class-validator';

export class BatchDeleteUsersDto {
    @ApiProperty({ example: ['BD7AB0C2-F8F1-4910-B546-F495C837D7FC'], isArray: true })
    @IsArray()
    @IsString({ each: true })
    @ArrayNotEmpty()
    public userIds: string[];
}
