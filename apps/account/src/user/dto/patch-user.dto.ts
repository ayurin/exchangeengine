import { ApiPropertyOptional } from '@nestjs/swagger';
import { IsBoolean, IsEnum, IsOptional } from 'class-validator';

import { EKycStatus } from '@account/dao/entities/user.entity';

import { UpdateUserBody } from './update-user.dto';

export class PatchUserBody extends UpdateUserBody {
    @ApiPropertyOptional({ enum: Object.values(EKycStatus), example: EKycStatus.Pro })
    @IsEnum(EKycStatus)
    @IsOptional()
    public kycStatus?: EKycStatus;

    @ApiPropertyOptional({ example: true })
    @IsBoolean()
    @IsOptional()
    public tfaEnable?: boolean;
}
