import { ApiPropertyOptional } from '@nestjs/swagger';
import { IsOptional, IsString } from 'class-validator';

export class UpdateUserBody {
    @ApiPropertyOptional({ example: 'Yusuf ibn Ayyub ibn Shadi' })
    @IsString()
    @IsOptional()
    public readonly fullName?: string;

    @ApiPropertyOptional({ example: 'Saladin' })
    @IsString()
    @IsOptional()
    public readonly shortName?: string;
}
