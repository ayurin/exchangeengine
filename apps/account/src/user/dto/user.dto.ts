/* eslint-disable max-classes-per-file */
import { PaginatedApiResponse } from '@app/utils';
import { ApiProperty } from '@nestjs/swagger';

import { EUserStatus, UserEntity } from '@account/dao';
import { EKycStatus } from '@account/dao/entities/user.entity';

export class UserResponse {
    @ApiProperty({ example: 'b56329c0-613e-41ff-bf81-f83d08b8f020' })
    public readonly id: string;

    @ApiProperty({ example: 'Yusuf ibn Ayyub ibn Shadi' })
    public readonly fullName: string;

    @ApiProperty({ example: 'Saladin' })
    public readonly shortName: string;

    @ApiProperty({ example: 'email@sdfdsf.com' })
    public readonly email: string;

    @ApiProperty({ enum: EUserStatus, example: EUserStatus.CREATED })
    public readonly status: EUserStatus;

    @ApiProperty({ enum: Object.values(EKycStatus), example: EKycStatus.Unverified })
    public readonly kycStatus: EKycStatus;

    @ApiProperty({ example: true })
    public readonly tfaEnable: boolean;

    @ApiProperty({ example: 'asjkb21b3' })
    public readonly taxNumber: string;

    @ApiProperty({ example: '100000000' })
    public readonly assetsValue: string;

    constructor(user: UserEntity) {
        this.id = user.id;
        this.fullName = user.fullName;
        this.shortName = user.shortName;
        this.email = user.email;
        this.kycStatus = user.kycStatus;
        this.status = user.status;
        this.taxNumber = user.taxNumber;
        this.tfaEnable = user.tfaEnable;
    }
}

export class PaginatedUserResponse extends PaginatedApiResponse<UserResponse> {
    @ApiProperty({ type: UserResponse, isArray: true })
    public readonly data!: UserResponse[];
}
