import { Module } from '@nestjs/common';

import { BalancesModule } from '@account/balances';
import { UserRepository } from '@account/dao';

import { BlockJobService } from './block-job.service';
import { UserController } from './user.controller';
import { UserService } from './user.service';

@Module({
    imports: [BalancesModule],
    controllers: [UserController],
    providers: [UserService, UserRepository, BlockJobService],
})
export class UserModule {}
