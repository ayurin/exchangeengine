import { ApiAuth, JwtGuard, JwtPayload, TokenAudience, TUserJwtPayload, UseAudiences } from '@app/jwt';
import {
    ACCOUNT_BATCH_DELETE_USERS_PATTERN,
    ACCOUNT_GET_USER_PATTERN,
    ACCOUNT_GET_USERS_PATTERN,
    ACCOUNT_LOCK_USER_PATTERN,
    ACCOUNT_UNLOCK_USER_PATTERN,
    ACCOUNT_UPDATE_USER_PATTERN,
    IGetUser,
    PatchUserBody,
    LockUserBody,
} from '@app/utils';
import { PaginateApiQuery } from '@app/utils';
import {
    Body,
    Controller,
    Delete,
    Get,
    HttpStatus,
    NotFoundException,
    Param,
    Patch,
    Put,
    UseGuards,
} from '@nestjs/common';
import { Ctx, MessagePattern, Payload, RmqContext } from '@nestjs/microservices';
import { ApiBearerAuth, ApiBody, ApiOperation, ApiParam, ApiQuery, ApiResponse, ApiTags } from '@nestjs/swagger';
import { Channel, Message } from 'amqplib';
import { Paginate, PaginateQuery } from 'nestjs-paginate';

import { UserEntity } from '@account/dao';
import { OkResponse } from '@account/dto';

import {
    BatchDeleteUsersDto,
    PaginatedUserResponse,
    // PatchUserBody,
    UpdateUserBody,
    UserResponse,
} from './dto';
import { UserService } from './user.service';

@ApiTags('users')
@Controller('users')
export class UserController {
    constructor(private readonly userService: UserService) {}

    @Get()
    @ApiBearerAuth()
    // @UseGuards(JwtGuard)
    // @UseAudiences(TokenAudience.ADMIN)
    @ApiOperation({ summary: 'Get list of users' })
    @ApiQuery({ type: PaginateApiQuery })
    @ApiResponse({ status: HttpStatus.OK, type: PaginatedUserResponse })
    public getUsers(@Paginate() query: PaginateQuery): Promise<PaginatedUserResponse> {
        return this.userService.getUsers(query);
    }

    @ApiBearerAuth()
    @Get('my')
    @UseGuards(JwtGuard)
    @UseAudiences(TokenAudience.ACCOUNT)
    @ApiOperation({ summary: 'Read authorized user account information' })
    @ApiResponse({ status: HttpStatus.OK, type: UserResponse })
    public async getMyAccount(@JwtPayload() jwtPayload: TUserJwtPayload): Promise<UserResponse> {
        // ToDo: кто так типы объявляет?
        // eslint-disable-next-line @typescript-eslint/no-unsafe-argument,@typescript-eslint/no-unsafe-member-access
        const user = await this.userService.getUser(jwtPayload.id);
        if (user === null) {
            throw new NotFoundException('User not found');
        }
        return new UserResponse(user);
    }

    @Get(':userId')
    @ApiBearerAuth()
    // @UseGuards(JwtGuard)
    // @UseAudiences(TokenAudience.ADMIN)
    @ApiOperation({ summary: 'Get user by id' })
    @ApiResponse({ status: HttpStatus.OK, type: UserResponse })
    public async getUser(@Param('userId') userId: string): Promise<UserResponse> {
        const user = await this.userService.getUser(userId);
        if (user === null) {
            throw new NotFoundException('User not found');
        }
        return new UserResponse(user);
    }

    @Put(':userId')
    @UseGuards(JwtGuard)
    @UseAudiences(TokenAudience.ACCOUNT)
    @ApiOperation({ summary: 'Update user' })
    @ApiBearerAuth()
    @ApiBody({ type: UpdateUserBody })
    @ApiResponse({ status: HttpStatus.OK, type: OkResponse })
    public async updateUser(
        @Body() body: UpdateUserBody,
        @JwtPayload() jwtPayload: TUserJwtPayload,
    ): Promise<OkResponse> {
        // ToDo: кто так типы объявляет?
        // eslint-disable-next-line @typescript-eslint/no-unsafe-argument,@typescript-eslint/no-unsafe-member-access
        await this.userService.updateUser(jwtPayload.id, body);

        return new OkResponse();
    }

    @Patch(':userId')
    // @ApiAuth(TokenAudience.ADMIN)
    @ApiOperation({ summary: 'Update user' })
    @ApiBody({ type: PatchUserBody })
    @ApiResponse({ status: HttpStatus.OK, type: OkResponse })
    public async patchUser(@Body() body: PatchUserBody, @Param('userId') userId: string): Promise<OkResponse> {
        await this.userService.updateUser(userId, body);
        return new OkResponse();
    }

    /*
     * если эту ручку дергаем сам юзер, то ему незачем передавать id в параметрах,
     * тк в этом случае его id берем из jwt payload
     * */
    @Delete(':userId')
    @ApiAuth(TokenAudience.ACCOUNT)
    @ApiOperation({ summary: 'Delete user' })
    @ApiResponse({ status: HttpStatus.OK, type: OkResponse })
    public async deleteUser(@JwtPayload() jwtPayload: TUserJwtPayload): Promise<OkResponse> {
        // ToDo: кто так типы объявляет?
        // eslint-disable-next-line @typescript-eslint/no-unsafe-argument,@typescript-eslint/no-unsafe-member-access
        await this.userService.deleteUser(jwtPayload.id);
        return new OkResponse();
    }

    @Delete('batch-delete')
    // @ApiAuth(TokenAudience.ADMIN)
    @ApiOperation({ summary: 'Batch delete users by admin' })
    @ApiResponse({ status: HttpStatus.OK, type: OkResponse })
    @ApiBody({ type: BatchDeleteUsersDto })
    public async batchDeleteUsersByAdmin(@Body() body: BatchDeleteUsersDto): Promise<OkResponse> {
        await this.userService.batchSoftDelete(body.userIds);
        return new OkResponse();
    }

    @Patch('lock/:userId')
    // @ApiAuth(TokenAudience.ADMIN)
    @ApiOperation({ summary: 'Lock user by admin' })
    @ApiResponse({ status: HttpStatus.OK, type: OkResponse })
    @ApiBody({ type: LockUserBody })
    @ApiParam({ name: 'userId', type: 'string' })
    public async lockUserByAdmin(@Param('userId') id: string, @Body() data: LockUserBody): Promise<OkResponse> {
        await this.userService.lockUserByAdmin(id, data);
        return new OkResponse();
    }

    @Patch('unlock/:userId')
    // @ApiAuth(TokenAudience.ADMIN)
    @ApiOperation({ summary: 'Unlock user by admin' })
    @ApiResponse({ status: HttpStatus.OK, type: OkResponse })
    @ApiParam({ name: 'userId', type: 'string' })
    public async unlockUserByAdmin(@Param('userId') id: string): Promise<OkResponse> {
        await this.userService.unlockUserByAdmin(id);
        return new OkResponse();
    }

    @MessagePattern(ACCOUNT_GET_USER_PATTERN)
    public async getPriceChangeForAssets(@Payload() data: IGetUser, @Ctx() context: RmqContext): Promise<UserEntity> {
        const channel = context.getChannelRef() as Channel;
        const originalMsg = context.getMessage() as Message;
        channel.ack(originalMsg);
        return this.userService.getUserOrFail(data.userId);
    }

    @MessagePattern(ACCOUNT_UPDATE_USER_PATTERN)
    public async rmqUpdateUser(
        @Payload() body: PatchUserBody,
        @Ctx() context: RmqContext,
    ): Promise<OkResponse> {
        const channel = context.getChannelRef() as Channel;
        const originalMsg = context.getMessage() as Message;
        channel.ack(originalMsg);
        await this.userService.updateUser(body.id, body);
        return new OkResponse();
    }

    @MessagePattern(ACCOUNT_BATCH_DELETE_USERS_PATTERN)
    public async rmqBatchSoftDelete(@Payload() userIds: string[], @Ctx() context: RmqContext): Promise<OkResponse> {
        const channel = context.getChannelRef() as Channel;
        const originalMsg = context.getMessage() as Message;
        channel.ack(originalMsg);
        await this.userService.batchSoftDelete(userIds);
        return new OkResponse();
    }

    @MessagePattern(ACCOUNT_GET_USERS_PATTERN)
    public async rmqGetUsers(
        @Payload() query: PaginateApiQuery,
        @Ctx() context: RmqContext,
    ): Promise<PaginatedUserResponse> {
        const channel = context.getChannelRef() as Channel;
        const originalMsg = context.getMessage() as Message;
        channel.ack(originalMsg);

        // TODO: find common desicion for pagination
        let sortBy: [string, string][] | undefined = undefined;
        if (query.sortBy) {
            const split = query.sortBy.split(':');
            sortBy = [[split[0], split[1]]];
        }

        let searchBy: string[] | undefined = undefined;
        if (query.searchBy) {
            searchBy = [query.searchBy];
        }        

        return this.userService.getUsers({ ...query, sortBy, searchBy } as unknown as PaginateQuery);
    }

    @MessagePattern(ACCOUNT_LOCK_USER_PATTERN)
    public async rmqLockUserByAdmin(
        @Payload() { userId, ...body }: LockUserBody & { userId: string },
        @Ctx() context: RmqContext,
    ): Promise<OkResponse> {
        const channel = context.getChannelRef() as Channel;
        const originalMsg = context.getMessage() as Message;
        channel.ack(originalMsg);
        await this.userService.lockUserByAdmin(userId, body);
        return new OkResponse();
    }

    @MessagePattern(ACCOUNT_UNLOCK_USER_PATTERN)
    public async rmqUnlockUserByAdmin(@Payload() userId: string, @Ctx() context: RmqContext): Promise<OkResponse> {
        const channel = context.getChannelRef() as Channel;
        const originalMsg = context.getMessage() as Message;
        channel.ack(originalMsg);
        await this.userService.unlockUserByAdmin(userId);
        return new OkResponse();
    }
}
