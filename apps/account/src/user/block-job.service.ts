import { Injectable } from '@nestjs/common';
import { Cron, CronExpression } from '@nestjs/schedule';
import { InjectPinoLogger, PinoLogger } from 'nestjs-pino';
import { DataSource, EntityManager, LessThanOrEqual } from 'typeorm';

import { UserEntity } from '@account/dao';

@Injectable()
export class BlockJobService {
    constructor(
        @InjectPinoLogger(BlockJobService.name)
        private readonly logger: PinoLogger,
        private readonly dataSource: DataSource,
    ) {}

    @Cron(CronExpression.EVERY_MINUTE)
    private async runUnblockUsers(): Promise<void> {
        const startMs = Date.now();
        this.logger.info('Start job for unlock users');

        try {
            await this.dataSource.transaction((txManager) => this.start(txManager));
        } catch (err) {
            this.logger.error('Error occurred by unlock users', err);
        } finally {
            this.logger.info(`End unlock job. Duration - ${Date.now() - startMs}ms`);
        }
    }

    private async start(manager: EntityManager): Promise<void> {
        await manager.update(
            UserEntity,
            { blockExpires: LessThanOrEqual(new Date()) },
            { blockExpires: null, blocked: false },
        );
    }
}
