/* eslint-disable */
import { randomUUID } from 'crypto';

import { promiseDelay, ZERO_BIGINT } from '@app/utils';
import { HttpException, HttpStatus } from '@nestjs/common';

import { PaginationDto } from '@account/dto';
import { PinoLogger } from 'nestjs-pino';

const logger = new PinoLogger({ renameContext: 'utils' });

export type ChangeBalanceType = 'inc' | 'dec';

export const DEFAULT_PAGE_SIZE = 100;
export const DEFAULT_START_PAGE_NUMBER = 1;
export const FIAT = 'USD';
export const ACCOUNT_DELIMITER = 'x';
export const BROKER_MOCK_ID = '00000000-0000-0000-0000-000000000000';
export const BA_SUFFIX = '00';
export const BA_LOCK_SUFFIX = '01';

export function changeBalance(
    type: ChangeBalanceType,
    balance: bigint | string,
    amount: string | number | bigint,
    checkNullBalance = true,
): bigint {
    let newBalance = BigInt(balance);
    if (type === 'inc') {
        return newBalance + BigInt(amount);
    }
    if (type === 'dec') {
        newBalance -= BigInt(amount);
    }
    if (newBalance < ZERO_BIGINT && checkNullBalance) {
        throw new HttpException('Insufficient funds', HttpStatus.NOT_ACCEPTABLE);
    }
    return newBalance;
}

export function getAccountName(currency: string, debitFlg: boolean, forLock = false): string {
    return `${debitFlg ? 'Debit' : 'Credit'} account for ${forLock ? 'locked' : ''} ${currency}`;
}

export function getAccountNumber(balanceAccountNumber: string, userId: string): string {
    return [balanceAccountNumber, userId, BA_SUFFIX].join(ACCOUNT_DELIMITER);
}

export function getBalanceAccountNumber(baNum: string, currency: string): string {
    return [baNum, currency].join(ACCOUNT_DELIMITER);
}

export const getSkipTakeOptions = (pagination: PaginationDto): { skip: number; take: number } => {
    const pageSize = pagination?.pageSize || DEFAULT_PAGE_SIZE;
    const pageNumber = pagination?.pageNumber || DEFAULT_START_PAGE_NUMBER;

    return {
        skip: pageSize * (pageNumber - 1),
        take: pageSize,
    };
};

const MAX_RETRY_COUNT = 5;
export const isLockError = (err: unknown): boolean => {
    return (err as Error)?.message?.startsWith('could not obtain lock on row in relation');
};

export async function retryTransactionFromLock<P, T>(
    remoteProcedure: (arg: P) => Promise<T>,
    arg: P,
    context: any,
    err: unknown,
    attempt: number,
    uuid = randomUUID(),
): Promise<T> {
    if (isLockError(err) && attempt <= MAX_RETRY_COUNT) {
        await promiseDelay(200);
        logger.error(`[${uuid}] - ${remoteProcedure.name} - retry attempt ${attempt}`);
        return remoteProcedure.bind(context)(...[arg, uuid, attempt]) as Promise<T>;
    }
    logger.error(
        `[${uuid}] - ${remoteProcedure.name} - ${attempt > MAX_RETRY_COUNT ? 'max retry.' : ''} ERROR: ${
            (err as Error)?.message
        }`,
    );
    throw err;
}
