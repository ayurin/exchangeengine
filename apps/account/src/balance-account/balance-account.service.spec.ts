import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { Paginated, PaginateQuery } from 'nestjs-paginate';
import { Repository } from 'typeorm';

import { BalanceAccountEntity } from '@account/dao';

import { BalanceAccountService } from './balance-account.service';

const balanceAccounts = [
    {
        id: '270a433e-d86c-4332-828d-6b2ee276a3ff',
        baNumber: 'C2600',
        description: 'credit accounts with available balance',
        debitFlg: false,
        lockFlg: false,
    },
    {
        id: '7d3c0cdc-b5a0-4fbd-ae7e-2b56c2cca166',
        baNumber: 'C2601',
        description: 'credit accounts with locked by orders balance',
        debitFlg: false,
        lockFlg: true,
    },
    {
        id: 'b89fa18d-6a01-4fa0-89b3-16af079d8414',
        baNumber: 'D1600',
        description: 'debit accounts with available balance',
        debitFlg: true,
        lockFlg: false,
    },
];

const baManyAndCount = [balanceAccounts, balanceAccounts.length];

describe('balance account service', () => {
    let service: BalanceAccountService;
    let balanceAccountRepository: Repository<BalanceAccountEntity>;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            providers: [
                BalanceAccountService,
                {
                    provide: getRepositoryToken(BalanceAccountEntity),
                    useFactory: () => ({
                        take: jest.fn(() => ({
                            skip: jest.fn().mockReturnThis(),
                            addOrderBy: jest.fn().mockReturnThis(),
                            getManyAndCount: jest.fn(() => baManyAndCount),
                        })),
                        createQueryBuilder: jest.fn(() => ({
                            take: jest.fn().mockReturnThis(),
                            skip: jest.fn().mockReturnThis(),
                            addOrderBy: jest.fn().mockReturnThis(),
                            getManyAndCount: jest.fn(() => baManyAndCount),
                        })),
                    }),
                },
            ],
        }).compile();

        service = module.get<BalanceAccountService>(BalanceAccountService);
        balanceAccountRepository = module.get(getRepositoryToken(BalanceAccountEntity));
    });

    describe('getBalanceAccounts()', () => {
        describe('returns paginated list of balanceAccounts', () => {
            let result: Paginated<BalanceAccountEntity>;

            const query: PaginateQuery = {
                page: undefined,
                limit: undefined,
                sortBy: undefined,
                search: undefined,
                searchBy: undefined,
                filter: undefined,
                path: '',
            };

            const expected: Paginated<BalanceAccountEntity> = {
                data: balanceAccounts,
                meta: {
                    itemsPerPage: 20,
                    totalItems: 3,
                    currentPage: 1,
                    totalPages: 1,
                    sortBy: [['baNumber', 'ASC']],
                    filter: undefined,
                },
                links: {
                    current: `${query.path}?page=1&limit=20&sortBy=baNumber:ASC`,
                    first: undefined,
                    last: undefined,
                    next: undefined,
                    previous: undefined,
                },
            } as Paginated<BalanceAccountEntity>;

            beforeEach(async () => {
                result = await service.getBalanceAccounts(query);
            });

            it('should check equal between result and expected', () => {
                expect(result).toEqual(expected);
                expect(result.data).toStrictEqual(expected.data);
            });
        });
    });
});
