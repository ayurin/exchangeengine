import { PaginatedApiResponse } from '@app/utils';
import { ApiProperty } from '@nestjs/swagger';

export class BalanceAccountResponse {
    @ApiProperty({ example: '83e67c7c-d130-456e-9c35-e2e1c13d5920' })
    public id: string;

    @ApiProperty({ example: 'C2600' })
    public baNumber: string;

    @ApiProperty({ example: 'credit accounts with available balance' })
    public description: string;

    @ApiProperty({ example: false })
    public debitFlg: boolean;
}

export class PaginatedBalanceAccountResponse extends PaginatedApiResponse<BalanceAccountResponse> {
    @ApiProperty({ type: BalanceAccountResponse, isArray: true })
    public readonly data!: BalanceAccountResponse[];
}
