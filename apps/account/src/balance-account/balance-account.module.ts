import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { BalanceAccountEntity } from '@account/dao';

import { BalanceAccountController } from './balance-account.controller';
import { BalanceAccountService } from './balance-account.service';

@Module({
    imports: [TypeOrmModule.forFeature([BalanceAccountEntity])],
    controllers: [BalanceAccountController],
    providers: [BalanceAccountService],
})
export class BalanceAccountModule {}
