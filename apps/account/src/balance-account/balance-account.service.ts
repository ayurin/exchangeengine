import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { paginate, Paginated, PaginateQuery } from 'nestjs-paginate';
import { Repository } from 'typeorm';

import { BalanceAccountEntity } from '@account/dao';

@Injectable()
export class BalanceAccountService {
    constructor(
        @InjectRepository(BalanceAccountEntity)
        private readonly balanceAccountRepository: Repository<BalanceAccountEntity>,
    ) {}

    public async getBalanceAccounts(query: PaginateQuery): Promise<Paginated<BalanceAccountEntity>> {
        return paginate(query, this.balanceAccountRepository, {
            sortableColumns: ['baNumber'],
            nullSort: 'first',
            searchableColumns: ['id', 'baNumber', 'description', 'debitFlg', 'lockFlg'],
            defaultSortBy: [['baNumber', 'ASC']],
        });
    }
}
