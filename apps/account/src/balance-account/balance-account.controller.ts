import { Controller, Get, HttpStatus } from '@nestjs/common';
import { ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { Paginate, Paginated, PaginateQuery } from 'nestjs-paginate';

import { BalanceAccountService } from '@account/balance-account/balance-account.service';
import { BalanceAccountEntity } from '@account/dao';

import { PaginatedBalanceAccountResponse } from './dto/balance-account.response';

@ApiTags('balance-accounts')
@Controller('balance-accounts')
export class BalanceAccountController {
    constructor(private readonly balanceAccountService: BalanceAccountService) {}

    @Get()
    @ApiOperation({ summary: 'Get list of balance accounts' })
    @ApiResponse({ status: HttpStatus.OK, type: PaginatedBalanceAccountResponse })
    public getBalanceAccounts(@Paginate() query: PaginateQuery): Promise<Paginated<BalanceAccountEntity>> {
        return this.balanceAccountService.getBalanceAccounts(query);
    }
}
