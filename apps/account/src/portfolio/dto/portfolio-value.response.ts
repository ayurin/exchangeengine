import { ArrowType } from '@app/ws';
import { ApiProperty } from '@nestjs/swagger';

export class PortfolioValueResponse {
    @ApiProperty({ example: (19624_5800_0000).toString() })
    public value: string;

    @ApiProperty({ example: (-237_4574_1800).toString() })
    public valueChange: string;

    @ApiProperty({ example: (-1_2100_0000).toString() })
    public valueChangePercent: string;

    @ApiProperty({ example: 'down' })
    public arrow: ArrowType;
}
