import { ECurrencyType } from '@app/utils';
import { ArrowType } from '@app/ws';
import { ApiProperty } from '@nestjs/swagger';
import { Exclude } from 'class-transformer';

export class MyAssetResponse {
    @ApiProperty({ example: 'BTC' })
    public asset: string;

    @ApiProperty({ example: 'Bitcoin' })
    public description?: string;

    @ApiProperty({ example: (5120_0000).toString() })
    public total: string;

    @ApiProperty({ example: (4320_0000).toString() })
    public available: string;

    @ApiProperty({ example: (253_1100_0000).toString() })
    public realizedPL: string;

    @ApiProperty({ example: (283_3500_0000).toString() })
    public unrealizedPL: string;

    @ApiProperty({ example: (39_1800_0000).toString() })
    public allocation: string;

    @ApiProperty({ example: (19624_5800_0000).toString() })
    public price: string;

    @ApiProperty({ example: (-237_4574_1800).toString() })
    public priceChange: string;

    @ApiProperty({ example: (-1_2100_0000).toString() })
    public priceChangePercent: string;

    @ApiProperty({ example: 'up' })
    public arrow: ArrowType;

    @ApiProperty({ example: (10047_7849_6000).toString() })
    public estimatedValue: string;

    @ApiProperty({ example: (-121_5781_3801).toString() })
    public estimatedValueChange: string;

    @Exclude()
    public type?: ECurrencyType;
}
