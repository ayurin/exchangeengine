import { PaginationQuery } from '@app/utils';
import { ApiPropertyOptional } from '@nestjs/swagger';
import { IsOptional, IsString } from 'class-validator';

import { OperationStatus } from '@account/dao';
import { OPERATION_STATUSES } from '@account/dao/entities/operation.entity';
import { RecentTxType } from '@account/portfolio/dto/recent-transaction.response';
import { GetTxPeriod } from '@app/utils';

export const getTxPeriods: GetTxPeriod[] = ['last week', '30 days', '90 days'];

export const recentTxPeriodTransform = (period: GetTxPeriod | undefined): number | undefined => {
    if (period === 'last week') return 7;
    if (period === '30 days') return 30;
    if (period === '90 days') return 90;
    return undefined;
};

export class GetRecentTransactionDto extends PaginationQuery {
    @ApiPropertyOptional({ type: 'string', enum: ['deposit', 'withdraw'] })
    @IsOptional()
    @IsString()
    public type?: RecentTxType;

    @ApiPropertyOptional({ type: 'string', enum: getTxPeriods })
    @IsOptional()
    @IsString()
    public period?: GetTxPeriod;

    @ApiPropertyOptional({ type: 'string', enum: OPERATION_STATUSES })
    @IsOptional()
    @IsString()
    public status?: OperationStatus;
}
