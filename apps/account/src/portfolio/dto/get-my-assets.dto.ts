import { ECurrencyType } from '@app/utils';
import { ApiPropertyOptional } from '@nestjs/swagger';
import { Transform } from 'class-transformer';
import { IsBoolean, IsOptional, IsString } from 'class-validator';

type GetAssetType = 'crypto' | 'fiat';

const currencyType: Record<GetAssetType, ECurrencyType> = {
    crypto: ECurrencyType.CRYPTO,
    fiat: ECurrencyType.FIAT,
};

export class GetMyAssetsDto {
    @ApiPropertyOptional({ type: 'string' })
    @IsOptional()
    @IsString()
    public currency?: string;

    @ApiPropertyOptional({ type: 'string', enum: ['crypto', 'fiat'] })
    @Transform(({ value }) => currencyType[value as GetAssetType])
    @IsOptional()
    @IsString()
    public type?: ECurrencyType;

    @ApiPropertyOptional({ example: null })
    @IsOptional()
    @Transform(({ value }) => JSON.parse(value as string) as boolean)
    @IsBoolean()
    public hideSmallBalances?: boolean;
}
