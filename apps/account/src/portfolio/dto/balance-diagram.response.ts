// eslint-disable-next-line max-classes-per-file
import { ArrowType } from '@app/ws';
import { ApiProperty } from '@nestjs/swagger';

export class BalanceDiagram {
    @ApiProperty({ example: 'BTC' })
    public asset: string;

    @ApiProperty({ example: (39_1800_0000).toString() })
    public allocation: string;
}

export class BalanceDiagramResponse {
    @ApiProperty({ example: (19624_5800_0000).toString() })
    public value: string;

    @ApiProperty({ example: (-237_4574_1800).toString() })
    public valueChange: string;

    @ApiProperty({ example: (-1_2100_0000).toString() })
    public valueChangePercent: string;

    @ApiProperty({ example: 'down' })
    public arrow: ArrowType;

    @ApiProperty({ type: BalanceDiagram, isArray: true })
    public data: BalanceDiagram[];

    constructor(object: BalanceDiagramResponse) {
        Object.assign(this, object);
    }
}

export const MOCK_BALANCE_DIAGRAM = new BalanceDiagramResponse({
    value: '0',
    valueChange: '0',
    valueChangePercent: '0',
    data: [],
    arrow: 'no arrow',
});
