// eslint-disable-next-line max-classes-per-file
import { PaginatedResponse } from '@app/utils';
import { ApiProperty } from '@nestjs/swagger';
import { IsOptional, IsString } from 'class-validator';

import { OPERATION_STATUSES, OperationStatus, OperationType } from '@account/dao/entities/operation.entity';

export type RecentTxType = 'deposit' | 'withdraw';

export const recentTxTypeTransform = {
    from: (type: RecentTxType): OperationType => {
        const map: Record<RecentTxType, OperationType> = {
            deposit: 'topupFromFaucet',
            withdraw: 'withdraw',
        };
        return map[type];
    },
    to: (type: OperationType): RecentTxType => {
        const map: Record<Exclude<OperationType, 'placeOrder' | 'cancelOrder' | 'settleOrder'>, RecentTxType> = {
            topupFromFaucet: 'deposit',
            withdraw: 'withdraw',
        };
        return map[type] as RecentTxType;
    },
};

export class RecentTransactionResponse {
    @ApiProperty({ example: '3CC08408-5B6C-4F07-9A3C-B8B0C2718EA5' })
    public id: string;

    @ApiProperty({ example: 1668619484335 })
    public dateTime: number;

    @ApiProperty({ type: 'string', enum: ['deposit', 'withdraw'], example: 'deposit' })
    public type?: RecentTxType;

    @ApiProperty({ example: 'Spot Wallet' })
    public depositWallet: string;

    @ApiProperty({ example: 'BTC' })
    public asset: string;

    @ApiProperty({ example: (253_1100_0000).toString() })
    public amount: string;

    @ApiProperty({ example: '0xdc3fd43453ea369e19ef672b7bb5056eef12f20e' })
    public destination: string;

    @ApiProperty({ example: '0x370c92408ce34a60c34d016c23be921995886d564731bd40e8a4b0d7502342ef' })
    public txId: string;

    @ApiProperty({ type: 'string', enum: OPERATION_STATUSES, example: 'confirmed' })
    @IsOptional()
    @IsString()
    public status?: OperationStatus;
}

export class PaginatedRecentTransactionResponse extends PaginatedResponse<RecentTransactionResponse> {
    @ApiProperty({ type: RecentTransactionResponse, isArray: true })
    public data: RecentTransactionResponse[];
}
