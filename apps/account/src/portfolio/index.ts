import { IPriceChangeForAsset } from '@app/utils';
import { ZERO_BIGINT } from '@app/utils';
import { ArrowType } from '@app/ws';

export { PortfolioModule } from './portfolio.module';

export interface IPriceChangeForBalanceDiagram {
    price: string;
    old_price: string;
}

export interface IAssetForBalanceDiagram {
    asset: string;
    allocation: string;
    estimatedValue: string;
    estimatedValueChange: string;
}

export const fiatPrices = (asset: string): IPriceChangeForAsset => ({
    arrow: 'no arrow',
    asset,
    old_price: (10 ** 8).toString(),
    price: (10 ** 8).toString(),
    price_change: '0',
    price_change_percent: '0',
});
export const arrowTypeFromChanges = (changes: bigint): ArrowType => {
    if (changes > ZERO_BIGINT) return 'up';
    if (changes < ZERO_BIGINT) return 'down';
    return 'no arrow';
};
