import { Module } from '@nestjs/common';

import { OperationRepository, UserAccountRepository, UserRepository } from '@account/dao';

import { PortfolioController } from './portfolio.controller';
import { PortfolioService } from './portfolio.service';

@Module({
    controllers: [PortfolioController],
    providers: [PortfolioService, UserAccountRepository, OperationRepository, UserRepository],
    exports: [PortfolioService],
})
export class PortfolioModule {}
