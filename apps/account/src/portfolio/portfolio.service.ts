import { createHash } from 'crypto';

import {
    DepositRequestDto,
    IGetPriceChangeForAssets,
    IPriceChangeForAsset,
    PaginatedDepositResponse,
    RmqClientService,
    TRADING_GET_PRICE_CHANGE_FOR_ASSETS,
} from '@app/utils';
import { percentFrom, RabbitService, reduceBigIntBy, uniqBy, ZERO_BIGINT } from '@app/utils';
import { Injectable } from '@nestjs/common';
import { plainToInstance } from 'class-transformer';
import { In } from 'typeorm';

import { IAssetBalance, OperationEntity, OperationRepository, UserAccountRepository, UserRepository } from '@account/dao';
import {
    arrowTypeFromChanges,
    fiatPrices,
    IAssetForBalanceDiagram,
    IPriceChangeForBalanceDiagram,
} from '@account/portfolio';
import {
    BalanceDiagram,
    BalanceDiagramResponse,
    GetMyAssetsDto,
    GetRecentTransactionDto,
    MyAssetResponse,
    PaginatedRecentTransactionResponse,
    PortfolioValueResponse,
    PriceCurrency,
    PriceNoFiatCurrency,
    RecentTransactionResponse,
} from '@account/portfolio/dto';
import { recentTxTypeTransform } from '@account/portfolio/dto/recent-transaction.response';
import { FIAT } from '@account/utils';

@Injectable()
export class PortfolioService {
    constructor(
        private userAccountRepository: UserAccountRepository,
        private rabbitService: RabbitService,
        private rmqClientService: RmqClientService,
        private operationRepository: OperationRepository,
        private userRepository: UserRepository,
    ) {}

    public async getRecentTransactions(params: {
        userId?: string;
        filters: GetRecentTransactionDto;
    }): Promise<PaginatedRecentTransactionResponse> {
        const [data, total] = await this.operationRepository.getOperationsForRecentTransaction(params);
        return { data: PortfolioService.mapOperationToTransaction(data), total };
    }

    public async getDeposits({ filters }: { filters: DepositRequestDto }): Promise<any> {
        const [operations, total] = await this.operationRepository.getOperationsForRecentTransaction({
            filters: {
                ...filters,
                type: 'deposit',
                paginationOptions: filters.paginationOptions,
            },
        });

        const userIds = operations.flatMap((item) => item.userIds);
        const users = await this.userRepository.findBy({ id: In(userIds) });
        const usersMap = new Map(users.map((u) => [u.id, u]));

        const patchedOperations = operations.map((op) => {
            // в депозитах может быть только один юзер (@Alena Abrosimova поправь, если ошибаюсь)
            const [customerId] = op.userIds;
            const transaction = PortfolioService.operationToTransaction(op);

            return {
                ...transaction,
                customerId,
                customerName: usersMap.get(customerId)?.fullName,
                createdAt: op.asOfDate,
            };
        });

        return { data: patchedOperations, total };
    }

    private static mapOperationToTransaction(data: OperationEntity[]): RecentTransactionResponse[] {
        return data.map((item) => PortfolioService.operationToTransaction(item));
    }

    private static operationToTransaction(item: OperationEntity): RecentTransactionResponse {
        const { asset, amount } = item.transactionData;
        const strItem = JSON.stringify(item);
        const bufferItem = Buffer.from(strItem);

        return {
            id: item.id,
            dateTime: item.asOfDate.valueOf(),
            type: recentTxTypeTransform.to(item.type),
            depositWallet: 'Spot Wallet',
            asset,
            amount,
            destination: createHash('sha3-256').update(bufferItem).digest('base64'),
            txId: createHash('sha3-256').update(bufferItem).digest('hex'),
            status: item.status,
        };
    }

    public async getBalanceDiagram(userId: string, priceCurrency: PriceCurrency): Promise<BalanceDiagramResponse> {
        const myAssets = await this.getMyAssetForDataBar(userId, priceCurrency);
        let valueChange = ZERO_BIGINT;
        const data: BalanceDiagram[] = [];
        const value = myAssets.reduce((acc, item) => {
            valueChange += BigInt(item.estimatedValueChange);
            if (BigInt(item.allocation) > 0) {
                data.push({ asset: item.asset, allocation: item.allocation });
            }
            return acc + BigInt(item.estimatedValue);
        }, ZERO_BIGINT);
        const valueChangePercent = percentFrom(value, valueChange)?.toString();
        return {
            arrow: arrowTypeFromChanges(valueChange),
            data,
            value: value.toString(),
            valueChange: valueChange.toString(),
            valueChangePercent,
        };
    }

    // ToDo: метод очень похож на верхний, но это изменится, когда мы реально начнем брать spot/margin/staking балансы
    public async getPortfolioValue(userId: string, priceCurrency: PriceCurrency): Promise<PortfolioValueResponse> {
        const myAssets = await this.getMyAssetForDataBar(userId, priceCurrency);
        let valueChange = ZERO_BIGINT;
        const value = myAssets.reduce((acc, item) => {
            valueChange += BigInt(item.estimatedValueChange);
            return acc + BigInt(item.estimatedValue);
        }, ZERO_BIGINT);
        const valueChangePercent = percentFrom(value, valueChange)?.toString();
        return {
            arrow: arrowTypeFromChanges(valueChange),
            value: value.toString(),
            valueChange: valueChange.toString(),
            valueChangePercent,
        };
    }

    private async getMyAssetForDataBar(
        userId: string,
        priceCurrency: PriceCurrency,
    ): Promise<IAssetForBalanceDiagram[]> {
        const myAssets = await this.getMyAssets(userId);
        if (priceCurrency === 'USD' || !myAssets.length) return myAssets;
        const btcAsset = myAssets.find((item) => item.asset === priceCurrency);
        const priceChange = await this.getNoFiatPriceChange(btcAsset, priceCurrency);
        const price = priceChange?.price ? BigInt(priceChange.price) : null;
        const oldPrice = priceChange?.old_price ? BigInt(priceChange.old_price) : null;
        return myAssets.map((item) => {
            const estimatedValue = price ? (BigInt(item.estimatedValue) * BigInt(10 ** 8)) / price : ZERO_BIGINT;
            const oldEstimatedValue = oldPrice
                ? (BigInt(item.estimatedValue) * BigInt(10 ** 8)) / oldPrice
                : ZERO_BIGINT;
            return {
                asset: item.asset,
                allocation: priceChange ? item.allocation : '0',
                estimatedValue: estimatedValue.toString(),
                estimatedValueChange: (estimatedValue - oldEstimatedValue).toString(),
            };
        });
    }

    private async getNoFiatPriceChange(
        asset: MyAssetResponse | undefined,
        priceCurrency: PriceNoFiatCurrency,
    ): Promise<IPriceChangeForBalanceDiagram | null> {
        if (asset) {
            return {
                old_price: String(BigInt(asset.price) - BigInt(asset.priceChange)),
                price: asset.price,
            };
        }
        const priceChanges = await this.getPriceChangeForAssets([priceCurrency]);
        return priceChanges?.[0];
    }

    public async getMyAssets(userId: string, filters?: GetMyAssetsDto): Promise<MyAssetResponse[]> {
        const accounts = await this.userAccountRepository.getAccountsForMyAssets(userId, false);
        const lockAccounts = await this.userAccountRepository.getAccountsForMyAssets(userId, true);
        const assets = uniqBy(accounts.concat(lockAccounts), 'asset').map((account) => account.asset);
        const priceChangeForAssets = await this.getPriceChangeForAssets(assets);
        return PortfolioService.mapToMyAssets(assets, accounts, lockAccounts, priceChangeForAssets, filters);
    }

    public async getPriceChangeForAssets(assets: string[]): Promise<IPriceChangeForAsset[]> {
        if (!assets.length) return [];
        const message = this.rabbitService.buildMessage<IGetPriceChangeForAssets>({ assets, fiat: FIAT });
        return this.rmqClientService.send(TRADING_GET_PRICE_CHANGE_FOR_ASSETS, message);
    }

    private static setDataToMyAsset(myAssets: MyAssetResponse[]): MyAssetResponse[] {
        const allEstimatedValue = reduceBigIntBy(myAssets, 'estimatedValue').toString();

        return myAssets.map((myAsset) => ({
            ...myAsset,
            allocation: percentFrom(allEstimatedValue, myAsset.estimatedValue)?.toString(),
        }));
    }

    private static getPricesForAsset(
        asset: string,
        priceChangeForAssets: IPriceChangeForAsset[],
    ): IPriceChangeForAsset | undefined {
        const isFiat = asset === FIAT;
        if (isFiat) {
            return fiatPrices(asset);
        }
        return priceChangeForAssets.find((item) => item.asset === asset);
    }

    private static mapToMyAsset(
        asset: string,
        accounts: IAssetBalance[],
        lockAccounts: IAssetBalance[],
        priceChangeForAssets: IPriceChangeForAsset[],
    ): MyAssetResponse {
        const account = accounts.find((item) => item.asset === asset);
        const lockAccount = lockAccounts.find((item) => item.asset === asset);
        const prices = PortfolioService.getPricesForAsset(asset, priceChangeForAssets);
        const available = BigInt(account?.balance || 0);
        const total = BigInt(lockAccount?.balance || 0) + available;
        const estimatedValue = (total * BigInt(prices?.price || '0')) / BigInt(10 ** 8);
        const oldEstimatedValue = (total * BigInt(prices?.old_price || '0')) / BigInt(10 ** 8);
        return {
            asset,
            description: account?.description || lockAccount?.description,
            total: total.toString(),
            available: available.toString(),
            realizedPL: '',
            unrealizedPL: '',
            allocation: '',
            price: prices?.price || '0',
            priceChange: prices?.price_change || '0',
            priceChangePercent: prices?.price_change_percent || '0',
            arrow: prices?.arrow || 'no arrow',
            estimatedValue: estimatedValue.toString(),
            estimatedValueChange: (estimatedValue - oldEstimatedValue).toString(),
            type: account?.type || lockAccount?.type,
        };
    }

    private static mapToMyAssets(
        currencies: string[],
        accounts: IAssetBalance[],
        lockAccounts: IAssetBalance[],
        priceChangeForAssets: IPriceChangeForAsset[],
        filters?: GetMyAssetsDto,
    ): MyAssetResponse[] {
        const myAssets = currencies.map((currency) =>
            PortfolioService.mapToMyAsset(currency, accounts, lockAccounts, priceChangeForAssets),
        );
        return PortfolioService.setDataToMyAsset(myAssets)
            .filter((myAsset) => PortfolioService.filterAssets(myAsset, filters))
            .sort((a, b) => PortfolioService.sortAssets(a, b))
            .map((item) => plainToInstance(MyAssetResponse, item));
    }

    private static sortAssets(a: MyAssetResponse, b: MyAssetResponse): number {
        return parseInt(b.allocation, 10) - parseInt(a.allocation, 10);
    }

    private static filterAssets(myAsset: MyAssetResponse, filter?: GetMyAssetsDto): boolean {
        let access = true;
        if (filter?.hideSmallBalances) {
            access = access && BigInt(myAsset.total) > 0;
        }
        if (filter?.currency) {
            const startWith =
                myAsset.asset.startsWith(filter.currency.toUpperCase()) ||
                !!myAsset.description?.toUpperCase().startsWith(filter.currency.toUpperCase());
            access = access && startWith;
        }
        if (filter?.type) {
            access = access && myAsset.type === filter.type;
        }

        return access;
    }
}
