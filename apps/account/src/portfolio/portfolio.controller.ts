import { JwtGuard, JwtPayload, TokenAudience, TUserJwtPayload, UseAudiences } from '@app/jwt';
import { Controller, Get, HttpStatus, Param, Query, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiOperation, ApiParam, ApiResponse, ApiTags } from '@nestjs/swagger';

import {
    BalanceDiagramResponse,
    GetMyAssetsDto,
    GetRecentTransactionDto,
    MyAssetResponse,
    PortfolioValueResponse,
    PriceCurrency,
} from '@account/portfolio/dto';
import { MOCK_BALANCE_DIAGRAM } from '@account/portfolio/dto/balance-diagram.response';
import { PaginatedRecentTransactionResponse } from '@account/portfolio/dto/recent-transaction.response';
import { PortfolioService } from '@account/portfolio/portfolio.service';

@ApiTags('portfolio')
@Controller('portfolio')
export class PortfolioController {
    constructor(private portfolioService: PortfolioService) {}

    @ApiBearerAuth()
    @Get('my-assets')
    @UseGuards(JwtGuard)
    @UseAudiences(TokenAudience.ACCOUNT)
    @ApiOperation({ summary: 'Get user assets' })
    @ApiResponse({ status: HttpStatus.OK, type: MyAssetResponse, isArray: true })
    public getMyAssets(
        @Query() filters: GetMyAssetsDto,
        @JwtPayload() { id }: TUserJwtPayload,
    ): Promise<MyAssetResponse[]> {
        return this.portfolioService.getMyAssets(id, filters);
    }

    @ApiBearerAuth()
    @Get('portfolio-value/:priceCurrency')
    @UseGuards(JwtGuard)
    @UseAudiences(TokenAudience.ACCOUNT)
    @ApiParam({ enum: ['USD', 'BTC'], name: 'priceCurrency' })
    @ApiOperation({ summary: 'Get user spot balance' })
    @ApiResponse({ status: HttpStatus.OK, type: PortfolioValueResponse })
    public getPortfolioValue(
        @Param('priceCurrency') priceCurrency: PriceCurrency,
        @JwtPayload() { id }: TUserJwtPayload,
    ): Promise<PortfolioValueResponse> {
        return this.portfolioService.getPortfolioValue(id, priceCurrency);
    }

    @ApiBearerAuth()
    @Get('spot-balance/:priceCurrency')
    @UseGuards(JwtGuard)
    @UseAudiences(TokenAudience.ACCOUNT)
    @ApiParam({ enum: ['USD', 'BTC'], name: 'priceCurrency' })
    @ApiOperation({ summary: 'Get user spot balance' })
    @ApiResponse({ status: HttpStatus.OK, type: BalanceDiagramResponse })
    public getSpotBalance(
        @Param('priceCurrency') priceCurrency: PriceCurrency,
        @JwtPayload() { id }: TUserJwtPayload,
    ): Promise<BalanceDiagramResponse> {
        return this.portfolioService.getBalanceDiagram(id, priceCurrency);
    }

    @ApiBearerAuth()
    @Get('margin-balance/:priceCurrency')
    @UseGuards(JwtGuard)
    @UseAudiences(TokenAudience.ACCOUNT)
    @ApiParam({ enum: ['USD', 'BTC'], name: 'priceCurrency' })
    @ApiOperation({ summary: 'Get user margin balance' })
    @ApiResponse({ status: HttpStatus.OK, type: BalanceDiagramResponse })
    public getMarginBalance(
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        @Param('priceCurrency') priceCurrency: PriceCurrency,
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        @JwtPayload() payload: TUserJwtPayload,
    ): Promise<BalanceDiagramResponse> {
        return Promise.resolve(MOCK_BALANCE_DIAGRAM);
    }

    @ApiBearerAuth()
    @Get('staking-balance/:priceCurrency')
    @UseGuards(JwtGuard)
    @UseAudiences(TokenAudience.ACCOUNT)
    @ApiParam({ enum: ['USD', 'BTC'], name: 'priceCurrency' })
    @ApiOperation({ summary: 'Get user staking balance' })
    @ApiResponse({ status: HttpStatus.OK, type: BalanceDiagramResponse })
    public getStakingBalance(
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        @Param('priceCurrency') priceCurrency: PriceCurrency,
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        @JwtPayload() payload: TUserJwtPayload,
    ): Promise<BalanceDiagramResponse> {
        return Promise.resolve(MOCK_BALANCE_DIAGRAM);
    }

    @ApiBearerAuth()
    @Get('recent-transactions')
    @UseGuards(JwtGuard)
    @UseAudiences(TokenAudience.ACCOUNT)
    @ApiOperation({ summary: 'Get recent transactions' })
    @ApiResponse({ status: HttpStatus.OK, type: PaginatedRecentTransactionResponse })
    public getRecentTransactions(
        @Query() filters: GetRecentTransactionDto,
        @JwtPayload() { id }: TUserJwtPayload,
    ): Promise<PaginatedRecentTransactionResponse> {
        return this.portfolioService.getRecentTransactions({ userId: id, filters });
    }
}
