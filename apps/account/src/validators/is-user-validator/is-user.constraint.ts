import { Injectable } from '@nestjs/common';
import {
    registerDecorator,
    ValidationOptions,
    ValidatorConstraint,
    ValidatorConstraintInterface,
} from 'class-validator';

import { UserRepository } from '@account/dao';

const DEFAULT_OPTIONS: ValidationOptions = {
    message: 'User not found',
};

@ValidatorConstraint({ async: true })
@Injectable()
export class IsUserConstraint implements ValidatorConstraintInterface {
    constructor(private userRepository: UserRepository) {}

    public validate(id: unknown): Promise<boolean> {
        return this.userRepository.findOneBy({ id: String(id) }).then((user) => {
            return !!user;
        });
    }
}

export function IsUser(validationOptions = DEFAULT_OPTIONS) {
    return function isUser(object: object, propertyName: string) {
        registerDecorator({
            target: object.constructor,
            propertyName,
            options: validationOptions,
            constraints: [],
            validator: IsUserConstraint,
        });
    };
}
