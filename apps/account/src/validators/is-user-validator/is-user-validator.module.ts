import { Module } from '@nestjs/common';

import { UserRepository } from '@account/dao';
import { IsUserConstraint } from '@account/validators';

@Module({
    providers: [IsUserConstraint, UserRepository],
    exports: [IsUserConstraint],
})
export class IsUserValidatorModule {}
