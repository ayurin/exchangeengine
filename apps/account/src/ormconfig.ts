import { DataSource } from 'typeorm';
import { SnakeNamingStrategy } from 'typeorm-naming-strategies';
import { PostgresConnectionOptions } from 'typeorm/driver/postgres/PostgresConnectionOptions';

import { dbConfig } from './config/db.config';

const migrationsPath =
    dbConfig.ENVIRONMENT === 'test' ? ['./migrations/*.ts', './migrations-spec/*.ts'] : ['./migrations/*.[tj]s'];

export const connectionSource = new DataSource({
    migrationsTableName: 'migrations',
    type: 'postgres',
    host: dbConfig.DB_HOST,
    port: dbConfig.DB_PORT,
    username: dbConfig.DB_USER,
    schema:
        dbConfig.DB_NAME === 'postgres' && !['test', 'local', undefined].includes(dbConfig.ENVIRONMENT)
            ? dbConfig.DB_USER
            : undefined,
    password: dbConfig.DB_PASSWORD,
    database: dbConfig.DB_NAME,
    synchronize: false,
    entities: ['./*/dao/*', './dao/*', './dao/entities/*'],
    logging: dbConfig.DB_ENABLE_LOGGING !== '0' || false,
    namingStrategy: new SnakeNamingStrategy(),
    migrationsRun: dbConfig.DB_MIGRATION_RUN,
    migrations: dbConfig.SKIP_MIGRATIONS === '1' ? [] : migrationsPath,
    extra: {
        // based on  https://node-postgres.com/api/pool
        // max connection pool size
        max: parseInt(dbConfig.DB_CONNECTIONS_POOL_SIZE || '10', 10),
        idleTimeoutMillis: parseInt(dbConfig.DB_CONNECTIONS_IDLE_TIMEOUT || '5000', 10),
    },
} as PostgresConnectionOptions);
