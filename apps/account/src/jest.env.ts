import { INestApplication, ValidationPipe } from '@nestjs/common';
import { FastifyAdapter } from '@nestjs/platform-fastify';
// eslint-disable-next-line import/no-extraneous-dependencies
import { TestingModule } from '@nestjs/testing';

import { API_PREFIX, appVersioningOptions } from './env';

export function createTestMicroservice(module: TestingModule, appValidationPipe: ValidationPipe): INestApplication {
    const app = module.createNestApplication(new FastifyAdapter());
    if (appValidationPipe) {
        app.useGlobalPipes(appValidationPipe);
    }

    app.setGlobalPrefix(API_PREFIX);
    app.enableVersioning(appVersioningOptions);

    return app;
}
