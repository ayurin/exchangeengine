import { JwtGuard, JwtPayload, TokenAudience, TUserJwtPayload, UseAudiences } from '@app/jwt';
import { Controller, Get, HttpStatus, Param, Query, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiOperation, ApiParam, ApiResponse, ApiTags } from '@nestjs/swagger';

import { BalancesService } from '@account/balances/balances.service';
import { BalanceResponse } from '@account/balances/dto';
import { GetMyBalancesDto } from '@account/balances/dto/get-my-balances.dto';

@ApiTags('balances')
@Controller('balances')
export class BalancesController {
    constructor(private balancesService: BalancesService) {}

    @ApiBearerAuth()
    @Get('my')
    @UseGuards(JwtGuard)
    @UseAudiences(TokenAudience.ACCOUNT)
    @ApiOperation({ summary: 'Get balances list for authorized user' })
    @ApiResponse({ status: HttpStatus.OK, type: BalanceResponse, isArray: true })
    public getMyBalances(
        @Query() query: GetMyBalancesDto,
        @JwtPayload() { id }: TUserJwtPayload,
    ): Promise<BalanceResponse[]> {
        return this.balancesService.getMyBalances(query, id);
    }

    @ApiBearerAuth()
    @ApiParam({ name: 'currency', example: 'BTC', type: 'string' })
    @Get('my/:currency')
    @UseGuards(JwtGuard)
    @UseAudiences(TokenAudience.ACCOUNT)
    @ApiOperation({ summary: 'Get balance of current currency for authorized user' })
    @ApiResponse({ status: HttpStatus.OK, type: BalanceResponse })
    public getMyCurrencyBalance(
        @Param('currency') currency: string,
        @JwtPayload() { id }: TUserJwtPayload,
    ): Promise<BalanceResponse> {
        return this.balancesService.getMyCurrencyBalance(currency, id);
    }
}
