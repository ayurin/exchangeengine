import { Module } from '@nestjs/common';

import { UserAccountRepository } from '@account/dao';
import { CurrencyPipeModule } from '@account/pipes';
import { PortfolioModule } from '@account/portfolio';

import { BalancesController } from './balances.controller';
import { BalancesService } from './balances.service';

@Module({
    controllers: [BalancesController],
    providers: [BalancesService, UserAccountRepository],
    imports: [PortfolioModule, CurrencyPipeModule],
    exports: [BalancesService],
})
export class BalancesModule {}
