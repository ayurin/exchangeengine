import { ApiPropertyOptional } from '@nestjs/swagger';
import { Transform } from 'class-transformer';
import { IsArray, IsBoolean, IsEnum, IsOptional } from 'class-validator';

import { EConvertTo } from '../balances.service';

export class GetMyBalancesDto {
    @ApiPropertyOptional({ isArray: true, type: 'string' })
    @IsOptional()
    @IsArray()
    public currency?: string[];

    @ApiPropertyOptional({ example: false })
    @IsOptional()
    @Transform(({ value }) => JSON.parse(value as string) as boolean)
    @IsBoolean()
    public withNull?: boolean;

    @ApiPropertyOptional({ example: EConvertTo.Usd })
    @IsOptional()
    @IsEnum(EConvertTo)
    public convertTo?: EConvertTo;
}
