import { ApiProperty } from '@nestjs/swagger';

export class BalanceResponse {
    @ApiProperty({ example: 'BTC' })
    public currency: string;

    @ApiProperty({ example: (1_7500_0000).toString() })
    public balance: string;

    @ApiProperty({ example: (500_0000).toString() })
    public locked: string;
}
