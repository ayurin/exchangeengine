import { ZERO_BIGINT } from '@app/utils';
import { Injectable } from '@nestjs/common';

import { mapToBalanceResponse } from '@account/balances';
import { BalanceResponse, GetMyBalancesDto } from '@account/balances/dto';
import { UserAccountRepository } from '@account/dao';
import { PortfolioService } from '@account/portfolio/portfolio.service';

export enum EConvertTo {
    Usd = 'usd',
}

@Injectable()
export class BalancesService {
    constructor(
        private userAccountRepository: UserAccountRepository,
        private readonly portfolioService: PortfolioService,
    ) {}

    public async getMyBalances(query: GetMyBalancesDto, userId: string): Promise<BalanceResponse[]> {
        const [accounts, lockAccounts] = await Promise.all([
            this.userAccountRepository.getAccountsForMyBalances(query, userId, false),
            this.userAccountRepository.getAccountsForMyBalances(query, userId, true),
        ]);

        const result = accounts.map((account) => mapToBalanceResponse(account, lockAccounts));

        switch (query.convertTo) {
            case EConvertTo.Usd:
                return this.convertToUsd(result);
            default:
                return result;
        }
    }

    public async getMyCurrencyBalance(currency: string, userId: string): Promise<BalanceResponse> {
        const account = await this.userAccountRepository.getAccountForMyBalance(currency, userId, false);
        const lockAccount = await this.userAccountRepository.getAccountForMyBalance(currency, userId, true);
        return {
            balance: String(account?.balance || ZERO_BIGINT),
            locked: String(lockAccount?.balance || ZERO_BIGINT),
            currency,
        };
    }

    private async convertToUsd(balances: BalanceResponse[]): Promise<BalanceResponse[]> {
        const assets = balances.map((b) => b.currency);

        const prices = await this.portfolioService.getPriceChangeForAssets(assets);
        const pricesByAsset = new Map(prices.map((p) => [p.asset.toUpperCase(), BigInt(p.price)]));

        return balances.map((balance) => {
            const price = pricesByAsset.get(balance.currency.toUpperCase()) ?? ZERO_BIGINT;

            const convert = (val: string): string => String((price * BigInt(val)) / BigInt(10 ** 8));

            return {
                currency: balance.currency,
                balance: convert(balance.balance),
                locked: convert(balance.locked),
            };
        });
    }
}
