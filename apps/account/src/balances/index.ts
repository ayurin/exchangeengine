import { ZERO_BIGINT } from '@app/utils';

import { BalanceResponse } from '@account/balances/dto';
import { UserAccountEntity } from '@account/dao';

export { BalancesModule } from './balances.module';

export function mapToBalanceResponse(account: UserAccountEntity, lockAccounts: UserAccountEntity[]): BalanceResponse {
    const lockAccount = lockAccounts.find((item) => item.currencyId === account.currencyId);
    return {
        balance: String(account.balance),
        locked: String(lockAccount?.balance || ZERO_BIGINT),
        currency: account.currencyCd,
    };
}
