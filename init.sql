CREATE SCHEMA account;
CREATE SCHEMA trading;
CREATE SCHEMA bffws;
CREATE SCHEMA auth;
CREATE SCHEMA matcher;
CREATE SCHEMA admin_backend;

create user account with encrypted password 'account';
create user trading with encrypted password 'trading';
create user bffws with encrypted password 'bffws';
create user auth with encrypted password 'auth';
create user matcher with encrypted password 'matcher';
create user admin_backend with encrypted password 'admin_backend';

GRANT all privileges ON SCHEMA account TO account;
GRANT all privileges ON SCHEMA trading TO trading;
GRANT all privileges ON SCHEMA bffws TO bffws;
GRANT all privileges ON SCHEMA auth TO auth;
GRANT all privileges ON SCHEMA matcher TO matcher;
GRANT all privileges ON SCHEMA admin_backend TO admin_backend;

CREATE EXTENSION IF NOT EXISTS "uuid-ossp";